<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Use the DS to separate the directories in other defines
 */
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

/**
 * These defines should only be edited if you have cake installed in
 * a directory layout other than the way it is distributed.
 * When using custom settings be sure to use the DS and do not add a trailing DS.
 */

/**
 * The full path to the directory which holds "src", WITHOUT a trailing DS.
 */
define('ROOT', dirname(__DIR__));

/**
 * The actual directory name for the application directory. Normally
 * named 'src'.
 */
define('APP_DIR', 'src');
define('GOOGLE_API_KEY_CLASSIFIED', 'AIzaSyBsoQOb5d2gnkd5eJ076CYhAYz8dJuJAi4');

/**
 * Path to the application's directory.
 */
define('APP', ROOT . DS . APP_DIR . DS);

/**
 * Path to the config directory.
 */
define('CONFIG', ROOT . DS . 'config' . DS);

/**
 * File path to the webroot directory.
 */
define('WWW_ROOT', ROOT . DS . 'webroot' . DS);

/**
 * Path to the tests directory.
 */
define('TESTS', ROOT . DS . 'tests' . DS);

/**
 * Path to the temporary files directory.
 */
define('TMP', ROOT . DS . 'tmp' . DS);

/**
 * Path to the logs directory.
 */
define('LOGS', ROOT . DS . 'logs' . DS);

/**
 * Path to the cache files directory. It can be shared between hosts in a multi-server setup.
 */
define('CACHE', TMP . 'cache' . DS);

define('SENDMAIL',1);
/**
 * The absolute path to the "cake" directory, WITHOUT a trailing DS.
 *73848, Kcwp3SbGpqjZXDK 6dzjxQxs2KEhWQH
 * CakePHP should always be installed with composer, so look there.
 */
define('CAKE_CORE_INCLUDE_PATH', ROOT . DS . 'vendor' . DS . 'cakephp' . DS . 'cakephp');

define('QUICKBLOX_APPLICATION_ID','73848');
define('QUICKBLOX_AUTH_KEY','Kcwp3SbGpqjZXDK');
define('QUICKBLOX_AUTHSECRET','6dzjxQxs2KEhWQH');
define('QUICKBLOX_API_URL','https://api.quickblox.com/');


/******** stripe payment key ******************/
define('STRIPE_SECRET_KEY','sk_test_TdCLecFxIvbFeFvKPwqCsiho');
define('STRIPE_PUBLISHABLE_KEY','pk_test_uHQL4avNMppU510XEmE6xhHn');


/******** AWS config path  ******************/
define('AMAZON_BUCKET_NAME','meenageet');
define('AMAZON_BUCKET_ACCESSKEY','AKIAIVQDG4VN7ADOMWBQ');
define('AMAZON_BUCKET_SECRETKEY','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');
define('AMAZON_BUCKET_URL','https://meenageet.s3.amazonaws.com/');
define('AMAZON_BUCKET_URL_DOWNLOAD','https://s3.ap-south-1.amazonaws.com/meenageet/');
define('AMAZON_BUCKET_REGION','ap-south-1');



/******** User Image path  ******************/
define('path_user_folder',"uploads/user");
define('path_user_images_folder',"uploads/user_images");
define('path_user',"http://".$_SERVER['SERVER_NAME']."/uploads/user/");

/******** Category Image path  ******************/
define('path_category_web_folder','uploads/category_web');
define('path_category_web_image',"http://".$_SERVER['SERVER_NAME']."/uploads/category_web/");

/******** Post Image path  ******************/
define('path_post_image_folder',"postimages/");
define('path_post_image', AMAZON_BUCKET_URL_DOWNLOAD."postimages/");

/******** Song Image path  ******************/
define('path_song_upload_image', AMAZON_BUCKET_URL_DOWNLOAD."images/");
define('path_song_image_folder','images/');

/******** Song Audio path  ******************/
define('path_song_audio_folder','songs/');
define('path_song_upload_audio', AMAZON_BUCKET_URL_DOWNLOAD."songs/");

/******** ID Proof Image path  ******************/
define('path_IDProof_image_folder','idproof/');
define('path_IDProof_image', AMAZON_BUCKET_URL_DOWNLOAD."idproof/");


/******** Banner Image path  ******************/
define('path_banner_image',"http://".$_SERVER['SERVER_NAME']."/uploads/banner_app/");


/**
 * Path to the cake directory. $this->project_title='Meena Geet';
 */
define('title', 'Meena Geet');
define('CORE_PATH', CAKE_CORE_INCLUDE_PATH . DS);
define('CAKE', CORE_PATH . 'src' . DS);
define('SERVERNAME', "http://".$_SERVER['SERVER_NAME']);
define('BASEURL', "http://".$_SERVER['SERVER_NAME']."");

/* custom songs image upload */
define('path_song_image_custom','uploads/song_upload_image/');
