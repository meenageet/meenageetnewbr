<?php
namespace App\Controller\Api;
use App\Controller\AppController; // HAVE TO USE App\Controller\AppController
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\Validation\Validation;
use Cake\I18n\Time;
use Stripe\Stripe;
use Paytm;

class UsersController extends AppController
{
	
	public function beforeFilter(Event $event)
    {
		 $this->Auth->allow();
		 $this->postPagination = 10;
	}
	
	/****************** Register function for api **********************/
    public function register()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('UserRegisters');
			$error = true; $code = 0;
			$message = $response = ''; 
			if( isset( $this->request->data['phone_number'] ) && isset( $this->request->data['device_type'] ) && isset( $this->request->data['device_token'] ) )
			{
				$otpNumber = rand(1111,9999);
				$userRegister = $this->UserRegisters->newEntity();
				$this->request->data['otp_number'] = $otpNumber;
				$this->request->data['phone_number'] = $this->request->data['phone_number'];
				$userRegister = $this->UserRegisters->patchEntity($userRegister, $this->request->data);
				if ($usrDetail = $this->UserRegisters->save($userRegister))
				{
					
					$userDataArr = [];
					$user_id = $usrDetail->id;
					$userData = $this->UserRegisters->find('all', array('conditions' => array('id' => $user_id)))->first();		
					$userDataArr['phone_number'] = $userData['phone_number'];
					$userDataArr['otp'] = $userData['otp_number'];
					
					$userMainData = $this->Users->find('all', array('conditions' => array('phone_number' => $userData['phone_number'])))->first();
					if(isset($userMainData) && $userMainData !=''){
						$userDataArr['user_id'] = $userMainData['id'];
					}else{
						$userDataArr['user_id'] = '0';
					}
					
					$response = $userDataArr;
					$error = false;
					$message = 'Congratulations!! You have registered successfully.';
						
					
				} else {
					$error = true;
					foreach($userRegister->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Login function for api *************************/
	public function otpLogin()
	{ 
 		if ($this->request->is(['post','put']))
    	{ 
			$this->loadModel('UserRegisters');
			$error = true; $code = 0;
			$message = $response = ''; 
			
			if (isset($this->request->data['phone_number']) && isset($this->request->data['otp_number']) && isset($this->request->data['user_id'])  && isset($this->request->data['device_token']) && isset($this->request->data['device_type']))
			{
				if($this->request->data['user_id'] == '0' ){
					
					$userregister_data = $this->UserRegisters->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
					/********* check otp exists in table are not **************/
					if($this->request->data['otp_number'] == $userregister_data['otp_number']){
						/******************** user register code ************/
						$user = $this->Users->newEntity();
						$this->request->data['username'] = 'user';
						$this->request->data['otp_number'] = $this->request->data['otp_number'];
						$this->request->data['phone_number'] = $this->request->data['phone_number'];	
						
						/*********** refer code assign to user code ************/
					
						if(isset($this->request->data['used_referal_code']) && $this->request->data['used_referal_code'] !='') {
							$user_refer = $this->Users->find('all', array(
									'fields' => array('Users.id', 'Users.refer_code', 'Users.free_subscription_time'),
									'conditions' => array('Users.refer_code' => $this->request->data['used_referal_code']),
							))->first();
							
							if(!empty($user_refer)) {
							
								$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
								$refer_code = "";
								for ($i = 0; $i < 10; $i++) {
									$refer_code .= $chars[mt_rand(0, strlen($chars)-1)];
								}
								$this->request->data['refer_code'] = $refer_code;	
								/*********** end refer code generate*********/			
								
								$user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'ApiOtp']);
								if ($usrDetail = $this->Users->save($user))
								{
										$user_id = $usrDetail->id;
									/************** get user detials after register ********************/
										$error = false;
										$message =  'Logged in successfully.';
										$user_data = $this->Users->find('all', array('conditions' => array('id' => $user_id)))->first();
										$user_data['device_token'] = $this->request->data['device_token']; 
										$user_data['device_type'] = $this->request->data['device_type'];
										$user_data['last_login'] = date('Y-m-d');
										$user_data['is_logged'] = 1;
										
										/*********** add free subscription time to user ************/
										$freeSubsDays = $this->Users->find('all', array(
												'fields' => array('Users.subscription_duration_month'),
												'conditions' => array('Users.access_level_id' => 1),
										))->first();
										if(!empty($freeSubsDays)) {
											$user_data['free_subscription_time'] = date('Y-m-d h:i:s', strtotime("+".$freeSubsDays['subscription_duration_month'].' '."months", strtotime($user_data['created'])));
											
											$user_free_time = $this->Users->get($user_id);
											$user_free_time->free_subscription_time = $user_data['free_subscription_time'];
											$this->Users->save($user_free_time);
										}
										$this->Users->save($user_data);
										/*********** End add free subscription time to user ************/
										
										$image_base_url = path_user;
										$user = $this->Users->find('all',array('conditions'=>array('id'=>$user['id'])))->first();
										if ($user['image'] != '')
										{
											$user['image'] = $image_base_url.$user['image'];
										} else {
											$user['image'] = '';
										}
										
										$response = $user;
										$this->set(array('user_id'=>$user['id'],'data'=>$response,'code'=>$code,'error'=>false,'message'=> $message,'_serialize'=>array('error','code','message','data','user_id')));
									
										/************** end get user detials after register ********************/
										$free_subscription_time = date('Y-m-d h:i:s', strtotime("+10 day", strtotime($user_refer['free_subscription_time'])));
									
										$update_free_time = $this->Users->get($user_refer['id']);
										$update_free_time->free_subscription_time = $free_subscription_time;
										$this->Users->save($update_free_time);
									
									
								} else {
									$error = true;
									foreach($user->errors() as $field_key =>  $error_data)
									{
										foreach($error_data as $error_text)
										{
											$message = $field_key.", ".$error_text;
											break 2;
										} 
									}
								}
								
							} else {
								$response = [];
								$message = 'Your referal code is incorrect.';
								$this->set(array('data'=>$response,'code'=>$code,'error'=>true,'message'=> $message,'_serialize'=>array('error','code','message','data')));
							}
						} else {
						/*********** refer code assign to user code ************/
						
							/*********** refer code generate*********/
							$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
							$refer_code = "";
							for ($i = 0; $i < 10; $i++) {
								$refer_code .= $chars[mt_rand(0, strlen($chars)-1)];
							}
							$this->request->data['refer_code'] = $refer_code;	
							/*********** end refer code generate*********/			
							
							$user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'ApiOtp']);
							if ($usrDetail = $this->Users->save($user))
							{
									$user_id = $usrDetail->id;
								/************** get user detials after register ********************/
									$error = false;
									$message =  'Logged in successfully.';
									$user_data = $this->Users->find('all', array('conditions' => array('id' => $user_id)))->first();
									$user_data['device_token'] = $this->request->data['device_token']; 
									$user_data['device_type'] = $this->request->data['device_type'];
									$user_data['last_login'] = date('Y-m-d');
									$user_data['is_logged'] = 1;
									
									/*********** add free subscription time to user ************/
									$freeSubsDays = $this->Users->find('all', array(
											'fields' => array('Users.subscription_duration_month'),
											'conditions' => array('Users.access_level_id' => 1),
									))->first();
									if(!empty($freeSubsDays)) {
										$user_data['free_subscription_time'] = date('Y-m-d h:i:s', strtotime("+".$freeSubsDays['subscription_duration_month'].' '."months", strtotime($user_data['created'])));
										
										$user_free_time = $this->Users->get($user_id);
										$user_free_time->free_subscription_time = $user_data['free_subscription_time'];
										$this->Users->save($user_free_time);
									}
									$this->Users->save($user_data);
									/*********** End add free subscription time to user ************/
									
									$image_base_url = path_user;
									$user = $this->Users->find('all',array('conditions'=>array('id'=>$user['id'])))->first();
									if ($user['image'] != '')
									{
										$user['image'] = $image_base_url.$user['image'];
									} else {
										$user['image'] = '';
									}
									$response = $user;
									$this->set(array('user_id'=>$user['id'],'data'=>$response,'code'=>$code,'error'=>false,'message'=> $message,'_serialize'=>array('error','code','message','data','user_id')));
								
								/************** end get user detials after register ********************/
								
							} else {
								$error = true;
								foreach($user->errors() as $field_key =>  $error_data)
								{
									foreach($error_data as $error_text)
									{
										$message = $field_key.", ".$error_text;
										break 2;
									} 
								}
							}
						}
						/******************** end user register code ************/
					}else{
						$response = [];
						$message = 'Please enter correct otp.';
						$this->set(array('data'=>$response,'code'=>$code,'error'=>false,'message'=> $message,'_serialize'=>array('error','code','message','data')));
					}
					
					/************** end otp exists in table ************************/
				}else{
					$userregister_data = $this->UserRegisters->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
					/********* check otp exists in table are not **************/
					if($this->request->data['otp_number'] == $userregister_data['otp_number']){
						/*********************** get user detsils *********************/
						$user = $this->Users->find('all',array('conditions'=>array('id'=>$this->request->data['user_id'])))->first();
						//print_r($user);die;
						if ($user['is_deleted'] == 'Y' || $user['enabled'] == 'N') {
							$message =  'Your account is blocked';
							$this->set(array('user_id'=>$user['id'], 'code'=>1, 'error'=>true, 'message'=> $message, '_serialize'=>array('error','code','message','user_id')));
						}
						else
						{
							$error = false;
							$message =  'Logged in successfully.';
							$user_data = $this->Users->find('all',array('conditions'=>array('id'=>$user['id'])))->first();
							$user_data['device_token'] = $this->request->data['device_token']; 
							$user_data['device_type'] = $this->request->data['device_type'];
							$user_data['last_login'] = date('Y-m-d');
							$user_data['is_logged'] = 1;
							$this->Users->save($user_data);
							$user = $this->Users->find('all',array('conditions'=>array('id'=>$user['id'])))->first();
							if ($user['image'] != '')
							{
								$user['image'] = $user['image'];
							} else {
								$user['image'] = '';
							}
							
							$response = $user;
							$this->set(array('user_id'=>$user['id'],'data'=>$response,'code'=>$code,'error'=>false,'message'=> $message,'_serialize'=>array('error','code','message','data','user_id')));
						}
						/*********************** end get user detsils *********************/
					}else{
						$response = [];
						$message = 'Please enter correct otp.';
						$this->set(array('data'=>$response,'code'=>$code,'error'=>false,'message'=> $message,'_serialize'=>array('error','code','message','data','user_id')));
					}
				}
			} else {
				$this->set(array('data' => array(), 'code' => 1,'error'=>true,'message'=> 'Incomplete data','_serialize'=>array('error','code','message','data')));
			} 						
		}
	}
	
	/****************** resend otp function for api ********************/
	public function resendOtp()
	{
		if ($this->request->is(array('post','put')))
		{
			$this->loadModel('UserRegisters');
			$error = true; $code=0;
			$message = $response = ''; 
			if(isset($this->request->data['phone_number']) && isset($this->request->data['user_id']))
			{
				
				if($this->request->data['user_id'] == '0'){
					$user = $this->UserRegisters->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
					$otpNumber = rand(1111,9999);
					if(!empty($user))
					{
						$user->otp_number = $otpNumber;
						$usrDetail = $this->UserRegisters->save($user);
						$userDataArr = [];
						$user_id = $usrDetail->id;
						$userData = $this->UserRegisters->find('all', array('conditions' => array('id' => $user_id)))->first();
						$userDataArr['phone_number'] = $userData['phone_number'];
						$userDataArr['otp'] = $userData['otp_number'];
						$userDataArr['user_id'] = '0';
						$response = $userDataArr;
						$message = "New OTP generate successfully.";
						$error = false;
						
					}
				}else{
					$userRegister = $this->UserRegisters->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
					$otpNumber = rand(1111,9999);
					
					if(!empty($userRegister))
					{
						$userRegister->otp_number = $otpNumber;
						$this->UserRegisters->save($userRegister);
						$user = $this->Users->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
						if(!empty($user))
						{
							$user->otp_number = $otpNumber;
							$usrDetail = $this->Users->save($user);
							$userDataArr = [];
							$user_id = $usrDetail->id;
							$userData = $this->Users->find('all', array('conditions' => array('id' => $user_id)))->first();
							$userDataArr['phone_number'] = $userData['phone_number'];
							$userDataArr['otp'] = $userData['otp_number'];
							$userDataArr['user_id'] = '0';
							$response = $userDataArr;
							$message = "New OTP generate successfully.";
							$error = false;
							
						}
					}
				}
				
			}
			else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
		
	}
	
	/****************** logout function for api ************************/
	public function logout()
	{
		if($this->request->is(['post','put']))
    	{
			$error = true; $code = 0;
			$message = $response = ''; 
			if (isset( $this->request->data['user_id']))
			{
				$user_data = $this->Users->find('all',array('conditions'=>array('id'=>$this->request->data['user_id'])))->first();
				if ($user_data['device_token'] ==  $this->request->data['device_token'])
				{
					$user_data['last_login'] = date('Y-m-d');
					$user_data['is_logged'] = 0;
					$user_data['device_token'] = '';
					$this->Users->save($user_data);
					$error = false;
					$message =  'Logged out successfully.';
				} else {
					if(!empty($user_data))
					{
						$user_data['last_login'] = date('Y-m-d');
						$user_data['is_logged'] = 0;
						$this->Users->save($user_data);
						$error = false;
						$message =  'Logged out successfully.';
					} else $message =  'User not found.';
				}
			}
			else $message =  'Incomplete data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('error','code','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('error','code','message','data')));
			
		}		
	}

	/****************** My profile function for api ********************/
	public function myProfile()
	{
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = ''; 
			$image_base_user_url = path_user;
			
			if(isset($this->request->data['user_id']))
			{
				$id = $this->request->data['user_id'];
				$users = $this->Users->find('all', array(
					'conditions' => array('Users.id' => $id, 'Users.access_level_id' => 2)
				))
				->hydrate(false)->first();
				
				if(!empty($users)) {
					if($users['image'] != ''){
						$image_base_url = path_user;
						$users['image'] = $users['image'];
					} else {
							$users['image']= '';
					}
					$error = false;
					$users['dob'] = date('Y-m-d', strtotime($users['dob']));
					$response =  $users;
					$response['image_base_user_url'] = $image_base_user_url;
				} 
				else $message = 'No Record Found';
			
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** edit user function for api *********************/
	public function editUser()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = ''; 
			
			 if(isset($this->request->data['user_id']))
			{
				$id = $this->request->data['user_id'];
				$users  = $this->Users->find('all',array('conditions'=>array('id'=>$id,'access_level_id'=>2)))->hydrate(false)->first();
				if(!empty($users))
				{
					$users  = $this->Users->get($id);
					
					
					$user = $this->Users->patchEntity($users, $this->request->data, ['validate' => 'ApiEditProfile']);
					$before_image = $user->image;
				
					if($saveUser = $this->Users->save($user))
					{ 
						$user = $this->Users->get($id);
						if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
						{
							$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $_FILES['image']['name']);
							$ext = pathinfo($filename, PATHINFO_EXTENSION);
							$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
							
							if ($this->uploadImage($_FILES['image']['tmp_name'], $_FILES['image']['type'], path_user_folder.'/', $filename)) {
								$url = path_user.$filename;
								$info = getimagesize($url);
								$width = 100;
								$aspectRatio = $info[1] / $info[0];
								$height = (int)($aspectRatio * $width);
								$this->createThumbnail($filename, path_user_folder, path_user_images_folder, $width,$height); 
								$user['image'] = $filename;
							}
							else  $user->image = $before_image;
						} else  $user->image = $before_image;
						$this->Users->save($user);
						$response  = $this->Users->find('all',array('conditions'=>array('id'=>$id)))->hydrate(false)->first();
						$response['dob'] = date('Y-m-d', strtotime($response['dob']));
						if($response['image'] != ''){
							$path = WWW_ROOT.path_user_folder."/".$response['image'];
							if(file_exists($path )) $response['image'] = $response['image'];
							else $response['image'] = '';
						} else $response['image'] = '';
						$image_base_user_url = path_user;
						$response['image_base_user_url'] = $image_base_user_url;
						$message = "Profile updated successfully";
						$error = false;
					}
					else
					{
						foreach($user->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$message = $field_key.", ".$error_text;
								break 2;
							} 
							
						}
					}
				} else $message = 'No Record Found';
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data'))); 
		}	
	}
	
	/****************** contact us function for api ********************/
	public function contactUs()
    {
		if ($this->request->is(array('post','put')))
		{
			if( isset( $this->request->data['subject'] ) && isset( $this->request->data['message'] ) )
			{
				$error = true; $code = 0;
				$message = $response = ''; 
				$this->loadModel('ContactUs');
				
				$contact = $this->ContactUs->newEntity();
				$contact = $this->ContactUs->patchEntity($contact, $this->request->data);
					
				if ($contactDetail = $this->ContactUs->save($contact))
				{ 
					$response = $contactDetail;
					$error = false;
					$message = 'Congratulations!! Your message has been submitted successfully.';
				} else {
					$error = true;
					foreach($contact->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}	
	}
	
	/****************** cms page function for api **********************/
	public function cmsPages()
    {
		if ($this->request->is(array('post','put')))
		{
			if ( isset( $this->request->data['id'] ) )
			{
				$error = true; $code = 0;
				$message = $response = ''; 
				$this->loadModel('Pages');
				$pages = $this->Pages->find('all',array('conditions'=>array('id'=>$this->request->data['id'], 'enabled'=>'1')))->hydrate(false)->toArray();
				if(!empty($pages))
				{ 
					$error = false;
					$response = $pages;
				} else { 
					$error = true;
					$message = "No record available";
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** get category list function fo*******************/
	public function getCategories()
	{		
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = ''; 
			$this->loadModel('Categories');
			$category_data = array();
			if(isset($this->request->data['category_id']) && $this->request->data['category_id']!='') {
				$categories = $this->Categories->find('all', ['conditions' => array('enabled' => 'Y', 'is_deleted' => 'N', 'parent_category_id' => $this->request->data['category_id']), 'order' => array('category_name' => 'ASC')])->toArray();				
				foreach($categories as $key => $value) {
					$category_data[$key] = $value;
					$category_data[$key]['cat_logo_web'] = path_category_web_image.$value['cat_logo_web'];
				}
			} else {
				$categories = $this->Categories->find('all', ['conditions' => array('enabled' => 'Y', 'is_deleted' => 'N', 'parent_category_id' => 0), 'order' => array('category_name' => 'ASC')])->toArray();			
				foreach($categories as $key => $value) {
					$category_data[$key] = $value;
					$category_data[$key]['cat_logo_web'] = path_category_web_image.$value['cat_logo_web'];
				}
							
			}
			if(!empty($categories)) {
				$error = false;
				$response = $category_data;
			} else {
				$error = true;
				$message = "No record available";	
			}
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** upload song / edit song by user function for api ***************************/
    public function songUpload()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				if(isset($this->request->data['song_upload_id']) && $this->request->data['song_upload_id'] !=''){
					$songUpload = $this->SongUploads->get($this->request->data['song_upload_id']);
					$songUpload = $this->SongUploads->patchEntity($songUpload, $this->request->data,['validate'=>'ApiEditPost']);
					$before_image = $songUpload->image;
					$before_audio = $songUpload->audio;
				}else{
					$songUpload = $this->SongUploads->newEntity();
					$songUpload = $this->SongUploads->patchEntity($songUpload, $this->request->data,['validate'=>'ApiPost']);
					$before_image = $songUpload->image;
					$before_audio = $songUpload->audio;
				}
					
			
				if($saved_songUpload = $this->SongUploads->save($songUpload))
				{
					$songUpload_id = $saved_songUpload->id;
					$songUpload = $this->SongUploads->get($songUpload_id);
					/******************* upload image **************************/
					if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
					{
						$image_base_url = path_song_upload_image;
						$songimages = $this->uploadFile($_FILES['image'],'song');
						
						if(isset($songimages) && $songimages!=''){
							$songUpload['image'] = $songimages;
						}else{
							$songUpload->image = $before_image;
						}
						
					} else  $songUpload->image = $before_image;
					/******************* end upload image **************************/
					
					/******************* upload audio **************************/
					if(isset($_FILES['audio']) && $_FILES['audio']['tmp_name'] !='')
					{
						
						$fileName = $_FILES['audio']['name'];
						$songaudio = $this->uploadFile($_FILES['audio'],'audio');
						//print_r($songaudio);die;
						if(isset($songaudio) && $songaudio!=''){
							$songUpload['audio'] = $songaudio;
						}else{
							$songUpload->audio = $before_audio;
						}
						
						
					} else  $songUpload->audio = $before_audio;
					/******************* end upload audio **************************/
					
					$this->SongUploads->save($songUpload);
					$responsepost  = $this->SongUploads->find('all',array('conditions'=>array('id'=>$songUpload_id)))->hydrate(false)->first();
					if($responsepost['image'] != ''){
						$response['image'] = path_song_upload_image.$responsepost['image'];
					} else $response['image'] = '';
					
					if($responsepost['audio'] != ''){
						$response['audio'] = path_song_upload_audio.$responsepost['audio'];
					} else $response['audio'] = '';
						
					//Send Push Notification to users
					$this->sendNotification($this->request->data['user_id'], $responsepost['id'], $responsepost['title']);
					
					$error = false;
					$message = 'Congratulations!! Your song uploaded successfully.';
					$response['song'] = $songUpload;
				}
				else
				{
					$error = true;
						
					foreach($songUpload->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
					}
				}
			}else{
				 $message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** upload song delete function for api ***************************/
	public function deleteSong()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$error =true;$code=0;
			$message= $response = '';
			if( isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['song_upload_id']) && $this->request->data['song_upload_id'] !='')
			{
				$user_id = $this->request->data['user_id'];
				$song_upload_id = $this->request->data['song_upload_id'];
				$songUpload = $this->SongUploads->get($song_upload_id);
				$songUpload->is_deleted = 'Y';
				$songUpload->enabled = 'N';
				$this->SongUploads->save($songUpload);
				$error = false;
				$message = 'Your song deleted successfully.';		
				
			}
			else $message = 'Incomplete Data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));			
		}
	}
	
	/****************** all song / My song list function for api ***************************/
	public function allSong()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			$image_base_url = path_song_upload_image;
			$audio_base_url = path_song_upload_audio;
			
			$searchData = array();
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !=''){
				$searchData['AND'][] = array("SongUploads.user_id "=>$this->request->data['user_id'] , "SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y');
			}else{
				$searchData['AND'][] = array("SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y');
			}
			
			$songUploads = $this->Paginator->paginate(
				$this->SongUploads, [
					'limit' => '10',
					'page' => $page,
					'order'=>['id'=>'asc'],
					'contain'=>[
						'user'=>['fields' => ['full_name']],
						'category'=>['fields' => ['category_name']],
						'subcategory'=>['fields' => ['category_name']]
					],
					'conditions'=>$searchData,
			]);
			$song_post_data = array();
			if (isset($songUploads) &&  $songUploads !='' && !empty($songUploads)) {
				foreach ($songUploads as $key => $value) {
					$song_post_data['id'] = $value['id'];
					$song_post_data['user_id'] = $value['user_id'];
					$song_post_data['title'] = $value['title'];
					$song_post_data['cat_id'] = $value['cat_id'];
					$song_post_data['sub_cat_id'] = $value['sub_cat_id'];
					$song_post_data['image'] = $value['image'];
					$song_post_data['audio'] = $value['audio'];
					$song_post_data['size'] = $value['size'];
					$song_post_data['duration'] = $value['duration'];
					$song_post_data['enabled'] = $value['enabled'];
					$song_post_data['is_deleted'] = $value['is_deleted'];
					$song_post_data['created'] = date('Y m d h:i:s', strtotime($value['created']));
					$song_post_data['cat_title'] = $value['category']['category_name'];
					$song_post_data['sub_cat_title'] = $value['subcategory']['category_name'];
					$song_post_data['user_name'] = $value['user']['full_name'];
					
					$songDetail = $this->showLike($value['id'], $value['user_id']);
					$song_post_data['likes'] = $songDetail['likes'];
					$song_post_data['total_likes'] = $songDetail['total_likes'];
					$song_post_data['unlikes'] = $songDetail['unlikes'];
					$song_post_data['total_unlikes'] = $songDetail['total_unlikes'];
					$song_post_data['is_comment'] = $songDetail['is_comment'];
					$song_post_data['total_comments'] = $songDetail['total_comments'];
					
					//print_r($likeUnlikeArr); die;
					
					$song_post_dataArr[] = $song_post_data;
				}
				$error = false;
				$message = 'All Post';
				$response['image_base_url'] = $image_base_url;
				$response['audio_base_url'] = $audio_base_url;
				if(isset($song_post_dataArr) &&  $song_post_dataArr !='' && !empty($song_post_dataArr)) {
					$response['song'] = $song_post_dataArr;
				}else{
					$response['song'] = [];
				}
				
			} else {
				$error = true;
				$message = "No record available";	
				
			}
			
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Download song by user function for api ***************************/
    public function downloadSongUpload()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Downloads');
			$this->loadModel('SongUploads');
			$this->loadModel('PurchasePlans');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				
				/*********** Check if user has bought a plan or not ************/
				$userPlan = $this->PurchasePlans->find('all', array(
					'fields' => array('PurchasePlans.id', 'PurchasePlans.status', 'PurchasePlans.created'),
					'conditions' => array('PurchasePlans.user_id' => $this->request->data['user_id'], 'PurchasePlans.status' => 'C', 'PurchasePlans.is_deleted' => 'N'),
					'contain'=>[
						'Premiums'=>['fields' => ['id','duration_type','duration']]
					]
				))->first();
				
				if($userPlan['premium']['duration_type'] == 'Monthly') { 
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."months", strtotime($userPlan['created'])));
				} elseif($userPlan['Premiums']['duration_type'] == 'Yearly') {	
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."years", strtotime($userPlan['created'])));
				}
				
				//calulation current time
				date_default_timezone_set('Asia/Kolkata');
				$currrenttime = time();
				$current_date = date('Y-m-d', $currrenttime);
			
				if ($userPlan =='' && empty($userPlan)) { 
					
					$free_subscription = $this->Users->find('all', array(
							'fields' => array('Users.id', 'Users.free_subscription_time'),
							'conditions' => array('Users.id' => $this->request->data['user_id']),
					))->first();
					
					if (isset($free_subscription['free_subscription_time']) && $free_subscription['free_subscription_time'] !='' && !empty($free_subscription['free_subscription_time'])) {
						$subsExpireDate =  date('Y-m-d', strtotime($free_subscription['free_subscription_time']));
						$previousSubsExpireDate = date('Y-m-d', strtotime('-1 day', strtotime($subsExpireDate)));
						//print_r($previousSubsExpireDate); die;
						if($current_date >= $subsExpireDate) {
							$free_plan = $this->Users->get($free_subscription['id']);
							$free_plan->free_subscription_time = null;
							$this->Users->save($free_plan);
							
							// push notification code here
							$message = 'Your free subscription plan has been expired. you have you purchse a subscription plan to download the song.';
							$this->sendPushNotification('Song_download', null, null, null, $this->request->data['user_id'], $message);
							
						} else {
							if ($current_date == $previousSubsExpireDate) {
								// push notification code here
								$message = 'Your free subscription plan is going to expire today.';
								$this->sendPushNotification('Song_download', null, null, null, $this->request->data['user_id'], $message);
							} 
							$download = $this->Downloads->newEntity();
							$download = $this->Downloads->patchEntity($download, $this->request->data);
							
							if($saved_download = $this->Downloads->save($download))
							{
								$download_id = $saved_download->id;
								$download = $this->Downloads->get($download_id);
								
								$responsepost  = $this->SongUploads->find('all',array('conditions'=>array('id'=>$download->song_id)))->hydrate(false)->first();
								if($responsepost['image'] != ''){
									$path = WWW_ROOT.path_song_image_folder."/".$responsepost['image'];
									if(file_exists($path )) $response['image'] = path_song_upload_image.$responsepost['image'];
									else $response['image'] = '';
								} else $response['image'] = '';
								
								if($responsepost['audio'] != ''){
									$path = WWW_ROOT.path_song_audio_folder."/".$responsepost['audio'];
									if(file_exists($path )) $response['audio'] = path_song_upload_audio.$responsepost['audio'];
									else $response['audio'] = '';
								} else $response['audio'] = '';
									
								
									$error = false;
									$message = 'Congratulations!! Your song downloaded successfully.';
									$response['download'] = $download;
							}
							else
							{
								$error = true;
									
								foreach($download->errors() as $field_key =>  $error_data)
								{
									foreach($error_data as $error_text)
									{
										$message = $field_key.", ".$error_text;
										break 2;
									} 
								}
							} 
						} 
					} else {
						// push notification code here
						$message = 'Your plan has been expired. Please upgrade your plan to download the song.';
						$this->sendPushNotification('Song_download', null, null, null, $this->request->data['user_id'], $message);
					}
				} elseif($current_date >= $planExpireDate) {
					$purchase_plan = $this->PurchasePlans->get($userPlan['id']);
					$purchase_plan->status = 'E';
					$this->PurchasePlans->save($purchase_plan);
					// push notification code here
					$message = 'Your plan has been expired. Please upgrade your plan to download the song.';
					$this->sendPushNotification('Song_download', null, null, null, $this->request->data['user_id'], $message);
				} else {
					
					$download = $this->Downloads->newEntity();
					$download = $this->Downloads->patchEntity($download, $this->request->data);
					
					if($saved_download = $this->Downloads->save($download))
					{
						$download_id = $saved_download->id;
						$download = $this->Downloads->get($download_id);
						
						$responsepost  = $this->SongUploads->find('all',array('conditions'=>array('id'=>$download->song_id)))->hydrate(false)->first();
						if($responsepost['image'] != ''){
							$path = WWW_ROOT.path_song_image_folder."/".$responsepost['image'];
							if(file_exists($path )) $response['image'] = path_song_upload_image.$responsepost['image'];
							else $response['image'] = '';
						} else $response['image'] = '';
						
						if($responsepost['audio'] != ''){
							$path = WWW_ROOT.path_song_audio_folder."/".$responsepost['audio'];
							if(file_exists($path )) $response['audio'] = path_song_upload_audio.$responsepost['audio'];
							else $response['audio'] = '';
						} else $response['audio'] = '';
							
						
							$error = false;
							$message = 'Congratulations!! Your song downloaded successfully.';
							$response['download'] = $download;
					}
					else
					{
						$error = true;
							
						foreach($download->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$message = $field_key.", ".$error_text;
								break 2;
							} 
						}
					} 
				}
			} else {
				$message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Download song delete function for api ***************************/
	public function deleteDownloadSong()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Downloads');
			$error =true;$code=0;
			$message= $response = '';
			if( isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['download_id']) && $this->request->data['download_id'] !='')
			{
				$user_id = $this->request->data['user_id'];
				$download_id = $this->request->data['download_id'];
				$download = $this->Downloads->get($download_id);
				$download->is_deleted = 'Y';
				$this->Downloads->save($download);
				$error = false;
				$message = 'Your download song deleted successfully.';		
				
			}
			else $message = 'Incomplete Data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));			
		}
	}
	
	/****************** all song list function for api ***************************/
	public function allDownloadSong()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Downloads');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['user_id']))
			{
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$image_base_url = path_song_upload_image;
				$audio_base_url = path_song_upload_audio;
				
				$searchData = array();
				$searchData['AND'][] = array("Downloads.is_deleted !=" => 'Y',"Downloads.user_id" => $this->request->data['user_id']);
				$download = $this->Paginator->paginate(
					$this->Downloads, [
						'limit' => '10',
						'page' => $page,
						'order'=>['id'=>'asc'],
						'contain'=>[
							'user'=>['fields' => ['full_name']],'songUploads'=>function($q){ 
										return $q->find('all', ['conditions' => array('songUploads.is_deleted'=>'N')])->contain(['category','subcategory']);
									}
						],
						'conditions'=>$searchData,
				]);
				$download_data = array();
				$download_dataArr = array();
				//print_r($download);die;
				if (isset($download) &&  $download !='' && !empty($download)) {
					foreach ($download as $key => $value) {
						$download_data['id'] = $value['id'];
						$download_data['song_id'] = $value['song_upload']['id'];
						$download_data['title'] = $value['song_upload']['title'];
						$download_data['category_name'] = $value['song_upload']['category']['category_name'];
						$download_data['sub_category_name'] = $value['song_upload']['subcategory']['category_name'];
						$download_data['image'] = $image_base_url.$value['song_upload']['image'];
						$download_data['audio'] = $audio_base_url.$value['song_upload']['audio'];
						$download_data['size'] = $value['song_upload']['size'];
						$download_data['duration'] = $value['song_upload']['duration'];
						
						$songDetail = $this->showLike($value['song_id'], $value['user_id']);
						$download_data['likes'] = $songDetail['likes'];
						$download_data['total_likes'] = $songDetail['total_likes'];
						$download_data['unlikes'] = $songDetail['unlikes'];
						$download_data['total_unlikes'] = $songDetail['total_unlikes'];
						$download_data['is_comment'] = $songDetail['is_comment'];
						$download_data['total_comments'] = $songDetail['total_comments'];
						
						$download_dataArr[] = $download_data;
						
					}
					$error = false;
					$message = 'All Song';
					$response['image_base_url'] = $image_base_url;
					$response['audio_base_url'] = $audio_base_url;
					
					if(isset($download_dataArr) &&  $download_dataArr !='' && !empty($download_dataArr)) {
						$response['song'] = $download_dataArr;
					}else{
						$response['song'] = [];
					}
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Send Request for Become a singer / Dj function for api ***************************/
	public function requestSingerDj()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('RequestDjSingers');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				$requestDjSinger = $this->RequestDjSingers->newEntity();
				$requestDjSinger = $this->RequestDjSingers->patchEntity($requestDjSinger, $this->request->data,['validate'=>'ApiAdd']);					
				if($saved_request = $this->RequestDjSingers->save($requestDjSinger))
				{
					$image_base_url = path_IDProof_image;
					$request_id = $saved_request->id;
					$requestDjSinger = $this->RequestDjSingers->get($request_id);
					if(isset($_FILES['id_proof']) && $_FILES['id_proof']['tmp_name'] !='')
					{
						
						$id_proofimages = $this->uploadFile($_FILES['id_proof'],'idproof');
						if(isset($id_proofimages) && $id_proofimages!=''){
							$requestDjSinger['id_proof'] = $id_proofimages;
						}else{
							$requestDjSinger['id_proof'] = '';
						}
						
					}
					$this->RequestDjSingers->save($requestDjSinger);
					$responseRequest  = $this->RequestDjSingers->find('all',array('conditions'=>array('id'=>$request_id)))->hydrate(false)->first();
					if($responseRequest['id_proof'] != ''){
						$path = WWW_ROOT.path_IDProof_image_folder."/".$responseRequest['id_proof'];
						$response['id_proof'] = path_IDProof_image.$responseRequest['id_proof'];
					} else $response['id_proof'] = '';
						$error = false;
						$message = 'Congratulations!! Your form have Submitted successfully.';
						$response = $requestDjSinger;
				}
				else
				{
					$error = true;
					foreach($requestDjSinger->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
					}
				}
			}else{
				 $message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** Create Challenge function for api ***************************/
	public function createChallenge()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Challenges');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				if(isset($this->request->data['challenge_id']) && $this->request->data['challenge_id'] !=''){
					$challenges = $this->Challenges->get($this->request->data['challenge_id']);
					date_default_timezone_set('Asia/Kolkata');
					$currrenttime = time();
					$challenge_time = $challenges->challenge_time;
					$hours = floor($challenge_time / 3600);
					$total_time = date("Y-m-d H:i:s", strtotime('+'.$hours.' hours'));
					$this->request->data['remaining_time'] =  $total_time;
					$challenges = $this->Challenges->patchEntity($challenges, $this->request->data,['validate'=>'ApiEditChallenge']);
					$before_image = $challenges->image;
					
				}else{
					$challenges = $this->Challenges->newEntity();
					date_default_timezone_set('Asia/Kolkata');
					$currrenttime = time();
					$challenge_time = $this->request->data['challenge_time'];
					$hours = floor($challenge_time / 3600);
					$total_time = date("Y-m-d H:i:s", strtotime('+'.$hours.' hours'));
					$this->request->data['remaining_time'] =  $total_time;
					$challenges = $this->Challenges->patchEntity($challenges, $this->request->data,['validate'=>'ApiChallenge']);
					$before_image = $challenges->image;
				}
				
				
				if($saved_challenges = $this->Challenges->save($challenges))
				{
					$image_base_url = path_song_upload_image;
					$challenges_id = $saved_challenges->id;
					$challenge = $this->Challenges->get($challenges_id);
					/******************* upload image **************************/
					if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
					{
						
						$challengeimages = $this->uploadFile($_FILES['image'],'song');
						if(isset($challengeimages) && $challengeimages!=''){
							$challenge['image'] = $challengeimages;
						}else{
							$challenge->image = $before_image;
						}
						
					} else  $challenge->image = $before_image;
					/******************* end upload image **************************/
					
					$this->Challenges->save($challenge);
					$responsechallenges  = $this->Challenges->find('all',array('conditions'=>array('id'=>$challenges_id)))->hydrate(false)->first();
					if($responsechallenges['image'] != ''){
						$responsechallenges['image'] = $responsechallenges['image'];
					} else $responsechallenges['image'] = '';
						
						$challenge['created'] = date('Y-m-d h:i:s', strtotime($challenge['created']));
						$error = false;
						$message = 'Congratulations!! Your challenge created successfully.';
						$response['challenge'] = $challenge;
				}
				else
				{
					$error = true;
						
					foreach($challenges->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
					}
				}
			}else{
				 $message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	
	/****************** comment List function for api ***************************/
	public function comments()
	{
		if($this->request->is(['post','put']))
    	{
			
			$this->loadModel('Comments');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			
			$searchData = array();
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && $this->request->data['song_id'] !=''&& isset($this->request->data['song_id'])){
				$searchData['AND'][] = array("Comments.user_id"=>$this->request->data['user_id'],"Comments.is_deleted !=" => 'Y',"Comments.song_id"=>$this->request->data['song_id']);
				$commentLists = $this->Paginator->paginate(
				$this->Comments, [
						'limit' => '10',
						'page' => $page,
						'order'=>['id'=>'asc'],
						'contain'=>[
							'Users'=>['fields' => ['full_name']],
							'SongUploads'=>['fields' => ['title']],
							
						],
						'conditions'=>$searchData,
				]);
				$song_comment_data = array();
				if (isset($commentLists) &&  $commentLists !='' && !empty($commentLists)) {
					foreach ($commentLists as $key => $value) {
						$song_comment_data['id'] = $value['id'];
						$song_comment_data['user_id'] = $value['user_id'];
						$song_comment_data['title'] = $value['song_upload']['title'];
						$song_comment_data['song_id'] = $value['song_id'];
						$song_comment_data['is_deleted'] = $value['is_deleted'];
						$song_comment_data['comments'] = $value['comment'];
						$song_comment_data['created'] = date('Y m d h:i:s', strtotime($value['created']));
						$song_comment_data['user_name'] = $value['user']['full_name'];
						$song_comment_dataArr[] = $song_comment_data;
					}
					$error = false;
					$message = 'All Comments';
					
						
					if(isset($song_comment_dataArr) &&  $song_comment_dataArr !='' && !empty($song_comment_dataArr)) {
						$response['comment'] = $song_comment_dataArr;
					}else{
						$response['comment'] = [];
					}
					
				} else {
					$error = true;
					$message = "No record available";
					$response['comment'] = [];	
				}
				
			}else{
				 $message = 'Incomplete Data';
			}
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	
	/****************** Add comment api **********************/
    public function addComment()
    {
		if($this->request->is(['post','put']))
    	{
    	
			$this->loadModel('Comments');
			$this->loadModel('PurchasePlans');
			$error = true; $code = 0;
			$message = $response = ''; 
			if( isset( $this->request->data['user_id'] ) && isset( $this->request->data['song_id'] ) && isset( $this->request->data['comment'] )) 
			{
				
				/*********** Check if user has bought a plan or not ************/
				$userPlan = $this->PurchasePlans->find('all', array(
					'fields' => array('PurchasePlans.id', 'PurchasePlans.status', 'PurchasePlans.created'),
					'conditions' => array('PurchasePlans.user_id' => $this->request->data['user_id'], 'PurchasePlans.status' => 'C', 'PurchasePlans.is_deleted' => 'N'),
					'contain'=>[
						'Premiums'=>['fields' => ['id','duration_type','duration']]
					]
				))->first();
				
				if($userPlan['premium']['duration_type'] == 'Monthly') { 
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."months", strtotime($userPlan['created'])));
				} elseif($userPlan['premium']['duration_type'] == 'Yearly') {	
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."years", strtotime($userPlan['created'])));
				}
				
				//calulation current time
				date_default_timezone_set('Asia/Kolkata');
				$currrenttime = time();
				$current_date = date('Y-m-d', $currrenttime);
			
				if ($userPlan =='' && empty($userPlan)) { 
					
					$free_subscription = $this->Users->find('all', array(
							'fields' => array('Users.id', 'Users.free_subscription_time'),
							'conditions' => array('Users.id' => $this->request->data['user_id']),
					))->first();
					
					if (isset($free_subscription['free_subscription_time']) && $free_subscription['free_subscription_time'] !='' && !empty($free_subscription['free_subscription_time'])) {
						$subsExpireDate =  date('Y-m-d', strtotime($free_subscription['free_subscription_time']));
						$previousSubsExpireDate = date('Y-m-d', strtotime('-1 day', strtotime($subsExpireDate)));
						//print_r($previousSubsExpireDate); die;
						if($current_date >= $subsExpireDate) {
							$free_plan = $this->Users->get($free_subscription['id']);
							$free_plan->free_subscription_time = null;
							$this->Users->save($free_plan);
							
							// push notification code here
							$message = 'Your free subscription plan has been expired. you have you purchse a subscription plan to comment.';
							$this->sendPushNotification('add_comment', $this->request->data['song_id'], null, null, $this->request->data['user_id'], $message);
						} else {
							if ($current_date == $previousSubsExpireDate) {
								// push notification code here
								$message = 'Your free subscription plan is going to expire today.';
								$this->sendPushNotification('add_comment', $this->request->data['song_id'], null, null, $this->request->data['user_id'], $message);
							} 
							// add comment code
							$addcomment = $this->Comments->newEntity();
							$addcomments = $this->Comments->patchEntity($addcomment, $this->request->data);
							if ($CommentsDetail = $this->Comments->save($addcomments))
							{
								$error = false;
								$message = 'Comment Added Successfuly';
							} else {
								$error = true;
								foreach($addcomments->errors() as $field_key =>  $error_data)
								{
									foreach($error_data as $error_text)
									{
										$message = $field_key.", ".$error_text;
										break 2;
									} 
									
								}
							} 
						} 
					} else {
						// push notification code here
						$message = 'Your plan has been expired. Please upgrade your plan to comment.';
						$this->sendPushNotification('add_comment', $this->request->data['song_id'], null, null, $this->request->data['user_id'], $message);
					}
				} elseif($current_date >= $planExpireDate) {
					$purchase_plan = $this->PurchasePlans->get($userPlan['id']);
					$purchase_plan->status = 'E';
					$this->PurchasePlans->save($purchase_plan);
					// push notification code here
					$message = 'Your plan has been expired. Please upgrade your plan to comment.';
					$this->sendPushNotification('add_comment', $this->request->data['song_id'], null, null, $this->request->data['user_id'], $message);
				} else {
					
					// add comment code
					$addcomment = $this->Comments->newEntity();
					$addcomments = $this->Comments->patchEntity($addcomment, $this->request->data);
					if ($CommentsDetail = $this->Comments->save($addcomments))
					{
						$error = false;
						$message = 'Comment Added Successfuly';
					} else {
						$error = true;
						foreach($addcomments->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$message = $field_key.", ".$error_text;
								break 2;
							} 
							
						}
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
		
	/****************** Like/Dislike api **********************/
    public function likeDislike()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Likes');
			$error = true; $code = 0;
			$message = $response = array(); 
			
			if( isset( $this->request->data['user_id'] ) && isset( 
			$this->request->data['song_id'] ) && isset( $this->request->data['like'] )) 
			{
				$likedata = $this->Likes->find()->where(['user_id'=>$this->request->data['user_id'],
				'song_id'=>$this->request->data['song_id']])->toArray();
				if(!empty($likedata)){
					$likesdetail  = $this->Likes->get($likedata[0]['id']);
					if($this->request->data['like']==0){
						$this->request->data['status']=0;
						$message = 'You dislike this song';
					}else{
						$this->request->data['status']=1;
						$message = 'Thanks for like this song';
					}
					$likeadd = $this->Likes->patchEntity($likesdetail, $this->request->data);
					$saveLikes = $this->Likes->save($likeadd);
					$error = false;
					$response['total_likes'] = $this->Likes->find()->where(['song_id'=>$this->request->data['song_id'],'status'=>1])->count();
					$response['total_unlikes'] = $this->Likes->find()->where(['song_id'=>$this->request->data['song_id'],'status'=>0])->count();
				}else{
					$addLikes = $this->Likes->newEntity();
					$addLike = $this->Likes->patchEntity($addLikes, $this->request->data);
					if ($LikesDetail = $this->Likes->save($addLike))
					{
						$response['total_likes'] = $this->Likes->find()->where(['song_id'=>$this->request->data['song_id'],'status'=>1])->count();
						$response['total_unlikes'] = $this->Likes->find()->where(['song_id'=>$this->request->data['song_id'],'status'=>0])->count();
						$error = false;
						$message = 'Thanks for like this song';
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data'=>array(),'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Search songs api **********************/
	public function searchSongs()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['page']) && $this->request->data['page'] !='' ){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			$image_base_url = path_song_upload_image;
			$audio_base_url = path_song_upload_audio;
			
			$searchData = array();
			/*********** Search using by text ***********/
			if(isset($this->request->data['text']) || isset($this->request->data['user_id'])  || isset($this->request->data['cat_id'])  || isset($this->request->data['sub_cat_id'])){
				
				if($this->request->data['cat_id']!='' && $this->request->data['sub_cat_id'] !='' && $this->request->data['text']!='' && $this->request->data['user_id']!=''){
					$searchData['AND'][] = ["SongUploads.cat_id" => $this->request->data['cat_id'],"SongUploads.sub_cat_id" => $this->request->data['sub_cat_id'],'SongUploads.title LIKE' => '%'.$this->request->data['text'].'%',"SongUploads.user_id" => $this->request->data['user_id']];
				}else if($this->request->data['cat_id']!='' && $this->request->data['sub_cat_id'] !='' && $this->request->data['user_id']!=''){
					$searchData['AND'][] = ["SongUploads.cat_id" => $this->request->data['cat_id'],"SongUploads.sub_cat_id" => $this->request->data['sub_cat_id'],"SongUploads.user_id" => $this->request->data['user_id']];
				}else if($this->request->data['user_id']!='' && $this->request->data['text'] !=''){
					$searchData['AND'][] = ["SongUploads.user_id" => $this->request->data['user_id'],'SongUploads.title LIKE' => '%'.$this->request->data['text'].'%'];
				}else if($this->request->data['user_id']!='' && $this->request->data['text'] ==''){
					$searchData['AND'][] = ["SongUploads.user_id" => $this->request->data['user_id']];
				}else{
					$searchData['AND'][] = ['SongUploads.title LIKE' => '%'.$this->request->data['text'].'%'];
				}
				$searchData['AND'][] = array("SongUploads.is_deleted !=" => 'Y');
			}
			
				$searchList = $this->Paginator->paginate(
				$this->SongUploads, [
					'limit' => '10',
					'page' => $page,
					'order'=>['id'=>'asc'],
					'contain'=>['user'],
					'conditions'=>$searchData,
				]);
				$searchList_data = array();
				$searchList_dataArr = array();
				if (isset($searchList) &&  $searchList !='' && !empty($searchList)) {
					foreach ($searchList as $key => $value) {
						$searchList_data['id'] = $value['id'];
						$searchList_data['user_id'] = $value['user_id'];
						$searchList_data['title'] = $value['title'];
						$searchList_data['cat_id'] = $value['cat_id'];
						$searchList_data['sub_cat_id'] = $value['sub_cat_id'];
						$searchList_data['image'] = $value['image'];
						$searchList_data['audio'] = $value['audio'];
						$searchList_data['enabled'] = $value['enabled'];
						$searchList_data['is_deleted'] = $value['is_deleted'];
						$searchList_data['created'] = date('Y m d h:i:s', strtotime($value['created']));
						$searchList_data['cat_title'] = $value['category']['category_name'];
						$searchList_data['sub_cat_title'] = $value['subcategory']['category_name'];
						$searchList_data['user_name'] = $value['user']['full_name'];
						
						$songDetail = $this->showLike($value['id'], $value['user_id']);
						$searchList_data['likes'] = $songDetail['likes'];
						$searchList_data['total_likes'] = $songDetail['total_likes'];
						$searchList_data['unlikes'] = $songDetail['unlikes'];
						$searchList_data['total_unlikes'] = $songDetail['total_unlikes'];
						$searchList_data['is_comment'] = $songDetail['is_comment'];
						$searchList_data['total_comments'] = $songDetail['total_comments'];
						
						$searchList_dataArr[] = $searchList_data;
					}
					$error = false;
					$message = 'All Song';
					$response['image_base_url'] = $image_base_url;
					$response['audio_base_url'] = $audio_base_url;
					
					if(isset($searchList_dataArr) &&  $searchList_dataArr !='' && !empty($searchList_dataArr)) {
						$response['song'] = $searchList_dataArr;
					}else{
						$response['song'] = [];
					}
					
				}else {
					$response['song'] = [];
					$error = false;
					$message = "all songs";	
				}
		
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
			
	/****************** Search song Detail function for api ***************************/
	public function songDetail()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$this->loadModel('Likes');
			$this->loadModel('Comments');
			$error =true;$code=0;
			$message= ""; $response = [];
		
			$image_base_url = path_song_upload_image;
			$audio_base_url = path_song_upload_audio;
			
			$songDetails=$this->SongUploads->get($this->request->data['song_id'], ['contain'=> ['category','subcategory','likes','comment']])->toArray();
			$songDetails_dataArr = array();
			if (isset($songDetails) &&  $songDetails !='' && !empty($songDetails)) {
				$songDetails_data['id'] = $songDetails['id'];
				$songDetails_data['user_id'] = $songDetails['user_id'];
				$songDetails_data['cat_id'] = $songDetails['cat_id'];
				$songDetails_data['cat_name'] = $songDetails['category']['category_name'];
				$songDetails_data['sub_cat_id'] = $songDetails['sub_cat_id'];
				$songDetails_data['sub_cat_name'] = $songDetails['subcategory']['category_name'];
				$songDetails_data['title'] = $songDetails['title'];
								
				$songDetail = $this->showLike($songDetails['id'], $songDetails['user_id']);
				$songDetails_data['likes'] = $songDetail['likes'];
				$songDetails_data['total_likes'] = $songDetail['total_likes'];
				$songDetails_data['unlikes'] = $songDetail['unlikes'];
				$songDetails_data['total_unlikes'] = $songDetail['total_unlikes'];
				$songDetails_data['is_comment'] = $songDetail['is_comment'];
				$songDetails_data['total_comments'] = $songDetail['total_comments'];
				
				$songDetails_data['image'] = $image_base_url.$songDetails['image'];
				$songDetails_data['audio'] = $audio_base_url.$songDetails['audio'];
				$error = false;
				$message = 'Song Details';
				$response = $songDetails_data;
			} else {
				$error = true;
				$message = "No record available";	
				$response = [];
			}
			
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** My Challenge Other user challange list function for api ***************************/
	public function myChallengeList()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Challenges');
			$error =true;$code=0;
				$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$searchData = array();
				if(isset($this->request->data['status']) && $this->request->data['status'] =='1'){
					$searchData['AND'][] = array("Challenges.user_id"=>$this->request->data['user_id'], "Challenges.is_deleted !=" => 'Y', "Challenges.status" => '1');
				}else{
					$searchData['AND'][] = array("Challenges.user_id"=>$this->request->data['user_id'], "Challenges.is_deleted !=" => 'Y');
				}
				
				$challengesData = $this->Paginator->paginate(
					$this->Challenges, [
						'limit' => '10',
						'page' => $page,
						'order'=>['id'=>'asc'],
						'contain'=>[
							'user'=>['fields' => ['full_name','image']],
							'song'=>['fields' => ['title']]
						],
						'conditions'=>$searchData,
				]);
				$challengesDataArr = array();
				$image_base_user_url = path_user;
				$image_base_url = path_song_upload_image;
				if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
					foreach ($challengesData as $key => $value) {
						
						$challenges['id'] = $value['id'];
						$challenges['user_id'] = $value['user_id'];
						$challenges['accept_user_id'] = $value['accept_user_id'];
						$challenges['type'] = $value['type'];
						$challenges['song_id'] = $value['song_id'];
						$challenges['challenge_time'] = $value['challenge_time'];
						$challenges['challenge_amount'] = $value['challenge_amount'];
						$challenges['challenge_image'] = $value['image'];
						$challenges['song_name'] = $value['song']['title'];
						$challenges['full_name'] = $value['user']['full_name'];
						$challenges['userimage'] = $value['user']['image'];
						$challenges['status'] = $value['status'];
						
						$this->loadModel('ChallengeLikes');
						$total_challenge_likes = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user_id'],'status'=>1])->count();

						$total_challenge_unlikes = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user_id'],'status'=>0])->count();
						
						$challenges['total_challenge_likes'] = $total_challenge_likes;
						$challenges['total_challenge_unlikes'] = $total_challenge_unlikes;
						
						$challengesDataArr[] = $challenges;
					}
					$error = false;
					$message = 'My Challenge';
					$response['image_base_user_url'] = $image_base_user_url;
					$response['image_base_url'] = $image_base_url;
					
					if(isset($challengesDataArr) &&  $challengesDataArr !='' && !empty($challengesDataArr)) {
						$response['challenge'] = $challengesDataArr;
					}else{
						$response['challenge'] = [];
					}
					
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	
	/****************** Add Song in Recently list function for api ***************************/
	public function recentlyPlayed()
	{
		if($this->request->is(['post','put']))
    	{
    	
			$this->loadModel('RecentlySongs');
			$error = true; $code = 0;
			$message = $response = ''; 
			if( isset( $this->request->data['user_id'] ) && $this->request->data['user_id']  !='' && isset( $this->request->data['song_id'] ) && $this->request->data['song_id'] !='') 
			{
				$recentlySong = $this->RecentlySongs->newEntity();
				$recentlySongs = $this->RecentlySongs->patchEntity($recentlySong, $this->request->data);
				if ($recentlyDetail = $this->RecentlySongs->save($recentlySongs))
				{
					$error = false;
					$message = 'Song Added in Recently list.';
				} else {
					$error = true;
					foreach($recentlySongs->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	
	
	/****************** Singer / DJ lsit function for api ***************************/
	public function singerDj()
	{
		if($this->request->is(['post','put']))
    	{
			$error =true;$code=0;
			$message= ""; $response = [];
			$this->loadModel('RequestDjSingers');
			$this->loadModel('Likes');
			$this->loadModel('NotificationStatus');
			if(isset($this->request->data['type']) && isset($this->request->data['user_id']))
			{ 
				
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$searchData = array();
				if($this->request->data['type'] =='S'){
					$typeRequest = 'Singer';
					$searchData['AND'][] = array("RequestDjSingers.type"=>'S',"RequestDjSingers.user_id !="=>$this->request->data['user_id'],"RequestDjSingers.is_approved"=>'Y', "RequestDjSingers.is_deleted !=" => 'Y');
				}else{
					$typeRequest = 'D';
					$searchData['AND'][] = array("RequestDjSingers.type"=>'D',"RequestDjSingers.user_id !="=>$this->request->data['user_id'],"RequestDjSingers.is_approved"=>'Y', "RequestDjSingers.is_deleted !=" => 'Y');
				}
				
				$requestDjSingers = $this->Paginator->paginate(
					$this->RequestDjSingers, [
						'limit' => '10',
						'page' => $page,
						'order'=>['id'=>'asc'],
						'contain'=>[
							'user'=>['fields' => ['full_name','image','email','dob','address']]
						],
						'conditions'=>$searchData,
				]);
				$requestDjSingersdata = array();
				
				$image_base_url = path_user;
				if (isset($requestDjSingers) &&  $requestDjSingers !='' && !empty($requestDjSingers)) {
					foreach ($requestDjSingers as $key => $value) {
						
						$total_likes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>1])->count();
						$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>0])->count();
						
						$requestDj['id'] = $value['id'];
						$requestDj['user_id'] = $value['user_id'];
						$requestDj['full_name'] = $value['user']['full_name'];
						$requestDj['email'] = $value['user']['email'];
						$requestDj['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$requestDj['address'] = $value['user']['address'];
						$requestDj['image'] = $value['user']['image'];
						$requestDj['like_count'] = $total_likes;
						$requestDj['unlike_count'] = $total_unlikes;
						
						$isNotificationStatus  = $this->NotificationStatus->find('all',array('conditions'=>array('other_user_id'=>$value['user_id'])))->hydrate(false)->first();
						if(isset($isNotificationStatus) && $isNotificationStatus !='' && !empty($isNotificationStatus)){
							$requestDj['notification'] = true;
						}else{
							$requestDj['notification'] = false;
						}
						
						$requestDjSingersdata[] = $requestDj;
					}
					$error = false;
					$message = 'All '.$typeRequest;
					$response['image_base_url'] = $image_base_url;
					
					if(isset($requestDjSingersdata) &&  $requestDjSingersdata !='' && !empty($requestDjSingersdata)) {
						$response['user'] = $requestDjSingersdata;
					}else{
						$response['user'] = [];
					}
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	
	/******************Accept Challenge function for api ***************************/
	public function acceptChallenge()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Challenges');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']) && isset($this->request->data['challenge_id']) && $this->request->data['challenge_id'] !=''  && isset($this->request->data['accepted_user_song_id']) && $this->request->data['accepted_user_song_id'] !=''  && isset($this->request->data['accepted_user_image']) && $this->request->data['accepted_user_image'] !='' && isset($this->request->data['status']) && $this->request->data['status'] !='')
			{ 		
				
				//calculate remaining time
				$challenges = $this->Challenges->get($this->request->data['challenge_id']);
				
				if($this->request->data['status'] =='1'){
					$action = 'Accepted';
				}else{
					$action = 'Declined';
				}
				
				$isChkchallenges  = $this->Challenges->find('all',array('conditions'=>array('song_id'=>$challenges->song_id,'status'=>'1')))->hydrate(false)->first();
				//print_r( $this->request->data['accepted_user_song_id']);die;
				if(!isset($isChkchallenges) && empty($isChkchallenges)){
						//calulation time
					
						date_default_timezone_set('Asia/Kolkata');
						$currrenttime = time();
						$date = strtotime(date('Y-m-d h:i:s',$currrenttime));
						$date2 = strtotime(date('Y-m-d h:i:s',strtotime($challenges->remaining_time)));
						$diff = $date2 - $date;
					$challenges->remaining_second =  abs($diff);
					if($this->request->data['status'] =='1'){
						$challenges->status =  '1';
					}else{
						$challenges->status =  '2';
					}
					$challenges->accept_user_id =  $this->request->data['user_id'];
					$challenges->accepted_user_song_id =  $this->request->data['accepted_user_song_id'];
					
					
					if($saved_challenges = $this->Challenges->save($challenges))
					{
						
						/*************accept user upload image functionlity ****************/
						$image_base_url = path_song_upload_image;
						$challenges_id = $saved_challenges->id;
						$challenge = $this->Challenges->get($challenges_id);
						/******************* upload image **************************/
						if(isset($_FILES['accepted_user_image']) && $_FILES['accepted_user_image']['tmp_name'] !='')
						{
							
							$challengeimages = $this->uploadFile($_FILES['accepted_user_image'],'song');
							if(isset($challengeimages) && $challengeimages!=''){
								$challenge['accepted_user_image'] = $challengeimages;
							}else{
								$challenge->image = $before_image;
							}
							
						} else  $challenge->image = $before_image;
						/******************* end upload image **************************/
						
						$this->Challenges->save($challenge);
						$responsechallenges  = $this->Challenges->find('all',array('conditions'=>array('id'=>$challenges_id)))->hydrate(false)->first();
						if($responsechallenges['accepted_user_image'] != ''){
							$responsechallenges['accepted_user_image'] = $responsechallenges['accepted_user_image'];
						} else $responsechallenges['accepted_user_image'] = '';
							
						/*************accept user upload image functionlity ****************/
						$responsechallenges['created'] = date('Y-m-d h:i:s', strtotime($responsechallenges['created']));
						$error = false;
						$message = 'Congratulations!! Your challenge '.$action.' successfully.';
						$response['challenge'] = $responsechallenges;
					}
					else
					{
						$error = true;
							
						foreach($challenges->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$message = $field_key.", ".$error_text;
								break 2;
							} 
						}
					}
				}else{
					$image_base_url = path_song_upload_image;
					$challenges_id = $this->request->data['challenge_id'];
					$responsechallenges  = $this->Challenges->find('all',array('conditions'=>array('id'=>$challenges_id)))->hydrate(false)->first();
					if($responsechallenges['image'] != ''){
						$responsechallenges['image'] = $responsechallenges['image'];
					} else $responsechallenges['image'] = '';
						
						$responsechallenges['created'] = date('Y-m-d h:i:s', strtotime($responsechallenges['created']));
						$error = false;
						$message = 'Sorry!! This challenge allready '.$action.'.';
						$response['challenge'] = $responsechallenges;
				}
				
			}else{
				 $message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	}
	
		/****************** Live Challenge for accept / decline list function for api ***************************/
	public function liveChallengeList()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Challenges');
			$error =true;$code=0;
				$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$searchData = array();
				$searchData['AND'][] = array("Challenges.user_id !="=>$this->request->data['user_id'], "Challenges.is_deleted !=" => 'Y', "Challenges.status" => '0');
				
				$challengesData = $this->Paginator->paginate(
					$this->Challenges, [
						'limit' => '10',
						'page' => $page,
						'order'=>['id'=>'asc'],
						'contain'=>[
							'user'=>['fields' => ['full_name','image']],
							'song'=>['fields' => ['title']]
						],
						'conditions'=>$searchData,
				]);
				$challengesDataArr = array();
				$image_base_user_url = path_user;
				$image_base_url = path_song_upload_image;
				if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
					foreach ($challengesData as $key => $value) {
						
						$challenges['id'] = $value['id'];
						$challenges['user_id'] = $value['user_id'];
						$challenges['type'] = $value['type'];
						$challenges['song_id'] = $value['song_id'];
						$challenges['challenge_time'] = $value['challenge_time'];
						$challenges['challenge_amount'] = $value['challenge_amount'];
						$challenges['challenge_image'] = $value['image'];
						$challenges['song_name'] = $value['song']['title'];
						$challenges['full_name'] = $value['user']['full_name'];
						$challenges['userimage'] = $value['user']['image'];
						$challenges['status'] = $value['status'];
						
						//second calculation ago time
						date_default_timezone_set('Asia/Kolkata');
						$createdTime = strtotime(date('Y-m-d h:i:s',strtotime($value['created'])));
						$challenges['created'] = $this->time_elapsed_string($createdTime);
						
						$songDetail = $this->showLike($value['song_id'], $value['user_id']);
						$challenges['likes'] = $songDetail['likes'];
						$challenges['total_likes'] = $songDetail['total_likes'];
						$challenges['unlikes'] = $songDetail['unlikes'];
						$challenges['total_unlikes'] = $songDetail['total_unlikes'];
						$challenges['is_comment'] = $songDetail['is_comment'];
						$challenges['total_comments'] = $songDetail['total_comments'];
						
						$challengesDataArr[] = $challenges;
					}
					$error = false;
					$message = 'My Challenge';
					$response['image_base_user_url'] = $image_base_user_url;
					$response['image_base_url'] = $image_base_url;
					
					if(isset($challengesDataArr) &&  $challengesDataArr !='' && !empty($challengesDataArr)) {
						$response['challenge'] = $challengesDataArr;
					}else{
						$response['challenge'] = [];
					}
						
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Create New Playlist api **********************/
    public function createNewPlaylist()
    {
		if($this->request->is(['post','put']))
    	{   	
			$this->loadModel('PlaylistCategories');
			$error = true; $code = 0;
			$message = $response = []; 
			if( isset( $this->request->data['user_id'] ) && isset( $this->request->data['category_name'] )) 
			{
				$addplaylist = $this->PlaylistCategories->newEntity();
				$addplaylists = $this->PlaylistCategories->patchEntity($addplaylist, $this->request->data);
				if ($PlaylistsDetail = $this->PlaylistCategories->save($addplaylists))
				{
					$error = false;
					$message = 'Playlist Added Successfuly';
				} else {
					$error = true;
					foreach($addplaylists->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Delete Playlist api ***************************/
	public function deletePlaylist()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('PlaylistCategories');
			$error = true; $code = 0;
			$message = $response = '';
			if( isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['playlist_id']) && $this->request->data['playlist_id'] !='')
			{
				$user_id = $this->request->data['user_id'];
				$playlist_id = $this->request->data['playlist_id'];
				$playlist = $this->PlaylistCategories->get($playlist_id);
				$playlist->is_deleted = 'Y';
				$this->PlaylistCategories->save($playlist);
				$error = false;
				$message = 'Your Playlist category has been deleted successfully.';						
			}
			else $message = 'Incomplete Data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));			
		}
	}
	
	/****************** All playlist categories list function for api ***************************/
	public function playlistCategoriesList()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('PlaylistCategories');
			$error = true; $code = 0;
			$message = ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$searchData = array();
				
				$searchData['AND'][] = array("PlaylistCategories.user_id"=>$this->request->data['user_id'], "PlaylistCategories.is_deleted !=" => 'Y');
								
				$playlistsData = $this->Paginator->paginate(
					$this->PlaylistCategories, [
						'limit' => '10',
						'page' => $page,
						'order'=>['id'=>'asc'],
						'contain'=>[
							'user'=>['fields' => ['full_name','image']]
						],
						'conditions'=>$searchData,
				]);
				$playlistsDataArr = array();
				$image_base_user_url = path_user;
				$image_base_url = path_song_upload_image;
				if (isset($playlistsData) &&  $playlistsData !='' && !empty($playlistsData)) {
					foreach ($playlistsData as $key => $value) {
						
						$playlists['id'] = $value['id'];
						$playlists['user_id'] = $value['user_id'];
						$playlists['category_name'] = $value['category_name'];
						$playlists['full_name'] = $value['user']['full_name'];
						$playlists['userimage'] = $value['user']['image'];
						
						$playlistsDataArr[] = $playlists;
					}
					$error = false;
					$message = 'Playlist Categories';
					$response['image_base_user_url'] = $image_base_user_url;
					$response['image_base_url'] = $image_base_url;
					if(isset($playlistsDataArr) &&  $playlistsDataArr !='' && !empty($playlistsDataArr)) {
						$response['playlist_categories'] = $playlistsDataArr;
					}else{
						$response['playlist_categories'] = [];
					}
					
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Playlist songs listing by category function for api ***************************/
	public function songListingByPlaylistCategory()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Playlists');
			$error = true; $code = 0;
			$message = ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['playlist_category_id']) && $this->request->data['playlist_category_id'] !='')
			{ 
				$playlistsData = $this->Playlists->find('all', array(
					'fields' => array('Playlists.id', 'Playlists.song_id', 'Playlists.user_id'),
					'conditions' => array('Playlists.user_id' => $this->request->data['user_id'], 'Playlists.playlist_category_id' => $this->request->data['playlist_category_id']),
					'contain'=>[
						'user'=>['fields' => ['full_name','image']],
						'songUploads'=>['fields' => ['id','title','image','audio']]
					]	
				));
			
				$playlistsDataArr = array();
				$image_base_user_url = path_user;
				$image_base_url = path_song_upload_image;
				$audio_base_url = path_song_upload_audio;
				
				if (isset($playlistsData) &&  $playlistsData !='' && !empty($playlistsData)) {
					foreach ($playlistsData as $key => $value) {
						
						//$playlists['id'] = $value['id'];
						//$playlists['user_id'] = $value['user_id'];
						//$playlists['song_id'] = $value['song_id'];
						//$playlists['user'] = $value['user'];
						$playlists = $value['song_upload'];
						
						$songDetail = $this->showLike($value['song_id'], $value['user_id']);
						$playlists['likes'] = $songDetail['likes'];
						$playlists['total_likes'] = $songDetail['total_likes'];
						$playlists['unlikes'] = $songDetail['unlikes'];
						$playlists['total_unlikes'] = $songDetail['total_unlikes'];
						$playlists['is_comment'] = $songDetail['is_comment'];
						$playlists['total_comments'] = $songDetail['total_comments'];
						
						$playlistsDataArr[] = $playlists;
					}
					$error = false;
					$message = 'Playlist Category Songs';
					$response['image_base_user_url'] = $image_base_user_url;
					$response['image_base_url'] = $image_base_url;
					$response['audio_base_url'] = $audio_base_url;
					
					if(isset($playlistsDataArr) &&  $playlistsDataArr !='' && !empty($playlistsDataArr)) {
						$response['playlist_category_songs'] = $playlistsDataArr;
					}else{
						$response['playlist_category_songs'] = [];
					}
						
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			} else {
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Delete song from playlist function for api ***************************/
	public function deleteSongFromPlaylist()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Playlists');
			$error = true; $code = 0;
			$message = $response = '';
			if( isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['playlist_id']) && $this->request->data['playlist_id'] !='')
			{
				$user_id = $this->request->data['user_id'];
				$playlist_id = $this->request->data['playlist_id'];
				$playlist = $this->Playlists->get($playlist_id);
				$playlist->is_deleted = 'Y';
				$this->Playlists->save($playlist);
				$error = false;
				$message = 'Song from Playlist category has been deleted successfully.';						
			}
			else $message = 'Incomplete Data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));			
		}
	}
	
	/****************** Add Songs to Playlist function for api ***************************/
	public function addSongsToPlaylist()
	{
		if($this->request->is(['post','put']))
    	{   	
			$this->loadModel('Playlists');
			$error = true; $code = 0;
			$message = $response = ''; 
			
			if( isset( $this->request->data['user_id'] ) && isset( $this->request->data['song_ids']) && $this->request->data['song_ids'] != "" && isset( $this->request->data['playlist_category_id'] ) )
			{
				$song_ids =  $this->request->data['song_ids'];
				$song_ids = json_decode($song_ids);
				//print_r($song_ids); die;
				if(is_array($song_ids) && !empty($song_ids))
				{ 
					foreach($song_ids as $song_id)
					{
						$prev_user_playlist = $this->Playlists->find('all', array(
							'fields' => array('Playlists.id'),
							'conditions' => array('Playlists.user_id' => $this->request->data['user_id'], 'Playlists.song_id' => $song_id, 'Playlists.playlist_category_id' => $this->request->data['playlist_category_id'])
						))->first();
						
						if(!empty($prev_user_playlist)) {
							$entity = $this->Playlists->get($prev_user_playlist['id']);
							$this->Playlists->delete($entity);
						}
						
						$this->request->data['song_id'] = $song_id;
						//print_r($this->request->data); die;
						$addplaylist = $this->Playlists->newEntity();
						$addplaylists = $this->Playlists->patchEntity($addplaylist, $this->request->data);
						if ($PlaylistsDetail = $this->Playlists->save($addplaylists))
						{
							$error = false;
							$message = 'Songs are Added to Playlist Successfuly';
						} else {
							$error = true;
							foreach($addplaylists->errors() as $field_key => $error_data)
							{
								foreach($error_data as $error_text)
								{
									$message = $field_key.", ".$error_text;
									break 2;
								} 
							}
						}
						
					} 
				}
			}
			
			else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** Home page function for api ***************************/
	public function homePage()
	{
		if($this->request->is(['post','put']))
    	{
									
			//$this->sendPushNotification('24', 'abc', 'hello');
			$this->loadModel('RecentlySongs');
			$this->loadModel('Challenges');
			$this->loadModel('SongUploads');
			$this->loadModel('Posts');
			$this->loadModel('Categories');
			$this->loadModel('RequestDjSingers');
			$this->loadModel('Likes');
			$this->loadModel('ChallengeLikes');
			$this->loadModel('ChallengeWinners');
			$error = true; $code = 0;
			$message = ""; $response = array();
			$responseA = array();
			$response_base = array();
			$allresponse = array();
			$allresponseArr = array(); 
			
			$image_base_user_url = path_user;
			$post_image_base_url = path_post_image;
			$song_image_base_url = path_song_upload_image;
			$song_audio_base_url = path_song_upload_audio;
			$image_banner_url = path_banner_image;
			
			$response_base['image_base_user_url'] = $image_base_user_url;
			$response_base['post_image_base_url'] = $post_image_base_url;
			$response_base['song_image_base_url'] = $song_image_base_url;
			$response_base['song_audio_base_url'] = $song_audio_base_url;
			$response_base['image_banner_url'] = $image_banner_url;
			
			if (isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
				
				
				/*********** Check if user is requested as Singer/Dj or not ************/
				$checkSingerDj = $this->RequestDjSingers->find('all', array(
					'fields' => array('RequestDjSingers.id','RequestDjSingers.is_approved'),
					'conditions' => array('RequestDjSingers.user_id' => $this->request->data['user_id'], 'RequestDjSingers.is_deleted' => 'N')
				))->first();
			
				if (isset($checkSingerDj) &&  $checkSingerDj !='' && !empty($checkSingerDj)) {
					if($checkSingerDj['is_approved'] == 'Y'){
						$response_base['is_approved'] = true;
					}else{
						$response_base['is_approved'] = false;
					}
					$response_base['singerDjCreated'] = true;
				} else {
					$response_base['is_approved'] = false;
					$response_base['singerDjCreated'] = false;
				}
			
			} else {
				$response_base['is_approved'] = false;
				$response_base['singerDjCreated'] = false;
			}
			
			$allresponseArr = $response_base;
			
			if (isset($this->request->data['type']) && !empty($this->request->data['type'])) {
				
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				} else {
					$page = '1';
				}
				
				
				if ($this->request->data['type'] == 'recently_played') {
					/*********** Recently Played Songs List ************/
					if (isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
						
						$recentlySongsData = $this->Paginator->paginate(
							$this->RecentlySongs, [
								'fields' => array('RecentlySongs.id', 'RecentlySongs.song_id', 'RecentlySongs.user_id'),
								'limit' => '1',
								'page' => $page,
								'order'=>['RecentlySongs.id'=>'desc'],
								'group'=>['RecentlySongs.song_id'],	
								'contain'=>[
									'user'=>['fields' => ['full_name','image']],
									'song'=> function(\Cake\ORM\Query $q) {
										return $q->find('all', array(
											'fields' => ['id','user_id','cat_id','sub_cat_id','title','image','audio','size','duration','created'],
											'conditions' => array("song.is_deleted !=" => 'Y', "song.enabled" => 'Y'),
											'contain'=>[
												'user'=>['fields' => ['id','full_name']],
												'category'=>['fields' => ['id','category_name']],
												'subcategory'=>['fields' => ['id','category_name']]
											],
											'order'=>['song.id'=>'desc'],	
										));
									}
								],
								'conditions' => array('RecentlySongs.user_id' => $this->request->data['user_id'], 'RecentlySongs.is_deleted' => 'N', 'song.is_deleted !=' => 'Y', 'song.enabled' => 'Y')
						]);
						
						
						$recentlySongsDataArr = array();
						
						if (isset($recentlySongsData) &&  $recentlySongsData !='' && !empty($recentlySongsData)) {
							foreach ($recentlySongsData as $key => $value) {
								
								$recentlysongs['song']['id'] = $value['song']['id'];
								$recentlysongs['song']['user_id'] = $value['song']['user_id'];
								$recentlysongs['song']['title'] = $value['song']['title'];
								$recentlysongs['song']['cat_id'] = $value['song']['cat_id'];
								$recentlysongs['song']['sub_cat_id'] = $value['song']['sub_cat_id'];
								$recentlysongs['song']['image'] = $value['song']['image'];
								$recentlysongs['song']['audio'] = $value['song']['audio'];
								$recentlysongs['song']['size'] = $value['song']['size'];
								$recentlysongs['song']['duration'] = $value['song']['duration'];
								$recentlysongs['song']['created'] = date('Y m d h:i:s', strtotime( $value['song']['created']));
								$recentlysongs['song']['cat_title'] = $value['song']['category']['category_name'];
								$recentlysongs['song']['sub_cat_title'] = $value['song']['subcategory']['category_name'];
								$recentlysongs['song']['user_name'] = $value['song']['user']['full_name'];
								
								$songDetail = $this->showLike($value['song_id'], $value['user_id']);
								$recentlysongs['song']['likes'] = $songDetail['likes'];
								$recentlysongs['song']['total_likes'] = $songDetail['total_likes'];
								$recentlysongs['song']['unlikes'] = $songDetail['unlikes'];
								$recentlysongs['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$recentlysongs['song']['is_comment'] = $songDetail['is_comment'];
								$recentlysongs['song']['total_comments'] = $songDetail['total_comments'];
								
								$recentlySongsDataArr[] = $recentlysongs;
							}
							$error = false;
							$message = 'Recently Played';
							$response['recently_played'] = $recentlySongsDataArr;
							
						} else {
							$error = true;
							$recentlySongsDataArr = array();
							$response['recently_played'] = array();
						}
					} else { $error = true; $response['recently_played'] = array(); }
					
				} else if ($this->request->data['type'] == 'challenge') {
					/*********** Challenge List ************/
					$challengesData = $this->Paginator->paginate(
						$this->Challenges, [
							'limit' => '10',
							'fields' => array('Challenges.id', 'Challenges.remaining_time', 'Challenges.challenge_time', 'Challenges.image', 'Challenges.accepted_user_image'),
							'page' => $page,
							'order'=>['Challenges.id'=>'desc'],	
							'contain'=>[
								'user1'=>['fields' => ['id','full_name','image']],
								'user2'=>['fields' => ['id','full_name','image']],
								'song'=>['fields' => ['title','image','audio']],
								'acceptusersong'=>['fields' => ['title','image','audio']]
							],
							'conditions' => array('Challenges.accept_user_id !=' => '0', 'Challenges.status' => '1', 'Challenges.is_deleted' => 'N')
					]);
					$challengesDataArr = array();
				
					if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
						foreach ($challengesData as $key => $value) {
							
							
							$challengesA['id'] = $value['id'];
							$challengesA['user1']['id'] = $value['user1']['id'];
							$challengesA['user1']['full_name'] = $value['user1']['full_name'];
							$challengesA['user1']['userimage'] = $value['user1']['image'];
							$challengesA['user1']['challenge_image'] = $value['image'];
							
							$challengesA['user2']['id'] = $value['user2']['id'];
							$challengesA['user2']['full_name'] = $value['user2']['full_name'];
							$challengesA['user2']['userimage'] = $value['user2']['image'];
							$challengesA['user2']['challenge_image'] = $value['accepted_user_image'];
							
							$challenges['challenge'] = $challengesA;
							
							//$challenges['challenge']['total_time'] = $value['challenge_time'];
							/******* update challenge time ************/
							date_default_timezone_set('Asia/Kolkata');
							$currrenttime = time();
							$date = strtotime(date('Y-m-d h:i:s',$currrenttime));
							$date2 = strtotime(date('Y-m-d h:i:s',strtotime($value['remaining_time'])));
							$diff = $date2 - $date;
							
							if($diff < 0) {
								$challenge = $this->Challenges->get($value['id']);
								$challenge->remaining_second = 0;
								$challenge->status = '3';
								$this->Challenges->save($challenge);
								
								/******* challenge winner like count ************/
								$total_ch_likes_user1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>1])->count();

								$total_ch_likes_user2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>1])->count();
							
								if($total_ch_likes_user1 > $total_ch_likes_user2) {
									$winner = $this->ChallengeWinners->newEntity();
									$this->request->data['challenge_id'] = $value['id'];
									$this->request->data['user_id'] = $value['user1']['id'];
									$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
									$this->ChallengeWinners->save($winners);
								} elseif($total_ch_likes_user2 > $total_ch_likes_user1) {
									$winner = $this->ChallengeWinners->newEntity();
									$this->request->data['challenge_id'] = $value['id'];
									$this->request->data['user_id'] = $value['user2']['id'];
									$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
									$this->ChallengeWinners->save($winners);
								} elseif($total_ch_likes_user2 == $total_ch_likes_user1) {
									
									$total_ch_unlikes_user1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>0])->count();

									$total_ch_unlikes_user2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>0])->count();
									
									if($total_ch_unlikes_user1 < $total_ch_unlikes_user2) {
										$winner = $this->ChallengeWinners->newEntity();
										$this->request->data['challenge_id'] = $value['id'];
										$this->request->data['user_id'] = $value['user1']['id'];
										$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
										$this->ChallengeWinners->save($winners);
									} elseif($total_ch_unlikes_user2 < $total_ch_unlikes_user1) {
										$winner = $this->ChallengeWinners->newEntity();
										$this->request->data['challenge_id'] = $value['id'];
										$this->request->data['user_id'] = $value['user2']['id'];
										$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
										$this->ChallengeWinners->save($winners);
									}
								}
								/******* end challenge winner like count ************/
								
							}
						
							/******* end update challenge time ************/
							
							$challengesDataArr[] = $challenges;
						}
						$error = false;
						$message = 'Challenge';
						$response['challenge'] = $challengesDataArr;
						
					} else {
						$error = true;
						$challengesDataArr = array();
						$response['challenge'] = [];
					}
				} else if ($this->request->data['type'] == 'new_added_by_singer') {
					/*********** New Added Songs List (By Singer) ************/
					
					$req_song = $this->Paginator->paginate(
						$this->RequestDjSingers, [
							'fields' => array('RequestDjSingers.user_id'),
							'page' => $page,
							'limit' => '10',
							'order'=>['song.id'=>'desc'],	
							'contain'=>[
								'user'=>['fields' => ['id','full_name','image']],
								'song'=>
								function(\Cake\ORM\Query $q) {
										return  $q->find('all', array(
											'fields' => ['id','user_id','cat_id','sub_cat_id','title','image','audio','size','duration','created'],
											'conditions' => array("song.is_deleted !=" => 'Y', "song.enabled" => 'Y'),
											'contain'=>[
												'user'=>['fields' => ['id','full_name']],
												'category'=>['fields' => ['id','category_name']],
												'subcategory'=>['fields' => ['id','category_name']]
											],
											//'order'=>['song.id'=>'DESC'],	
										));
									}
							],
							'conditions' => array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N')
					]);
					print_r($req_song->toArray());die;
					if (isset($req_song) &&  $req_song !='' && !empty($req_song)) {
						$req_song_Arr = array();
						foreach ($req_song as $key1 => $value1) {
							
							if (isset($value1['song']) &&  $value1['song'] !='' && !empty($value1['song'])) {
								foreach($value1['song'] as $songsDataVal) {
									$req_songA['song']['id'] = $songsDataVal['id'];
									$req_songA['song']['user_id'] = $songsDataVal['user_id'];
									$req_songA['song']['title'] = $songsDataVal['title'];
									$req_songA['song']['cat_id'] = $songsDataVal['cat_id'];
									$req_songA['song']['sub_cat_id'] = $songsDataVal['sub_cat_id'];
									$req_songA['song']['image'] = $songsDataVal['image'];
									$req_songA['song']['audio'] = $songsDataVal['audio'];
									$req_songA['song']['size'] = $songsDataVal['size'];
									$req_songA['song']['duration'] = $songsDataVal['duration'];
									$req_songA['song']['enabled'] = $songsDataVal['enabled'];
									$req_songA['song']['is_deleted'] = $songsDataVal['is_deleted'];
									$req_songA['song']['created'] = date('Y m d h:i:s', strtotime($songsDataVal['created']));
									$req_songA['song']['cat_title'] = $songsDataVal['category']['category_name'];
									$req_songA['song']['sub_cat_title'] = $songsDataVal['subcategory']['category_name'];
									$req_songA['song']['user_name'] = $songsDataVal['user']['full_name'];
									
									$songDetail = $this->showLike($songsDataVal['id'], $songsDataVal['user_id']);
									$req_songA['song']['likes'] = $songDetail['likes'];
									$req_songA['song']['total_likes'] = $songDetail['total_likes'];
									$req_songA['song']['unlikes'] = $songDetail['unlikes'];
									$req_songA['song']['total_unlikes'] = $songDetail['total_unlikes'];
									$req_songA['song']['is_comment'] = $songDetail['is_comment'];
									$req_songA['song']['total_comments'] = $songDetail['total_comments'];
									
									$req_song_Arr[] = $req_songA;
								}
							}
						}
						$error = false;
						$message = 'New Added By Singer';
						$response['new_added_by_singer'] = $req_song_Arr;
					} else {
						$error = true;
						$response['new_added_by_singer'] = [];
					}
				} else if ($this->request->data['type'] == 'new_added_by_dj') {
					/*********** New Added Songs List (By Dj) ************/
					$newdj_song = $this->RequestDjSingers->find('all', array(
						'fields' => array('RequestDjSingers.user_id'),
						'conditions' => array('RequestDjSingers.type' => 'D', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
						'order'=>['RequestDjSingers.id'=>'desc']	
					));
					if (isset($newdj_song) &&  $newdj_song !='' && !empty($newdj_song)) {
						$newdj_song_Arr = array();
						foreach ($newdj_song as $key1 => $value1) {
							$newdjsongsData = $this->Paginator->paginate(
								$this->SongUploads, [
									'fields' => array('SongUploads.id', 'SongUploads.title', 'SongUploads.image', 'SongUploads.audio','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.size','SongUploads.duration','SongUploads.created'),
									'page' => $page,
									'order'=>['SongUploads.id'=>'desc'],
									'conditions' => array('SongUploads.user_id' => $value1['user_id'], 'SongUploads.enabled' => 'Y', 'SongUploads.is_deleted' => 'N'),
									'contain'=>[
										'user'=>['fields' => ['full_name']],
										'category'=>['fields' => ['category_name']],
										'subcategory'=>['fields' => ['category_name']]
									],
							]);
							if (isset($newdjsongsData) &&  $newdjsongsData !='' && !empty($newdjsongsData)) {
								foreach($newdjsongsData as $songsDataVal) {
									$new_dj_songA['song']['id'] = $songsDataVal['id'];
									$new_dj_songA['song']['user_id'] = $songsDataVal['user_id'];
									$new_dj_songA['song']['title'] = $songsDataVal['title'];
									$new_dj_songA['song']['cat_id'] = $songsDataVal['cat_id'];
									$new_dj_songA['song']['sub_cat_id'] = $songsDataVal['sub_cat_id'];
									$new_dj_songA['song']['image'] = $songsDataVal['image'];
									$new_dj_songA['song']['audio'] = $songsDataVal['audio'];
									$new_dj_songA['song']['size'] = $songsDataVal['size'];
									$new_dj_songA['song']['duration'] = $songsDataVal['duration'];
									$new_dj_songA['song']['enabled'] = $songsDataVal['enabled'];
									$new_dj_songA['song']['is_deleted'] = $songsDataVal['is_deleted'];
									$new_dj_songA['song']['created'] = date('Y m d h:i:s', strtotime($songsDataVal['created']));
									$new_dj_songA['song']['cat_title'] = $songsDataVal['category']['category_name'];
									$new_dj_songA['song']['sub_cat_title'] = $songsDataVal['subcategory']['category_name'];
									$new_dj_songA['song']['user_name'] = $songsDataVal['user']['full_name'];
									
									$songDetail = $this->showLike($songsDataVal['id'], $songsDataVal['user_id']);
									$new_dj_songA['song']['likes'] = $songDetail['likes'];
									$new_dj_songA['song']['total_likes'] = $songDetail['total_likes'];
									$new_dj_songA['song']['unlikes'] = $songDetail['unlikes'];
									$new_dj_songA['song']['total_unlikes'] = $songDetail['total_unlikes'];
									$new_dj_songA['song']['is_comment'] = $songDetail['is_comment'];
									$new_dj_songA['song']['total_comments'] = $songDetail['total_comments'];
									
									$newdj_song_Arr[] = $new_dj_songA;
								}
							}
						}
						$error = false;
						$message = 'New Added By Dj';
						$response['new_added_by_dj'] = $newdj_song_Arr;
					} else {
						$error = true;
						$response['new_added_by_dj'] = [];
					}
				} else if ($this->request->data['type'] == 'top_10_dj_songs') {
					/*********** Top 10 Dj Songs List ************/
					$req_dj = $this->RequestDjSingers->find('all', array(
						'fields' => array('RequestDjSingers.user_id'),
						'conditions' => array('RequestDjSingers.type' => 'D', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
						'order' => ['RequestDjSingers.id'=>'desc']	
					));
					if (isset($req_dj) &&  $req_dj !='' && !empty($req_dj)) {
						$djsongsDataArr = array();
						foreach ($req_dj as $key1 => $value1) {
							$djsongsData = $this->Paginator->paginate(
								$this->SongUploads, [
									'fields' => array('SongUploads.id', 'SongUploads.title', 'SongUploads.image', 'SongUploads.audio','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.size','SongUploads.duration','SongUploads.created'),
									'page' => $page,
									'order'=>['SongUploads.total_likes'=>'DESC'],
									'conditions' => array('SongUploads.user_id' => $value1['user_id'], 'SongUploads.enabled' => 'Y', 'SongUploads.is_deleted' => 'N'),
									'contain'=>[
										'user'=>['fields' => ['full_name']],
										'category'=>['fields' => ['category_name']],
										'subcategory'=>['fields' => ['category_name']]
									],
							]);
							if (isset($djsongsData) &&  $djsongsData !='' && !empty($djsongsData)) {
								foreach($djsongsData as $djsongsDataVal) {
									$req_djsongA['song']['id'] = $djsongsDataVal['id'];
									$req_djsongA['song']['user_id'] = $djsongsDataVal['user_id'];
									$req_djsongA['song']['title'] = $djsongsDataVal['title'];
									$req_djsongA['song']['cat_id'] = $djsongsDataVal['cat_id'];
									$req_djsongA['song']['sub_cat_id'] = $djsongsDataVal['sub_cat_id'];
									$req_djsongA['song']['image'] = $djsongsDataVal['image'];
									$req_djsongA['song']['audio'] = $djsongsDataVal['audio'];
									$req_djsongA['song']['size'] = $djsongsDataVal['size'];
									$req_djsongA['song']['duration'] = $djsongsDataVal['duration'];
									$req_djsongA['song']['enabled'] = $djsongsDataVal['enabled'];
									$req_djsongA['song']['is_deleted'] = $djsongsDataVal['is_deleted'];
									$req_djsongA['song']['created'] = date('Y m d h:i:s', strtotime($djsongsDataVal['created']));
									$req_djsongA['song']['cat_title'] = $djsongsDataVal['category']['category_name'];
									$req_djsongA['song']['sub_cat_title'] = $djsongsDataVal['subcategory']['category_name'];
									$req_djsongA['song']['user_name'] = $djsongsDataVal['user']['full_name'];
									
									$songDetail = $this->showLike($djsongsDataVal['id'], $djsongsDataVal['user_id']);
									$req_djsongA['song']['likes'] = $songDetail['likes'];
									$req_djsongA['song']['total_likes'] = $songDetail['total_likes'];
									$req_djsongA['song']['unlikes'] = $songDetail['unlikes'];
									$req_djsongA['song']['total_unlikes'] = $songDetail['total_unlikes'];
									$req_djsongA['song']['is_comment'] = $songDetail['is_comment'];
									$req_djsongA['song']['total_comments'] = $songDetail['total_comments'];
									
									$djsongsDataArr[] = $req_djsongA;
								}
							}
						}
						$error = false;
						$message = 'Top 10 Songs Dj Songs';
						$response['top_10_dj_songs'] = $djsongsDataArr;
					} else {
						$error = true;
						$response['top_10_dj_songs'] = [];
					}
				} else if ($this->request->data['type'] == 'top_10_singer_songs') {
					/*********** Top 10 Singer Songs List ************/
					$req_singer = $this->RequestDjSingers->find('all', array(
						'fields' => array('RequestDjSingers.user_id'),
						'conditions' => array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
						'order' => ['RequestDjSingers.id'=>'desc']	
					));
					if (isset($req_singer) &&  $req_singer !='' && !empty($req_singer)) {
						$singersongsDataArr = array();
						foreach ($req_singer as $key1 => $value1) {
							$singersongsData = $this->Paginator->paginate(
								$this->SongUploads, [
									'fields' => array('SongUploads.id', 'SongUploads.title', 'SongUploads.image', 'SongUploads.audio','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.size','SongUploads.duration','SongUploads.created'),
									'page' => $page,
									'order'=>['SongUploads.total_likes'=>'DESC'],
									'conditions' => array('SongUploads.user_id' => $value1['user_id'], 'SongUploads.enabled' => 'Y', 'SongUploads.is_deleted' => 'N'),
									'contain'=>[
										'user'=>['fields' => ['full_name']],
										'category'=>['fields' => ['category_name']],
										'subcategory'=>['fields' => ['category_name']]
									],
							]);
							if (isset($singersongsData) &&  $singersongsData !='' && !empty($singersongsData)) {
								foreach($singersongsData as $singersongsDataVal) {
									
									$req_singersongA['song']['id'] = $singersongsDataVal['id'];
									$req_singersongA['song']['user_id'] = $singersongsDataVal['user_id'];
									$req_singersongA['song']['title'] = $singersongsDataVal['title'];
									$req_singersongA['song']['cat_id'] = $singersongsDataVal['cat_id'];
									$req_singersongA['song']['sub_cat_id'] = $singersongsDataVal['sub_cat_id'];
									$req_singersongA['song']['image'] = $singersongsDataVal['image'];
									$req_singersongA['song']['audio'] = $singersongsDataVal['audio'];
									$req_singersongA['song']['size'] = $singersongsDataVal['size'];
									$req_singersongA['song']['duration'] = $singersongsDataVal['duration'];
									$req_singersongA['song']['enabled'] = $singersongsDataVal['enabled'];
									$req_singersongA['song']['is_deleted'] = $singersongsDataVal['is_deleted'];
									$req_singersongA['song']['created'] = date('Y m d h:i:s', strtotime($singersongsDataVal['created']));
									$req_singersongA['song']['cat_title'] = $singersongsDataVal['category']['category_name'];
									$req_singersongA['song']['sub_cat_title'] = $singersongsDataVal['subcategory']['category_name'];
									$req_singersongA['song']['user_name'] = $singersongsDataVal['user']['full_name'];
									
									$songDetail = $this->showLike($singersongsDataVal['id'], $singersongsDataVal['user_id']);
									$req_singersongA['song']['likes'] = $songDetail['likes'];
									$req_singersongA['song']['total_likes'] = $songDetail['total_likes'];
									$req_singersongA['song']['unlikes'] = $songDetail['unlikes'];
									$req_singersongA['song']['total_unlikes'] = $songDetail['total_unlikes'];
									$req_singersongA['song']['is_comment'] = $songDetail['is_comment'];
									$req_singersongA['song']['total_comments'] = $songDetail['total_comments'];
									
									$singersongsDataArr[] = $req_singersongA;
								}
							}
						}
						$error = false;
						$message = 'Top 10 Songs Singer Songs';
						$response['top_10_Singer_songs'] = $singersongsDataArr;
					} else {
						$error = true;
						$response['top_10_Singer_songs'] = [];
					}
				} else if ($this->request->data['type'] == 'top_post') {
					/*********** Top Posts ************/
					$topsongsData = $this->Paginator->paginate(
						$this->Posts, [
							'fields' => array('Posts.id', 'Posts.title', 'Posts.images', 'Posts.description'),
							'page' => $page,
							'order'=>['Posts.id'=>'DESC'],
							'conditions' => array("Posts.status"=>'1',"Posts.enabled "=>'Y' , "Posts.is_deleted !=" => 'Y')
					]);
					$topsongsDataArr = array();
				
					if (isset($topsongsData) &&  $topsongsData !='' && !empty($topsongsData)) {
						foreach ($topsongsData as $key => $value) {
							$topsongs['post'] = $value;
							$topsongsDataArr[] = $topsongs;
						}
						$error = false;
						$message = 'Top Post';
						$response['top_post'] = $topsongsDataArr;
						
					} else {
						$error = true;
						$topsongsDataArr = array();
						$response['top_post'] = [];
					}
				} else if ($this->request->data['type'] == 'top_singer_profile') {
					/*********** Top Singer Profile List ************/
					$singerProfileData = $this->Paginator->paginate(
						$this->RequestDjSingers, [
							'fields' => array('RequestDjSingers.user_id'),
							'page' => $page,
							'order'=>['RequestDjSingers.id'=>'desc'],
							'contain'=>[
								'user'=>['fields' => ['id','full_name','image','email','dob','address']]
							],
							'conditions' => array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N')
					]);
					$singerProfileDataArr = array();
				
					if (isset($singerProfileData) &&  $singerProfileData !='' && !empty($singerProfileData)) {
						foreach ($singerProfileData as $key => $value) {
							$total_likes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>1])->count();
							$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>0])->count();
												
							
							$singer_profile['profile']['user_id'] = $value['user']['id'];
							$singer_profile['profile']['full_name'] = $value['user']['full_name'];
							$singer_profile['profile']['email'] = $value['user']['email'];
							$singer_profile['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
							$singer_profile['profile']['address'] = $value['user']['address'];
							$singer_profile['profile']['image'] = $value['user']['image'];
							$singer_profile['profile']['like_count'] = $total_likes;
							$singer_profile['profile']['unlike_count'] = $total_unlikes;
							
							$singerProfileDataArr[] = $singer_profile;
						}
						$error = false;
						$message = 'Top Singer Profile';
						$response['top_singer_profile'] = $singerProfileDataArr;
					} else {
						$error = true;
						$response['top_singer_profile'] = [];
					}
				} else if ($this->request->data['type'] == 'top_dj_profile') {
					/*********** Top dj Profile List ************/
					$djProfileData = $this->Paginator->paginate(
						$this->RequestDjSingers, [
							'fields' => array('RequestDjSingers.user_id'),
							'page' => $page,
							'order'=>['RequestDjSingers.id'=>'desc'],
							'contain'=>[
								'user'=>['fields' => ['id','full_name','image','email','dob','address']]
							],
							'conditions' => array('RequestDjSingers.type' => 'D', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N')
					]);
					$djProfileDataArr = array();
				
					if (isset($djProfileData) &&  $djProfileData !='' && !empty($djProfileData)) {
						foreach ($djProfileData as $key => $value) {							
							$total_likes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>1])->count();
							$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>0])->count();	
							
							$dj_profile['profile']['user_id'] = $value['user']['id'];
							$dj_profile['profile']['full_name'] = $value['user']['full_name'];
							$dj_profile['profile']['email'] = $value['user']['email'];
							$dj_profile['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
							$dj_profile['profile']['address'] = $value['user']['address'];
							$dj_profile['profile']['image'] = $value['user']['image'];
							$dj_profile['profile']['like_count'] = $total_likes;
							$dj_profile['profile']['unlike_count'] = $total_unlikes;
							
							$djProfileDataArr[] = $dj_profile;
						}
						$error = false;
						$message = 'Top DJ Profile';
						$response['top_dj_profile'] = $djProfileDataArr;
					} else {
						$error = true;
						$response['top_dj_profile'] = [];
					}
				} else if ($this->request->data['type'] == 'last_challenge_winner') {
					/*********** Last challenge winner List ************/
					$lastChWinnerData = $this->Paginator->paginate(
						$this->ChallengeWinners, [
							'fields' => array('ChallengeWinners.user_id'),
							'page' => $page,
							'order'=>['ChallengeWinners.id'=>'desc'],
							'contain'=>[
								'Users'=>['fields' => ['id','full_name','image','email','dob','address']]
							],
							'conditions' => array('ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N')
					]);
					$lastChWinnerDataArr = array();
				
					if (isset($lastChWinnerData) &&  $lastChWinnerData !='' && !empty($lastChWinnerData)) {
						foreach ($lastChWinnerData as $key => $value) { //print_r($value); die;
							
							$last_challenge_winner['profile']['user_id'] = $value['user']['id'];
							$last_challenge_winner['profile']['full_name'] = $value['user']['full_name'];
							$last_challenge_winner['profile']['email'] = $value['user']['email'];
							$last_challenge_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
							$last_challenge_winner['profile']['address'] = $value['user']['address'];
							$last_challenge_winner['profile']['image'] = $value['user']['image'];
							
							$lastChWinnerDataArr[] = $last_challenge_winner;
						}
						$error = false;
						$message = 'Last Challenge Winner';
						$response['last_challenge_winner'] = $lastChWinnerDataArr;
					} else {
						$error = true;
						$response['last_challenge_winner'] = [];
					}
				} else if ($this->request->data['type'] == 'last_10_day_winner') {
					/*********** Last 10 day winners List ************/
					$last_10_day = date("Y-n-j", strtotime("10 days ago"));
					$lastWinnerData = $this->Paginator->paginate(
						$this->ChallengeWinners, [
							'fields' => array('ChallengeWinners.user_id'),
							'page' => $page,
							'order'=>['ChallengeWinners.id'=>'desc'],
							'contain'=>[
								'Users'=>['fields' => ['id','full_name','image','email','dob','address']]
							],
							'conditions' => array('ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N', 'ChallengeWinners.created >=' => $last_10_day)
					]);
					$lastWinnerDataArr = array();
				
					if (isset($lastWinnerData) &&  $lastWinnerData !='' && !empty($lastWinnerData)) {
						foreach ($lastWinnerData as $key => $value) { //print_r($value); die;
							
							$last_winner['profile']['user_id'] = $value['user']['id'];
							$last_winner['profile']['full_name'] = $value['user']['full_name'];
							$last_winner['profile']['email'] = $value['user']['email'];
							$last_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
							$last_winner['profile']['address'] = $value['user']['address'];
							$last_winner['profile']['image'] = $value['user']['image'];
							
							$lastWinnerDataArr[] = $last_winner;
						}
						$error = false;
						$message = 'Last 10 Day Winner';
						$response['last_10_day_winner'] = $lastWinnerDataArr;
					} else {
						$error = true;
						$response['last_10_day_winner'] = [];
					}
				} else if ($this->request->data['type'] == 'last_month_winner_singer_dj') {
					/*********** Last month winners List ************/
					$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
					$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
					
					$lastMonthWinnerData = $this->Paginator->paginate(
						$this->ChallengeWinners, [
							'fields' => array('ChallengeWinners.user_id'),
							'page' => $page,
							'order'=>['ChallengeWinners.id'=>'desc'],
							'contain'=>[
								'Users'=>['fields' => ['id','full_name','image','email','dob','address']]
							],
							'conditions' => array('ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N', 'ChallengeWinners.created >=' => $last_month_start, 'ChallengeWinners.created <=' => $last_month_end)
					]);
					$lastMonthWinnerDataArr = array();
				
					if (isset($lastMonthWinnerData) &&  $lastMonthWinnerData !='' && !empty($lastMonthWinnerData)) {
						foreach ($lastMonthWinnerData as $key => $value) { //print_r($value); die;
							
							$last_month_winner['profile']['user_id'] = $value['user']['id'];
							$last_month_winner['profile']['full_name'] = $value['user']['full_name'];
							$last_month_winner['profile']['email'] = $value['user']['email'];
							$last_month_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
							$last_month_winner['profile']['address'] = $value['user']['address'];
							$last_month_winner['profile']['image'] = $value['user']['image'];
							
							$lastMonthWinnerDataArr[] = $last_month_winner;
						}
						$error = false;
						$message = 'Last Month Winner Singer/DJ';
						$response['last_month_winner_singer_dj'] = $lastMonthWinnerDataArr;
					} else {
						$error = true;
						$response['last_month_winner_singer_dj'] = [];
					}
				} else if ($this->request->data['type'] == 'singer_list_whos_birthday_today') {
					/*********** Singer List Who's Birthday Today ************/
					$singerBdyData = $this->RequestDjSingers->find('all', array(
						'fields' => array('RequestDjSingers.user_id'),
						'conditions' => array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N')	
					));
					if (isset($singerBdyData) &&  $singerBdyData !='' && !empty($singerBdyData)) {
						$singerBdyDataArr = array();
						foreach ($singerBdyData as $key1 => $value1) {
							$userBdyData = $this->Paginator->paginate(
								$this->Users, [
									'fields' =>['id','full_name','image','email','dob','address'],
									'page' => $page,
									'order'=>['Users.id'=>'desc'],	
									'conditions' => array('Users.id' => $value1['user_id'], 'Users.enabled' => 'Y', 'Users.is_deleted' => 'N')
							]);
							if (isset($userBdyData) &&  $userBdyData !='' && !empty($userBdyData)) {
								foreach($userBdyData as $userBdyDataVal) {
									$old_date = $userBdyDataVal['dob'];  
									$old_date_timestamp = strtotime($old_date);
									$new_date = date('m-d', $old_date_timestamp);   
									if($new_date == date("m-d")) {
										$singerBdyARR['profile'] = $userBdyDataVal;
										$singerBdyDataArr[] = $singerBdyARR;
									}
								}
							}
						}
						$error = false;
						$message = "Singer List who's Birthday Today";
						$response['singer_list_whos_birthday_today'] = $singerBdyDataArr;
					} else {
						$error = true;
						$response['singer_list_whos_birthday_today'] = [];
					}
				}
				foreach($response as $keydata=>$valdata){
					
					if($keydata == 'recently_played'){
						$responserecord['title'] = 'Recently Played' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'challenge'){
						$responserecord['title'] = 'Challenge' ;
						$responserecord['type'] = 'challenge' ;
					}elseif($keydata == 'new_added_by_singer'){
						$responserecord['title'] = 'New Added (By Singer)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'new_added_by_dj'){
						$responserecord['title'] = 'New Added (By Dj)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_10_dj_songs'){
						$responserecord['title'] = 'Top 10 Songs Dj Songs' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_10_Singer_songs'){
						$responserecord['title'] = 'Top 10 Songs Singer Songs' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_post'){
						$responserecord['title'] = 'Top Post' ;
						$responserecord['type'] = 'post' ;
					}elseif($keydata == 'top_singer_profile'){
						$responserecord['title'] = 'Top Singer Profile' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'top_dj_profile'){
						$responserecord['title'] = 'Top Dj Profile' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'singer_list_whos_birthday_today'){
						$responserecord['title'] = "Singer List Who's Birthday Today" ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_10_day_winner'){
						$responserecord['title'] = 'Last 10 Day Winner' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_month_winner_singer_dj'){
						$responserecord['title'] = 'Last Month Winner Singer/DJ' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_challenge_winner'){
						$responserecord['title'] = 'Last Challenge Winner' ;
						$responserecord['type'] = 'profile' ;
					}
					$responserecord['name'] =  $keydata;
					$responserecord['data'] =  $valdata;
					$allresponse[] = $responserecord;
				}
				$allresponseArr['category'] = $allresponse;
				
				
				
			} else {
				
				/*********** Recently Played Songs List ************/
				if (isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
				
					$recentlySongsData = $this->RecentlySongs->find('all', array(
						'fields' => array('RecentlySongs.id', 'RecentlySongs.song_id', 'RecentlySongs.user_id'),
						'conditions' => array('RecentlySongs.user_id' => $this->request->data['user_id'], 'RecentlySongs.is_deleted' => 'N', 'song.is_deleted !=' => 'Y', 'song.enabled' => 'Y'),
						'contain'=>[
							'user'=>['fields' => ['full_name','image']],
							'song'=> function(\Cake\ORM\Query $q) {
										return $q->find('all', array(
											'fields' => ['id','user_id','cat_id','sub_cat_id','title','image','audio','size','duration','created'],
											'conditions' => array('song.is_deleted !=' => 'Y', 'song.enabled' => 'Y'),
											'contain'=>[
												'user'=>['fields' => ['id','full_name']],
												'category'=>['fields' => ['id','category_name']],
												'subcategory'=>['fields' => ['id','category_name']]
											],
											'order'=>['song.id'=>'asc'],	
										));
									}
						],
						'limit' => '10',
						'order'=>['RecentlySongs.id'=>'desc'],	
						'group'=>['RecentlySongs.song_id']
					));
					$recentlySongsDataArr = array();
					if (isset($recentlySongsData) &&  $recentlySongsData !='' && !empty($recentlySongsData)) {
						foreach ($recentlySongsData as $key => $value) {
							$recentlysongs['song']['id'] = $value['song']['id'];
							$recentlysongs['song']['user_id'] = $value['song']['user_id'];
							$recentlysongs['song']['title'] = $value['song']['title'];
							$recentlysongs['song']['cat_id'] = $value['song']['cat_id'];
							$recentlysongs['song']['sub_cat_id'] = $value['song']['sub_cat_id'];
							$recentlysongs['song']['image'] = $value['song']['image'];
							$recentlysongs['song']['audio'] = $value['song']['audio'];
							$recentlysongs['song']['size'] = $value['song']['size'];
							$recentlysongs['song']['duration'] = $value['song']['duration'];
							$recentlysongs['song']['created'] = date('Y m d h:i:s', strtotime( $value['song']['created']));
							$recentlysongs['song']['cat_title'] = $value['song']['category']['category_name'];
							$recentlysongs['song']['sub_cat_title'] = $value['song']['subcategory']['category_name'];
							$recentlysongs['song']['user_name'] = $value['song']['user']['full_name'];
							
							$songDetail = $this->showLike($value['song_id'], $value['user_id']);
							$recentlysongs['song']['likes'] = $songDetail['likes'];
							$recentlysongs['song']['total_likes'] = $songDetail['total_likes'];
							$recentlysongs['song']['unlikes'] = $songDetail['unlikes'];
							$recentlysongs['song']['total_unlikes'] = $songDetail['total_unlikes'];
							$recentlysongs['song']['is_comment'] = $songDetail['is_comment'];
							$recentlysongs['song']['total_comments'] = $songDetail['total_comments'];
							
							$recentlySongsDataArr[] = $recentlysongs;
						}
						$error = false;
						$message = 'Recently Played Songs';
						$response['recently_played'] = $recentlySongsDataArr;
						
					} else {
						$error = true;
						$recentlySongsDataArr = array();
						$response['recently_played'] = array();
					}
				
				}else{
					$error = true;
					$recentlySongsDataArr = array();
					$response['recently_played'] = array();
				}
				/*********** Challenge List ************/
				$challengesData = $this->Challenges->find('all', array(
					'fields' => array('Challenges.id', 'Challenges.remaining_time', 'Challenges.challenge_time', 'Challenges.image', 'Challenges.accepted_user_image'),
					'conditions' => array('Challenges.accept_user_id !=' => '0', 'Challenges.status' => '1', 'Challenges.is_deleted' => 'N'),
					'contain'=>[
						'user1'=>['fields' => ['id','full_name','image']],
						'user2'=>['fields' => ['id','full_name','image']],
						'song'=>['fields' => ['title','image','audio']],
						'acceptusersong'=>['fields' => ['title','image','audio']]
					],
					'limit' => '10',
					'order'=>['Challenges.id'=>'desc'],	
				));
				//print_r($challengesData->toArray()); die;
				$challengesDataArr = array();
			
				if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
					foreach ($challengesData as $key => $value) {
						
							$challengesA['id'] = $value['id'];
							$challengesA['user1']['user_id'] = $value['user1']['id'];
							$challengesA['user1']['full_name'] = $value['user1']['full_name'];
							$challengesA['user1']['userimage'] = $value['user1']['image'];
							$challengesA['user1']['challenge_image'] = $value['image'];
							
							$challengesA['user2']['user_id'] = $value['user2']['id'];
							$challengesA['user2']['full_name'] = $value['user2']['full_name'];
							$challengesA['user2']['userimage'] = $value['user2']['image'];
							$challengesA['user2']['challenge_image'] = $value['accepted_user_image'];
							
							$challenges['challenge'] = $challengesA;
							//$challenges['challenge']['total_time'] = $value['challenge_time'];
						
							/******* update challenge time ************/
							date_default_timezone_set('Asia/Kolkata');
							$currrenttime = time();
							$date = strtotime(date('Y-m-d h:i:s',$currrenttime));
							$date2 = strtotime(date('Y-m-d h:i:s',strtotime($value['remaining_time'])));
							$diff = $date2 - $date;
								
							if($diff < 0) {
								$challenge = $this->Challenges->get($value['id']);
								$challenge->remaining_second = 0;
								$challenge->status = '3';
								$this->Challenges->save($challenge);
							
							
								/******* challenge winner like count ************/
								$total_ch_likes_user1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>1])->count();

								$total_ch_likes_user2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>1])->count();
							
								if($total_ch_likes_user1 > $total_ch_likes_user2) {
									$winner = $this->ChallengeWinners->newEntity();
									$this->request->data['challenge_id'] = $value['id'];
									$this->request->data['user_id'] = $value['user1']['id'];
									$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
									$this->ChallengeWinners->save($winners);
								} elseif($total_ch_likes_user2 > $total_ch_likes_user1) {
									$winner = $this->ChallengeWinners->newEntity();
									$this->request->data['challenge_id'] = $value['id'];
									$this->request->data['user_id'] = $value['user2']['id'];
									$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
									$this->ChallengeWinners->save($winners);
								} elseif($total_ch_likes_user2 == $total_ch_likes_user1) {
									
									$total_ch_unlikes_user1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>0])->count();

									$total_ch_unlikes_user2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>0])->count();
									
									if($total_ch_unlikes_user1 < $total_ch_unlikes_user2) {
										$winner = $this->ChallengeWinners->newEntity();
										$this->request->data['challenge_id'] = $value['id'];
										$this->request->data['user_id'] = $value['user1']['id'];
										$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
										$this->ChallengeWinners->save($winners);
									} elseif($total_ch_unlikes_user2 < $total_ch_unlikes_user1) {
										$winner = $this->ChallengeWinners->newEntity();
										$this->request->data['challenge_id'] = $value['id'];
										$this->request->data['user_id'] = $value['user2']['id'];
										$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
										$this->ChallengeWinners->save($winners);
									}
								}
								/******* end challenge winner like count ************/
							}
						
							/******* end update challenge time ************/
						
						
						$challengesDataArr[] = $challenges;
					}
					$error = false;
					$message = 'Recently Played Songs';
					$response['challenge'] = $challengesDataArr;
					
				} else {
					$error = true;
					$challengesDataArr = array();
					$response['challenge'] = array();
				}
				
				/*********** New Added Songs List (By Singer) ************/
				$req_song = $this->RequestDjSingers->find('all', array(
					'fields' => array('RequestDjSingers.user_id'),
					'conditions' => array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
					'order'=>['RequestDjSingers.id'=>'desc']	
				));
				if (isset($req_song) &&  $req_song !='' && !empty($req_song)) {
					$req_song_Arr = array();
					foreach ($req_song as $key1 => $value1) {
						$allsongsData = $this->SongUploads->find('all', array(
							'fields' => array('SongUploads.id', 'SongUploads.title', 'SongUploads.image', 'SongUploads.audio','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.size','SongUploads.duration','SongUploads.created'),
							'conditions' => array('SongUploads.user_id' => $value1['user_id'], 'SongUploads.enabled' => 'Y', 'SongUploads.is_deleted' => 'N'),
							'contain'=>[
								'user'=>['fields' => ['full_name']],
								'category'=>['fields' => ['category_name']],
								'subcategory'=>['fields' => ['category_name']]
							],
							'limit' => '10',
							'order'=>['SongUploads.id'=>'desc']	
						));
						if (isset($allsongsData) &&  $allsongsData !='' && !empty($allsongsData)) {
							foreach($allsongsData as $songsDataVal) {
								
								$req_songA['song']['id'] = $songsDataVal['id'];
								$req_songA['song']['user_id'] = $songsDataVal['user_id'];
								$req_songA['song']['title'] = $songsDataVal['title'];
								$req_songA['song']['cat_id'] = $songsDataVal['cat_id'];
								$req_songA['song']['sub_cat_id'] = $songsDataVal['sub_cat_id'];
								$req_songA['song']['image'] = $songsDataVal['image'];
								$req_songA['song']['audio'] = $songsDataVal['audio'];
								$req_songA['song']['size'] = $songsDataVal['size'];
								$req_songA['song']['duration'] = $songsDataVal['duration'];
								$req_songA['song']['enabled'] = $songsDataVal['enabled'];
								$req_songA['song']['is_deleted'] = $songsDataVal['is_deleted'];
								$req_songA['song']['created'] = date('Y m d h:i:s', strtotime($songsDataVal['created']));
								$req_songA['song']['cat_title'] = $songsDataVal['category']['category_name'];
								$req_songA['song']['sub_cat_title'] = $songsDataVal['subcategory']['category_name'];
								$req_songA['song']['user_name'] = $songsDataVal['user']['full_name'];
								
								$songDetail = $this->showLike($songsDataVal['id'], $songsDataVal['user_id']);
								$req_songA['song']['likes'] = $songDetail['likes'];
								$req_songA['song']['total_likes'] = $songDetail['total_likes'];
								$req_songA['song']['unlikes'] = $songDetail['unlikes'];
								$req_songA['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$req_songA['song']['is_comment'] = $songDetail['is_comment'];
								$req_songA['song']['total_comments'] = $songDetail['total_comments'];
								
								$req_song_Arr[] = $req_songA;
							}
						}
					}
					$error = false;
					$message = 'New Added Songs By Singer';
					$response['new_added_by_singer'] = $req_song_Arr;
				} else {
					$error = true;
					$response['new_added_by_singer'] = [];
				}
				
				/*********** New Added Songs List (By Dj) ************/
					$newdj_song = $this->RequestDjSingers->find('all', array(
						'fields' => array('RequestDjSingers.user_id'),
						'conditions' => array('RequestDjSingers.type' => 'D', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
						'order'=>['RequestDjSingers.id'=>'desc']	
					));
					if (isset($newdj_song) &&  $newdj_song !='' && !empty($newdj_song)) {
						$newdj_song_Arr = array();
						foreach ($newdj_song as $key1 => $value1) {
							$newdjsongsData = $this->SongUploads->find('all', array(
								'fields' => array('SongUploads.id', 'SongUploads.title', 'SongUploads.image', 'SongUploads.audio','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.size','SongUploads.duration','SongUploads.created'),
								'conditions' => array('SongUploads.user_id' => $value1['user_id'], 'SongUploads.enabled' => 'Y', 'SongUploads.is_deleted' => 'N'),
								'contain'=>[
									'user'=>['fields' => ['full_name']],
									'category'=>['fields' => ['category_name']],
									'subcategory'=>['fields' => ['category_name']]
								],
								'limit' => '10',
								'order'=>['SongUploads.id'=>'desc']	
							));
							if (isset($newdjsongsData) &&  $newdjsongsData !='' && !empty($newdjsongsData)) {
								foreach($newdjsongsData as $songsDataVal) {
									$new_dj_songA['song']['id'] = $songsDataVal['id'];
									$new_dj_songA['song']['user_id'] = $songsDataVal['user_id'];
									$new_dj_songA['song']['title'] = $songsDataVal['title'];
									$new_dj_songA['song']['cat_id'] = $songsDataVal['cat_id'];
									$new_dj_songA['song']['sub_cat_id'] = $songsDataVal['sub_cat_id'];
									$new_dj_songA['song']['image'] = $songsDataVal['image'];
									$new_dj_songA['song']['audio'] = $songsDataVal['audio'];
									$new_dj_songA['song']['size'] = $songsDataVal['size'];
									$new_dj_songA['song']['duration'] = $songsDataVal['duration'];
									$new_dj_songA['song']['enabled'] = $songsDataVal['enabled'];
									$new_dj_songA['song']['is_deleted'] = $songsDataVal['is_deleted'];
									$new_dj_songA['song']['created'] = date('Y m d h:i:s', strtotime($songsDataVal['created']));
									$new_dj_songA['song']['cat_title'] = $songsDataVal['category']['category_name'];
									$new_dj_songA['song']['sub_cat_title'] = $songsDataVal['subcategory']['category_name'];
									$new_dj_songA['song']['user_name'] = $songsDataVal['user']['full_name'];
									
									$songDetail = $this->showLike($songsDataVal['id'], $songsDataVal['user_id']);
									$new_dj_songA['song']['likes'] = $songDetail['likes'];
									$new_dj_songA['song']['total_likes'] = $songDetail['total_likes'];
									$new_dj_songA['song']['unlikes'] = $songDetail['unlikes'];
									$new_dj_songA['song']['total_unlikes'] = $songDetail['total_unlikes'];
									$new_dj_songA['song']['is_comment'] = $songDetail['is_comment'];
									$new_dj_songA['song']['total_comments'] = $songDetail['total_comments'];
									
									$newdj_song_Arr[] = $new_dj_songA;
								}
							}
						}
						$error = false;
						$message = 'New Added By Dj';
						$response['new_added_by_dj'] = $newdj_song_Arr;
					} else {
						$error = true;
						$response['new_added_by_dj'] = [];
					}
				
				/*********** Top 10 Dj Songs List ************/
				$req_dj = $this->RequestDjSingers->find('all', array(
					'fields' => array('RequestDjSingers.user_id'),
					'conditions' => array('RequestDjSingers.type' => 'D', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
					'order' => ['RequestDjSingers.id'=>'desc']	
				));
				if (isset($req_dj) &&  $req_dj !='' && !empty($req_dj)) {
					$djsongsDataArr = array();
					foreach ($req_dj as $key1 => $value1) {
						$djsongsData = $this->SongUploads->find('all', array(
							'fields' => array('SongUploads.id', 'SongUploads.title', 'SongUploads.image', 'SongUploads.audio','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.size','SongUploads.duration','SongUploads.created'),
							'conditions' => array('SongUploads.user_id' => $value1['user_id'], 'SongUploads.enabled' => 'Y', 'SongUploads.is_deleted' => 'N'),
							'contain'=>[
								'user'=>['fields' => ['full_name']],
								'category'=>['fields' => ['category_name']],
								'subcategory'=>['fields' => ['category_name']]
							],
							'limit' => '10',
							'order'=>['SongUploads.total_likes'=>'DESC']	
						)); 
						//print_r($djsongsData->toArray()); die;
						if (isset($djsongsData) &&  $djsongsData !='' && !empty($djsongsData)) {
							foreach($djsongsData as $djsongsDataVal) {
								
								$req_djsongA['song']['id'] = $djsongsDataVal['id'];
								$req_djsongA['song']['user_id'] = $djsongsDataVal['user_id'];
								$req_djsongA['song']['title'] = $djsongsDataVal['title'];
								$req_djsongA['song']['cat_id'] = $djsongsDataVal['cat_id'];
								$req_djsongA['song']['sub_cat_id'] = $djsongsDataVal['sub_cat_id'];
								$req_djsongA['song']['image'] = $djsongsDataVal['image'];
								$req_djsongA['song']['audio'] = $djsongsDataVal['audio'];
								$req_djsongA['song']['size'] = $djsongsDataVal['size'];
								$req_djsongA['song']['duration'] = $djsongsDataVal['duration'];
								$req_djsongA['song']['enabled'] = $djsongsDataVal['enabled'];
								$req_djsongA['song']['is_deleted'] = $djsongsDataVal['is_deleted'];
								$req_djsongA['song']['created'] = date('Y m d h:i:s', strtotime($djsongsDataVal['created']));
								$req_djsongA['song']['cat_title'] = $djsongsDataVal['category']['category_name'];
								$req_djsongA['song']['sub_cat_title'] = $djsongsDataVal['subcategory']['category_name'];
								$req_djsongA['song']['user_name'] = $djsongsDataVal['user']['full_name'];
								
								$songDetail = $this->showLike($djsongsDataVal['id'], $djsongsDataVal['user_id']);
								$req_djsongA['song']['likes'] = $songDetail['likes'];
								$req_djsongA['song']['total_likes'] = $songDetail['total_likes'];
								$req_djsongA['song']['unlikes'] = $songDetail['unlikes'];
								$req_djsongA['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$req_djsongA['song']['is_comment'] = $songDetail['is_comment'];
								$req_djsongA['song']['total_comments'] = $songDetail['total_comments'];
								
								$djsongsDataArr[] = $req_djsongA;
							}
						}
					}
					$error = false;
					$message = 'Top 10 Songs Dj Songs';
					$response['top_10_dj_songs'] = $djsongsDataArr;
				} else {
					$error = true;
					$response['top_10_dj_songs'] = [];
				}
				
				/*********** Top 10 Singer Songs List ************/
				$req_singer = $this->RequestDjSingers->find('all', array(
					'fields' => array('RequestDjSingers.user_id'),
					'conditions' => array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
					'order' => ['RequestDjSingers.id'=>'desc']	
				));
				if (isset($req_singer) &&  $req_singer !='' && !empty($req_singer)) {
					$singersongsDataArr = array();
					foreach ($req_singer as $key1 => $value1) {
						$singersongsData = $this->SongUploads->find('all', array(
							'fields' => array('SongUploads.id', 'SongUploads.title', 'SongUploads.image', 'SongUploads.audio','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.size','SongUploads.duration','SongUploads.created'),
							'conditions' => array('SongUploads.user_id' => $value1['user_id'], 'SongUploads.enabled' => 'Y', 'SongUploads.is_deleted' => 'N'),
							'contain'=>[
								'user'=>['fields' => ['full_name']],
								'category'=>['fields' => ['category_name']],
								'subcategory'=>['fields' => ['category_name']]
							],
							'limit' => '10',
							'order'=>['SongUploads.total_likes'=>'DESC']	
						)); 
						
						if (isset($singersongsData) &&  $singersongsData !='' && !empty($singersongsData)) {
							foreach($singersongsData as $singersongsDataVal) {
								
								$req_singersongA['song']['id'] = $singersongsDataVal['id'];
								$req_singersongA['song']['user_id'] = $singersongsDataVal['user_id'];
								$req_singersongA['song']['title'] = $singersongsDataVal['title'];
								$req_singersongA['song']['cat_id'] = $singersongsDataVal['cat_id'];
								$req_singersongA['song']['sub_cat_id'] = $singersongsDataVal['sub_cat_id'];
								$req_singersongA['song']['image'] = $singersongsDataVal['image'];
								$req_singersongA['song']['audio'] = $singersongsDataVal['audio'];
								$req_singersongA['song']['size'] = $singersongsDataVal['size'];
								$req_singersongA['song']['duration'] = $singersongsDataVal['duration'];
								$req_singersongA['song']['enabled'] = $singersongsDataVal['enabled'];
								$req_singersongA['song']['is_deleted'] = $singersongsDataVal['is_deleted'];
								$req_singersongA['song']['created'] = date('Y m d h:i:s', strtotime($singersongsDataVal['created']));
								$req_singersongA['song']['cat_title'] = $singersongsDataVal['category']['category_name'];
								$req_singersongA['song']['sub_cat_title'] = $singersongsDataVal['subcategory']['category_name'];
								$req_singersongA['song']['user_name'] = $singersongsDataVal['user']['full_name'];
								
								$songDetail = $this->showLike($singersongsDataVal['id'], $singersongsDataVal['user_id']);
								$req_singersongA['song']['likes'] = $songDetail['likes'];
								$req_singersongA['song']['total_likes'] = $songDetail['total_likes'];
								$req_singersongA['song']['unlikes'] = $songDetail['unlikes'];
								$req_singersongA['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$req_singersongA['song']['is_comment'] = $songDetail['is_comment'];
								$req_singersongA['song']['total_comments'] = $songDetail['total_comments'];
								
								$singersongsDataArr[] = $req_singersongA;
							}
						}
					}
					$error = false;
					$message = 'Top 10 Songs Singer Songs';
					$response['top_10_Singer_songs'] = $singersongsDataArr;
				} else {
					$error = true;
					$response['top_10_Singer_songs'] = [];
				}
				
				/*********** Top Posts ************/
				$toppostsData = $this->Posts->find('all', array(
					'fields' => array('Posts.id', 'Posts.title', 'Posts.images', 'Posts.description'),
					'conditions' => array("Posts.status"=>'1',"Posts.enabled "=>'Y' , "Posts.is_deleted !=" => 'Y'),
					'limit' => '10',
					'order'=>['Posts.id'=>'DESC'],	
				)); 
				
				$toppostsDataArr = array();
			
				if (isset($toppostsData) &&  $toppostsData !='' && !empty($toppostsData)) {
					foreach ($toppostsData as $key => $value) {
						$topposts['post'] = $value;
						$toppostsDataArr[] = $topposts;
					}
					$error = false;
					$message = 'Top Posts';
					$response['top_post'] = $toppostsDataArr;
					
				} else {
					$error = true;
					$toppostsDataArr = array();
					$response['top_post'] = [];
				}
				
				/*********** Top Singer Profile List ************/
				$singerProfileData = $this->RequestDjSingers->find('all', array(
					'fields' => array('RequestDjSingers.user_id'),
					'conditions' => array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
					'contain'=>[
						'user'=>['fields' => ['id','full_name','image','email','dob','address']]
					],
					'limit' => '10',
					'order'=>['RequestDjSingers.id'=>'desc'],	
				));
				
				$singerProfileDataArr = array();
			
				if (isset($singerProfileData) &&  $singerProfileData !='' && !empty($singerProfileData)) {
					foreach ($singerProfileData as $key => $value) { //print_r($value); die;
						
						$total_likes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>1])->count();
						$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>0])->count();
											
						
						$singer_profile['profile']['user_id'] = $value['user']['id'];
						$singer_profile['profile']['full_name'] = $value['user']['full_name'];
						$singer_profile['profile']['email'] = $value['user']['email'];
						$singer_profile['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$singer_profile['profile']['address'] = $value['user']['address'];
						$singer_profile['profile']['image'] = $value['user']['image'];
						$singer_profile['profile']['like_count'] = $total_likes;
						$singer_profile['profile']['unlike_count'] = $total_unlikes;
						
						
						$singerProfileDataArr[] = $singer_profile;
					}
					$error = false;
					$message = 'Singers Profile';
					$response['top_singer_profile'] = $singerProfileDataArr;
				} else {
					$error = true;
					$response['top_singer_profile'] = [];
				}
				
				/*********** Top dj Profile List ************/
				$djProfileData = $this->RequestDjSingers->find('all', array(
					'fields' => array('RequestDjSingers.user_id'),
					'conditions' => array('RequestDjSingers.type' => 'D', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
					'contain'=>[
						'user'=>['fields' => ['id','full_name','image','email','dob','address']]
					],
					'limit' => '10',
					'order'=>['RequestDjSingers.id'=>'desc'],	
				));
				
				$djProfileDataArr = array();
			
				if (isset($djProfileData) &&  $djProfileData !='' && !empty($djProfileData)) {
					foreach ($djProfileData as $key => $value) {
						
						$total_likes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>1])->count();
						$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>0])->count();
											
						
						$dj_profile['profile']['user_id'] = $value['user']['id'];
						$dj_profile['profile']['full_name'] = $value['user']['full_name'];
						$dj_profile['profile']['email'] = $value['user']['email'];
						$dj_profile['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$dj_profile['profile']['address'] = $value['user']['address'];
						$dj_profile['profile']['image'] = $value['user']['image'];
						$dj_profile['profile']['like_count'] = $total_likes;
						$dj_profile['profile']['unlike_count'] = $total_unlikes;
						
						
						$djProfileDataArr[] = $dj_profile;
					}
					$error = false;
					$message = 'DJ Profile';
					$response['top_dj_profile'] = $djProfileDataArr;
				} else {
					$error = true;
					$response['top_dj_profile'] = [];
				}
				
				
				/*********** Last 10 day winners List ************/
				$last_10_day = date("Y-n-j", strtotime("10 days ago"));
				$lastWinnerData = $this->ChallengeWinners->find('all', array(
					'fields' => array('ChallengeWinners.user_id'),
					'conditions' => array('ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N', 'ChallengeWinners.created >=' => $last_10_day),
					'contain'=>[
						'Users'=>['fields' => ['id','full_name','image','email','dob','address']]
					],
					'limit' => '10',
					'order'=>['ChallengeWinners.id'=>'desc'],	
				));
				
				$lastWinnerDataArr = array();
			
				if (isset($lastWinnerData) &&  $lastWinnerData !='' && !empty($lastWinnerData)) {
					foreach ($lastWinnerData as $key => $value) { //print_r($value); die;
						
						$last_winner['profile']['user_id'] = $value['user']['id'];
						$last_winner['profile']['full_name'] = $value['user']['full_name'];
						$last_winner['profile']['email'] = $value['user']['email'];
						$last_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$last_winner['profile']['address'] = $value['user']['address'];
						$last_winner['profile']['image'] = $value['user']['image'];
						
						$lastWinnerDataArr[] = $last_winner;
					}
					$error = false;
					$message = 'Last 10 Day Winner';
					$response['last_10_day_winner'] = $lastWinnerDataArr;
				} else {
					$error = true;
					$response['last_10_day_winner'] = [];
				}
				
				
				/*********** Last month winners List ************/
				$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
				$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
				
				$lastMonthWinnerData = $this->ChallengeWinners->find('all', array(
					'fields' => array('ChallengeWinners.user_id'),
					'conditions' => array('ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N', 'ChallengeWinners.created >=' => $last_month_start, 'ChallengeWinners.created <=' => $last_month_end),
					'contain'=>[
						'Users'=>['fields' => ['id','full_name','image','email','dob','address']]
					],
					'limit' => '10',
					'order'=>['ChallengeWinners.id'=>'desc'],	
				));
				
				$lastMonthWinnerDataArr = array();
			
				if (isset($lastMonthWinnerData) &&  $lastMonthWinnerData !='' && !empty($lastMonthWinnerData)) {
					foreach ($lastMonthWinnerData as $key => $value) { //print_r($value); die;
						
						$last_month_winner['profile']['user_id'] = $value['user']['id'];
						$last_month_winner['profile']['full_name'] = $value['user']['full_name'];
						$last_month_winner['profile']['email'] = $value['user']['email'];
						$last_month_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$last_month_winner['profile']['address'] = $value['user']['address'];
						$last_month_winner['profile']['image'] = $value['user']['image'];
						
						$lastMonthWinnerDataArr[] = $last_month_winner;
					}
					$error = false;
					$message = 'Last Month Winner Singer/DJ';
					$response['last_month_winner_singer_dj'] = $lastMonthWinnerDataArr;
				} else {
					$error = true;
					$response['last_month_winner_singer_dj'] = [];
				}
				
				/*********** Last challenge winner List ************/
				$lastChWinnerData = $this->ChallengeWinners->find('all', array(
					'fields' => array('ChallengeWinners.user_id'),
					'conditions' => array('ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N'),
					'contain'=>[
						'Users'=>['fields' => ['id','full_name','image','email','dob','address']]
					],
					'limit' => '10',
					'order'=>['ChallengeWinners.id'=>'desc'],	
				));
				
				$lastChWinnerDataArr = array();
			
				if (isset($lastChWinnerData) &&  $lastChWinnerData !='' && !empty($lastChWinnerData)) {
					foreach ($lastChWinnerData as $key => $value) { //print_r($value); die;
						
						$last_challenge_winner['profile']['user_id'] = $value['user']['id'];
						$last_challenge_winner['profile']['full_name'] = $value['user']['full_name'];
						$last_challenge_winner['profile']['email'] = $value['user']['email'];
						$last_challenge_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$last_challenge_winner['profile']['address'] = $value['user']['address'];
						$last_challenge_winner['profile']['image'] = $value['user']['image'];
						
						$lastChWinnerDataArr[] = $last_challenge_winner;
					}
					$error = false;
					$message = 'Last Challenge Winner';
					$response['last_challenge_winner'] = $lastChWinnerDataArr;
				} else {
					$error = true;
					$response['last_challenge_winner'] = [];
				}
				
				/*********** Singer List Who's Birthday Today ************/
				$singerBdyData = $this->RequestDjSingers->find('all', array(
					'fields' => array('RequestDjSingers.user_id'),
					'conditions' => array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N')	
				)); 
				if (isset($singerBdyData) &&  $singerBdyData !='' && !empty($singerBdyData)) {
					$singerBdyDataArr = array();
					foreach ($singerBdyData as $key1 => $value1) {
						$userBdyData = $this->Users->find('all', array(
							'fields' =>['id','full_name','image','email','dob','address'],
							'conditions' => array('Users.id' => $value1['user_id'],  'Users.enabled' => 'Y', 'Users.is_deleted' => 'N'),
							'limit' => '10',
							'order'=>['Users.id'=>'desc'],	
						));
						if (isset($userBdyData) &&  $userBdyData !='' && !empty($userBdyData)) {
							foreach($userBdyData as $userBdyDataVal) {
								$old_date = $userBdyDataVal['dob'];  
								$old_date_timestamp = strtotime($old_date);
								$new_date = date('m-d', $old_date_timestamp);   
								
								if($new_date == date("m-d")) {
									$singerBdyAr['profile'] = $userBdyDataVal;
									$singerBdyDataArr[] = $singerBdyAr;
								}
							}
						}
					}
					$error = false;
					$message = "Singer List who's Birthday Today";
					$response['singer_list_whos_birthday_today'] = $singerBdyDataArr;
				} else {
					$error = true;
					$response['singer_list_whos_birthday_today'] = [];
				}
			
				//$response['last_10_day_winner'] = [];
				//$response['last_month_winner_singer_dj'] = [];
				//$response['last_challenge_winner'] = [];
				
				
				$responseA['cat'] = $response;
				foreach($responseA['cat'] as $keydata=>$valdata){
					
					if($keydata == 'recently_played'){
						$responserecord['title'] = 'Recently Played' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'challenge'){
						$responserecord['title'] = 'Challenge' ;
						$responserecord['type'] = 'challenge' ;
					}elseif($keydata == 'new_added_by_singer'){
						$responserecord['title'] = 'New Added (By Singer)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'new_added_by_dj'){
						$responserecord['title'] = 'New Added (By Dj)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_10_dj_songs'){
						$responserecord['title'] = 'Top 10 Songs Dj Songs' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_10_Singer_songs'){
						$responserecord['title'] = 'Top 10 Songs Singer Songs' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_post'){
						$responserecord['title'] = 'Top Post' ;
						$responserecord['type'] = 'post' ;
					}elseif($keydata == 'top_singer_profile'){
						$responserecord['title'] = 'Top Singer Profile' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'top_dj_profile'){
						$responserecord['title'] = 'Top Dj Profile' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'singer_list_whos_birthday_today'){
						$responserecord['title'] = "Singer List Who's Birthday Today" ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_10_day_winner'){
						$responserecord['title'] = 'Last 10 Day Winner' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_month_winner_singer_dj'){
						$responserecord['title'] = 'Last Month Winner Singer/DJ' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_challenge_winner'){
						$responserecord['title'] = 'Last Challenge Winner' ;
						$responserecord['type'] = 'profile' ;
					}
					$responserecord['name'] =  $keydata;
					$responserecord['data'] =  $valdata;
					$allresponse[] = $responserecord;
				}
				$allresponseArr['category'] = $allresponse;
				
				
			}
	
			//pr(json_encode($response));die;
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $allresponseArr,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Profile Like/Dislike api **********************/
    public function profileLikeDislike()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Likes');
			$error = true; $code = 0;
			$message = $response = array(); 
			
			if( isset( $this->request->data['user_id'] ) && isset( 
			$this->request->data['profile_id'] ) && isset( $this->request->data['like'] )) 
			{
				$likedata = $this->Likes->find()->where(['user_id'=>$this->request->data['user_id'],
				'profile_id'=>$this->request->data['profile_id']])->toArray();
				if(!empty($likedata)){
					$likesdetail  = $this->Likes->get($likedata[0]['id']);
					if($this->request->data['like']==0){
						$this->request->data['status']=0;
						$message = 'You dislike this profile';
					}else{
						$this->request->data['status']=1;
						$message = 'Thanks for like this profile';
					}
					$likeadd = $this->Likes->patchEntity($likesdetail, $this->request->data);
					$saveLikes = $this->Likes->save($likeadd);
					$error = false;
					$response['total_likes'] = $this->Likes->find()->where(['profile_id'=>$this->request->data['profile_id'],'status'=>1])->count();
					$response['total_unlikes'] = $this->Likes->find()->where(['profile_id'=>$this->request->data['profile_id'],'status'=>0])->count();
				}else{
					$addLikes = $this->Likes->newEntity();
					$addLike = $this->Likes->patchEntity($addLikes, $this->request->data);
					if ($LikesDetail = $this->Likes->save($addLike))
					{
						$response['total_likes'] = $this->Likes->find()->where(['profile_id'=>$this->request->data['profile_id'],'status'=>1])->count();
						$response['total_unlikes'] = $this->Likes->find()->where(['profile_id'=>$this->request->data['profile_id'],'status'=>0])->count();
						$error = false;
						$message = 'Thanks for like this profile';
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data'=>array(),'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Challenge function for api ***************************/
	public function challengeDetail()
	{
		if($this->request->is(['post','put']))
    	{			
			$this->loadModel('Challenges');
			$this->loadModel('SongUploads');
			$this->loadModel('Likes');
			$this->loadModel('ChallengeLikes');
			$error = true; $code = 0;
			$message = ""; $response = [];
			
			$image_base_user_url = path_user;
			$song_image_base_url = path_song_upload_image;
			$song_audio_base_url = path_song_upload_audio;
			
			$response['image_base_user_url'] = $image_base_user_url;
			$response['song_image_base_url'] = $song_image_base_url;
			$response['song_audio_base_url'] = $song_audio_base_url;
			
			if (isset($this->request->data['challenge_id']) && !empty($this->request->data['challenge_id'])) {
			
				/*********** Challenge List ************/
				$challengesData = $this->Challenges->find('all', array(
					'fields' => array('Challenges.id','Challenges.remaining_second', 'Challenges.remaining_time', 'Challenges.challenge_time', 'Challenges.image', 'Challenges.accepted_user_image'),
					'conditions' => array(
						'Challenges.id' => $this->request->data['challenge_id'], 
						'Challenges.status' => '1', 
						'Challenges.is_deleted' => 'N'
					),
					'contain'=>[
						'user1'=>['fields' => ['id','full_name','image']],
						'user2'=>['fields' => ['id','full_name','image']],
						'song'=>['fields' => ['title','image','audio']],
						'acceptusersong'=>['fields' => ['title','image','audio']]
					],
					'limit' => '10',
					'order'=>['Challenges.id'=>'desc']
				));
				//print_r($challengesData->toArray()); die;
				$challengesDataArr = array();
			
				if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
					foreach ($challengesData as $key => $value) { //print_r($value); die;
						
						/*********** Challenge like and unlike count ************/
						$total_challenge_likes1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>1])->count();
						$total_challenge_unlikes1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>0])->count();
						
						$total_challenge_likes2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>1])->count();
						$total_challenge_unlikes2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>0])->count();
						
						/**** Is like user1 *****/
						$datalike1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'user_id'=>$value['user1']['id'],'status'=>1])->toArray();
						if(empty($datalike1)){
							$is_like1 = false;
						}else{
							$is_like1 = true;
						}
						$dataunlike1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'user_id'=>$value['user1']['id'],'status'=>0])->toArray();
						if(empty($dataunlike1)){
							$is_unlike1 = false;
						}else{
							$is_unlike1 = true;
						}
						
						/**** Is like user2 *****/
						$datalike2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'user_id'=>$value['user2']['id'],'status'=>1])->toArray();
						if(empty($datalike2)){
							$is_like2 = false;
						}else{
							$is_like2 = true;
						}
						$dataunlike2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'user_id'=>$value['user2']['id'],'status'=>0])->toArray();
						if(empty($dataunlike2)){
							$is_unlike2 = false;
						}else{
							$is_unlike2 = true;
						}
						
						$challenges['challenge_id'] = $value['id'];
						
						//calulation time
						date_default_timezone_set('Asia/Kolkata');
						$currrenttime = time();
						$date = strtotime(date('Y-m-d h:i:s',$currrenttime));
						$date2 = strtotime(date('Y-m-d h:i:s',strtotime($value['remaining_time'])));
						$diff = $date2 - $date;
						if($diff < 0) {
							$challenge = $this->Challenges->get($value['id']);
							$challenge->remaining_second = 0;
							//$challenge->status = '3';
							$this->Challenges->save($challenge);
						}
						
						if($value['remaining_second'] == 0) {
							$challenges['remaining_time'] = 0;
						} else {
							$challenges['remaining_time'] = $diff;
						}
						$challenges['total_time'] = $value['challenge_time'];
						$challenges['user1']['id'] = $value['user1']['id'];
						$challenges['user1']['full_name'] = $value['user1']['full_name'];
						$challenges['user1']['userimage'] = $value['user1']['image'];
						$challenges['user1']['challenge_image'] = $value['image'];
						$challenges['user1']['total_challenge_likes'] = $total_challenge_likes1;
						$challenges['user1']['total_challenge_unlikes'] = $total_challenge_unlikes1;
						$challenges['user1']['is_like'] = $is_like1;
						$challenges['user1']['is_unlike'] = $is_unlike1;
						$challenges['user1']['song']['title'] = $value['song']['title'];
						$challenges['user1']['song']['image'] = $value['song']['image'];
						$challenges['user1']['song']['audio'] = $value['song']['audio'];
						
						$challenges['user2']['id'] = $value['user2']['id'];
						$challenges['user2']['full_name'] = $value['user2']['full_name'];
						$challenges['user2']['userimage'] = $value['user2']['image'];
						$challenges['user2']['challenge_image'] = $value['accepted_user_image'];
						$challenges['user2']['total_challenge_likes'] = $total_challenge_likes2;
						$challenges['user2']['total_challenge_unlikes'] = $total_challenge_unlikes2;
						$challenges['user2']['is_like'] = $is_like2;
						$challenges['user2']['is_unlike'] = $is_unlike2;
						$challenges['user2']['song']['title'] = $value['acceptusersong']['title'];
						$challenges['user2']['song']['image'] = $value['acceptusersong']['image'];
						$challenges['user2']['song']['audio'] = $value['acceptusersong']['audio'];
						
					    // $challenges[$key] = $value;
						
						$challengesDataArr[] = $challenges;
					}
					$error = false;
					$message = 'Challenge';
					$response['challenge'] = $challengesDataArr;
					
				} else {
					$error = true;
					$challengesDataArr = array();
					$response['challenge'] = array();
				}
				
			} else $message = 'Incomplete Data';
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	private function showLike($song_id, $user_id) {
				
		$this->loadModel('Likes');
		$this->loadModel('Comments');
		$this->loadModel('SongUploads');
		
		$songDetails_data = array();
		$songDetails=$this->SongUploads->get($song_id, ['contain'=> ['likes','comment']])->toArray();
		//print_r($songDetails); die;
		/**** total like *****/
		$datalike = $this->Likes->find()->where(['song_id'=>$song_id,'user_id'=>$user_id,'status'=>1])->toArray();
		if(empty($datalike)){
			$songDetails_data['likes'] = false;
		}else{
			$songDetails_data['likes'] = true;
		}
		
		$dataunlike = $this->Likes->find()->where(['song_id'=>$song_id,'user_id'=>$user_id,'status'=>0])->toArray();
		if(empty($dataunlike)){
			$songDetails_data['unlikes'] = false;
		}else{
			$songDetails_data['unlikes'] = true;
		}
		
		$like = $this->Likes->find()->where(['song_id'=>$song_id,'status'=>1])->count();
		$songDetails_data['total_likes'] = $like;
		$unlike = $this->Likes->find()->where(['song_id'=>$song_id,'status'=>0])->count();
		$songDetails_data['total_unlikes'] = $unlike;	
		
		/**** end total like *****/
		
		/**** total comment *****/
		$data_comment=$this->Comments->find()->where(['song_id'=>$song_id,'user_id'=>$user_id,'is_deleted !='=>'Y'])->toArray();
		if(empty($data_comment)){
			$songDetails_data['is_comment'] = false;
		}else{
			$songDetails_data['is_comment'] = true;
		}
		$songDetails_data['total_comments'] = count($songDetails['comment']);
		/**** end total comment *****/
		return $songDetails_data;
				
	}
	
	/****************** Banner list function for api **********************/
	public function banner()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = []; 
			$this->loadModel('Banners');
			
			
			if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			
			$allbanners = $this->Paginator->paginate(
				$this->Banners, [
					'limit' => '10',
					'page' => $page,
					'order'=>['id'=>'asc'],
					'conditions'=>array('enabled !='=>'Y'),
			]);
			
			$banner_dataArr = array();
			if (isset($allbanners) &&  $allbanners !='' && !empty($allbanners)) {
				foreach ($allbanners as $key => $value) {
					$banner_data['id'] = $value['id'];
					$banner_data['text'] = $value['text'];
					$banner_data['image'] = $value['image'];
					$banner_dataArr[] = $banner_data;
				}
				$error = false;
				$message = 'All Banners';
				$image_banner_url = path_banner_image;
				$response['image_banner_url'] = $image_banner_url;
				$response['banner'] = $banner_dataArr;	
				
			} else {
				$error = true;
				$message = "No record available";
				$response['banner'] = [];	
			}
		
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	
	/****************** song paly or stop rewards api **********************/
    public function songPlayStop()
    {
		if($this->request->is(['post','put']))
    	{
			date_default_timezone_set('Asia/Kolkata');
			$this->loadModel('UserRewards');
			$error = true; $code = 0;
			$message = $response = []; 
			if( isset( $this->request->data['user_id'] ) &&  $this->request->data['user_id'] !='' && isset( $this->request->data['video_type'] )  &&  $this->request->data['video_type'] !='') 
			{
				$minRewards = $this->Users->find('all', array(
					'fields' => array('Users.app_use_time'),
					'conditions' => array('Users.access_level_id' => 1),
				))->first();
				
				if($this->request->data['video_type'] =='P'){
					$chkRewards = $this->UserRewards->find()->where(['user_id'=>$this->request->data['user_id'],'is_deleted'=>'N','enabled'=>'Y'])->first();
					if(!empty($chkRewards)){
						
						$currentdate = strtotime(date('Y-m-d'));
						$cdate = strtotime(date('Y-m-d H:i:s'));
						$start_time = strtotime($chkRewards['start_time']);
						
						
						$start_timeoneday = strtotime(date('Y-m-d',strtotime("+1 days",strtotime($chkRewards['start_time']))));
						$diff = $cdate - $start_time;
						
						if($currentdate == $start_timeoneday){
							$userrewards = $this->UserRewards->get($chkRewards['id']);
							$userrewards->time = '0';
							$userrewards->start_time = date('Y-m-d H:i:s');
							$this->UserRewards->save($userrewards);
							$error = false;
							$user_rewardsArr['total_reward_time'] = (int)$userrewards->time;
							$user_rewardsArr['app_use_time'] = (int)$minRewards['app_use_time'];
							$response = $user_rewardsArr;
							$message = 'Rewards Added Successfuly';
						}else{
							if($diff >= 0) {
								$userrewards = $this->UserRewards->get($chkRewards['id']);
								$userrewards->start_time = date('Y-m-d H:i:s');
								$this->UserRewards->save($userrewards);
								$error = false;
								$user_rewardsArr['total_reward_time'] = (int)$userrewards->time;
								$user_rewardsArr['app_use_time'] = (int)$minRewards['app_use_time'];
								$response = $user_rewardsArr;
								$message = 'Rewards Added Successfuly';
							}
						}
						
						
						
					}else{
						$addReward = $this->UserRewards->newEntity();
						$this->request->data['start_time'] = date('Y-m-d H:i:s');
						$addRewards = $this->UserRewards->patchEntity($addReward, $this->request->data);
						if ($rewardsDetail = $this->UserRewards->save($addRewards))
						{
							$error = false;
							$user_rewardsArr['total_reward_time'] = (int)$rewardsDetail->time;
							$user_rewardsArr['app_use_time'] = (int)$minRewards['app_use_time'];
							$response = $user_rewardsArr;
							$message = 'Rewards Added Successfuly';
						} else {
							$error = true;
							foreach($addRewards->errors() as $field_key =>  $error_data)
							{
								foreach($error_data as $error_text)
								{
									$message = $field_key.", ".$error_text;
									break 2;
								} 
							}
						}
					}
				}else{
					$chkRewards = $this->UserRewards->find()->where(['user_id'=>$this->request->data['user_id'],'is_deleted'=>'N','enabled'=>'Y'])->first();
					if(!empty($chkRewards)){
						
						$cdate = strtotime(date('Y-m-d H:i:s'));
						$start_time = strtotime($chkRewards['start_time']->i18nFormat('yyyy-MM-dd HH:mm:ss'));
						$diff = $cdate - $start_time;
						if($diff >= 0) {
							$totaltime = $diff + $chkRewards['time'];
							$userrewards = $this->UserRewards->get($chkRewards['id']);
							$userrewards->time = $totaltime;
							$userrewards->start_time = date('Y-m-d H:i:s');
							$userrewardsdetails = $this->UserRewards->save($userrewards);
							$error = false;
							$user_rewardsArr['total_reward_time'] = (int)$userrewardsdetails['time'];
							$user_rewardsArr['app_use_time'] = (int)$minRewards['app_use_time'];
							$response = $user_rewardsArr;
							$message = 'Rewards Added Successfuly';
						}
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Add Payment Details api **********************/
    public function addPaymentDetail()
    {
		if($this->request->is(['post','put']))
    	{
    	
			$this->loadModel('PaymentDetails');
			$error = true; $code = 0;
			$message = $response = []; 
			if( isset( $this->request->data['user_id'] )) 
			{
				$addPaymentDetail = $this->PaymentDetails->newEntity();
				$addPaymentDetails = $this->PaymentDetails->patchEntity($addPaymentDetail, $this->request->data);
				//				print_r($addRewards);die;

				if ($paymentDetail = $this->PaymentDetails->save($addPaymentDetails))
				{
					$error = false;
					$message = 'Submitted Successfuly';
				} else {
					$error = true;
					foreach($addPaymentDetails->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}	
	}
	
	/****************** challenge Like/Dislike api **********************/
    public function challengeLikeDislike()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('ChallengeLikes');
			$error = true; $code = 0;
			$message = $response = array(); 
			
			if( isset( $this->request->data['challenge_id'] ) && isset( $this->request->data['user_id'] ) && isset( 
			$this->request->data['ch_user_id'] ) && isset( $this->request->data['like'] )) 
			{
				$likedata = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'user_id'=>$this->request->data['user_id'],'ch_user_id'=>$this->request->data['ch_user_id']])->toArray();
				if(!empty($likedata)){
					$likesdetail  = $this->ChallengeLikes->get($likedata[0]['id']);
					if($this->request->data['like']==0){
						$this->request->data['status']=0;
						$message = 'You dislike this challenge';
					}else{
						$this->request->data['status']=1;
						$message = 'Thanks for like this challenge';
					}
					$likeadd = $this->ChallengeLikes->patchEntity($likesdetail, $this->request->data);
					$saveLikes = $this->ChallengeLikes->save($likeadd);
					$error = false;
					$response['total_challenge_likes'] = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'ch_user_id'=>$this->request->data['ch_user_id'],'status'=>1])->count();
					$response['total_challenge_unlikes'] = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'ch_user_id'=>$this->request->data['ch_user_id'],'status'=>0])->count();
				}else{
					$addLikes = $this->ChallengeLikes->newEntity();
					$this->request->data['status'] = $this->request->data['like'];
					$addLike = $this->ChallengeLikes->patchEntity($addLikes, $this->request->data);
					//print_r($addLike); die;
					if ($LikesDetail = $this->ChallengeLikes->save($addLike))
					{
						$response['total_challenge_likes'] = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'ch_user_id'=>$this->request->data['ch_user_id'],'status'=>1])->count();
						$response['total_challenge_unlikes'] = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'ch_user_id'=>$this->request->data['ch_user_id'],'status'=>0])->count();
						$error = false;
						$message = 'Thanks for like this challenge';
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data'=>array(),'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** My rewards list function for api ***************************/
	public function myRewards()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('UserRewards');
			$this->loadModel('Users');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				
				$userRewards = $this->UserRewards->find('all', array(
					'fields' => array('UserRewards.id', 'UserRewards.user_id', 'UserRewards.point'),
					'conditions' => array('UserRewards.user_id' => $this->request->data['user_id'], 'UserRewards.enabled' => 'Y', 'UserRewards.is_deleted' => 'N'),
				))->first();
				$minRewards = $this->Users->find('all', array(
					'fields' => array('Users.minimum_rewards'),
					'conditions' => array('Users.access_level_id' => 1),
				))->first();
				
				if (isset($userRewards) &&  $userRewards !='' && !empty($userRewards)) {
					
					$user_rewards['id'] = $userRewards['id'];
					$user_rewards['user_id'] = $userRewards['user_id'];
					$user_rewards['total_reward_points'] = (int)$userRewards['point'];
					$user_rewards['minimum_reward_points'] = (int)$minRewards['minimum_rewards'];
						
					$response = $user_rewards;
					$error = false;
					$message = 'My Reward';
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Daily Task calculate your remaining rewards time function for api ***************************/
	public function dailyTask()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('UserRewards');
			$this->loadModel('Users');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				
				$userRewards = $this->UserRewards->find('all', array(
					'fields' => array('UserRewards.id', 'UserRewards.user_id', 'UserRewards.time'),
					'conditions' => array('UserRewards.user_id' => $this->request->data['user_id'], 'UserRewards.enabled' => 'Y', 'UserRewards.is_deleted' => 'N'),
				))->first();
				$minRewards = $this->Users->find('all', array(
					'fields' => array('Users.app_use_time'),
					'conditions' => array('Users.access_level_id' => 1),
				))->first();
				
				if (isset($userRewards) &&  $userRewards !='' && !empty($userRewards)) {
					
					$user_rewards['id'] = $userRewards['id'];
					$user_rewards['user_id'] = $userRewards['user_id'];
					$user_rewards['total_reward_time'] = (int)$userRewards['time'];
					$user_rewards['app_use_time'] = (int)$minRewards['app_use_time'];
						
					$response = $user_rewards;
					$error = false;
					$message = 'My Daily task Time';
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Subscriptions list function for api **********************/
	public function subscriptionsList()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = []; 
			$this->loadModel('Premiums');
			
			
			if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			
			$allsubscriptions = $this->Paginator->paginate(
				$this->Premiums, [
					'limit' => '10',
					'page' => $page,
					'order'=>['id'=>'asc'],
					'conditions'=>array('status'=>'Y'),
			]);
			
			$subscriptions_dataArr = array();
			if (isset($allsubscriptions) &&  $allsubscriptions !='' && !empty($allsubscriptions)) {
				foreach ($allsubscriptions as $key => $value) {
					$subscriptions_data['id'] = $value['id'];
					$subscriptions_data['plan_name'] = $value['plan_name'];
					$subscriptions_data['plan_desc'] = $value['plan_desc'];
					$subscriptions_data['duration'] = $value['duration'];
					$subscriptions_data['duration_type'] = $value['duration_type'];
					$subscriptions_data['amount'] = $value['amount'];
					$subscriptions_dataArr[] = $subscriptions_data;
				}
				$error = false;
				$message = 'All Subscriptions';
				$response['subscriptions'] = $subscriptions_dataArr;	
				
			} else {
				$error = true;
				$message = "No record available";
				$response['subscriptions'] = [];	
			}
		
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** Refer friend function for api **********************/
	public function referFriend()
	{
		if($this->request->is(['post','put']))
    	{
			
			$this->loadModel('Users');
			$this->loadModel('Pages');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				$referal_code = $this->Users->find('all', array(
					'fields' => array('Users.refer_code'),
					'conditions' => array('Users.id' => $this->request->data['user_id']),
				))->first();
				if (isset($referal_code) &&  $referal_code !='' && !empty($referal_code)) {
					$user_data['referal_code'] = $referal_code['refer_code'];
				}
				
				$terms = $this->Pages->find('all', array(
					'fields' => array('Pages.title', 'Pages.description'),
					'conditions' => array('Pages.id' => 20, 'Pages.enabled' => 1),
				))->first();
				if (isset($terms) &&  $terms !='' && !empty($terms)) {
					$user_data['terms_and_conditions'] = $terms['description'];
				}
				
				$refer_earn = $this->Pages->find('all', array(
					'fields' => array('Pages.title', 'Pages.description'),
					'conditions' => array('Pages.id' => 19, 'Pages.enabled' => 1),
				))->first();
				if (isset($refer_earn) &&  $refer_earn !='' && !empty($refer_earn)) {
					$user_data['refer_and_earn'] = $refer_earn['description'];
				}
				
				$response = $user_data;
				$error = false;
				$message = 'Refer Friend';
				
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Check subscriber plan function for api ***************************/
    public function checkSubscriberPlan()
    {
		if($this->request->is(['post','put']))
    	{
			
			$this->loadModel('PurchasePlans');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				
				$freeSubsDays = $this->Users->find('all', array(
					'fields' => array('Users.subscription_duration_month'),
					'conditions' => array('Users.access_level_id' => 1),
				))->first();
				
				/*********** Check if user has bought a plan or not ************/
				$userPlan = $this->PurchasePlans->find('all', array(
					'fields' => array('PurchasePlans.id', 'PurchasePlans.status', 'PurchasePlans.created'),
					'conditions' => array('PurchasePlans.user_id' => $this->request->data['user_id'], 'PurchasePlans.status' => 'C', 'PurchasePlans.is_deleted' => 'N'),
					'contain'=>[
						'Premiums'=>['fields' => ['id','duration_type','duration']]
					]
				))->first();
				
				if($userPlan['premium']['duration_type'] == 'Monthly') { 
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."months", strtotime($userPlan['created'])));
				} elseif($userPlan['premium']['duration_type'] == 'Yearly') {	
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."years", strtotime($userPlan['created'])));
				}
				
				//calulation current time
				date_default_timezone_set('Asia/Kolkata');
				$currrenttime = time();
				$current_date = date('Y-m-d', $currrenttime);
			
				if ($userPlan =='' && empty($userPlan)) { 
					
					$free_subscription = $this->Users->find('all', array(
							'fields' => array('Users.id', 'Users.free_subscription_time', 'Users.created'),
							'conditions' => array('Users.id' => $this->request->data['user_id']),
					))->first();
					
					if (isset($free_subscription['free_subscription_time']) && $free_subscription['free_subscription_time'] !='' && !empty($free_subscription['free_subscription_time'])) {
						$subsExpireDate =  date('Y-m-d', strtotime($free_subscription['free_subscription_time']));
						$previousSubsExpireDate = date('Y-m-d', strtotime('-1 day', strtotime($subsExpireDate)));
						
						if($current_date >= $subsExpireDate) {
							$free_plan = $this->Users->get($free_subscription['id']);
							$free_plan->free_subscription_time = null;
							$this->Users->save($free_plan);
							
							// user plan details
							$error = false;
							$response['plan_type'] = 'free';
							$response['plan_purchase_date'] = date('Y-m-d', strtotime($free_subscription['created']));
							$response['plan_expire_date'] = $subsExpireDate;
							$response['plan_duration_type'] = 'monthly';
							$response['plan_duration'] = $freeSubsDays['subscription_duration_month'];
							$response['is_running'] = false;
							$response['used_time'] = ((strtotime($response['plan_expire_date'])-strtotime($response['plan_purchase_date']))/86400).' days';
							$response['remaining_time'] = '0 days';
							$response['message'] = 'Your free subscription plan has been expired.';
							
						} else {
							if ($current_date == $previousSubsExpireDate) {
								$response['message'] = 'Your free subscription plan is going to expire today.';
							} 
							// user plan details
							$error = false;
							$response['plan_type'] = 'free';
							$response['plan_purchase_date'] = date('Y-m-d', strtotime($free_subscription['created']));
							$response['plan_expire_date'] = $subsExpireDate;
							$response['plan_duration_type'] = 'monthly';
							$response['plan_duration'] = $freeSubsDays['subscription_duration_month'];
							$response['is_running'] = true;
							$response['used_time'] = ((strtotime($current_date)-strtotime($response['plan_purchase_date']))/86400).' days';
							$response['remaining_time'] = ((strtotime($response['plan_expire_date'])-strtotime($current_date))/86400).' days';
						} 
					} else {
						// user plan details
						$error = false;
						$response['plan_type'] = 'premium';
						$response['is_running'] = false;
						$response['message'] = 'Your plan has been expired.';
					}
				} elseif($current_date >= $planExpireDate) {
					$purchase_plan = $this->PurchasePlans->get($userPlan['id']);
					$purchase_plan->status = 'E';
					$this->PurchasePlans->save($purchase_plan);
					
					// user plan details
					$error = false;
					$response['plan_type'] = 'premium';
					$response['plan_purchase_date'] = date('Y-m-d', strtotime($userPlan['created']));
					$response['plan_expire_date'] = $planExpireDate;
					$response['plan_duration_type'] = $userPlan['premium']['duration_type'];
					$response['plan_duration'] = $userPlan['premium']['duration'];
					$response['is_running'] = false;
					$response['used_time'] = ((strtotime($response['plan_expire_date'])-strtotime($response['plan_purchase_date']))/86400).' days';
					$response['remaining_time'] = '0 days';
					$response['message'] = 'Your plan has been expired.';
					
				} else {
					// user plan details
					$error = false;
					$response['plan_type'] = 'premium';
					$response['plan_purchase_date'] = date('Y-m-d', strtotime($userPlan['created']));
					$response['plan_expire_date'] = $planExpireDate;
					$response['plan_duration_type'] = $userPlan['premium']['duration_type'];
					$response['plan_duration'] = $userPlan['premium']['duration'];
					$response['is_running'] = true;
					$response['used_time'] = ((strtotime($current_date)-strtotime($response['plan_purchase_date']))/86400).' days';
					$response['remaining_time'] = ((strtotime($response['plan_expire_date'])-strtotime($current_date))/86400).' days';
					$response['message'] = '';
					
				}
			} else {
				$message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/******** On off notification for a user function for Api *******/
	public function notificationOnOff()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('NotificationStatus');
			$error = true; $code = 0;
			$message = $response = array(); 
			
			if( isset( $this->request->data['user_id'] ) && isset( $this->request->data['other_user_id'] )) 
			{
				$statusdata = $this->NotificationStatus->find()->where(['user_id'=>$this->request->data['user_id'],
				'other_user_id'=>$this->request->data['other_user_id']])->toArray();
				if(!empty($statusdata)) {
					$statusdetail  = $this->NotificationStatus->get($statusdata[0]['id']);
					$this->NotificationStatus->delete($statusdetail);
					$message = 'Notification is on for user id '.$this->request->data['other_user_id']. '.';
				} else {
					$addStatus = $this->NotificationStatus->newEntity();
					$off_noti = $this->NotificationStatus->patchEntity($addStatus, $this->request->data);
					if ($StatusDetail = $this->NotificationStatus->save($off_noti))
					{
						$message = 'Notification is off for user id '.$this->request->data['other_user_id']. '.';
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data'=>array(),'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}	
	}
	
	/****************** Send notification when singer/dj upload a song function for api ***************************/
	public function sendNotification($user_id, $song_id, $song_name=null)
	{		
		$this->loadModel('RequestDjSingers');
		$this->loadModel('NotificationStatus');
							
		$user_data = $this->RequestDjSingers->find('list', array(
			'keyField' => 'id',
			'valueField' => 'user_id',
			'conditions' => array('RequestDjSingers.user_id !=' => $user_id, 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'),
		))->toArray();
		
		$notificationsDataArr = array();
		if (isset($user_data) &&  $user_data !='' && !empty($user_data)) {

			$check_noti_on = $this->NotificationStatus->find('list', array(
				'valueField' => 'user_id',
				'conditions' => array('NotificationStatus.user_id IN' => $user_data, 'NotificationStatus.other_user_id' => $user_id)
			))->toArray();
				
			if (isset($check_noti_on) &&  $check_noti_on !='' && !empty($check_noti_on)) {
				$notificationsDataArr = array_diff($user_data, $check_noti_on);
				
				foreach($notificationsDataArr as $key => $notify_user_id) {
					// Send Push Notification	
					$this->sendPushNotification('Song_upload', $song_id, $song_name, null, $notify_user_id, 'Congratulations!! Your song uploaded successfully.');
				}
				
				// puch notification code here			
			}
		} 
		return true;
	}
	
	public function paytm()
	{	
		require_once(ROOT . '/vendor' . DS  . 'Paytm' . DS . 'lib' . DS . 'encdec_paytm.php');
		
		define("merchantMid", "rxazcv89315285244163");
		// Key in your staging and production MID available in your dashboard
		define("merchantKey", "gKpu7IKaLSbkchFS");
		// Key in your staging and production MID available in your dashboard
		define("orderId", "order1");
		define("channelId", "WAP");
		define("custId", "cust123");
		define("mobileNo", "7777777777");
		define("email", "username@emailprovider.com");
		define("txnAmount", "100.12");
		define("website", "WEBSTAGING");
		// This is the staging value. Production value is available in your dashboard
		define("industryTypeId", "Retail");
		// This is the staging value. Production value is available in your dashboard
		define("callbackUrl", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=order1");
		$paytmParams = array();
		$paytmParams["MID"] = merchantMid;
		$paytmParams["ORDER_ID"] = orderId;
		$paytmParams["CUST_ID"] = custId;
		$paytmParams["MOBILE_NO"] = mobileNo;
		$paytmParams["EMAIL"] = email;
		$paytmParams["CHANNEL_ID"] = channelId;
		$paytmParams["TXN_AMOUNT"] = txnAmount;
		$paytmParams["WEBSITE"] = website;
		$paytmParams["INDUSTRY_TYPE_ID"] = industryTypeId;
		$paytmParams["CALLBACK_URL"] = callbackUrl;
		$paytmChecksum = getChecksumFromArray($paytmParams, merchantKey);
		print_r($paytmChecksum);die;
	}
	
	/****************** Rewards history list function for api ***************************/
	public function rewardsHistory()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('HistoryRewards');
			$error =true;$code=0;
			$message= ""; $response = [];
			
			if(isset($this->request->data['user_id']))
			{
					
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$searchData = array();
				$searchData['AND'][] = array("HistoryRewards.is_deleted !=" => 'Y',"HistoryRewards.user_id" =>$this->request->data['user_id']);
				$historyRewards = $this->Paginator->paginate(
					$this->HistoryRewards, [
						'limit' => '10',
						'page' => $page,
						'order'=>['id'=>'asc'],
						'contain'=>[
							'user'=>['fields' => ['full_name']]
						],
						'conditions'=>$searchData,
				]);
				$rewardsHistory_data = array();
				if (isset($historyRewards) &&  $historyRewards !='' && !empty($historyRewards)) {
					foreach ($historyRewards as $key => $value) {
						$rewards_data['id'] = $value['id'];
						$rewards_data['user_id'] = $value['user_id'];
						$rewards_data['title'] = 'Rewards Title';
						$rewards_data['point'] = $value['point'];
						$rewards_data['created'] = date('Y-m-d',strtotime($value['created']));
						$rewardsHistory_data[] = $rewards_data;
					}
					$error = false;
					$message = 'All Rewards History';
					if(isset($rewardsHistory_data) &&  $rewardsHistory_data !='' && !empty($rewardsHistory_data)) {
						$response = $rewardsHistory_data;
					}else{
						$response = [];
					}
					
				} else {
					$error = true;
					$message = "No record available";	
					
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	
}
