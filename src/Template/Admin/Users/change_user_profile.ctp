<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>
<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create($userProfile,array('class'=>'form-horizontal form-label-left','enctype'=>'multipart/form-data' ,'novalidate','method'=>'post'));
				  ?>
					<?= $this->Flash->render() ?>
                      <span class="section" style="text-align:center;">Become Singer & DJ</span>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Type <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                       <?php 

								echo $this->Form->radio(
									'type',
									[
										['value' => 'S', 'text' => 'Singer','checked'],
										['value' => 'D', 'text' => 'DJ'],
										
									]
								);
							?>
                       </div>
                      </div>
                      
                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Address <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('address',array('id'=>'geocompleteadd','class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
                        
                      
                      <div class="item form-group"  >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Document<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<?php  echo $this->Form->input('id_proof',array('class' => 'form-control col-md-7 col-xs-12','type'=>'file','label' =>false));
							if($userProfile->id_proof != '') echo '<img width="50px" src="'.path_IDProof_image.$userProfile->image.'"/>';
							 ?>
                        </div>
                      </div>
                     
                       <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							<a href="<?php echo	$this->Url->build(['controller'=>'Users','action'=>'manage']);?>">
								<button type="button" class="btn btn-primary">Cancel</button>
							</a>
							<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
         
          </div>
        </div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function() {
	$("#geocompleteadd").geocomplete({
	  details: "form",
	  types: ["geocode", "establishment"],
	}).bind("geocode:result", function(event, result){
	});
}) ;
</script>
