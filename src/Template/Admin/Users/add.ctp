<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>
<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create($user,array('class'=>'form-horizontal form-label-left','enctype'=>'multipart/form-data' ,'novalidate','method'=>'post'));
				  ?>
					<?= $this->Flash->render() ?>
                      <span class="section" style="text-align:center;">Add User</span>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gender <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                       <?php 

								echo $this->Form->radio(
									'gender',
									[
										['value' => 'M', 'text' => 'Male','checked'],
										['value' => 'F', 'text' => 'Female'],
										
									]
								);
							?>
                       </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Full Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							
						 <?php  echo $this->Form->input('full_name',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text"));
						 
						  ?>
                         </div>
                      </div>
                      

					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('email',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"email")); ?>
                        </div>
                      </div>
                     
                    
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Current Address <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('address',array('id'=>'geocompleteadd','class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                           <input type="hidden" name="latitude" id="latbox" value=""/>
						   <input type="hidden" name="longitude" id="lngbox" value=""/>
                        </div>
                      </div>
                        
                      
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Phone Number <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('phone_number',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Date of Birth <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							 <?php  echo $this->Form->input('dob',array('readonly','class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                           
                        </div>
                      </div>
                      
                      
                      <!-- div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('password',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"password")); ?>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Confirm Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('confirm_password',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"password")); ?>
                        </div>
                      </div -->
					
                    
                       <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
         
          </div>
        </div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function() {
   $('#dob').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-100:+0",
		dateFormat: 'yy-mm-dd',
		maxDate: '0', 
	});
	
	$("#geocompleteadd").geocomplete({
	  details: "form",
	  types: ["geocode", "establishment"],
	}).bind("geocode:result", function(event, result){
		$("#latbox").val(result.geometry.location.lat());
		$("#lngbox").val(result.geometry.location.lng());
	});
}) ;
</script>
<script>
	<!--get state onChnage on country dropdown-->
	$('#getstate').change(function(){	
		var getId = $(this).find(":selected").val();
		$.ajax({
			type: "POST",
			url: "<?php echo $this->Url->build(['controller'=>'users','action'=>'get_state']); ?>",
			data: {getId: getId},
			success: function(data){
				$( "#getcity" ).html( data );
			}
		});
	});
	<!--get city onChnage on state dropdown-->
	$('#getcity').change(function(){	
		var getId = $(this).find(":selected").val();
		$.ajax({
			type: "POST",
			url: "<?php echo $this->Url->build(['controller'=>'users','action'=>'get_city']); ?>",
			data: {getId: getId},
			success: function(data){
				$( "#city-id" ).html( data );
			}
		});
	});
</script>
