 <div class="">
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>

      <div id="wrapper">
        <div id="login" class=" form">
          <section class="login_content">
            <?php echo $this->Form->create('login');?>
              <h1>Admin Login</h1>
			  <?php echo $this->Flash->render();
					echo $this->Flash->render('auth'); 
				?>
              <div>
                <?= $this->Form->input('username',array('class'=>'form-control','placeholder'=>'Username or email','label'=>false,'div'=>false , 'required' => 'required'))?>
              </div>
              <div>
               <?= $this->Form->input('password',array('class'=>'form-control','placeholder'=>'Password','label'=>false,'div'=>false , 'required' => 'required'))?>
              </div>
              <div>
               <button class="btn btn-default submit" type="submit">Sign in</button>
                
              </div>
              <div class="clearfix"></div>
              <div class="separator">

               
                <div class="clearfix"></div>
                <br />
                <div>
                  <h1><i class="fa fa-paw" style="font-size: 26px;"></i> <?=$project_title?>!</h1>

                  <p>©2016 All Rights Reserved.  <?=$project_title?>! </p>
                </div>
              </div>
             <?php echo $this->Form->end();?>
          </section>
        </div>
      </div>
    </div>
