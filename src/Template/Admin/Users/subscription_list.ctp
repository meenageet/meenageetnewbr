<style>
.text-padding{padding-top: 7px;}
.w3-panel{
    padding: 0.01em 16px;
    margin-top: 16px!important;
    margin-bottom: 16px!important;
}.w3-note {
    background-color: #fcf8e3;
    border-left: 6px solid #faebcc;
    color:#8a6d3b;
}.w4-note {
    background-color: #f2dede;
    border-left: 6px solid #ebccd1;
    color:#a94442;
}
</style>
<div class="right_col" role="main">

	<div class="">
            
		<div class="clearfix"></div>

			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_content form-horizontal">
								<!-- user one row -->
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<span class="section">Current Plan</span>
											<?php if(isset($response['plan_type']) && $response['plan_type'] !=''){?>
												<div class="item form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Type : </label>
													<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
														<?php echo $response['plan_type'];?>
													</div>
												</div>
											<?php } ?>
											<?php if(isset($response['plan_purchase_date']) && $response['plan_purchase_date'] !=''){?>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Purchase Date : </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php echo $response['plan_purchase_date'];?>
												</div>
											</div>
											<?php } ?>
											<?php if(isset($response['plan_expire_date']) && $response['plan_expire_date'] !=''){?>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Expire Date: </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php echo $response['plan_expire_date'];?>
												</div>
											</div>
											<?php } ?>
											<?php if(isset($response['plan_duration_type']) && $response['plan_duration_type'] !=''){?>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Duration Type: </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php echo $response['plan_duration_type'];?>
												</div>
											</div>
											<?php } ?>
											<?php if(isset($response['plan_duration']) && $response['plan_duration'] !=''){?>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Duration: </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php echo $response['plan_duration'];?>
												</div>
											</div>
											<?php } ?>
											<?php if(isset($response['is_running']) && $response['is_running'] !=''){?>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Running Plan: </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php 
													if($response['is_running'] =='1'){
														echo 'Yes';
													}else{
														echo 'No';
													}
													?>
												</div>
											</div>
											<?php } ?>
											<?php if(isset($response['remaining_time']) && $response['remaining_time'] !=''){?>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Remaining Time: </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php echo $response['remaining_time'];?>
												</div>
											</div>
											<?php } ?>
											<?php if(isset($response['message']) && $response['message'] !=''){?>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Remaining Time: </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php echo $response['message'];?>
												</div>
											</div>
											<?php }else{ ?>
												<div class="item form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Remaining Time: </label>
													<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
														<?php echo 'This Plan Currently running.';?>
													</div>
												</div>
											<?php }?>
											
											
									</div>
								<!-- end user one row -->
								</div>
								
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
        <!-- /page content -->
