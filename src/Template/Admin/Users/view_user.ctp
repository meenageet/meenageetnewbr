
<style>
 .gallery img{
            width:179px;
            cursor:pointer;
        }
.w3-panel{
    padding: 0.01em 16px;
    margin-top: 16px!important;
    margin-bottom: 16px!important;
}.w3-note {
    background-color: #fcf8e3;
    border-left: 6px solid #faebcc;
    color:#8a6d3b;
}.w4-note {
    background-color: #f2dede;
    border-left: 6px solid #ebccd1;
    color:#a94442;
}
.img-circle {
    border-radius: 50%;
}
</style>
<?php
echo $this->Html->css('Admin/ninja-slider.css');
?>
<div class="right_col" role="main">

          <div class="">
            <div class="clearfix"></div>
			 <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div id="divLoading" > </div>
                  <div class="x_content form-horizontal">
					<?= $this->Flash->render() ?>
					<?php 
					//pr($user);
					//pr($open_bookings);
					//pr($close_bookings);die;
					
					?>
					   <span class="section">User Detail</span>
					   <?php
					   if($user['image'] != ''){
							$path = WWW_ROOT."uploads/user_images/".$user['image'];
							if(file_exists($path )){ ?>
								
							  <div class="item form-group">
								<div class="col-md-offset-2 col-md-6 col-sm-6 col-xs-12" >
								   <?php echo '<img width="100px" height=" 100px" class="img-circle"  id="preview1" src="'.$this->request->webroot.'uploads/user_images/'.$user['image'].'"  />';  ?>
								</div>
							  </div>
							<?php } }
					    ?>
					   
					    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Name</label>
                        <div class="col-md-6 col-sm-6 col-xs-6" style="line-height: 34px;">
						<?=$user['full_name'] ?>	
                       
                       </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                        <div class="col-md-6 col-sm-6 col-xs-6" style="line-height: 34px;">
						<?=$user['email'] ?>	
                       
                       </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Phone Number</label>
                        <div class="col-md-6 col-sm-6 col-xs-6" style="line-height: 34px;">
						<?=$user['phone_number'] ?>	
                       
                       </div>
                      </div>
                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                        <div class="col-md-6 col-sm-6 col-xs-6" style="line-height: 34px;">
						<?php if($user['lat'] != '' && $user['lng'] != '') echo '<a title="'.$user['address'].'" target="_blank" href="http://maps.google.com/?q='.$user['lat'].','.$user['lng'].'"><i class="fa fa-map-marker"></i> '.$user['address'].'</a>';
						else echo $user['address']; ?>
                       </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Payment Method</label>
                        <div class="col-md-6 col-sm-6 col-xs-6" style="line-height: 34px;">
						<?=$user['payment_method']['method_name'] ?>	
                       </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">User Type</label>
                        <div class="col-md-6 col-sm-6 col-xs-6" style="line-height: 34px;">
						<?php if($user['device_type'] == 'A' && $user['device_token'] != '') echo "Android";
							  else if($user['device_type'] == 'I' && $user['device_token'] != '') echo "Iphone";
							  else '-';
						 ?>	
                       </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Account Detail</label>
                        <div class="col-md-6 col-sm-6 col-xs-6" style="line-height: 34px;">
						<?php if($user['bank_account'] == '') echo "<a target='_blank' href='".$this->Url->build(['controller'=>'BankAccounts','action'=>'manage',$user['id']])."'>Add Bank Account Detail</a>";
						else {
							echo "<a target='_blank' href='".$this->Url->build(['controller'=>'BankAccounts','action'=>'manage',$user['id']])."'><i class='fa fa-pencil'></i></a> Bank Name: ".$user['bank_account']['bank_name']."<br/>
							Account Number: ".$user['bank_account']['account_no']."<br/>
							IFSC Code: ".$user['bank_account']['ifsc_code']."<br/>
							Branch Name: ".$user['bank_account']['branch_name']."<br/>
							Branch Location: ".$user['bank_account']['branch_location']."<br/>
							";
						
						}
						?>	
                       
                       </div>
                      </div>
                      
                         <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Total Amount</label>
                        <div class="col-md-6 col-sm-6 col-xs-6" style="line-height: 34px;">
						<span class="btn btn-success"><?=$totalAmount." INR" ?>	</span>
                       </div>
                      </div>
                      <div style="max-height: 200px;overflow-y: auto;" class="w3-panel w3-note">
							<h4> Open Service Request : Total <?=count($open_bookings)?></h4>
						<table class="table" style="margin-bottom:0px;">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Booking ID </th>
                            <th class="column-title">Service Type</th>
                             <th class="column-title">Priority Type</th>
                            <th class="column-title">Service DateTime</th> 
                            <th class="column-title">Service Status</th> 
                            <th class="column-title">Book At</th> 
                            </th>
                          </tr>
                        </thead>
						<tbody>
							<?php foreach($open_bookings as $data){
								if(!empty($data['applied_jobs'])) $bg_color= '#ecfbea';
								else if($this->Utility->get_min( strtotime( $data['created']->format('Y-m-d H:i') ) ) > 30) $bg_color= '#f9e5e5';
								else $bg_color= '#f0f3d2';
							 ?>
							<tr style="background-color:<?=$bg_color; ?>" >
							<td><?php echo '<a target="_blank" data-toggle="tooltip"  title="View Detail" href="'.$this->Url->build(['controller'=>'Bookings','action'=>'detail',$data['id']]).'">'.$data['id'].'</a>'; ?>
							</td>
							<td><?= $data['category']['category_name']?></td>
							<td><?= ($data['priority_status']=='S')? 'Simple':'Ver Urgent';?></td>
							<td class=" "><?php echo date('M d, Y',strtotime($data['service_date']->format('Y-m-d')))." ".date('g:i A',strtotime($data['service_time']->format('H:i'))); ?></td>
							<td class=" "><?php echo "<span class='btn btn-xs btn-".$data['booking_status']['button']."'>".$data['booking_status']['booking_status']."</span>"?></td>
							<td><?php echo $this->Utility->timeFunc( strtotime( $data['created']->format('Y-m-d H:i') ) )?></td>
							</tr>
							
							<?php 	} ?>	

                         </tbody>
                      </table>
					  </div>
					  
					  
					   <div style="max-height: 200px;overflow-y: auto;" class="w3-panel w3-note">
							<h4> Close Service Request : Total <?=count($close_bookings)?></h4>
						<table class="table" style="margin-bottom:0px;">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Booking ID </th>
                            <th class="column-title">Service Type</th>
                             <th class="column-title">Priority Type</th>
                            <th class="column-title">Vistiting Amount</th> 
							<th class="column-title">Final Amount</th> 
							<th class="column-title">Admin commission%</th> 
							<th class="column-title">Serviceman Amount</th> 
							<th class="column-title">Serviceman Paid</th> 
                           
                            </th>
                          </tr>
                        </thead>
						<tbody>
							<?php foreach($close_bookings as $data){
								
							 ?>
							<tr  >
							<td><?php echo '<a target="_blank" data-toggle="tooltip"  title="View Detail" href="'.$this->Url->build(['controller'=>'Bookings','action'=>'detail',$data['id']]).'">'.$data['id'].'</a>'; ?>
							</td>
							<td><?= $data['category']['category_name']?></td>
							<td><?= ($data['priority_status']=='S')? 'Simple':'Ver Urgent';?></td>
							<td><?= $data['visiting_amount']." INR";?></td>
							<td><?= $data['final_amount']." INR";?></td>
							<td><?= $data['commision_percentage']."%";?></td>
							<td><?= $data['serviceman_amount']." INR";?></td>
							<td><?php if($data['serviceman_paid'] == 'Y') echo  "<i class='fa fa-check'></i>";
								else echo  "<i class='fa fa-close'></i>";?> </td>
							
							</tr>
							
							<?php 	} ?>	

                         </tbody>
                      </table>
					  </div>
					  
					  
                      
                      
                   
                     
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
 
