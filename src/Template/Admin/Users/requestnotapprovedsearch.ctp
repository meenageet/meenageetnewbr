                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">S.No. </th>
                            <th class="column-title">User Name </th>
                            <th class="column-title">Type </th>
                            <th class="column-title">Address</th> 
                            <th class="column-title">Id Proof </th>
                            <th class="column-title">Status </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
							<?php 
							$count = 1;
							
							foreach($RequestDjSingers->toArray() as $data){
								
							
								 ?>
							<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
								<td class=" "><?php 
								echo ucwords($data['user']['full_name']); ?></td>
								<td class=" ">
									<?php
									if($data['type'] == 'S'){
										echo "Singer";
									}else{
										echo "DJ";
									}
									 ?> 
								</td>
								<td class=" "><?php 
								echo $data['address'];?></td>
								
								<td class=" ">
									<?php if(isset($data['id_proof']) && $data['id_proof'] !=''){?>
										<img src = "<?php echo path_IDProof_image.$data['id_proof']; ?>" width = "50" height="40">
									<?php } ?>
								</td>
								
								<td class=" ">
									<input type="hidden" id="user_verified<?= $data['id'] ?>" value ="<?= $data['is_approved']; ?>" />
									<a href="javascript:void(0)" id="verified_id_<?= $data['id']; ?>" onclick="change_user_verified(<?php echo $data['id'] ?>)">
									<?php  if($data['is_approved'] == 'N'){
										echo '<button type="button" class="btn btn-success btn-xs" >Approved</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs" >Not Approved</button>';
									} ?></a>
								</td>
								<td class=" last">
									<a  title="Delete User"  href="#" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($RequestDjSingers->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php 
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                 
