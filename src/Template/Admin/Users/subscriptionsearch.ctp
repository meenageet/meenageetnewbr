                       <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							<th class="column-title">S.No. </th>
							<th class="column-title">User </th>
							<th class="column-title">Type </th>
							<th class="column-title">Point </th>
							<th class="column-title">Account Name </th>
							<th class="column-title">Paytm number </th>
							<th class="column-title">UPI id </th>
							<th class="column-title">Account Number </th>
							<th class="column-title">Bank Name </th>
							<th class="column-title">IFSC Code </th>
							<th class="column-title">Payment Status </th>
							<th class="column-title no-link last"><span class="nobr">Action</span></th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php //pr($PaymentDetails);die;
							$count = 1;
							
							foreach($PaymentDetails->toArray() as $data) {
						    ?>
								
							<tr id ="bank_row_<?= $data['id']; ?>" >
								
								<td><?= $count?>.</td>
								<td class=" "><?=$data['user']['full_name'] ?></td>
								
								<?php if($data['type'] == 'P') { ?>
									<td class=" "><?='Paytm'; ?></td>
								<?php } elseif($data['type'] == 'U') { ?>
									<td class=" "><?='UPI'; ?></td>
								<?php } elseif($data['type'] == 'B') { ?>
									<td class=" "><?='Bank'; ?></td>
								<?php } ?>
																
								<td><?= isset($data['point']) ? $data['point'] : '0' ;?>.</td>
								
								<?php if(!empty($data['account_name'])) { ?>
									<td class=" "><?=$data['account_name']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['paytm_number'])) { ?>
									<td class=" "><?=$data['paytm_number']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['upi_id'])) { ?>
									<td class=" "><?=$data['upi_id']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['account_number'])) { ?>
									<td class=" "><?=$data['account_number']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['bank_name'])) { ?>
									<td class=" "><?=$data['bank_name']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['ifsc_code'])) { ?>
									<td class=" "><?=$data['ifsc_code']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<td class=" ">
									<input type="hidden" id="bank_status_<?= $data['id'] ?>" value ="<?= $data['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_bank_status(<?php echo $data['id'] ?>)">
									<?php
									if($data['status'] == 'C'){
										echo '<button type="button" class="btn btn-success btn-xs">Complete</button>';
									}else if($data['status'] == 'P'){
										echo '<button type="button" class="btn btn-danger btn-xs">Pending</button>';
									} ?>
									</a>
								</td>
								
								<?php /*if($data['status'] == 'P') { ?>
									<td class=" "><?='Pending'; ?></td>
								<?php } elseif($data['status'] == 'C') { ?>
									<td class=" "><?='Complete'; ?></td>
								<?php } */?>
								
								<td class=" last">
									
									<input type="hidden" id="web_url_<?= $data['id'] ?>" value ="<?php echo $this->Url->build(['prefix'=>false,'controller'=>'PaymentDetails','action'=>'detail',$data['id']]); ?>" />
									
									<a href="javascript:void(0)" onclick="delete_bank(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
									</div>
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($PaymentDetails->toArray()) < 1) {
								echo "<tr><th colspan = '6'>No record found</th></tr>";
							} ?>	
							
							
                         </tbody>
                      </table>
                      <?php //$this->Paginator->options(array('url' => array('controller' => 'Users', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                 
