 <div class="right_col" role="main">
          <div class="">

            <div class="page-title">
				<?= $this->Flash->render() ?>
				<div class="title_left">
					<h3>
						Request List For Singer / DJ
						<small>
							Listing
						</small>
					</h3>
				</div>

				<div class="title_right">
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
						<div class="input-group">
							<div class="input text"><input type="text" id="search" placeholder="Search for..." onKeyup = "search_result();" class="form-control" name="key"></div>
							<span class="input-group-btn" style='display:none;'><button class="btn btn-default" type="button" onclick = "search_resudlt();">Go!</button></span>
						</div>
					</div>
				</div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
				<form method="post" class="form-horizontal form-label-left input_mask">

                    <div class="form-group">
						<div class="col-md-4 col-sm-4 col-xs-12">
                            <?php  echo $this->Form->input('title',array('placeholder'=>'User Name','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'text')); ?>
                        </div>
						<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
							<?php  
							$request_options = array('singer' => 'Singer', 'dj' => 'DJ');
							echo $this->Form->input('request_type',array('empty'=>'Select request','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>$request_options)); 
						  ?>
						</div>
                   </div>
                   
				   <div class="ln_solid" style="clear:both"></div>
				   <div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<a class="btn btn-primary" href="<?php echo $this->Url->build(['controller'=>'Users','action'=>'request_list']); ?>"> Clear </a>
							<button type="submit" class="btn btn-success">Filter</button>
						</div>
				   </div>
				</form>


              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">S.No. </th>
                            <th class="column-title">User Name </th>
                            <th class="column-title">Type </th>
                            <th class="column-title">Address</th> 
                            <th class="column-title">Id Proof </th>
                            <th class="column-title">Status </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
							<?php 
							$count = 1;
							
							foreach($RequestDjSingers->toArray() as $data){
								
							
								 ?>
							<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
								<td class=" "><?php 
								echo ucwords($data['user']['full_name']); ?></td>
								<td class=" ">
									<?php
									if($data['type'] == 'S'){
										echo "Singer";
									}else{
										echo "DJ";
									}
									 ?> 
								</td>
								<td class=" "><?php 
								echo $data['address'];?></td>
								
								<td class=" ">
									<?php if(isset($data['id_proof']) && $data['id_proof'] !=''){?>
										<img src = "<?php echo path_IDProof_image.$data['id_proof']; ?>" width = "50" height="40">
									<?php } ?>
								</td>
								
								<td class=" ">
									<input type="hidden" id="user_verified<?= $data['id'] ?>" value ="<?= $data['is_approved']; ?>" />
									<a href="javascript:void(0)" id="verified_id_<?= $data['id']; ?>" onclick="change_user_verified(<?php echo $data['id'] ?>)">
									<?php  if($data['is_approved'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs" >Approved</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs" >Not Approved</button>';
									} ?></a>
								</td>
								<td class=" last">
									<a  title="Delete User"  href="#" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($RequestDjSingers->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php $this->Paginator->options(array('url' => array('controller' => 'Users', 'action' => 'requestnotapprovedsearch')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<script>





function delete_user(id){
	bootbox.confirm("Are you sure?", function(result) {
		if(result == true){
			jQuery.ajax({ 
				//url: 'delete',
				url: '<?php echo $this->Url->build(['controller'=>'Users','action'=>'requestdelete']); ?>',
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#user_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record Delete successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}if(data == 'forbidden'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},
				error: function (request) {
					new PNotify({
							  title: 'Error',
							  text: 'This record is being referenced in other place. You cannot delete it.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
					
				},
			});
		}
	});
	
}
<!-- user varified function-->
function change_user_verified(id){
	var status = $("#user_verified"+id).val();
	if(status == 'Y'){
		
		var ques= "Do you want rejected of user request";
		var status = "N";
		var change = '<button type="button" class="btn btn-danger btn-xs" >Not Approved</button>';
	}else{
		var ques= "Do you want approved of user request";	
		var status = "Y";
		var change = '<button type="button" class="btn btn-success btn-xs" >Approved</button>';
		
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'Users','action'=>'verifiedrequest']); ?>',
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#verified_id_"+id).html(change);
						jQuery("#user_verified"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'User Request Status changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
		}
	});

	
}


function search_result(){
			
			var key = jQuery("#search").val();
			
			jQuery.ajax({ 
						url: '<?php echo $this->Url->build(['controller'=>'Users' , 'action'=>'requestsearch']);  ?>',
						data: {'key':key},
						type: 'POST',
						success: function(data) {
							if(data){
								
								jQuery('.table-responsive').html(data);
								
							}
						}
			});
			
		}
		
		jQuery('.table-responsive').on('click','.pagination li a',function(event){
			event.preventDefault() ;
			if($(this).parent().hasClass('active')){
				return false;
			}
			var keyy = jQuery('#search').val();
			var urli = jQuery(this).attr('href');
			jQuery.ajax({ 
						url: urli,
						data: {key:keyy},
						type: 'POST',
						success: function(data) {
							if(data){
								
								jQuery('.table-responsive').html(data);
								
							}
						}
			});
			
		});
		
</script>
