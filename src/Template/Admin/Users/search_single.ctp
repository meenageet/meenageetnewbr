                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">S.No. </th>
                            <th class="column-title">Name </th>
                            <th class="column-title">Country</th> 
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
							<?php 
							$count = 1;
							//echo '<pre>';print_r($Users);die;
							foreach($Users->toArray() as $data){
								  ?>
							<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
								<td class=" "><?php echo ucwords($data['full_name']); ?></td>
								<td class=" "><?php if(!empty($data['country'])) echo $data['country']['country_name']; else echo ''; ?> </td>
								<td class=" last">
									<a target="_blank" title="View Detail" href="<?php echo
									$this->Url->build(['controller'=>'Users','action'=>'edit',$data['id']]);
									; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit user</a>
									<a target="_blank" title="View Detail" href="<?php echo
									$this->Url->build(['controller'=>'Users','action'=>'edit_single',$data['id']]);
									; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit Detail</a>
									<?php if($data['single_registered'] == 'P'){ ?>
										<span id="delete_button_<?=$data['id']?>">
										<a   title="Make Online"  href="javascript:void();" onclick="make_online(<?= $data['id'] ?>)" class="btn btn-primary btn-xs"> Make Online</a>
										</span>
									<?php }else{ ?>
										<a  title="Delete User"  href="javascript:void();" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"> </i> Delete From single</a>
									
									<?php }?>
								
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Users->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php //$this->Paginator->options(array('url' => array('controller' => 'Users', 'action' => 'search_single')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                 
