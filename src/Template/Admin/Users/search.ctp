                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">S.No. </th>
                            <th class="column-title">Profile </th>
                            <th class="column-title">Name </th>
                            <th class="column-title">Email </th>
                            <th class="column-title">Phone Number</th> 
                            <th class="column-title">Refer Code</th> 
                            <th class="column-title">Status </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
							<?php 
							$count = 1;
							
							foreach($Users->toArray() as $data){
								
							
								 ?>
							<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
								<td class=" ">
									<?php
										if(isset($data['requestdjsinger']) ==''){
									?>
										<a href="<?php echo	$this->Url->build(['controller'=>'Users','action'=>'change_user_profile',$data['id']]);?>">
											<button type="button" class="btn btn-success btn-xs">Become Singer & DJ</button>
										</a>
									<?php	
										}else{
											if($data['requestdjsinger']['type'] == 'D'){
												echo 'Dj'; 
											}else{
												echo 'Singer'; 
											}
										}
									?>
								</td>
								<td class=" ">
									<?php 
										if($data['full_name'] !=''){
											echo ucwords($data['full_name']); 
										}else{
											echo "-----"; 
										}
										?>
								</td>
								<td class=" "><?= $data['email'] ? $data['email'] : '------'; ?> </td>
								<td class=" "><?php 
								$phoncode = ($data['country'] != '') ? "(".$data['country']['phone_code'].")": "";
								echo $phon =  $phoncode." ".$data['phone_number'];?></td>
								<td class=" "><?= $data['refer_code']; ?> </td>
								
								<td class=" ">
									<input type="hidden" id="user_status_<?= $data['id'] ?>" value ="<?= $data['enabled']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_user_status(<?php echo $data['id'] ?>)">
									<?php  if($data['enabled'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
								</td>
								<td class=" last">
									
									<a   title="Edit Detail"  href="<?php echo
									$this->Url->build(['controller'=>'Users','action'=>($data['access_level_id'] =='2')? 'edit' : 'serviceman_edit',$data['id']]);
									; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </a>
									<a  title="Delete User"  href="#" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
									
									<a   title="Subscription List"  href="<?php echo
									$this->Url->build(['controller'=>'Users','action'=>'subscription_list',$data['id']]);
									; ?>" class="btn btn-info btn-xs"><i class="fa fa-strikethrough"></i> </a>
									
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Users->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                 
