<div class="right_col" role="main">

	<div class="">
            
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
			 
					<div class="x_content">

						<?php echo $this->Form->create($users,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post'));?>
			  
							<?= $this->Flash->render() ?>
							<span class="section">Change App Use Time (In hours)</span>
							<?php $app_user_time_h = $usetime['app_use_time']/3600; ?>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Minimum App Use Time <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('app_use_time',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"number", 'value' => $app_user_time_h)); ?>
					 
								</div>
							</div>
				 
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
									<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
	
