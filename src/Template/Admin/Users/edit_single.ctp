<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>
<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create($user,array('class'=>'form-horizontal form-label-left','enctype'=>'multipart/form-data' ,'novalidate','method'=>'post'));
				  ?>
					<?= $this->Flash->render() ?>
                      <span class="section" style="text-align:center;">Edit Single</span>
                     <div class="item form-group"  >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Image<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<?php  echo $this->Form->input('image',array('class' => 'form-control col-md-7 col-xs-12','type'=>'file','label' =>false));
							if($user->image != '') echo '<img width="50px" src="'.$this->request->webroot.'uploads/user_images/'.$user->image.'"/>';
							 ?>
						</div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gender <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                       <?php 

								echo $this->Form->radio(
									'gender',
									[
										['value' => 'M', 'text' => 'Male','class'=>'business_type','checked'=>'checked'],
										['value' => 'F', 'text' => 'Female'],
										
									]
								);
							?>
                       </div>
                      </div>
                    
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">About Me <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<?php  echo $this->Form->input('about_me',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text"));
							?>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Looking for <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							
						 <?php  echo $this->Form->input('looking_for',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"textarea"));
						
						  ?>
                         </div>
                      </div>
                 
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Date of Birth <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							 <?php  echo $this->Form->input('dob',array('readonly','class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                           
                        </div>
                      </div>
                     <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
         
          </div>
        </div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(document).ready(function() {
		   $('#dob').datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0",
				dateFormat: 'yy-mm-dd',
				maxDate: '0', 
			})
	}) ;
	</script>
