<style>
	#point_redeem{display:none;}
	#submit_redeem{display:none;}
	#output{color:red;font-size:18px;}
</style>
<div class="right_col" role="main">
          <div class="">
			
            <div class="page-title">
				<?= $this->Flash->render() ?>
              <div class="title_left">
                <h3>
                    Top Winner Challenges  
                      <small>
                         Listing
                      </small>
                  </h3>
              </div>

              </div>

            <div class="clearfix"></div>
            
            <div class="row">
				
				<form method="post" class="form-horizontal form-label-left input_mask">

                    <div class="form-group">
						<div class="col-md-4 col-sm-4 col-xs-12">
                            <?php  echo $this->Form->input('user_id',array('empty'=>'Select user','class' => 'form-control 	col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>$users)); 
						  ?>
                        </div>
                   </div>
				   <div class="ln_solid" style="clear:both"></div>
				   <div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<a class="btn btn-primary" href="<?php echo $this->Url->build(['controller'=>'Challenges','action'=>'winnerChallenge']); ?>"> Clear </a>
							<button type="submit" class="btn btn-success">Filter</button>
						</div>
				   </div>
				</form>
			 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
						
					<table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							
							<th class="column-title"><input type="checkbox" name="ChallengeWinners" id="select_all"> </th>
							<th class="column-title">S.No. </th>
							<th class="column-title">User Name </th>
							<th class="column-title">Song Name </th>
							<th class="column-title">Details </th>
							<th class="column-title">Created </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php // echo "<pre>"; print_r($Challenges->toArray());die;
							$count = 1;
							
							foreach($Challenges->toArray() as $data){
							
						    ?>
								
							<tr id ="bank_row_<?= $data['id']; ?>" >
								<td>
									<?= $this->Form->checkbox('id[].', array(
										'value' => $data['user']['id'], 
										'name' => 'ChallengeWinners[id][]', 
										'class' => 'checkbox', 
										'checked' => false, 
										'hiddenField' => false
									)) ?>
									</td>
								<td><?= $count?>.</td>
								<td class=" "><?=$data['user']['full_name'] ?></td>
								<td class=" ">
									<?php
										if($data['challenge']['user1']['id'] == $data['user']['id']){
											echo $data['challenge']['song']['title'] ;
										}else{
											echo $data['challenge']['acceptusersong']['title'] ;
										}
									?>
								</td>
								<td class="last">
									<a class="btn btn-info btn-xs" target="_blank" href="<?php echo $this->Url->build(['controller'=>'Challenges','action'=>'winner_detail',$data['challenge_id']]); ?>"><i class="fa fa-eye"></i> View </a>
								</td>
								<td class="last">
									<?php echo date('Y-m-d',strtotime($data['created']));?>
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Challenges->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php $this->Paginator->options(array('url' => array('controller' => 'Challenges', 'action' => 'winnersearch')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                    </div>
                    <!--------------------------- redeem div ------------------->
                    <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<button type="button" id="point_redeem_btn" class="btn btn-success">Redeem Points</button>
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<form method="post" class="form-horizontal form-label-left input_mask" id="point_rdm_form">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<?php echo $this->Form->input('point_redeem', array('placeholder' => 'Point Redeem', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'text', 'id' => 'point_redeem', 'required' => true)); ?>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<button type="submit" id ="submit_redeem" class="btn btn-success">Submit</button>
								</div>
								<?php echo $this->Form->input('point_redeem_val', array('label' => false, 'type' => 'hidden', 'id' => 'point_redeem_val')); ?>
							</form>
							<div id="output"></div>
							<div id="redeem_message"></div>
						</div>
					</div>
				
                    <!--------------------------- redeem div ------------------->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
         <?php
		echo $this->Html->script(
				array(
					'Admin/moment.min.js',
					'Admin/daterangepicker.js',
					
				));
				echo $this->fetch('script');
	?>
<script>
$('#point_redeem_btn').click(function(){
	//if($('.checkbox').prop("checked") == true) {
	if($('.checkbox').is(":checked")){
		$('#point_redeem').show();
		$('#submit_redeem').show();
	} else {
		new PNotify({
			title: 'Error',
			text: 'please select atleast one checkbox.',
			type: 'error',
			styling: 'bootstrap3',
			delay:1200
		});
	}
});

$("#point_redeem").focusout(function() {
    $('#point_redeem_val').val($("#point_redeem").val());
});
	
//select all checkboxes
$("#select_all").change(function(){  //"select all" change 
	var status = this.checked; // "select all" checked status
	$('.checkbox').each(function(){ //iterate all listed checkbox items
		this.checked = status; //change ".checkbox" checked status
	});
});

$('.checkbox').change(function(){ //".checkbox" change 
	//uncheck "select all", if one of the listed checkbox item is unchecked
	if(this.checked == false){ //if this item is unchecked
		$("#select_all")[0].checked = false; //change "select all" checked status to false
	}
	
	//check "select all" if all checkbox items are checked
	if ($('.checkbox:checked').length == $('.checkbox').length ){ 
		$("#select_all")[0].checked = true; //change "select all" checked status to true
	}
});	
	
	
jQuery(document).ready(function() {

	jQuery('#submit_redeem').click(function(event) {  

		if(jQuery('#point_redeem').val() == '') {
			new PNotify({
				title: 'Error',
				text: 'Please enter points to redeem.',
				type: 'error',
				styling: 'bootstrap3',
				delay:1200
			});
			return false;
		}
		
		if(jQuery('.checkbox').is(":checked")) {
			
			var memoData = [];
			jQuery.each(jQuery("input[name='ChallengeWinners[id][]']:checked"), function(){            
				memoData.push(jQuery(this).val());
			});
			var points = jQuery('#point_redeem_val').val();
			
			jQuery.ajax({ 
				type: 'POST', 
				url: '<?php echo $this->Url->build(['controller'=>'challenges','action'=>'pointredeem']); ?>',
				data: {'checkbox_id': memoData, 'points':points}, 

				success : function(data) {
					if(data == 1){
						new PNotify({
							  title: 'Success',
							  text: 'Points has been redeemed to selected users successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						  jQuery('input:checkbox').removeAttr('checked');
						  jQuery('#point_redeem_val').val('');
						  location.reload();
					}
				},
				error : function() {
					new PNotify({
						title: 'Error',
						text: 'There is an error in point redeem.',
						type: 'error',
						styling: 'bootstrap3',
						delay:1200
					});
				}
			});
		} else {
			new PNotify({
				title: 'Error',
				text: 'please select atleast one checkbox.',
				type: 'error',
				styling: 'bootstrap3',
				delay:1200
			});
			//alert("please select atleast one checkbox.");
		}
		return false;
	}); //end of button click

}); 	
$(document).ready(function() {
        
		$('#start-date').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			format: 'YYYY-MM-DD',
			maxDate:0

		});
		$('#end-date').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			format: 'YYYY-MM-DD',
			maxDate:0

		});

});

jQuery('.table-responsive').on('click','.pagination li a',function(event){
	event.preventDefault() ;
	if($(this).parent().hasClass('active')){
		return false;
	}
	var keyy = $('form').serialize();
	var urli = jQuery(this).attr('href');
	jQuery.ajax({ 
				url: urli,
				data: {key:keyy},
				type: 'POST',
				success: function(data) {
					if(data){
						
						jQuery('.table-responsive').html(data);
						
					}
				}
	});
	
});
</script>
