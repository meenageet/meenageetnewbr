<style>
.text-padding{padding-top: 7px;}
.w3-panel{
    padding: 0.01em 16px;
    margin-top: 16px!important;
    margin-bottom: 16px!important;
}.w3-note {
    background-color: #fcf8e3;
    border-left: 6px solid #faebcc;
    color:#8a6d3b;
}.w4-note {
    background-color: #f2dede;
    border-left: 6px solid #ebccd1;
    color:#a94442;
}
</style>
<div class="right_col" role="main">

	<div class="">
            
		<div class="clearfix"></div>

			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_content form-horizontal">
							<span class="section">View Challenge User</span>
						
								<div class="row">
									<!-- user one row -->
									<div class="col-md-6 col-sm-6 col-xs-6">
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">User Name : </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?=$data['user1']['full_name'] ?>
												</div>
											</div>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Challenge Image : </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php if(!empty($data['image'])){
														echo '<img src="'.path_song_upload_image.$data['image'].'" width="100px">';
													}else{ echo "No Image";}?>
												</div>
											</div>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Song Name : </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?=$data['song']['title'] ?>
												</div>
											</div>
											
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Total Like : </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?=$total_challenge_likes1 ?>
												</div>
											</div>
											
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Total DisLike : </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?=$total_challenge_unlikes1 ?>
												</div>
											</div>
									</div>
									<!-- end user one row -->
									
									<!-- user two row -->
									<div class="col-md-6 col-sm-12 col-xs-6">
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">User Name : </label>
											<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
												<?=$data['user2']['full_name'] ?>
											</div>
										</div>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Challenge Image : </label>
											<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
												<?php if(!empty($data['accepted_user_image'])){
													echo '<img src="'.path_song_upload_image.$data['accepted_user_image'].'" width="100px">';
												}else{ echo "No Image";}?>
											</div>
										</div>
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Song Name : </label>
											<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
												<?=$data['acceptusersong']['title'] ?>
											</div>
										</div>
										
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Total Like : </label>
											<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
												<?=$total_challenge_likes2 ?>
											</div>
										</div>
										
										<div class="item form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Total DisLike : </label>
											<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
												<?=$total_challenge_unlikes2 ?>
											</div>
										</div>
									</div>
									<!-- end user two row -->
								</div>
								
								<!--- winner row -->
								<!-- user one row -->
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<span class="section">Winner Challenge Date Time</span>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Challenge Status : </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php
														if($data['status'] == '0'){
															echo 'Pending';
														}else if($data['status'] == '1'){
															echo 'Accepted , now this challenge is LIVE';
														}else if($data['status'] == '2'){
															echo 'Rejected';
														}else if($data['status'] == '3'){
															echo 'Completed';
														} 
													?>
												</div>
											</div>
											
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Total time : </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php 
													$init = $data['challenge_time'];
													$hours = floor($init / 3600).' hours';
													$minutes = floor(($init / 60) % 60).' minutes';
													$seconds = $init % 60 .' seconds';
													echo "$hours:$minutes:$seconds";
													
													?>
												</div>
											</div>
											
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Remaining time : </label>
												<div class="col-md-6 col-sm-6 col-xs-12 text-padding">
													<?php 
													echo $remaining_time;
													
													?>
												</div>
											</div>
											
									</div>
								<!-- end user one row -->
								</div>
								
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
        <!-- /page content -->
