                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							
							<th class="column-title"><input type="checkbox" name="ChallengeWinners" id="select_all"> </th>
							<th class="column-title">S.No. </th>
							<th class="column-title">User Name </th>
							<th class="column-title">Song Name </th>
							<th class="column-title">Details </th>
							<th class="column-title">Created </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php // echo "<pre>"; print_r($Challenges->toArray());die;
							$count = 1;
							
							foreach($Challenges->toArray() as $data){
							
						    ?>
								
							<tr id ="bank_row_<?= $data['id']; ?>" >
								<td>
									<?= $this->Form->checkbox('id[].', array(
										'value' => $data['user']['id'], 
										'name' => 'ChallengeWinners[id][]', 
										'class' => 'checkbox', 
										'checked' => false, 
										'hiddenField' => false
									)) ?>
									</td>
								<td><?= $count?>.</td>
								<td class=" "><?=$data['user']['full_name'] ?></td>
								<td class=" ">
									<?php
										if($data['challenge']['user1']['id'] == $data['user']['id']){
											echo $data['challenge']['song']['title'] ;
										}else{
											echo $data['challenge']['acceptusersong']['title'] ;
										}
									?>
								</td>
								<td class="last">
									<a class="btn btn-info btn-xs" target="_blank" href="<?php echo $this->Url->build(['controller'=>'Challenges','action'=>'winner_detail',$data['challenge_id']]); ?>"><i class="fa fa-eye"></i> View </a>
								</td>
								<td class="last">
									<?php echo date('Y-m-d',strtotime($data['created']));?>
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Challenges->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php //$this->Paginator->options(array('url' => array('controller' => 'Users', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                 
