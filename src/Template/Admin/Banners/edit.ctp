<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create($banner,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
					<?= $this->Flash->render() ?>
                      <span class="section">Edit Banner</span>
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Banner Text <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('text',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>

                      </div>

					 
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Banner Image  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							
						 <?php  
						  if($banner['image'] != ''){
							echo $this->Html->image('/webroot/uploads/banner_app/'.$banner['image'],['style'=>'width:100%;'] );

						  }
						 echo $this->Form->input('image',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"file")); ?>
                         
                        </div>

                      </div>
                      <span style="color: red;margin-left: 266px;">Banner image should be height/width=.40 to .42 ratio.</span>
                   
                     
					    
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
	
