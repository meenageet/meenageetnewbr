<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create($state,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
					<?= $this->Flash->render() ?>
                      <span class="section">Edit State</span>
					    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Select Country <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<?php  echo $this->Form->input('country_id',array( 'empty' => 'Select Country','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>$country,'onchange'=>'get_attribute(this.value)')); ?>
                        </div>
                      </div>
                      
					
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">State Name<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('state_name',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
                     
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
	
<script>
function get_attribute(val){
	
	if(val != ''){
		$.ajax({ 
			//url: "get_network",
			url: '<?php echo $this->Url->build(['controller'=>'BusCatSubAttributes','action'=>'attributes']); ?>',
			data: {val: val},
			type: "POST",
			success: function(output) {
				
					$(".option_cat").html(output);
			}
		});
	}else{
		$(".option_cat").html('<option value = "">select business category attribute</option>');
	}

}
</script>
