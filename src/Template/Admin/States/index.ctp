<div class="right_col" role="main">
    <div class="">
		<div class="title_middle">
		</div>
        <div class="page-title">
			<?= $this->Flash->render() ?>
          	<div class="title_left">
            	<h3>
                	States
                  <small>
                     Listing
                  </small>
              	</h3>
          	</div>

          	<div class="title_right">			 
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">              
            	</div>
			
          	</div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  	<div id="divLoading"> </div><!--Loading class -->
                  	<div class="x_content">

	                    <div class="table-responsive">
		                    <table class="table table-striped jambo_table bulk_action" id = "datatable">
		                        <thead>
		                          <tr class="headings">
									<th class="column-title">S.No. </th>
									<th class="column-title">Country  </th>
									<th class="column-title">State Name</th>
									<th class="column-title">Status</th>
									<th class="column-title no-link last"><span class="nobr">Action</span></th>
		                          </tr>
		                        </thead>

		                        <tbody>
									<?php 
									$count = 1;
									foreach($States->toArray() as $data){
										 ?>
									<tr id ="bank_row_<?= $data['id']; ?>">
										<td><?= $count?>.</td>
										<td class=" "><?php 
										echo ucwords($data['country']['country_name']) ?></td>
										<td class=" "><?= $data['state_name']; ?> </td>
										
										
										<td class=" ">
											<input type="hidden" id="bank_status_<?= $data['id'] ?>" value ="<?= $data['enabled']; ?>" />
											<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_bank_status(<?php echo $data['id'] ?>)">
											<?php  if($data['enabled'] == 'Y'){
												echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
											}else{
												echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
											} ?></a>
										</td>
										<td class=" last">
											<a class="btn btn-info btn-xs" href="<?php echo $this->Url->build(['controller'=>'States','action'=>'edit',$data['id']]); ?>"><i class="fa fa-pencil"></i> Edit </a>
											<a href="#" onclick="delete_bank(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
										</td>
									</tr>
									
									<?php  $count++;
									}
									 ?> 
									
									<?php  if(count($States->toArray()) < 1) {
												echo "<tr><th colspan = '6'>No record found</th></tr>";
										   } ?>	
		                        </tbody>
	                    	</table>	                      	
	                    </div>
                  	</div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->scriptStart(['block'=>true]); ?>                   

function delete_bank(id){
	bootbox.confirm("Do you want to delete the detail ?", function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'States','action'=>'delete']); ?>',
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#bank_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record Delete successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},
				error:function(data){
					new PNotify({
							  title: 'Error',
							  text: 'This record is being referenced in other place. You cannot delete it.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
				}
			});
		}
	});
	
}

function change_bank_status(id){
	var status = $("#bank_status_"+id).val();
	if(status == 'Y'){
		var ques= "Do you want change the status to DEACTIVE";	
		var status = "N";
		var change = '<button type="button" class="btn btn-danger btn-xs">Deactive</button>'
	}else{
		var ques= "Do you want change the status to ACTIVE";
		var status = "Y";
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>';
	}
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'States','action'=>'status']); ?>',
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#status_id_"+id).html(change);
						jQuery("#bank_status_"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'Status changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},
				error: function (request) {
					new PNotify({
							  title: 'Error',
							  text: 'Invalid request!',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
					
				},
			});
		}
	});	
}

<?php echo $this->Html->scriptEnd(); ?>           
