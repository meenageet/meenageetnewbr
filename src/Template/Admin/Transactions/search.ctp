                       <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							<th class="column-title">S.No. </th>
							<th class="column-title">User </th>
							<th class="column-title">Type </th>
							<th class="column-title">Transaction Id </th>
							<th class="column-title">Order Id </th>
							<th class="column-title">Amount </th>
							<th class="column-title">Transaction Date </th>
							<th class="column-title">Payment Status </th>
							<th class="column-title no-link last"><span class="nobr">Action</span></th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php //pr($PaymentDetails);die;
							$count = 1;
							
							foreach($Transactions->toArray() as $data) {
						    ?>
								
							<tr id ="bank_row_<?= $data['id']; ?>" >
								
								<td><?= $count?>.</td>
								<td class=" "><?=$data['user']['full_name'] ?></td>
								
								<?php if($data['type'] == 'C') { ?>
									<td class=" "><?='Challenge'; ?></td>
								<?php } elseif($data['type'] == 'U') { ?>
									<td class=" "><?='Customer'; ?></td>
								<?php }?>
																
								<td><?= isset($data['txnid']) ? $data['txnid'] : '0' ;?>.</td>
								<td><?= isset($data['orderid']) ? $data['orderid'] : '0' ;?>.</td>
								
								<?php if(!empty($data['txnamount'])) { ?>
									<td class=" "><?=$data['txnamount']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['txndate'])) { ?>
									<td class=" "><?= date('Y-m-d',strtotime($data['txndate'])); ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if($data['status'] == 'F') { ?>
									<td class=" "><?='Fail'; ?></td>
								<?php } elseif($data['status'] == 'S') { ?>
									<td class=" "><?='Success'; ?></td>
								<?php }?>
								
								<td class=" ">
									<a href="javascript:void(0)" onclick="delete_bank(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
									</div>
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Transactions->toArray()) < 1) {
								echo "<tr><th colspan = '6'>No record found</th></tr>";
							} ?>	
							
							
                         </tbody>
                      </table>
                      <?php //$this->Paginator->options(array('url' => array('controller' => 'Users', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                 
