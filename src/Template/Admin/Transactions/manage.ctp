<div class="right_col" role="main">
	<div class="">
			
		<div class="page-title">
			<?= $this->Flash->render() ?>
			<div class="title_left">
				<h3>
					Transaction Details
					<small>
						Listing
					</small>
				</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
				
			<form method="post" class="form-horizontal form-label-left input_mask">

				<div class="form-group">
					<!--<div class="col-md-4 col-sm-4 col-xs-12">
						<?php //echo $this->Form->input('title', array('placeholder' => 'Post title', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'text')); ?>
					</div>-->
					<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
						<?php echo $this->Form->input('user_id', array('empty' => 'Select user', 'class' => 'form-control 	col-md-7 col-xs-12', 'label' => false, 'type' => 'select', 'options' => $users)); ?>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
						<?php  
						$payment_options = array('c' => 'Challenge', 'u' => 'Customer');
						echo $this->Form->input('user_status',array('empty'=>'Select User Type','class' => 'form-control	col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>$payment_options)); 
					  ?>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
						<?php  
						$payment_types = array('s' => 'Success', 'f' => 'Fail');
						echo $this->Form->input('payment_type',array('empty'=>'Select Payment Type','class' => 'form-control	col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>$payment_types)); 
					  ?>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
						<?php echo $this->Form->input('from_date', array('placeholder' => 'From date', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'text', 'id' => 'start-date' ,'readonly')); ?>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
						<?php echo $this->Form->input('to_date', array('placeholder' => 'To date', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'text', 'id' => 'end-date' ,'readonly')); ?>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
						<?php  
						$export_types = array('c' => 'CSV File');
						echo $this->Form->input('export_type',array('empty'=>'Select Export Type','class' => 'form-control	col-md-7 col-xs-12 export_type','label' =>false,'type'=>'select','options'=>$export_types)); 
					  ?>
					</div>
			   </div>
			   <div class="ln_solid" style="clear:both"></div>
			   <div class="form-group">
					<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
						<a class="btn btn-primary" href="<?php echo $this->Url->build(['controller'=>'PaymentDetails','action'=>'manage']); ?>"> Clear </a>
						<button type="submit" class="btn btn-success">Filter</button>
						<button type="submit" class="btn btn-success inputDisabled" disabled>Export</button>
					</div>
			   </div>
			</form>
			 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
						
					<table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							<th class="column-title">S.No. </th>
							<th class="column-title">User </th>
							<th class="column-title">Type </th>
							<th class="column-title">Transaction Id </th>
							<th class="column-title">Order Id </th>
							<th class="column-title">Amount </th>
							<th class="column-title">Transaction Date </th>
							<th class="column-title">Payment Status </th>
							<th class="column-title no-link last"><span class="nobr">Action</span></th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php //pr($PaymentDetails);die;
							$count = 1;
							
							foreach($Transactions->toArray() as $data) {
						    ?>
								
							<tr id ="bank_row_<?= $data['id']; ?>" >
								
								<td><?= $count?>.</td>
								<td class=" "><?=$data['user']['full_name'] ?></td>
								
								<?php if($data['type'] == 'C') { ?>
									<td class=" "><?='Challenge'; ?></td>
								<?php } elseif($data['type'] == 'U') { ?>
									<td class=" "><?='Customer'; ?></td>
								<?php }?>
																
								<td><?= isset($data['txnid']) ? $data['txnid'] : '0' ;?>.</td>
								<td><?= isset($data['orderid']) ? $data['orderid'] : '0' ;?>.</td>
								
								<?php if(!empty($data['txnamount'])) { ?>
									<td class=" "><?=$data['txnamount']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['txndate'])) { ?>
									<td class=" "><?= date('Y-m-d',strtotime($data['txndate'])); ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if($data['status'] == 'F') { ?>
									<td class=" "><?='Fail'; ?></td>
								<?php } elseif($data['status'] == 'S') { ?>
									<td class=" "><?='Success'; ?></td>
								<?php }?>
								
								<td class=" ">
									<a href="javascript:void(0)" onclick="delete_bank(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
									</div>
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Transactions->toArray()) < 1) {
								echo "<tr><th colspan = '6'>No record found</th></tr>";
							} ?>	
							
							
                         </tbody>
                      </table>
                      <?php $this->Paginator->options(array('url' => array('controller' => 'PaymentDetails', 'action' => 'search')));
						echo "<div class='pagination' style = 'float:right'>";
	 
						// the 'first' page button
						$paginator = $this->Paginator;
						echo $paginator->first("First");

						// 'prev' page button, 
						// we can check using the paginator hasPrev() method if there's a previous page
						// save with the 'next' page button
						if($paginator->hasPrev()){
						echo $paginator->prev("Prev");
						}

						// the 'number' page buttons
						echo $paginator->numbers(array('modulus' => 2));

						// for the 'next' button
						if($paginator->hasNext()){
						echo $paginator->next("Next");
						}

						// the 'last' page button
						echo $paginator->last("Last");

						echo "</div>";
								
						?>
						
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->script(
	array(
		'Admin/moment.min.js',
		'Admin/daterangepicker.js'
	));
	echo $this->fetch('script');
?>
<script> 	
	
$(document).ready(function() {
        
	$('#start-date').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		format: 'YYYY-MM-DD',
		maxDate:0

	});
	$('#end-date').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		format: 'YYYY-MM-DD',
		maxDate:0

	});
	
	$(".export_type").click(function(){
	   event.preventDefault();
		$('.inputDisabled').removeAttr("disabled");
	});
	
});

function delete_bank(id){
	bootbox.confirm("Do you want to delete the detail ?", function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'PaymentDetails','action'=>'delete']); ?>',
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#bank_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record Delete successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},error: function (request) {
					new PNotify({
						title: 'Error',
						text: 'This record is being referenced in other place. You cannot delete it.',
						type: 'error',
						styling: 'bootstrap3',
						delay:1200
					});
				},
			});
		}
	});
}
function change_bank_status(id){
	var status = $("#bank_status_"+id).val();
	if(status == 'P'){
		var ques= "Do you want change the status to COMPLETE";	
		var status = "C";
		var change = '<button type="button" class="btn btn-success btn-xs">Complete</button>'
	}else{
		var ques= "Do you want change the status to PENDING";
		var status = "P";
		var change = '<button type="button" class="btn btn-danger btn-xs">Pending</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'PaymentDetails','action'=>'status']); ?>',
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						if(status =='P'){
							
							$("#webpage"+id).html('<a target="_blank" class="btn btn-info btn-xs" href="'+$("#web_url_"+id).val()+'"><i class="fa fa-globe"></i> Webpage </a>');
						}else{
							$("#webpage"+id).html('');
						}
						jQuery("#status_id_"+id).html(change);
						jQuery("#bank_status_"+id).val(status);
						new PNotify({
							title: 'Success',
							text: 'Status changed successfully!',
							type: 'success',
							styling: 'bootstrap3',
							delay:1200
						});
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							title: '403 Error',
							text: 'You donot have permission to access this action.',
							type: 'error',
							styling: 'bootstrap3',
							delay:1200
						});
					}
				},
				error: function (request) {
					new PNotify({
						title: 'Error',
						text: 'Invalid request!',
						type: 'error',
						styling: 'bootstrap3',
						delay:1200
					});
				},
			});
		}
	});

}
	
jQuery('.table-responsive').on('click','.pagination li a',function(event){
	event.preventDefault() ;
	if($(this).parent().hasClass('active')){
		return false;
	}
	var keyy = $('form').serialize();
	var urli = jQuery(this).attr('href');
	jQuery.ajax({ 
		url: urli,
		data: {key:keyy},
		type: 'POST',
		success: function(data) {
			if(data){
				jQuery('.table-responsive').html(data);
			}
		}
	});
});
		
</script>
