<div class="right_col" role="main">
	<div class="">
			
		<div class="page-title">
			<?= $this->Flash->render() ?>
			<div class="title_left">
				<h3>
					Payment Details
					<small>
						Listing
					</small>
				</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
				
			<form method="post" class="form-horizontal form-label-left input_mask">

				<div class="form-group">
					<!--<div class="col-md-4 col-sm-4 col-xs-12">
						<?php //echo $this->Form->input('title', array('placeholder' => 'Post title', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'text')); ?>
					</div>-->
					<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
						<?php echo $this->Form->input('user_id', array('empty' => 'Select user', 'class' => 'form-control 	col-md-7 col-xs-12', 'label' => false, 'type' => 'select', 'options' => $users)); ?>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
						<?php  
						$payment_options = array('pending' => 'Pending', 'complete' => 'Complete');
						echo $this->Form->input('payment_status',array('empty'=>'Select Payment Status','class' => 'form-control	col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>$payment_options)); 
					  ?>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
						<?php  
						$payment_types = array('paytm' => 'Paytm', 'upi' => 'UPI', 'bank' => 'Bank');
						echo $this->Form->input('payment_type',array('empty'=>'Select Payment Type','class' => 'form-control	col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>$payment_types)); 
					  ?>
					</div>
			   </div>
			   <div class="ln_solid" style="clear:both"></div>
			   <div class="form-group">
					<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
						<a class="btn btn-primary" href="<?php echo $this->Url->build(['controller'=>'PaymentDetails','action'=>'manage']); ?>"> Clear </a>
						<button type="submit" class="btn btn-success">Filter</button>
					</div>
			   </div>
			</form>
			 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
						
					<table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							<th class="column-title">S.No. </th>
							<th class="column-title">User </th>
							<th class="column-title">Type </th>
							<th class="column-title">Point </th>
							<th class="column-title">Account Name </th>
							<th class="column-title">Paytm number </th>
							<th class="column-title">UPI id </th>
							<th class="column-title">Account Number </th>
							<th class="column-title">Bank Name </th>
							<th class="column-title">IFSC Code </th>
							<th class="column-title">Payment Status </th>
							<th class="column-title no-link last"><span class="nobr">Action</span></th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php //pr($PaymentDetails);die;
							$count = 1;
							
							foreach($PaymentDetails->toArray() as $data) {
						    ?>
								
							<tr id ="bank_row_<?= $data['id']; ?>" >
								
								<td><?= $count?>.</td>
								<td class=" "><a target="_blank" href="<?php echo $this->Url->build(['controller'=>'users','action'=>'edit',$data['user_id']]); ?>"  class="btn btn-info btn-xs"><?=$data['user']['full_name'] ?></a></td>
								
								<?php if($data['type'] == 'P') { ?>
									<td class=" "><?='Paytm'; ?></td>
								<?php } elseif($data['type'] == 'U') { ?>
									<td class=" "><?='UPI'; ?></td>
								<?php } elseif($data['type'] == 'B') { ?>
									<td class=" "><?='Bank'; ?></td>
								<?php } ?>
																
								<td><?= isset($data['point']) ? $data['point'] : '0' ;?>.</td>
								
								<?php if(!empty($data['account_name'])) { ?>
									<td class=" "><?=$data['account_name']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['paytm_number'])) { ?>
									<td class=" "><?=$data['paytm_number']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['upi_id'])) { ?>
									<td class=" "><?=$data['upi_id']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['account_number'])) { ?>
									<td class=" "><?=$data['account_number']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['bank_name'])) { ?>
									<td class=" "><?=$data['bank_name']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<?php if(!empty($data['ifsc_code'])) { ?>
									<td class=" "><?=$data['ifsc_code']; ?></td>
								<?php } else { ?>
										<td class=" "><?='--'; ?></td>
								<?php } ?>
								
								<td class=" ">
									<input type="hidden" id="bank_status_<?= $data['id'] ?>" value ="<?= $data['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_bank_status(<?php echo $data['id'] ?>)">
									<?php
									if($data['status'] == 'C'){
										echo '<button type="button" class="btn btn-success btn-xs">Complete</button>';
									}else if($data['status'] == 'P'){
										echo '<button type="button" class="btn btn-danger btn-xs">Pending</button>';
									} ?>
									</a>
								</td>
								
								<?php /*if($data['status'] == 'P') { ?>
									<td class=" "><?='Pending'; ?></td>
								<?php } elseif($data['status'] == 'C') { ?>
									<td class=" "><?='Complete'; ?></td>
								<?php } */?>
								
								<td class=" last">
									
									<input type="hidden" id="web_url_<?= $data['id'] ?>" value ="<?php echo $this->Url->build(['prefix'=>false,'controller'=>'PaymentDetails','action'=>'detail',$data['id']]); ?>" />
									
									<a href="javascript:void(0)" onclick="delete_bank(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
									</div>
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($PaymentDetails->toArray()) < 1) {
								echo "<tr><th colspan = '6'>No record found</th></tr>";
							} ?>	
							
							
                         </tbody>
                      </table>
                      <?php $this->Paginator->options(array('url' => array('controller' => 'PaymentDetails', 'action' => 'search')));
						echo "<div class='pagination' style = 'float:right'>";
	 
						// the 'first' page button
						$paginator = $this->Paginator;
						echo $paginator->first("First");

						// 'prev' page button, 
						// we can check using the paginator hasPrev() method if there's a previous page
						// save with the 'next' page button
						if($paginator->hasPrev()){
						echo $paginator->prev("Prev");
						}

						// the 'number' page buttons
						echo $paginator->numbers(array('modulus' => 2));

						// for the 'next' button
						if($paginator->hasNext()){
						echo $paginator->next("Next");
						}

						// the 'last' page button
						echo $paginator->last("Last");

						echo "</div>";
								
						?>
						
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->script(
	array(
		'Admin/moment.min.js',
		'Admin/daterangepicker.js'
	));
	echo $this->fetch('script');
?>
<script> 	
	
$(document).ready(function() {
        
	$('#start-date').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		format: 'YYYY-MM-DD',
		maxDate:0

	});
	$('#end-date').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		format: 'YYYY-MM-DD',
		maxDate:0

	});
});

function delete_bank(id){
	bootbox.confirm("Do you want to delete the detail ?", function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'PaymentDetails','action'=>'delete']); ?>',
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#bank_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record Delete successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},error: function (request) {
					new PNotify({
						title: 'Error',
						text: 'This record is being referenced in other place. You cannot delete it.',
						type: 'error',
						styling: 'bootstrap3',
						delay:1200
					});
				},
			});
		}
	});
}
function change_bank_status(id){
	var status = $("#bank_status_"+id).val();
	if(status == 'P'){
		var ques= "Do you want change the status to COMPLETE";	
		var status = "C";
		var change = '<button type="button" class="btn btn-success btn-xs">Complete</button>'
	}else{
		var ques= "Do you want change the status to PENDING";
		var status = "P";
		var change = '<button type="button" class="btn btn-danger btn-xs">Pending</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'PaymentDetails','action'=>'status']); ?>',
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						if(status =='P'){
							
							$("#webpage"+id).html('<a target="_blank" class="btn btn-info btn-xs" href="'+$("#web_url_"+id).val()+'"><i class="fa fa-globe"></i> Webpage </a>');
						}else{
							$("#webpage"+id).html('');
						}
						jQuery("#status_id_"+id).html(change);
						jQuery("#bank_status_"+id).val(status);
						new PNotify({
							title: 'Success',
							text: 'Status changed successfully!',
							type: 'success',
							styling: 'bootstrap3',
							delay:1200
						});
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							title: '403 Error',
							text: 'You donot have permission to access this action.',
							type: 'error',
							styling: 'bootstrap3',
							delay:1200
						});
					}
				},
				error: function (request) {
					new PNotify({
						title: 'Error',
						text: 'Invalid request!',
						type: 'error',
						styling: 'bootstrap3',
						delay:1200
					});
				},
			});
		}
	});

}
	
jQuery('.table-responsive').on('click','.pagination li a',function(event){
	event.preventDefault() ;
	if($(this).parent().hasClass('active')){
		return false;
	}
	var keyy = $('form').serialize();
	var urli = jQuery(this).attr('href');
	jQuery.ajax({ 
		url: urli,
		data: {key:keyy},
		type: 'POST',
		success: function(data) {
			if(data){
				jQuery('.table-responsive').html(data);
			}
		}
	});
});
		
</script>
