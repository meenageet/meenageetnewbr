                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							
							<th class="column-title">S.No. </th>
							<th class="column-title">User </th>
							<th class="column-title">Title </th>
							<th class="column-title">Description </th>
							<th class="column-title no-link last"><span class="nobr">Action</span></th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php //pr($Posts);die;
							$count = 1;
							
							foreach($Posts->toArray() as $data){
							
						    ?>
								
							<tr id ="bank_row_<?= $data['id']; ?>" >
								
								<td><?= $count?>.</td>
								<td class=" "><a target="_blank" href="<?php echo $this->Url->build(['controller'=>'users','action'=>'edit',$data['user_id']]); ?>"  class="btn btn-info btn-xs"><?=$data['user']['full_name'] ?></a></td>
								<td class=" "><?=$data['title']; ?></td>
								<td class=" "><?=$data['description']; ?></td>
								<td class=" last">
									<input type="hidden" id="bank_status_<?= $data['id'] ?>" value ="<?= $data['enabled']; ?>" />
									<input type="hidden" id="web_url_<?= $data['id'] ?>" value ="<?php echo $this->Url->build(['prefix'=>false,'controller'=>'Posts','action'=>'detail',$data['id']]); ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_bank_status(<?php echo $data['id'] ?>)">
									<?php
									if($data['enabled'] == 'P'){
										echo '<button type="button" class="btn btn-success btn-xs">Public</button>';
									}else if($data['enabled'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>';
									}else if($data['enabled'] == 'N'){
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?>
									</a>
									<a class="btn btn-info btn-xs" target="_blank" href="<?php echo $this->Url->build(['controller'=>'Posts','action'=>'detail',$data['id']]); ?>"><i class="fa fa-eye"></i> View </a>
									<a href="javascript:void(0)" onclick="delete_bank(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
									</div>
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Posts->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php //$this->Paginator->options(array('url' => array('controller' => 'Users', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                 
