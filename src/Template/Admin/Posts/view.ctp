<style>
.text-padding{padding-top: 7px;}
.w3-panel{
    padding: 0.01em 16px;
    margin-top: 16px!important;
    margin-bottom: 16px!important;
}.w3-note {
    background-color: #fcf8e3;
    border-left: 6px solid #faebcc;
    color:#8a6d3b;
}.w4-note {
    background-color: #f2dede;
    border-left: 6px solid #ebccd1;
    color:#a94442;
}
</style>
<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content form-horizontal">
					<span class="section">View Post</span>
                      
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Post title : </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-padding">
                           <?php echo $data['title']; ?>
                        </div>
                      </div>
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Description : </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-padding">
                           <?php  echo $data['description']; ?>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Hastag : </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-padding">
							<?php  echo $data['hastag']; ?>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category Name : </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-padding">
							<?php  echo $data['category']['category_name']; ?>
                       </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Sub Category Name : </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-padding">
							<?php  echo $data['subcategory']['category_name']; ?>
                       </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country Name : </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-padding">
							<?php  echo $data['country']['country_name']; ?>
                       </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">State Name : </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-padding">
							<?php  echo $data['state']['state_name']; ?>
                       </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">City Name : </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-padding">
							<?php  echo $data['city']['city_name']; ?>
                       </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Address : </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-padding">
							<?php  echo $data['address']; ?>
                       </div>
                      </div>
				  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
