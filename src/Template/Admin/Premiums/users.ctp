 <div class="right_col" role="main">
          <div class="">

            <div class="page-title">
				<?= $this->Flash->render() ?>
              <div class="title_left">
                <h3>
                      Users Package
                      <small>
                         Listing
                      </small>
                  </h3>
              </div>

              </div>

            <div class="clearfix"></div>

            <div class="row">
				
					<form method="post" class="form-horizontal form-label-left input_mask">

					 
                     <div class="form-group">
                        <div class="col-md-2 col-sm-2 col-xs-12">
                           <?php  echo $this->Form->input('username',array('placeholder'=>'User first/last name','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'text')); ?>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
							<?php  echo $this->Form->input('premium',array('empty'=>'Select package type','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>array('HOT'=>'HOT','TOP'=>'TOP','HOT & TOP'=>'HOT & TOP'))); 
							?>
                       </div>
                       <div class="col-md-2 col-sm-2 col-xs-12 form-group has-feedback">
							<?php  echo $this->Form->input('plan_type',array('empty'=>'Select plan type','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>array('BASIC'=>'BASIC','MEDIUM'=>'MEDIUM','LARGE'=>'LARGE'))); 
							?>
                       </div>
                       <div class="col-md-2 col-sm-2 col-xs-12 form-group has-feedback">
							<?php  echo $this->Form->input('status',array('empty'=>'Select Status','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>array('active'=>'Active','not_active'=>'Not active'))); 
							?>
                       </div>
                        <div class="col-md-1 col-sm-1 col-xs-12 form-group has-feedback">
                          <button type="submit" class="btn btn-success" style ='float:right'>Filter</button>
                        </div>
                      </div>
                    <div class="ln_solid" style="clear:both"></div>
                   
				</form>
				
			

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">S.No. </th>
                            <th class="column-title">User </th>
                            <th class="column-title">Package Type </th>
                            <th class="column-title">Plan Type </th>
                            <th class="column-title">Days </th>
                            <th class="column-title">Price</th> 
                            <th class="column-title">Start Date</th> 
                            <th class="column-title">End Date </th>
                            <th class="column-title">Active</th>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
							<?php 
							$count = 1;
							
							foreach($UserPremiums->toArray() as $data){
								$start_date =  date('Y-m-d',strtotime($data['start_date']->format('Y-m-d')));
								$end_date =  date('Y-m-d',strtotime($data['end_date']->format('Y-m-d')));
								
								?>
							<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
								<td class=" "><?php 
								echo ucwords($data['user']['first_name'])." ".ucwords($data['user']['last_name']); ?></td>
								<td class=" "><?= $data['premium']['premium']; ?> </td>
								<td class=" "><?= $data['premium']['plan_type']; ?> </td>
								<td class=" "><?= $data['days']; ?> </td>
								<td class=" "><?= $data['price']; ?> </td>
								<td class=" "><?= $start_date; ?> </td>
								<td class=" "><?= $end_date; ?> </td>
								<td class=" "><?php 
									if(date('Y-m-d') >= $start_date &&  date('Y-m-d') <= $end_date) echo "Active";
									else echo "Not Active";
								 ?> </td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($UserPremiums->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php $this->Paginator->options(array('url' => array('controller' => 'UserPremiums', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<script>


		
		jQuery('.table-responsive').on('click','.pagination li a',function(event){
			event.preventDefault() ;
			var keyy = $('form').serialize();
			var urli = jQuery(this).attr('href');
			jQuery.ajax({ 
						url: urli,
						data: {key:keyy},
						type: 'POST',
						success: function(data) {
							if(data){
								
								jQuery('.table-responsive').html(data);
								
							}
						}
			});
			
		});
		
</script>
