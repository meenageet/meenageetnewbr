<style type="text/css">
	label{padding-left:10px;padding-right:10px;}
</style>
<div class="right_col" role="main">
	<div class="">
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<?php echo $this->Form->create($premium, array('class'=>'form-horizontal form-label-left', 'novalidate', 'method'=>'post')); ?>
							<?= $this->Flash->render() ?>
							<span class="section" style="text-align:center;">Add Plan</span>
                      
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Name <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php echo $this->Form->input('plan_name', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Plan Description <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php echo $this->Form->input('plan_desc', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"textarea", 'rows'=>'5')); ?>
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Plan Duration <span class="required">*</span></label>
								<div class="col-md-3 col-sm-3 col-xs-6">
								<?php
									 echo $this->Form->input('duration', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"number"));
								?>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-6">
								<?php
									$plantype = array('Monthly'=>'Monthly', 'Yearly'=>'Yearly');
									echo $this->Form->input('duration_type', array('class' => 'form-control col-md-7 col-xs-12', 'empty' => 'Select Plan Type','label' => false, 'options' => $plantype)); 
								?>
								</div>
							</div>

							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Amount <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php echo $this->Form->input('amount', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
								</div>
							</div>
                      
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
								<?php
									$status = array('Y' => 'Active', 'N' => 'Inactive');
									echo $this->Form->input('status', array('class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'options' => $status)); 
								?>
								</div>
							</div>
                      
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<?php  echo $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn btn-primary']); ?>
									<?php  echo $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-success']); ?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
