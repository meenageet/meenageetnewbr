                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">S.No. </th>
                            <th class="column-title">User </th>
                            <th class="column-title">Package Type </th>
                            <th class="column-title">Plan Type </th>
                            <th class="column-title">Days </th>
                            <th class="column-title">Price</th> 
                            <th class="column-title">start Date</th> 
                            <th class="column-title">End Date </th>
                            <th class="column-title">Active</th>
                          </tr>
                        </thead>
                        <tbody>
							<?php 
							$count = $serial_num;
							
							foreach($UserPremiums->toArray() as $data){
								$start_date =  date('Y-m-d',strtotime($data['start_date']->format('Y-m-d')));
								$end_date =  date('Y-m-d',strtotime($data['end_date']->format('Y-m-d')));
								
							
								 ?>
								<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
								<td class=" "><?php 
								echo ucwords($data['user']['first_name'])." ".ucwords($data['user']['last_name']); ?></td>
								<td class=" "><?= $data['premium']['premium']; ?> </td>
								<td class=" "><?= $data['premium']['plan_type']; ?> </td>
								<td class=" "><?= $data['days']; ?> </td>
								<td class=" "><?= $data['price']; ?> </td>
								<td class=" "><?= $start_date; ?> </td>
								<td class=" "><?= $end_date; ?> </td>
								<td class=" "><?php 
									if(date('Y-m-d') >= $start_date &&  date('Y-m-d') <= $end_date) echo "Active";
									else echo "Not Active";
								 ?> </td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($UserPremiums->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php //$this->Paginator->options(array('url' => array('controller' => 'Users', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                 
