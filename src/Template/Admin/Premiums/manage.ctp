 <div class="right_col" role="main">
          <div class="">

            <div class="page-title">
				<?= $this->Flash->render() ?>
              <div class="title_left">
                <h3>
                      Premiums
                      <small>
                         Listing
                      </small>
                  </h3>
              </div>
              
              <div class="title_right">
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
						<div class="input-group">
							<div class="input text"><input type="text" id="search" placeholder="Search for..." onKeyup = "search_result();" class="form-control" name="key"></div>
							<span class="input-group-btn" style='display:none;'><button class="btn btn-default" type="button" onclick = "search_resudlt();">Go!</button></span>
						</div>
					</div>
				</div>

              </div>

            <div class="clearfix"></div>

            <div class="row">
				
					<!--<form method="post" class="form-horizontal form-label-left input_mask">

					 
                     <div class="form-group">
                        <div class="col-md-2 col-sm-2 col-xs-12">
                           <?php  echo $this->Form->input('username',array('placeholder'=>'User first/last name','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'text')); ?>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
							<?php  echo $this->Form->input('premium',array('empty'=>'Select package type','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>array('HOT'=>'HOT','TOP'=>'TOP','HOT & TOP'=>'HOT & TOP'))); 
							?>
                       </div>
                       <div class="col-md-2 col-sm-2 col-xs-12 form-group has-feedback">
							<?php  echo $this->Form->input('plan_type',array('empty'=>'Select plan type','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>array('BASIC'=>'BASIC','MEDIUM'=>'MEDIUM','LARGE'=>'LARGE'))); 
							?>
                       </div>
                       <div class="col-md-2 col-sm-2 col-xs-12 form-group has-feedback">
							<?php  echo $this->Form->input('status',array('empty'=>'Select Status','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>array('active'=>'Active','not_active'=>'Not active'))); 
							?>
                       </div>
                        <div class="col-md-1 col-sm-1 col-xs-12 form-group has-feedback">
                          <button type="submit" class="btn btn-success" style ='float:right'>Filter</button>
                        </div>
                      </div>
                    <div class="ln_solid" style="clear:both"></div>
                   
				</form>-->
				
			

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div id="divLoading"> </div><!--Loading class -->
							<div class="x_content">

								<div class="table-responsive">
									<table class="table table-striped jambo_table bulk_action">
										<thead>
											<tr class="headings">
												<th class="column-title">S.No. </th>
												<th class="column-title">Plan Name </th>
												<th class="column-title">Duration </th>
												<th class="column-title">Duration Type </th>
												<th class="column-title">Amount </th>
												<th class="column-title">Active</th>
												<th class="column-title">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$count = 1;
												//echo '<pre>'; print_r($Premiums->toArray()); die;
												foreach($Premiums->toArray() as $data) {
											?>
												<tr id ="user_row_<?= $data['id']; ?>">
													<td><?= $count?>.</td>
													<td class=" "><?php echo $data->plan_name; ?></td>
													<td class=" "><?php echo $data->duration; ?></td>
													<td class=" "><?php echo $data->duration_type; ?></td>
													<td class=" "><?php echo $data->amount; ?></td>
													<td class=" ">
														<input type="hidden" id="user_status_<?= $data['id'] ?>" value ="<?= $data['enabled']; ?>" />
														<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_user_status(<?php echo $data['id'] ?>)">
														<?php  if($data['status'] == 'Y') {
															echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
														} else {
															echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
														} ?></a>
													</td>
													<td class=" last">
														<a title="Edit Detail" href="<?php echo $this->Url->build(['controller'=>'Premiums','action'=>'edit', $data['id']]);
														; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </a>
														<a title="Delete Premium"  href="#" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
													</td>
												</tr>
							
											<?php  $count++;
											} ?> 
							
							<?php  if(count($Premiums->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php $this->Paginator->options(array('url' => array('controller' => 'Premiums', 'action' => 'search')));
						echo "<div class='pagination' style = 'float:right'>";
	 
						// the 'first' page button
						$paginator = $this->Paginator;
						echo $paginator->first("First");

						// 'prev' page button, 
						// we can check using the paginator hasPrev() method if there's a previous page
						// save with the 'next' page button
						if($paginator->hasPrev()){
						echo $paginator->prev("Prev");
						}

						// the 'number' page buttons
						echo $paginator->numbers(array('modulus' => 2));

						// for the 'next' button
						if($paginator->hasNext()){
						echo $paginator->next("Next");
						}

						// the 'last' page button
						echo $paginator->last("Last");

						echo "</div>";
						?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<script>
function delete_user(id) {
	bootbox.confirm("Are you sure?", function(result) {
		if(result == true) {
			jQuery.ajax({ 
				//url: 'delete',
				url: '<?php echo $this->Url->build(['controller'=>'Premiums', 'action'=>'delete']); ?>',
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					if(data == 1) {
						jQuery("#user_row_"+id).remove();
						new PNotify({
							title: 'Success',
							text: 'Record Delete successfully!',
							type: 'success',
							styling: 'bootstrap3',
							delay:1200
						});
						
					} if(data == 'forbidden') {
						
						new PNotify({
							title: '403 Error',
							text: 'You donot have permission to access this action.',
							type: 'error',
							styling: 'bootstrap3',
							delay:1200
						});
					}
				},
				error: function (request) {
					new PNotify({
						title: 'Error',
						text: 'This record is being referenced in other place. You cannot delete it.',
						type: 'error',
						styling: 'bootstrap3',
						delay:1200
					});
				},
			});
		}
	});	
}

function change_user_status(id) {
	var status = $("#user_status_"+id).val();
	if(status == 'Y') {
		var ques= "Do you want change the status to DEACTIVE";	
		var status = "N";
		var change = '<button type="button" class="btn btn-danger btn-xs">Deactive</button>'
	} else {
		var ques= "Do you want change the status to ACTIVE";
		var status = "Y";
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'Premiums','action'=>'status']); ?>',
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#status_id_"+id).html(change);
						jQuery("#user_status_"+id).val(status);
						new PNotify({
							title: 'Success',
							text: 'Status changed successfully!',
							type: 'success',
							styling: 'bootstrap3',
							delay:1200
						});
					}
					if(data == 'forbidden') {
						
						new PNotify({
							title: '403 Error',
							text: 'You donot have permission to access this action.',
							type: 'error',
							styling: 'bootstrap3',
							delay:1200
						});
					}
				}
			});
		}
	});
}

function search_result() {
			
	var key = jQuery("#search").val();
	
	jQuery.ajax({ 
		url: '<?php echo $this->Url->build(['controller' => 'Premiums', 'action' => 'search']); ?>',
		data: {'key':key},
		type: 'POST',
		success: function(data) {
			if(data) {
				jQuery('.table-responsive').html(data);
			}
		}
	});	
}

		
jQuery('.table-responsive').on('click','.pagination li a',function(event) {
	event.preventDefault() ;
	var keyy = $('form').serialize();
	var urli = jQuery(this).attr('href');
	jQuery.ajax({ 
		url: urli,
		data: {key:keyy},
		type: 'POST',
		success: function(data) {
			if(data) {
				jQuery('.table-responsive').html(data);
			}
		}
	});
});
		
</script>
