                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							   <th class="column-title">S.No. </th>
								<th class="column-title">Plan Name </th>
								<th class="column-title">Duration </th>
								<th class="column-title">Duration Type </th>
								<th class="column-title">Amount </th>
								<th class="column-title">Active</th>
								<th class="column-title">Action</th>
                          </tr>
                        </thead>
                        <tbody>
							<?php 
								$count = 1;
								//echo '<pre>'; print_r($Premiums->toArray()); die;
								foreach($Premiums->toArray() as $data) {
							?>
								<tr id ="user_row_<?= $data['id']; ?>">
									<td><?= $count?>.</td>
									<td class=" "><?php echo $data->plan_name; ?></td>
									<td class=" "><?php echo $data->duration; ?></td>
									<td class=" "><?php echo $data->duration_type; ?></td>
									<td class=" "><?php echo $data->amount; ?></td>
									<td class=" ">
										<input type="hidden" id="user_status_<?= $data['id'] ?>" value ="<?= $data['enabled']; ?>" />
										<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_user_status(<?php echo $data['id'] ?>)">
										<?php  if($data['status'] == 'Y') {
											echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
										} else {
											echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
										} ?></a>
									</td>
									<td class=" last">
										<a title="Edit Detail" href="<?php echo $this->Url->build(['controller'=>'Premiums','action'=>'edit', $data['id']]);
										; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </a>
										<a title="Delete Premium"  href="#" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
			
							<?php  $count++;
							} ?> 
			
			<?php  if(count($Premiums->toArray()) < 1) {
						echo "<tr><th colspan = '6'>No record found</th></tr>";
				   } ?>	

		 </tbody>
	  </table>
	  <?php //$this->Paginator->options(array('url' => array('controller' => 'Users', 'action' => 'search')));
	echo "<div class='pagination' style = 'float:right'>";

	// the 'first' page button
	$paginator = $this->Paginator;
	echo $paginator->first("First");

	// 'prev' page button, 
	// we can check using the paginator hasPrev() method if there's a previous page
	// save with the 'next' page button
	if($paginator->hasPrev()){
	echo $paginator->prev("Prev");
	}

	// the 'number' page buttons
	echo $paginator->numbers(array('modulus' => 2));

	// for the 'next' button
	if($paginator->hasNext()){
	echo $paginator->next("Next");
	}

	// the 'last' page button
	echo $paginator->last("Last");

	echo "</div>";
			
	?>
                 
