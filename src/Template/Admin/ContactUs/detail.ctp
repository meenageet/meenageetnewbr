<?php 
?>
<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>
<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
					 <div class="x_content">
					<?= $this->Flash->render() ?>
				<?php echo $this->Form->create($ContactUs,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>		
					
				 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Name
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12" style="line-height: 37px;">
                           <?= $ContactUs->name;?>
                        </div>
                      </div> 	
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12" style="line-height: 37px;">
                           <?= $ContactUs->email;?>
                        </div>
                      </div> 	
					 <div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Phone Number
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12" style="line-height: 37px;">
					   <?= $ContactUs->phone;?>
					</div>
				  </div> 	
					 <div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Subject
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12" style="line-height: 37px;">
					   <?= $ContactUs->subject;?>
					</div>
				  </div> 	
					 <div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Message
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12" style="line-height: 37px;">
					   <?= $ContactUs->message;?>
					</div>
                      </div>

                         <?php if($ContactUs->status == 1)
                         {?>
				            <span class="section">Sent Reply</span>
                            <div class="item form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Replied Subject
                             </label>
                             <div class="col-md-6 col-sm-6 col-xs-12" style="line-height: 37px;">
                                 <?= $ContactUs->reply_subject;?>
                             </div>
                            </div>
                         <div class="item form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Replied Message
                             </label>


                             <div class="col-md-6 col-sm-6 col-xs-12" style="line-height: 37px;">
                                    <?php echo $ContactUs->reply_message;?>

                             </div>
                         </div>
                        <?php  }

				else
				{ ?>
                         <span class="section">Send Reply</span>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Subject<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('reply_subject',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
                        
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Reply<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('reply_message',array('class' => 'editor form-control col-md-7 col-xs-12','label' =>false,"type"=>"textarea")); ?>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							
							<?php  echo $this->Form->button('Send', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                     
			<?php	}?>   
			
			</div>
               </form>
               
               
                </div>
                
                
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

