<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create($city,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
					<?= $this->Flash->render() ?>
                      <span class="section">Edit City</span>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Select Country <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php  echo $this->Form->input('country_id',array( 'empty' => 'Select Country','default' => $city['state']['country_id'],'class' => 'form-control col-md-7 col-xs-12 country','label' =>false,'type'=>'select','options'=>$country,'id'=>'getstate')); ?>
                        </div>
                      </div>

					            <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Select State <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							             <?php  echo $this->Form->input('state_id',array( 'empty' => 'Select state','class' => 'form-control col-md-7 col-xs-12 state','label' =>false,'type'=>'select','options'=>$state,'id'=>'getcity')); ?>
                        </div>
                      </div>
                      
					
				             <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">City Name<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('city_name',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Latitide<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('latitude',array('class' => 'coordinates form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Longitude<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('longitude',array('class' => 'coordinates form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
                     
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php echo $this->Html->scriptStart(['block'=>true]); ?>                   
  $('.coordinates').keypress(function(event) {
  /*if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }*/
    var charCode = event.which;

        if(charCode == 8 || charCode == 0)
        {
             return;
        }
        else
        {
            var keyChar = String.fromCharCode(charCode); 
            return /[.0-9]/.test(keyChar); 
        }
  
});
<?php echo $this->Html->scriptEnd(); ?>           
<script>
	<!--get state onChnage on country dropdown-->
	$('#getstate').change(function(){	
		var getId = $(this).find(":selected").val();
		$.ajax({
			type: "POST",
			url: "<?php echo $this->Url->build(['controller'=>'cities','action'=>'get_state']); ?>",
			data: {getId: getId},
			success: function(data){
				$( "#getcity" ).html( data );
			}
		});
	});
</script>
