<style>
audio {
    display: none;
}
</style>

<div class="right_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<?php echo $this->Form->create($songUpload, array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
							<?= $this->Flash->render() ?>
							<span class="section">Add Song</span>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Select Singer/Dj
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php echo $this->Form->input('user_id',array('class' => 'form-control col-md-7 col-xs-12','empty'=>'Select Singer/Dj','label' =>false,'options'=>$users)); ?>
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Parent Category
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('cat_id',array('class' => 'form-control col-md-7 col-xs-12','empty'=>'Select Parent Category','type'=>'select','label' =>false,'options'=>$ParentCategories, 'id'=>'getsubcat')); ?>
								</div>
							</div>
                      
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Sub Category
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('sub_cat_id',array('class' => 'form-control col-md-7 col-xs-12','empty'=>'Select Sub Category','label' =>false,'type'=>'select','id'=>'getsubcat_val')); ?>
								</div>
							</div>

							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Song Title <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('title',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
								</div>
							</div>
					
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Song Image<span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('image', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"file")); ?>
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Song Audio<span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('audio', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"file",'id'=>'song_audio')); ?>
								</div>
							</div>
						    <audio id="audio"></audio>
						    <?php  echo $this->Form->input('duration', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"hidden", 'id'=>'song_duration')); ?>
						    
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
									<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success','id'=>'submit_song']); ?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
	
<script>
	<!--get state onChnage on country dropdown-->
	$('#getsubcat').change(function(){
		var getId = $(this).find(":selected").val();
		$.ajax({
			type: "POST",
			url: "<?php echo $this->Url->build(['controller'=>'song_uploads','action'=>'get_subcat']); ?>",
			data: {getId: getId},
			success: function(data){
				$( "#getsubcat_val" ).html( data );
			}
		});
	});
</script>

<script>
	var objectUrl;

	$("#audio").on("canplaythrough", function(e){
		var seconds = e.currentTarget.duration;
		var duration = moment.duration(seconds, "seconds");
		
		var time = "";
		var hours = duration.hours();
		if (hours > 0) { time = hours + ":" ; }
		
		time = time + duration.minutes() + ":" + duration.seconds();
		//$("#duration").text(time);
		$("#song_duration").val(time);
		
		URL.revokeObjectURL(objectUrl);
	});

	$("#submit_song").click(function(e) {  
		if($("#song_duration").val() == '') { 
			new PNotify({
				title: 'Error',
				text: 'Invalid duration time. Please try to select a new audio',
				type: 'error',
				styling: 'bootstrap3',
				delay:1200
			});
			return false;
		}
	});

	$("#song_audio").change(function(e){
		var file = e.currentTarget.files[0];
	   
		$("#filename").text(file.name);
		$("#filetype").text(file.type);
		$("#filesize").text(file.size);
		
		objectUrl = URL.createObjectURL(file);
		$("#audio").prop("src", objectUrl);
	});

</script>
