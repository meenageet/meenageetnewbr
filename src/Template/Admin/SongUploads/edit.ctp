<style>
audio {
    display: none;
}
</style>

<div class="right_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<?php echo $this->Form->create($songUpload, array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
						<?php //echo '<pre>'; print_r(path_song_image_folder); die; ?>
							<?= $this->Flash->render() ?>
							<span class="section">Add Song</span>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Select Singer/Dj
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php echo $this->Form->input('user_id',array('class' => 'form-control col-md-7 col-xs-12','empty'=>'Select Singer/Dj','label' =>false,'options'=>$users)); ?>
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Parent Category
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('cat_id',array('class' => 'form-control col-md-7 col-xs-12','empty'=>'Select Parent Category','default' => $songUpload['category']['category_name'],'type'=>'select','label' =>false,'options'=>$ParentCategories, 'id'=>'getsubcat')); ?>
								</div>
							</div>
                      
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Sub Category
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('sub_cat_id',array('class' => 'form-control col-md-7 col-xs-12','empty'=>'Select Sub Category','label' =>false,'type'=>'select','options'=>$subcat,'id'=>'getsubcat_val')); ?>
								</div>
							</div>

							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Song Title <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('title',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
								</div>
							</div>
					
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Song Image
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('image', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,'required'=>false,"type"=>"file" ,'required'=>false, "id" => "song_img")); ?>
									<?php if($songUpload['image'] != ''){									
									
									 echo '<img width="50px" src="'.path_song_upload_image.$songUpload['image'].'" id = "picture"/>'; 
									
									 }else{?>
									 <img id="picture" src="<?php echo $this->request->webroot.'no-image.png';?>" alt= "picture extracted from ID3" width = "50"/>
									 <?php }
									 ?>
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Song Audio<span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<?php  echo $this->Form->input('audio', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"file",'id'=>'song_audio', 'onchange'=>'loadFile(this)', 'required'=>false)); ?>
									<?php if($songUpload['audio'] != '') echo path_song_upload_audio.$songUpload['audio']; ?>
								</div>
							</div>
						    <audio id="audio"></audio>
						    <?php  echo $this->Form->input('duration', array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"hidden", 'id'=>'song_duration')); ?>
						    
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
								<input type = "hidden" value="" id = "picture_id" name = "picture_id" >
									<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
									<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success','id'=>'submit_song']); ?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
	
<script>
	<!--get state onChnage on country dropdown-->
	$('#getsubcat').change(function(){
		var getId = $(this).find(":selected").val();
		$.ajax({
			type: "POST",
			url: "<?php echo $this->Url->build(['controller'=>'song_uploads','action'=>'get_subcat']); ?>",
			data: {getId: getId},
			success: function(data){
				$( "#getsubcat_val" ).html( data );
			}
		});
	});
</script>

<script>
	var objectUrl;

	$("#audio").on("canplaythrough", function(e){
		var seconds = e.currentTarget.duration;
		var duration = moment.duration(seconds, "seconds");
		
		var time = "";
		var hours = duration.hours();
		if (hours > 0) { time = hours + ":" ; }
		
		time = time + duration.minutes() + ":" + duration.seconds();
		//$("#duration").text(time);
		$("#song_duration").val(time);
		
		URL.revokeObjectURL(objectUrl);
	});

	/*$("#submit_song").click(function(e) {  
		if($("#song_duration").val() == '') { 
			new PNotify({
				title: 'Error',
				text: 'Invalid duration time. Please try to select a new audio',
				type: 'error',
				styling: 'bootstrap3',
				delay:1200
			});
			return false;
		}
	});*/

	$("#song_audio").change(function(e){
		var file = e.currentTarget.files[0];
	   
		$("#filename").text(file.name);
		$("#filetype").text(file.type);
		$("#filesize").text(file.size);
		
		objectUrl = URL.createObjectURL(file);
		$("#audio").prop("src", objectUrl);
	});
	
	
	
	/**
     * Loading the tags using XHR.
     */
    //sample.mp3 sits on your domain
    ID3.loadTags("sample.mp3", function() {
      showTags("sample.mp3");
    }, {
      tags: ["title","artist","album","picture"]
    });

    /**
     * Loading the tags using the FileAPI.
     */
    function loadFile(input) {
      var file = input.files[0],
        url = file.urn || file.name;

      ID3.loadTags(url, function() {
        showTags(url);
      }, {
        tags: ["title","artist","album","picture"],
        dataReader: ID3.FileAPIReader(file)
      });
    }

    /**
     * Generic function to get the tags after they have been loaded.
     */
    function showTags(url) {
      var tags = ID3.getAllTags(url);
      console.log(tags);
      
      var image = tags.picture;
      if (image) {
        var base64String = "";
        for (var i = 0; i < image.data.length; i++) {
            base64String += String.fromCharCode(image.data[i]);
        }
        var base64 = "data:" + image.format + ";base64," +
                window.btoa(base64String);
                console.log("123", base64);
        document.getElementById('picture').setAttribute('src',base64);
        document.getElementById('picture_id').setAttribute('value',base64);
      } else {
        //document.getElementById('picture').style.display = "none";
      }
    }
    
    
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#picture').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#song_img").change(function(){
    readURL(this);
});

</script>
