
<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create($category,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
					<?= $this->Flash->render() ?>
                      <span class="section">Add Category</span>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Parent Category
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('parent_category_id',array('class' => 'form-control col-md-7 col-xs-12','empty'=>'Select Parent Category','label' =>false,'options'=>$ParentCategories)); ?>
							
                        </div>
                      </div>

					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Category name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php  echo $this->Form->input('category_name',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
							
                        </div>
                      </div>

					
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category logo (Web) <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
						 <?php  echo $this->Form->input('cat_logo_web',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"file")); ?>
                         <span style="color: red;">Max Image size 116*116</span>
                        </div>
                       
                      </div>
                      
                       <!-- div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category logo (App 1) <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
						 <?php  echo $this->Form->input('cat_logo_app1',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"file")); ?>
						 <small class="col-md-6 col-md-offset-3"> For Big resolution screen</small>
                         
                        </div>
                        
                      </div>

					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category logo (App 2) <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
						 <?php  echo $this->Form->input('cat_logo_app2',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"file")); ?>
						 <small class="col-md-6 col-md-offset-3"> For Medium resolution screen</small>
                         
                        </div>
                        
                      </div>


					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category logo (App 3) <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
						 <?php  echo $this->Form->input('cat_logo_app3',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"file")); ?>
						 <small class="col-md-6 col-md-offset-3"> For Small resolution screen </small>
                         
                        </div>
                        
                      </div -->
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
	
