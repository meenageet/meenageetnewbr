
                     	<table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							
							<th class="column-title">S.No. </th>
							<th class="column-title">Parent Category </th>
							<th class="column-title">Category Name </th>
							<th class="column-title">Image </th>
							<th class="column-title">Status </th>
							<th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php 
							$count = $serial_num;
							
							foreach($Categories->toArray() as $data){
							
						    ?>
								
							<tr id ="bank_row_<?= $data['id']; ?>" >
								<td><?= $count?>.</td>
								<td class=" "><?php echo isset($data['parent_category']['category_name']) ? $data['parent_category']['category_name'] : '--------'; ?></td>
								<td class=" "><?php 
								echo ucwords($data['category_name']) ?></td>
								
								<td class=" ">
									<?php if(isset($data['cat_logo_web']) && $data['cat_logo_web'] !=''){?>
										<img src = "<?php echo $this->request->webroot.'uploads/category_web/'.$data['cat_logo_web']; ?>" width = "50" height="40">
									<?php } ?>
								</td>
								<td class=" ">
									
									<input type="hidden" id="bank_status_<?= $data['id'] ?>" value ="<?= $data['enabled']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_bank_status(<?php echo $data['id'] ?>)">
									<?php if($data['enabled'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
									
								</td>
								<td class=" last">
									<a class="btn btn-info btn-xs" href="<?php echo $this->Url->build(['controller'=>'Categories','action'=>'edit',$data['id']]); ?>"><i class="fa fa-pencil"></i> Edit </a>
									
									<a href="javascript:void(0)" onclick="delete_bank(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Categories->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php //$this->Paginator->options(array('url' => array('controller' => 'Categories', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                    
