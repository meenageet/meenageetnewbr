<div class="right_col" role="main">
          <div class="">
			
            <div class="page-title">
				<?= $this->Flash->render() ?>
              <div class="title_left">
                <h3>
                      Download Songs  
                      <small>
                         Listing
                      </small>
                  </h3>
              </div>

              </div>

            <div class="clearfix"></div>
            
            
            

            <div class="row">
				
				<form method="post" class="form-horizontal form-label-left input_mask">

                    <div class="form-group">
						<div class="col-md-4 col-sm-4 col-xs-12">
                            <?php  echo $this->Form->input('title',array('placeholder'=>'Post title','class' => 'form-control col-md-7 col-xs-12','label' =>false,'type'=>'text')); ?>
                            
                        </div>
						<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
							<?php  echo $this->Form->input('user_id',array('empty'=>'Select user','class' => 'form-control 	col-md-7 col-xs-12','label' =>false,'type'=>'select','options'=>$users)); 
						  ?>
                       
						</div>
                   </div>
				   <div class="ln_solid" style="clear:both"></div>
				   <div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<a class="btn btn-primary" href="<?php echo $this->Url->build(['controller'=>'downloads','action'=>'manage']); ?>"> Clear </a>
							<button type="submit" class="btn btn-success">Filter</button>
						</div>
				   </div>
				</form>
			 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
						
					<table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							
							<th class="column-title">S.No. </th>
							<th class="column-title">User </th>
							<th class="column-title">Title </th>
							<th class="column-title">Category Name </th>
							<th class="column-title">Sub Category Name</th>
							<th class="column-title no-link last"><span class="nobr">Action</span></th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php //pr($Posts);die;
							$count = 1;
							
							foreach($Downloads->toArray() as $data){
							
						    ?>
								
							<tr id ="bank_row_<?= $data['id']; ?>" >
								
								<td><?= $count?>.</td>
								<td class=" "><a target="_blank" href="<?php echo $this->Url->build(['controller'=>'users','action'=>'edit',$data['user_id']]); ?>"  class="btn btn-info btn-xs"><?=$data['user']['full_name'] ?></a></td>
								<td class=" "><?=$data['song_upload']['title']; ?></td>
								<td class=" "><?=$data['song_upload']['category']['category_name']; ?></td>
								<td class=" "><?=$data['song_upload']['subcategory']['category_name']; ?></td>
								<td class=" last">
									<a class="btn btn-info btn-xs" target="_blank" href="<?php echo $this->Url->build(['controller'=>'downloads','action'=>'detail',$data['id']]); ?>"><i class="fa fa-eye"></i> View </a>
									<a href="javascript:void(0)" onclick="delete_bank(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
									</div>
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Downloads->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php $this->Paginator->options(array('url' => array('controller' => 'downloads', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
         <?php
		echo $this->Html->script(
				array(
					'Admin/moment.min.js',
					'Admin/daterangepicker.js',
					
				));
				echo $this->fetch('script');
	?>
<script>
$(document).ready(function() {
        
			$('#start-date').daterangepicker({
				singleDatePicker: true,
				showDropdowns: true,
				format: 'YYYY-MM-DD',
				maxDate:0

			});
			$('#end-date').daterangepicker({
				singleDatePicker: true,
				showDropdowns: true,
				format: 'YYYY-MM-DD',
				maxDate:0

			});


      });

function delete_bank(id){
	bootbox.confirm("Do you want to delete the detail ?", function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'downloads','action'=>'delete']); ?>',
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#bank_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record Delete successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},error: function (request) {
					new PNotify({
							  title: 'Error',
							  text: 'This record is being referenced in other place. You cannot delete it.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
					
				},
			});
		}
	});
	
}
		jQuery('.table-responsive').on('click','.pagination li a',function(event){
			event.preventDefault() ;
			if($(this).parent().hasClass('active')){
				return false;
			}
			var keyy = $('form').serialize();
			var urli = jQuery(this).attr('href');
			jQuery.ajax({ 
						url: urli,
						data: {key:keyy},
						type: 'POST',
						success: function(data) {
							if(data){
								
								jQuery('.table-responsive').html(data);
								
							}
						}
			});
			
		});
		
	</script>
