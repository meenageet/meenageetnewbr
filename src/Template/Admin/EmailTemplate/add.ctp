<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create($EmailTemplate,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
					<?= $this->Flash->render() ?>
					   <span class="section">Add Email Template</span>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Template Title <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('title',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Sender Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('from_email',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Sender Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('from_name',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div>
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Subject <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('subject',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
                        </div>
                      </div> 
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Message <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('description',array('class' => 'form-control col-md-7 col-xs-12 editor','label' =>false,"type"=>"textarea")); ?>
                        </div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="alert-info alert-dismissible fade in" role="alert" style = "padding: 10px 0px 3px 10px;">
								
								<strong>Use Dynamic keywords -:</strong>&nbsp;&nbsp; {RECIVER NAME} , {RECIVER EMAIL} , {RECIVER PHONE NUMBER}  
							</div>
						</div>
                      </div>
                      
                       <div class="item form-group" style = "display:none">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">CC
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('cc',array('class' => 'form-control col-md-7 col-xs-12 editor','label' =>false,"type"=>"hidden")); ?>
                        </div>
                      </div>
					  
					  <div class="item form-group" style = "display:none">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Bcc
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php  echo $this->Form->input('bcc',array('class' => 'form-control col-md-7 col-xs-12 editor','label' =>false,"type"=>"hidden")); ?>
                        </div>
                      </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->