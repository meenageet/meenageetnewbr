
<div class="right_col" role="main">

          <div class="">
             <div class="page-title">
				<?= $this->Flash->render() ?>
              <div class="title_left">
                <h3>
                      Manage reward for top Singer
                  </h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create(NULL,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
					<?= $this->Flash->render() ?>
                      <div class="table-responsive">
						  <table class="table table-striped jambo_table bulk_action">
							<thead>
							  <tr class="headings">
							  
							 <th >Title</th>
							 <th>Top Singer 1</th>
							 <th>Top Singer 2</th>
							 <th>Top Singer 3</th>
							 <th>Top Singer 4</th>
							 <th>Top Singer 5</th>
							 <th>Top Singer 6</th>
							 <th>Top Singer 7</th>
							 <th>Top Singer 8</th>
							 <th>Top Singer 9</th>
							 <th>Top Singer 10</th>
							 

							  </tr>
							</thead>
							<tr>
							<td>Assign Points</td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger1']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger1']['point']?>"></td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger2']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger2']['point']?>"></td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger3']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger3']['point']?>"></td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger4']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger4']['point']?>"></td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger5']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger5']['point']?>"></td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger6']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger6']['point']?>"></td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger7']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger7']['point']?>"></td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger8']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger8']['point']?>"></td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger9']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger9']['point']?>"></td>
							
							<td class = "item"><input min = "0" class = "form-control" type = "number" required = "required" name = "<?=  $site_parameters_arr_arry['TopSinger10']['id']?>" value = "<?=  $site_parameters_arr_arry['TopSinger10']['point']?>"></td>
							</tr>
							<tbody>
							</tbody>
						  </table>

						</div>
						<?php  echo $this->Form->button('Update', ['type' => 'submit','class'=>'btn btn-success']); ?>
                    </form>
				  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
		
<script>
jQuery(document).ready(function(){
	
	

	jQuery('.cms_pages').change(function(){
		
		var cms_id = jQuery(this).val();
			
			jQuery.ajax({ 
						url: '<?php echo $this->Url->build(['controller'=>'Pages' , 'action'=>'manage_reward_search']);  ?>',
						data: {'cms_id':cms_id},
						type: 'POST',
						success: function(data) {
							if(data){
								var cmsDetails  =$.parseJSON(data);
								jQuery('.cms_id').val(cmsDetails.id);
								jQuery('.title').val(cmsDetails.title);
								jQuery('.point').val(cmsDetails.point);
								jQuery('.page_title').html('Manage  " '+ cmsDetails.title +' "');
								jQuery('#cms_main').hide();
								jQuery('.cms-details').show();
								jQuery('.bck-btn').show();
							}
						}
			});
		
	});	

	if(jQuery('.cms_pages').val() != '') {jQuery('.cms-details').show(); jQuery('#cms_main').hide();  jQuery('.page_title').html('Manage  " '+ jQuery("#name option:selected").text() +' "'); jQuery('.bck-btn').show();}
});

</script>		
		
