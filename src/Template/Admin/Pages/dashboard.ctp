  <!-- page content -->
        <div class="right_col" role="main" style ="min-height: 725px;">

          <!-- top tiles -->
          <div class="row tile_count">
            
             <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-users"></i> Total Users</span>
              <div class="count green"><?php echo $totalUsers; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'users','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
             <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-users"></i> Total Request Singer Users</span>
              <div class="count green"><?php echo $totalSingerUsers; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'users','action'=>'request-list','S']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-users"></i> Total Request Dj Users</span>
              <div class="count green"><?php echo $totalDjUsers; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'users','action'=>'request-list','D']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
            
             <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-users"></i> Total Pending Post</span>
              <div class="count green"><?php echo $totalPendingPostDetails; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'posts','action'=>'pending-manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-users"></i> Total Public Post</span>
              <div class="count green"><?php echo $totalPublicPostDetails; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'posts','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
          
              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-tag"></i> Total Banners</span>
              <div class="count green"><?php echo $totalBanners; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'banners','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
             <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-sitemap"></i> Total Categories</span>
              <div class="count green"><?php echo $totalCategories; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'categories','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
               
			<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-music"></i> Total DJ Songs</span>
              <div class="count green"><?php echo $totaldjSongUploads; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'song-uploads','action'=>'djManage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-music"></i> Total Singer Songs</span>
              <div class="count green"><?php echo $totalsingerSongUploads; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'song-uploads','action'=>'singerManage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
			<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-trophy"></i> Total Challenge</span>
              <div class="count green"><?php echo $totalChallenges; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'challenges','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
         
 		     
			<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-like"></i> Total Likes</span>
              <div class="count green"><?php echo $totalLikes; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'song-uploads','action'=>'like']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
           <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-like"></i> Total Unlikes</span>
              <div class="count green"><?php echo $totalUnLikes; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'song-uploads','action'=>'unlike']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
			<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-comments"></i> Total Comments</span>
              <div class="count green"><?php echo $totalComments; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'song-uploads','action'=>'comments']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
			<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-gift"></i> Total Reward Users</span>
              <div class="count green"><?php echo $totalUserRewards; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'user-rewards','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
			<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-download"></i> Total Dowanload Songs</span>
              <div class="count green"><?php echo $totalDownloads; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'downloads','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
              
			<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-download"></i> Total completed payment this month</span>
              <div class="count green"><?php echo $totalPaymentDetails; ?></div>
               <div class="count green">Amount: <?php echo $totalCompletePaymentAmount['sum']; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'paymentDetails','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-download"></i> Total Challange success transactions this month</span>
              <div class="count green"><?php echo $totalCTransactions; ?></div>
               <div class="count green">Amount: <?php echo $tcchallenagetAmount['sum']; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'transactions','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
			
			 <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-download"></i> Total User success transactions this month</span>
              <div class="count green"><?php echo $totalUTransactions; ?></div>
               <div class="count green">Amount: <?php echo $tcusertAmount['sum']; ?></div>
              <span class="count_bottom"><a href = "<?php echo $this->Url->build(['controller'=>'transactions','action'=>'manage']); ?>"><i class="green"> More <i class="fa fa-chevron-right"></i></i> </a> </span>
            </div>
			
			
          </div>
          <!-- /top tiles -->
		<br />


            <div class="row">
			 <div id="divLoading"> </div><!--Loading class -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
                    <div class="col-md-6">
                      <h3>User Activities <!---<small>Graph title sub-title</small>---></h3>
                    </div>
                    <div class="col-md-6">
                      <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                      </div>
                    </div>
                  </div>
                  <div class="x_content">
                    <div class="demo-container" style="height:250px">
                      <div id="placeholder3xx3" class="demo-placeholder" style="width: 100%; height:250px;"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>	
          <div class="row" id="dashboard_container" >
          </div>
          </div>
        <!-- /page content -->
<style>
	.u-tooltip{color:#1abb9c;}
</style>
<script>
 $(document).ready(function() {
        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2016',
          maxDate: moment(),
          dateLimit: {
            days: 365
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
		$('#reportrange span').bind('DOMSubtreeModified', function(e){
			
			var toDate = $('input[name=daterangepicker_start]').val();
			var fromDate = $('input[name=daterangepicker_end]').val();
			
			reportChart(toDate , fromDate);
			
			//alert('New value is: '+$('input[name=daterangepicker_start]').val()+' to '+$('input[name=daterangepicker_end]').val());
		});
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
		
		var toDate = $('input[name=daterangepicker_start]').val();
		var fromDate = $('input[name=daterangepicker_end]').val();
			
		reportChart(toDate , fromDate);
		
	
		
		function getTooltip(label, x, y) {
				var d = x/1000;
				return '<i class="fa fa-user border-blue u-tooltip"></i> &nbsp;&nbsp;' + moment.unix(d).format("YYYY-MM-DD") + '&nbsp;( ' + y + ' )'; 
		}
					
		function reportChart(to , from){
			
			
			var toDate = to.split("/");
			var fromDate = from.split("/");
			
			
			 var chartColours = ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];
			jQuery.ajax({ 
							url: '<?php echo $this->Url->build(['controller'=>'pages','action'=>'chart']);  ?>',
							data: {'mode':'Users',to:to,from:from},
							type: 'POST',
							async: false,
							success: function(data) {
								//alert(data);
								var result = jQuery.parseJSON(data);
								var usr = result.users;
								 var options = {
										  series: {
												lines: {
												  show: true,
												  fill: true,
												  lineWidth: 2,
												  steps: false
												},
												points: {
												  show: true,
												  radius: 4.5,
												  symbol: "circle",
												  lineWidth: 3.0
												}
											  },
											    legend: {
												position: "ne",
												margin: [0, -25],
												noColumns: 0,
												labelBoxBorderColor: null,
												labelFormatter: function(label, series) {
												  // just add some space to labes
												  return label + '&nbsp;&nbsp;';
												},
												width: 40,
												height: 1
											  },
										  colors: chartColours,
										  xaxis: {
												mode: "time",
												timeformat: "%d/%m/%y",
												minTickSize: [1, "day"],
												min: (new Date(toDate[2], parseInt(toDate[0])-1, toDate[1])).getTime(),
												max: (new Date(fromDate[2], parseInt(fromDate[0])-1, fromDate[1])).getTime()
												
												//min: (new Date(2013, 0, 13)).getTime(),
												//max: (new Date(2016, 11, 13)).getTime(),
												},
										    grid: {
											 	show: true,
												aboveData: true,
												color: "#3f3f3f",
												labelMargin: 10,
												axisMargin: 0,
												borderWidth: 0,
												borderColor: null,
												minBorderMargin: 5,
												clickable: true,
												hoverable: true,
												autoHighlight: true,
												mouseActiveRadius: 100
											  },
											   tooltip : true,
											  tooltipOpts : {
												content : getTooltip,
												defaultTheme : true
											  },
								};
									
								 var plot = $.plot($("#placeholder3xx3"), [
										{ data: usr, label: "&nbsp;Users" , color: "#1abb9c",
													lines: {
														fill: true,
														fillColor:  "rgba(26, 187, 156, 0.12)"
													},
													points: {
														/* show: true, */
														fillColor: '#fff'
										}},
								
									], options);
							} 
			});
		
		}
		
      });

</script>
