
<div class="right_col" role="main">

          <div class="">
             <div class="page-title">
				<?= $this->Flash->render() ?>
              <div class="title_left">
                <h3>
                      Manage Themes
                  </h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create(NULL,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
					<?= $this->Flash->render() ?>
                      <div class="table-responsive">
						  <table class="table table-striped jambo_table bulk_action">
							<thead>
							  <tr class="headings">
								 <th >Title</th>
								 <th>Theme 1</th>
								 <th>Theme 2</th>
								 <th>Theme 3</th>
								 <th>Theme 4</th>
								 <th>Theme 5</th>
								 <th>Theme 6</th>
							  </tr>
							</thead>
							<tr>
								<td>Theame Name</td>
								<td class = "item"><?=  $site_parameters_arr_arry['holi_theme']['name']?></td>
								<td class = "item"><?=  $site_parameters_arr_arry['diwali_theme']['name']?></td>
								<td class = "item"><?=  $site_parameters_arr_arry['sakrat_theme']['name']?></td>
								<td class = "item"><?=  $site_parameters_arr_arry['new_year_theme']['name']?></td>
								<td class = "item"><?=  $site_parameters_arr_arry['national_theme']['name']?></td>
								<td class = "item"><?=  $site_parameters_arr_arry['app_theme']['name']?></td>
							</tr>
							<tr>
								<td>Theme Active</td>
								<td class = "item">
									<input type="hidden" id="user_status_<?= $site_parameters_arr_arry['holi_theme']['id'] ?>" value ="<?= $site_parameters_arr_arry['holi_theme']['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $site_parameters_arr_arry['holi_theme']['id']; ?>" onclick="change_user_status(<?php echo $site_parameters_arr_arry['holi_theme']['id'] ?>)">
									<?php  if($site_parameters_arr_arry['holi_theme']['status'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
								</td>
								<td class = "item">
									<input type="hidden" id="user_status_<?= $site_parameters_arr_arry['diwali_theme']['id'] ?>" value ="<?= $site_parameters_arr_arry['diwali_theme']['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $site_parameters_arr_arry['diwali_theme']['id']; ?>" onclick="change_user_status(<?php echo $site_parameters_arr_arry['diwali_theme']['id'] ?>)">
									<?php  if($site_parameters_arr_arry['diwali_theme']['status'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
								</td>
								<td class = "item">
									<input type="hidden" id="user_status_<?= $site_parameters_arr_arry['sakrat_theme']['id'] ?>" value ="<?= $site_parameters_arr_arry['sakrat_theme']['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $site_parameters_arr_arry['sakrat_theme']['id']; ?>" onclick="change_user_status(<?php echo $site_parameters_arr_arry['sakrat_theme']['id'] ?>)">
									<?php  if($site_parameters_arr_arry['sakrat_theme']['status'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
								</td>
								<td class = "item">
									<input type="hidden" id="user_status_<?= $site_parameters_arr_arry['new_year_theme']['id'] ?>" value ="<?= $site_parameters_arr_arry['new_year_theme']['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $site_parameters_arr_arry['new_year_theme']['id']; ?>" onclick="change_user_status(<?php echo $site_parameters_arr_arry['new_year_theme']['id'] ?>)">
									<?php  if($site_parameters_arr_arry['new_year_theme']['status'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
								</td>
								<td class = "item">
									<input type="hidden" id="user_status_<?= $site_parameters_arr_arry['national_theme']['id'] ?>" value ="<?= $site_parameters_arr_arry['national_theme']['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $site_parameters_arr_arry['national_theme']['id']; ?>" onclick="change_user_status(<?php echo $site_parameters_arr_arry['national_theme']['id'] ?>)">
									<?php  if($site_parameters_arr_arry['national_theme']['status'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
								</td>
								<td class = "item">
									<input type="hidden" id="user_status_<?= $site_parameters_arr_arry['app_theme']['id'] ?>" value ="<?= $site_parameters_arr_arry['app_theme']['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $site_parameters_arr_arry['app_theme']['id']; ?>" onclick="change_user_status(<?php echo $site_parameters_arr_arry['app_theme']['id'] ?>)">
									<?php  if($site_parameters_arr_arry['app_theme']['status'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
								</td>
								
							</tr>
							<tr>
								<td>Upload Image</td>
								<td class = "item"><input class = "form-control" type = "file" name = "<?=  $site_parameters_arr_arry['holi_theme']['id']?>"></td>
								<td class = "item"><input class = "form-control" type = "file"  name = "<?=  $site_parameters_arr_arry['diwali_theme']['id']?>"></td>
								<td class = "item"><input class = "form-control" type = "file"  name = "<?=  $site_parameters_arr_arry['sakrat_theme']['id']?>"></td>
								<td class = "item"><input class = "form-control" type = "file"  name = "<?=  $site_parameters_arr_arry['new_year_theme']['id']?>"></td>
								<td class = "item"><input class = "form-control" type = "file"  name = "<?=  $site_parameters_arr_arry['national_theme']['id']?>"></td>
								<td class = "item"><input class = "form-control" type = "file"  name = "<?=  $site_parameters_arr_arry['app_theme']['id']?>"></td>
							</tr>
							<tr>
								<td>Theame Image</td>
								<td class = "item">
									<?php
										if($site_parameters_arr_arry['holi_theme']['image'] != '') echo '<img width="50px" src="'.$this->request->webroot.'uploads/user/'.$site_parameters_arr_arry['holi_theme']['image'].'"/>';
									?>
								</td>
								<td class = "item">
									<?php
										if($site_parameters_arr_arry['diwali_theme']['image'] != '') echo '<img width="50px" src="'.$this->request->webroot.'uploads/user/'.$site_parameters_arr_arry['diwali_theme']['image'].'"/>';
									?>
								</td>
								<td class = "item">
									<?php
										if($site_parameters_arr_arry['sakrat_theme']['image'] != '') echo '<img width="50px" src="'.$this->request->webroot.'uploads/user/'.$site_parameters_arr_arry['sakrat_theme']['image'].'"/>';
									?>
								</td>
								<td class = "item">
									<?php
										if($site_parameters_arr_arry['new_year_theme']['image'] != '') echo '<img width="50px" src="'.$this->request->webroot.'uploads/user/'.$site_parameters_arr_arry['new_year_theme']['image'].'"/>';
									?>
								</td>
								<td class = "item">
									<?php
										if($site_parameters_arr_arry['national_theme']['image'] != '') echo '<img width="50px" src="'.$this->request->webroot.'uploads/user/'.$site_parameters_arr_arry['national_theme']['image'].'"/>';
									?>
								</td>
								<td class = "item">
									<?php
										if($site_parameters_arr_arry['app_theme']['image'] != '') echo '<img width="50px" src="'.$this->request->webroot.'uploads/user/'.$site_parameters_arr_arry['app_theme']['image'].'"/>';
									?>
								</td>
								
							</tr>
							<tbody>
							</tbody>
						  </table>

						</div>
						<?php  echo $this->Form->button('Update', ['type' => 'submit','class'=>'btn btn-success']); ?>
                    </form>
				  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
		
<script>
function change_user_status(id){
	var status = $("#user_status_"+id).val();
	if(status == 'Y'){
		var ques= "Do you want change the status to DEACTIVE";	
		var status = "N";
		var change = '<button type="button" class="btn btn-danger btn-xs">Deactive</button>'
	}else{
		var ques= "Do you want change the status to ACTIVE";
		var status = "Y";
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'Pages','action'=>'themestatus']); ?>',
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#status_id_"+id).html(change);
						jQuery("#user_status_"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'Status changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 2){
						new PNotify({
							  title: '403 Error',
							  text: 'Single Theme should be actived at the time.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
		}
	});

	
}
</script>
