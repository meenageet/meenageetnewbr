                     <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							<th class="column-title">S.No. </th>
							<th class="column-title">User </th>
							<th class="column-title">Time </th>
							<th class="column-title">Date </th>
							<th class="column-title no-link last"><span class="nobr">Action</span></th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php //pr($appusetime);die;
							$count = 1;
							
							foreach($UserRewards->toArray() as $data){
						    
						    ?>
								
								<tr id ="bank_row_<?= $data['id']; ?>" >
								
									<td><?= $count?>.</td>
									<td class=" ">
										<span class='d_val'>
											<?php
												echo $data['user']['full_name']?$data['user']['full_name']:'';
											?>
										</span>
										<span class='d_val'>
											(
											<?php
												echo $data['user']['phone_number']?$data['user']['phone_number']:'';
											?>
											)
										</span>
										
									</td>
									<td class=" ">
										<?php 
											$init = $data['time'];
											$hours = floor($init / 3600).' hours';
											$minutes = floor(($init / 60) % 60).' minutes';
											$seconds = $init % 60 .' seconds';
											
											//if($init <= $appusetime->app_use_time){
											echo "$hours:$minutes:$seconds";
											/*}else{
											$init = $appusetime->app_use_time;
											$hours = floor($init / 3600).' hours';
											$minutes = floor(($init / 60) % 60).' minutes';
											$seconds = $init % 60 .' seconds';
											echo "$hours:$minutes:$seconds";
											}*/
										?>
									</td>
									<td class=" ">
										<?php 
											echo date('Y-m-d', strtotime($data['start_time']));
										?>
									</td>
									<td class=" last">
										<a class="btn btn-info btn-xs" target="_blank" href="<?php echo $this->Url->build(['controller'=>'UserRewards','action'=>'detail',$data['user_id']]); ?>"><i class="fa fa-eye"></i> History </a>
										
										</div>
										
									</td>
								</tr>
								
								<?php  $count++;
								
							
							} ?> 
							
							<?php  if(count($UserRewards->toArray()) < 1) {
								echo "<tr><th colspan = '6'>No record found</th></tr>";
							} ?>	
							
							
                         </tbody>
                      </table>
                      <?php //$this->Paginator->options(array('url' => array('controller' => 'Users', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                 
