<div class="right_col" role="main">
	<div class="">
			
		<div class="page-title">
			<?= $this->Flash->render() ?>
			<div class="title_left">
				<h3>
					User Reward Points  
					<small>
						Listing
					</small>
				</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
						
					<table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							<th class="column-title">S.No. </th>
							<th class="column-title">User </th>
							<th class="column-title">Points </th>
							<th class="column-title">Created </th>
							<th class="column-title">Type </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php //pr($appusetime);die;
							$count = 1;
							
							foreach($HistoryRewards->toArray() as $data) {
						    
						    ?>
								<tr id ="bank_row_<?= $data['id']; ?>" >
									
									<td><?= $count?>.</td>
									<td class=" "><a target="_blank" href="<?php echo $this->Url->build(['controller'=>'users','action'=>'edit',$data['user_id']]); ?>"  class="btn btn-info btn-xs"><?=$data['user']['full_name'] ?></a></td>
									<td class=" "><?=$data['point']; ?></td>
									<td class=" "><?=date('d M, Y', strtotime($data['created'])); ?></td>
									<td class=" ">
										<?php
											if($data['type']=='C'){
												echo "Challenge";
											}elseif($data['type']=='S'){
												echo "Top Singer Song";
											}elseif($data['type']=='D'){
												echo "Top DJ Song";
											}elseif($data['type']=='T'){
												echo "App Uses Time";
											}
										 ?>
									</td>
								
								</tr>
								
								<?php  $count++;
							} ?> 
							
							<?php  if(count($HistoryRewards->toArray()) < 1) {
								echo "<tr><th colspan = '6'>No record found</th></tr>";
							} ?>	
							
							
                         </tbody>
                      </table>
                      <?php $this->Paginator->options(array('url' => array('controller' => 'UserRewards', 'action' => 'search')));
						echo "<div class='pagination' style = 'float:right'>";
	 
						// the 'first' page button
						$paginator = $this->Paginator;
						echo $paginator->first("First");

						// 'prev' page button, 
						// we can check using the paginator hasPrev() method if there's a previous page
						// save with the 'next' page button
						if($paginator->hasPrev()){
						echo $paginator->prev("Prev");
						}

						// the 'number' page buttons
						echo $paginator->numbers(array('modulus' => 2));

						// for the 'next' button
						if($paginator->hasNext()){
						echo $paginator->next("Next");
						}

						// the 'last' page button
						echo $paginator->last("Last");

						echo "</div>";
								
						?>
						
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->script(
	array(
		'Admin/moment.min.js',
		'Admin/daterangepicker.js'
	));
	echo $this->fetch('script');
?>
<script>	
$(document).ready(function() {
        
	$('#start-date').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		format: 'YYYY-MM-DD',
		maxDate:0

	});
	$('#end-date').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		format: 'YYYY-MM-DD',
		maxDate:0

	});
});

function delete_bank(id){
	bootbox.confirm("Do you want to delete the detail ?", function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'UserRewards','action'=>'deletehistory']); ?>',
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery("#bank_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record Delete successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},error: function (request) {
					new PNotify({
						title: 'Error',
						text: 'This record is being referenced in other place. You cannot delete it.',
						type: 'error',
						styling: 'bootstrap3',
						delay:1200
					});
				},
			});
		}
	});
}
function change_bank_status(id){
	var status = $("#bank_status_"+id).val();
	if(status == 'P'){
		var ques= "Do you want Make this ad ONLINE";	
		var status = "Y";
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>'
	}else if(status == 'Y'){
		var ques= "Do you want change the status to DEACTIVE";	
		var status = "N";
		var change = '<button type="button" class="btn btn-danger btn-xs">Deactive</button>'
	}else{
		var ques= "Do you want change the status to ACTIVE";
		var status = "Y";
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'UserRewards','action'=>'status']); ?>',
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						if(status =='Y'){
							
							$("#webpage"+id).html('<a target="_blank" class="btn btn-info btn-xs" href="'+$("#web_url_"+id).val()+'"><i class="fa fa-globe"></i> Webpage </a>');
						}else{
							$("#webpage"+id).html('');
						}
						jQuery("#status_id_"+id).html(change);
						jQuery("#bank_status_"+id).val(status);
						new PNotify({
							title: 'Success',
							text: 'Status changed successfully!',
							type: 'success',
							styling: 'bootstrap3',
							delay:1200
						});
						
					}
					if(data == 'forbidden'){
						
						new PNotify({
							title: '403 Error',
							text: 'You donot have permission to access this action.',
							type: 'error',
							styling: 'bootstrap3',
							delay:1200
						});
					}
				},
				error: function (request) {
					new PNotify({
						title: 'Error',
						text: 'Invalid request!',
						type: 'error',
						styling: 'bootstrap3',
						delay:1200
					});
				},
			});
		}
	});

}
	
jQuery('.table-responsive').on('click','.pagination li a',function(event){
	event.preventDefault() ;
	if($(this).parent().hasClass('active')){
		return false;
	}
	var keyy = $('form').serialize();
	var urli = jQuery(this).attr('href');
	jQuery.ajax({ 
		url: urli,
		data: {key:keyy},
		type: 'POST',
		success: function(data) {
			if(data){
				jQuery('.table-responsive').html(data);
			}
		}
	});
});
		
</script>
