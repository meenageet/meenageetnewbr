
<div class="right_col" role="main">
          <div class="">
			
            <div class="page-title">
				<?= $this->Flash->render() ?>
              <div class="title_left">
                <h3>
                      Social 
                      <small>
                         Listing
                      </small>
                  </h3>
              </div>

              <div class="title_right">
				  
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group" style = "display:none;">
                    
                    <div class="input text"><input type="text" id="search" placeholder="Search for..." class="form-control" name="key"></div>
					<span class="input-group-btn"><button class="btn btn-default" type="button" onclick = "search_result();">Go!</button></span>
                  </div>
                </div>
				
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							<th class="column-title">S.No. </th>
							<th class="column-title">Title </th>
							<th class="column-title">Url</th>
							<th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php 
							$count = 1;
							foreach($SocialLinks->toArray() as $data){ ?>
							<tr>
								<td><?= $count?>.</td>
								<td class=" "><?php 
								echo ucwords($data['title']) ?></td>
								<td class=" "><?php echo $data['url']; ?> </td>
								<td class=" last">
									<a class="btn btn-info btn-xs" href="<?php echo $this->Url->build(['controller'=>'Settings','action'=>'sedit',$data['id']]); ?>"><i class="fa fa-pencil"></i> Edit </a>
	
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($SocialLinks->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

