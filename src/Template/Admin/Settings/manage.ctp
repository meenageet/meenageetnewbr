
<div class="right_col" role="main">
          <div class="">
			
            <div class="page-title">
				<?= $this->Flash->render() ?>
              <div class="title_left">
                <h3>
                      Setting
                      <small>
                         Listing
                      </small>
                  </h3>
              </div>

              <div class="title_right">
				  
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                   <div class="input text"><input type="text" id="search" placeholder="Search for..." onKeyup = "search_result();" class="form-control" name="key"></div>
					<span class="input-group-btn" style='display:none;'><button class="btn btn-default" type="button" onclick = "search_resudlt();">Go!</button></span>
                  </div>
                </div>
				
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
							<th class="column-title">S.No. </th>
							<th class="column-title">Module </th>
							<th class="column-title">value</th>
							<th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php 
							$count = 1;
							foreach($Settings->toArray() as $data){ ?>
							<tr>
								<td><?= $count?>.</td>
								<td class=" "><?php 
								echo ucwords($data['module_name']) ?></td>
								<td class=" "><?= $data['value']?></td>
								<td class=" last">
									<a class="btn btn-info btn-xs" href="<?php echo $this->Url->build(['controller'=>'Settings','action'=>'edit',$data['id']]); ?>"><i class="fa fa-pencil"></i> Edit </a>
	
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($Settings->toArray()) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                      <?php $this->Paginator->options(array('url' => array('controller' => 'Settings', 'action' => 'search')));
					echo "<div class='pagination' style = 'float:right'>";
 
					// the 'first' page button
					$paginator = $this->Paginator;
					echo $paginator->first("First");

					// 'prev' page button, 
					// we can check using the paginator hasPrev() method if there's a previous page
					// save with the 'next' page button
					if($paginator->hasPrev()){
					echo $paginator->prev("Prev");
					}

					// the 'number' page buttons
					echo $paginator->numbers(array('modulus' => 2));

					// for the 'next' button
					if($paginator->hasNext()){
					echo $paginator->next("Next");
					}

					// the 'last' page button
					echo $paginator->last("Last");

					echo "</div>";
							
					?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<script>
function search_result(){
			
			var key = jQuery("#search").val();
			
			
			jQuery.ajax({ 
						url: '<?php echo $this->Url->build(['controller'=>'Settings' , 'action'=>'search']);  ?>',
						data: {'key':key},
						type: 'POST',
						success: function(data) {
							if(data){
								
								jQuery('.table-responsive').html(data);
								
							}
						}
			});
			
		}
		
		jQuery('.table-responsive').on('click','.pagination li a',function(event){
			event.preventDefault() ;
			var keyy = jQuery('#search').val();
			var urli = jQuery(this).attr('href');
			jQuery.ajax({ 
						url: urli,
						data: {key:keyy},
						type: 'POST',
						success: function(data) {
							if(data){
								
								jQuery('.table-responsive').html(data);
								
							}
						}
			});
			
		});
	</script>

