<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				  <?php echo $this->Form->create($SocialLinks,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
					<?= $this->Flash->render() ?>
                      <span class="section">Edit </span>
					    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Title 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<?php echo $SocialLinks->title;  ?>
                        </div>
                      </div>
                     
                       <div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Url 
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<?php  echo $this->Form->input('url',array('class' => 'form-control col-md-7 col-xs-12','label' =>false,"type"=>"text")); ?>
							</div>
						</div>
					
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							<?php  echo $this->Form->button('Reset', ['type' => 'reset','class'=>'btn btn-primary']); ?>
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
	
