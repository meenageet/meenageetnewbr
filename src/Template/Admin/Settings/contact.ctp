<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
					   <?php echo $this->Form->create($contact_us,array('class'=>'form-horizontal form-label-left','novalidate','method'=>'post','enctype'=>'multipart/form-data'));?>
				  
					<?= $this->Flash->render() ?>
			    <div id="form2">
                    <!--Email -->
				 
				  
					
                      <span class="section">Email</span>
                      <?php if(!empty($email)){ 
						foreach($email as $val){?>
							 <div class="form-group" id="div_<?php echo $val['id']; ?>">
								
								
							<div class="col-xs-4 item">
								<?php  echo $this->Form->input('email[id][]',array("type"=>"hidden",'value'=>$val['id'])); ?>
							<?php  echo $this->Form->input('email[value][]',array('class' => 'form-control ','label' =>false,"type"=>"email",'placeholder'=>'eg: hello@ipix.com','value'=>$val['value'])); ?>
							</div>
							<div class="col-xs-1">
								<button type="button" id="<?php echo $val['id']; ?>" class="btn btn-default remove_value"><i class="fa fa-minus"></i></button>
								</div>
							</div>
						<?php }}else{ ?>
							 <div class="form-group">
						
							<div class="col-xs-4 item">
							<?php  echo $this->Form->input('email[value][]',array('class' => 'form-control ','label' =>false,"type"=>"email",'placeholder'=>'eg: hello@ipix.com')); ?>
							</div>
						</div>
							<?php }?>
					   
						 <div class="form-group hide" id="bookTemplate2">
							  <div class="col-xs-4">
									<?php  echo $this->Form->input('email[value][]',array('class' => 'form-control ','label' =>false,"type"=>"email",'placeholder'=>'eg: hello@LocalDunia.com')); ?>
							</div>
							<div class="col-xs-1">
							<button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
							</div>
					  </div>
					 <div class="form-group ">
							<div class="col-xs-4">
						<button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
						</div>
						
						</div>
						
                </div>    
                <div id="form3">
                    <!--Phone -->
					<span class="section">Phone Number</span>
					 <?php if(!empty($contact)){ 
						foreach($contact as $val){?>
							<div class="form-group" id="div_<?php echo $val['id']; ?>">
							
							<div class="col-xs-4 item">
								<?php  echo $this->Form->input('phone[id][]',array("type"=>"hidden",'value'=>$val['id'])); ?>
							
									<?php  echo $this->Form->input('phone[value][]',array('class' => 'form-control numbers ','label' =>false,"type"=>"text",'placeholder'=>'eg: 123450000','value'=>$val['value'])); ?>
							</div>
						
							<div class="col-xs-1">
							<button type="button" id="<?php echo $val['id']; ?>" class="btn btn-default remove_value"><i class="fa fa-minus"></i></button>
						</div>
						</div>
					<?php }}else{ ?>
						<div class="form-group">
						
						<div class="col-xs-4 item">
							<?php  echo $this->Form->input('phone[value][]',array('class' => 'numbers form-control ','label' =>false,"type"=>"text",'placeholder'=>'eg: 123450000')); ?>
						</div>
					
					</div>
					<?php }?>	
					
					
						
					
					
					<div class="form-group hide" id="bookTemplate3">
							 <div class="col-xs-4 item">
								<?php  echo $this->Form->input('phone[value][]',array('class' => 'numbers form-control ','label' =>false,"type"=>"text",'placeholder'=>'eg: +123450000')); ?>
							</div>
							
							<div class="col-xs-1">
							<button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
							</div>
					 </div>
					 <div class="form-group ">
							<div class="col-xs-4">
						<button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
						</div>
						
						</div>

				</div>		
                   
                  
                    
                    
                  </div>
					
                      <div class="form-group">
                        <div class="col-md-6">
							
							<?php  echo $this->Form->button('Submit', ['type' => 'submit','class'=>'btn btn-success']); ?>
                        </div>
                      </div> 
                 </form>
                
                
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
	
<script>
	
	
		$('.numbers').keypress(function(event) {
  /*if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }*/
		var charCode = event.which;

        if(charCode == 8 || charCode == 0)
        {
             return;
        }
        else
        {
            var keyChar = String.fromCharCode(charCode); 
            return /[0-9]/.test(keyChar); 
        }
  
});
$(".remove_value").click(function(){
	var id = $(this).attr('id');
	
	bootbox.confirm("Do you want to delete this record ?", function(result) {
		if(result == true){
			
			jQuery.ajax({ 
				url: '<?php echo $this->Url->build(['controller'=>'Settings','action'=>'delete']); ?>',
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					if(data == 1){
						$("#div_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record Delete successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					
				}
			});
		}
	});
	
	
	
})
$(document).ready(function() {
	 bookIndex = 0;
   
	 $('#form2')
		// Add button click handler
		.on('click', '.addButton', function() {
	
		bookIndex++;
		var $template = $('#bookTemplate2'),
			$clone    = $template
							.clone()
							.removeClass('hide')
							.addClass('new-email')
							.removeAttr('id')
							.attr('data-book-index', bookIndex)
							.insertBefore($template);

		})

		// Remove button click handler
		.on('click', '.removeButton', function() {

		var $row  = $(this).parents('.new-email'),
			index = $row.attr('data-book-index');
		//alert($row);
		// Remove element containing the fields
		$row.remove();
		})
		
		$('#form3')
		// Add button click handler
		.on('click', '.addButton', function() {
			//alert("asa");

		bookIndex++;
		var $template = $('#bookTemplate3'),
			$clone    = $template
							.clone()
							.removeClass('hide')
							.addClass('new-phone')
							.removeAttr('id')
							.attr('data-book-index', bookIndex)
							.insertBefore($template);

		})

		// Remove button click handler
		.on('click', '.removeButton', function() {

		var $row  = $(this).parents('.new-phone'),
			index = $row.attr('data-book-index');
		//alert($row);
		// Remove element containing the fields
		$row.remove();
		})
		
		 
});
</script>
