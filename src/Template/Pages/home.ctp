<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <section class="masthead relative indexbg" id="home">
    <div class="container-fluid h-100 head_banner_wrap">
      <div class="row h-100">
        <div class="col-lg-4 my-auto">
          <div class="header-content mx-auto spacer_left text-left">
            <h2 class="heading_blue">Why Consagous</h2>  
            <h3 class="mb-5">Consagous is the preeminent app for first time fathers.<br/>We provide easy to read preparation resources for expecting dads and content examining our changing rolr in the modern America family.Burn the baby books.You wont read them or need them.<br/>Join our movement</h3>
          </div>
        </div>
        <div class="col-lg-8 my-auto">
          <div class="">
            <div class="device-mockup iphone6_plus portrait white">
              <div class="device">
                <div class="screen text-right right-spacer">
                  <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                  <img src="img/banner-img.png" class="img-fluid" alt="">
                </div>
                <div class="button">
                  <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="redirect_text">
      <h2>Know More about us</h2>
      <a href="#about" class=" js-scroll-trigger"><img src="img/down-arrow.png" class="vert-move"></a>
    </div>
  </section>
  <section class="download about_bg text-center" id="about">
    <div class="container">
      <div class="row  mx-auto">
        <div class="col-md-4 text-center">
          <p><img src="img/required-post.png" alt="required post"/></p>
          <h3><b> Create your post</b></h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a </p>
        </div>
        <div class="col-md-4 text-center">
          <p><img src="img/location.png" alt="location"/></p>
          <h3><b> Location wise listing</b></h3>
          <p>DLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a</p>
        </div>
        <div class="col-md-4 text-center">
          <p><img src="img/buyer.png" alt="buyer"/></p>
          <h3><b> Connect to seller</b></h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a </p>
        </div>

        
      </div>
     </div>
   </section>
	<section class="features" id="contact">
		<div class="container">
			<div class="section-heading text-center ">
				<h2 class="heading_blue">Popular Cities and Area</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a</p>
				<div class="search_wrap">
					<?php //echo $this->Form->create('Forum', array('novalidate', 'method' => 'post', 'action' => 'forums/area_list')); ?>
					<?php echo $this->Form->create(null, ['url' => ['controller' => 'Forums', 'action' => 'area_list']]); ?>
						<div class="form-group relative">
							<?php echo $this->Form->input('keyword', array('class' => 'form-control', 'label' => false, 'type' => 'search', 'placeholder' => 'Popular Cities')); ?>
							<?php echo $this->Form->input('Search', ['type' => 'submit', 'name' => 'search', 'value' => 'Search']); ?>
						</div>
					<?php echo $this->Form->end(); ?>
            
					<!--<form action="" method="post">
					  <div class="form-group relative">
						<input type="search" name="cities" placeholder="Popular Cities/State" class="form-control">
						<input type="submit" name="search" value="Search">
					  </div>
					</form>-->
            
				</div>
			</div>
        <div class="popular_city_wrap">
			<?php if(!empty($Countries)) { //echo '<pre>'; print_r($States); die; 
				 foreach($Countries as $citydata) {
				 ?>
					<ul class="list-unstyled ">
						<li>
						  <h2><?php echo $citydata['country_name']; ?></h2>
						</li>
						<?php if(!empty($citydata['cities'])) { ?>
							<li>
								<div class="city_list">
									<?php foreach($citydata['cities'] as $citydataval) { ?>
										<a href="<?=$this->Url->build(["controller"=>"forums","action"=>"area_list", $citydataval['slug']]);?>" style="color:#2e252f;"><p><?php echo $citydataval['city_name']; ?></p></a>
									<?php } ?>
								</div>
							</li>
						<?php } ?>
					</ul>
				<?php } 
			} ?>
          <!--<ul class="list-unstyled ">
            <li>
              <h2>California</h2>
            </li>
            <li>
              <div class="city_list">
                <p>Los Angeles</p>
                <p>San Diego</p>
                <p>San Jos</p>
                <p>Fresno</p>
                <p>San Francisco</p>
                <p>Long beach</p>
                <p>Oakland</p>
                <p>Santa Ana</p>
              </div>
            </li>
          </ul>
          <ul class="list-unstyled ">
            <li>
              <h2>California</h2>
            </li>
            <li>
              <div class="city_list">
                <p>Los Angeles</p>
                <p>San Diego</p>
                <p>San Jos</p>
                <p>Fresno</p>
                <p>San Francisco</p>
                <p>Long beach</p>
                <p>Oakland</p>
                <p>Santa Ana</p>
              </div>
            </li>
          </ul>
          <ul class="list-unstyled ">
            <li>
              <h2>California</h2>
            </li>
            <li>
              <div class="city_list">
                <p>Los Angeles</p>
                <p>San Diego</p>
                <p>San Jos</p>
                <p>Fresno</p>
                <p>San Francisco</p>
                <p>Long beach</p>
                <p>Oakland</p>
                <p>Santa Ana</p>
              </div>
            </li>
          </ul>
          <ul class="list-unstyled ">
            <li>
              <h2>California</h2>
            </li>
            <li>
              <div class="city_list">
                <p>Los Angeles</p>
                <p>San Diego</p>
                <p>San Jos</p>
                <p>Fresno</p>
                <p>San Francisco</p>
                <p>Long beach</p>
                <p>Oakland</p>
                <p>Santa Ana</p>
              </div>
            </li>
          </ul>
          <ul class="list-unstyled ">
            <li>
              <h2>California</h2>
            </li>
            <li>
              <div class="city_list">
                <p>Los Angeles</p>
                <p>San Diego</p>
                <p>San Jos</p>
                <p>Fresno</p>
                <p>San Francisco</p>
                <p>Long beach</p>
                <p>Oakland</p>
                <p>Santa Ana</p>
              </div>
            </li>
          </ul>
          <ul class="list-unstyled ">
            <li>
              <h2>California</h2>
            </li>
            <li>
              <div class="city_list">
                <p>Los Angeles</p>
                <p>San Diego</p>
                <p>San Jos</p>
                <p>Fresno</p>
                <p>San Francisco</p>
                <p>Long beach</p>
                <p>Oakland</p>
                <p>Santa Ana</p>
              </div>
            </li>
          </ul>
          <ul class="list-unstyled ">
            <li>
              <h2>California</h2>
            </li>
            <li>
              <div class="city_list">
                <p>Los Angeles</p>
                <p>San Diego</p>
                <p>San Jos</p>
                <p>Fresno</p>
                <p>San Francisco</p>
                <p>Long beach</p>
                <p>Oakland</p>
                <p>Santa Ana</p>
              </div>
            </li>
          </ul>
          <ul class="list-unstyled ">
            <li>
              <h2>California</h2>
            </li>
            <li>
              <div class="city_list">
                <p>Los Angeles</p>
                <p>San Diego</p>
                <p>San Jos</p>
                <p>Fresno</p>
                <p>San Francisco</p>
                <p>Long beach</p>
                <p>Oakland</p>
                <p>Santa Ana</p>
              </div>
            </li>
          </ul>
          <ul class="list-unstyled ">
            <li>
              <h2>California</h2>
            </li>
            <li>
              <div class="city_list">
                <p>Los Angeles</p>
                <p>San Diego</p>
                <p>San Jos</p>
                <p>Fresno</p>
                <p>San Francisco</p>
                <p>Long beach</p>
                <p>Oakland</p>
                <p>Santa Ana</p>
              </div>
            </li>
          </ul>-->
          
        </div>
      </div>
    </section>
	
	<?php if(!empty($posts)) { ?>
		<section class="post" id="howweroll">
			<div class="container-fluid">
				<div class="section-heading text-left">
					<h2 class="color_blue">Recent Post</h2>
				</div>
				<div class="post_wrap row">
					<?php foreach($posts as $postdata) { ?>
						<div class="col-md-3 col-sm-12 col-xs-12">
							 <a href="<?=$this->Url->build(["controller"=>"forums", "action"=>"post_detail", $postdata['id']]);?>" class="">
							<div class="post_block">
								<div class="post_heading text-center">
									<p><?php echo $postdata->title; ?></p>
								</div>
								<div class="post_data">
									<div class="post_title">
										<p><?php echo substr($postdata->description, 0, 35).'...'; ?></p>
										<small>
											<?php echo isset($postdata->country->country_name) ? $postdata->country->country_name.' > ' :'';?>
											<?php echo isset($postdata->state->state_name) ? $postdata->state->state_name.' > ' :'';?>
											<?php echo isset($postdata->city->city_name) ? $postdata->city->city_name.' > ' :'';?>
											<?php echo isset($postdata->area->area_name) ? $postdata->area->area_name : '' ;?>
										</small> 
									</div>
									<div class="post_image">
										<?php if($postdata->post_image[0]->image_name!='') { ?>
											<img src="<?php echo $this->request->webroot.'uploads/post_image/'.$postdata->post_image[0]->image_name ;?>"   class="img-responsive padding-20" alt="recent post">
										<?php } else { ?>
											<img src="<?php echo $this->request->webroot.'img/user-img.jpg' ;?>"  class="img-responsive padding-20" alt="recent post">
										<?php } ?>
									</div>
								</div>
							</div>
						
							</a>
						</div> 
					<?php } ?>
				</div>
			</div>
		</section>
    <?php } ?>
    
    <section class="features" id="download">
      <div class="container">
        <div class="section-heading text-center">
          <h2 class="color_blue">Download Application</h2>
        </div>
        <div class="row">

          <div class="col-lg-5 my-auto text-center">
            <img src="img/download-app-img.png" alt="download" class="img-responsive" />
          </div>
          <div class="col-lg-7 my-auto">
            <div class="clearfix ml-70 mt-20 download_data">
             <p><b>The quickest way to immerse youself in the mission of preparing for fatherfood. Build your confidence on your path to becoming the talented and involved dad you want to be.</b></p>
             <ol class="download_listing">
              <li>Free to download</li>
              <li>Save time with quick summaries</li>
              <li>Become an expert with in-depth analysis</li>
              <li>Alleviated anxiety when you know what you know what's to come</li>
            </ol>
           
            <a href="#"><img src="img/download-app.png" alt="download app" class="img_100"></a>
          </div>
        </div>
      </div>
    </div>
    </section>


