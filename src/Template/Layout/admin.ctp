
<!DOCTYPE html>
<html lang="en">
  <head>
   

	<title><?= $title?></title>
	<?php
		
		echo $this->Html->meta('favicon.ico','images/favicon.png',array('type' => 'icon'));
		echo $this->Html->css(array(
						'Admin/bootstrap/dist/css/bootstrap.min.css',
						'Admin/font-awesome/css/font-awesome.min.css',
						'Admin/pnotify.css',
						
						'Admin/custom.css'
						
					));
		echo $this->Html->script(
				array(
					'Admin/jquery.min.js',
					
				
				));
				echo $this->Html->script(
				array(
					'Admin/dist/id3-minimized.js',
					
				
				));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		
	?>
	<script>
		$(document).ajaxStart(function(){
		    $("div#divLoading").addClass('show');
		});
		$(document).ajaxComplete(function(){
			 $("div#divLoading").removeClass('show');
		});

	</script>
  </head>
  
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php echo $this->element('Admin/left_sidebar'); ?>
		<?php echo $this->element('Admin/header'); ?> 
		<?php echo $this->fetch('content'); ?>
        <?php echo $this->element('Admin/footer'); ?>
	  </div>
    </div>
  </body>
</html>
