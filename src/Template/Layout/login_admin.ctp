<!DOCTYPE html>
<html>
  <head>
	<title><?= $title?></title>
	<?php
		
		echo $this->Html->meta('favicon.ico','images/favicon.png',array('type' => 'icon'));
		echo $this->Html->css(array(
						'Admin/bootstrap.min.css',
						'Admin/font-awesome/css/font-awesome.min.css',
						'Admin/custom.css'
					));
			
	
		echo $this->fetch('meta');
		echo $this->fetch('css');
		
	?>
  </head>

  <body style="background:#F7F7F7;">
    <?php echo $this->fetch('content');  ?>
  </body>
</html>
