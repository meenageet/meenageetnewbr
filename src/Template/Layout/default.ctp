<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title><?= $title?></title>
	 <meta name="google-signin-scope" content="profile email">
     <meta name="google-signin-client_id" content="686629995394-9uo2jcpuktko9g44nhbh97cosfeo1uci.apps.googleusercontent.com">
	<?php
		
		echo $this->Html->meta('favicon.ico','images/favicon.ico',array('type' => 'icon'));
		echo $this->Html->css(array(
						'vendor/bootstrap/css/bootstrap.min.css',
						'vendor/font-awesome/css/font-awesome.min.css',
						'vendor/simple-line-icons/css/simple-line-icons.css',
						'style.css',
						'contactus.css',
						'gridtab.min.css',
						'form-wizard-blue.css',
						'chat.css',
					));
		echo $this->Html->script(
				array(
					'Admin/jquery.min.js',
					'Admin/jquery.geocomplete.js',
					
				));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		
	?>
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

  </head>
  <style>

 
  </style>
  <body  id="page-top">
	  
	<?php // echo $this->element('header'); ?> 
	<?php echo $this->fetch('content'); ?>
    <?php // echo $this->element('footer'); ?>
  </body>
</html>
