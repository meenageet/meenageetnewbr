<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title><?= $title?></title>
	 <meta name="google-signin-scope" content="profile email">
     <meta name="google-signin-client_id" content="686629995394-9uo2jcpuktko9g44nhbh97cosfeo1uci.apps.googleusercontent.com">
	<?php
		
		echo $this->Html->meta('favicon.ico','images/favicon.ico',array('type' => 'icon'));
		echo $this->Html->css(array(
						'chatcss/main.css',
						'chatcss/dashboard.css',
						'chatcss/dialogs.css',
						'chatcss/reset.css',
						'chatcss/login.css',
					));
		echo $this->Html->script(
				array(
					'chatjs/quickblox.min.js',
					'Admin/jquery.min.js',					
				));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		
	?>
	<script src="https://unpkg.com/navigo@4.3.6/lib/navigo.min.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore.js" defer></script>
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

  </head>
  <style>

 
  </style>
  <body  id="page-top">
	  
	<?php // echo $this->element('header'); ?> 
	<?php echo $this->fetch('content'); ?>
    <?php // echo $this->element('footer'); ?>
  </body>
</html>
