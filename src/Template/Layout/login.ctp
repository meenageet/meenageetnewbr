<!DOCTYPE html>
<html>
  <head>
	<title><?= $title?></title>
	<?php
		
		echo $this->Html->meta('favicon.ico','images/favicon.png',array('type' => 'icon'));
		echo $this->Html->css(array(
						'vendor/bootstrap/css/bootstrap.min.css',
						'vendor/font-awesome/css/font-awesome.min.css',
						'loginsignup.css',
					));
		echo $this->Html->script(
				array(
					'Admin/jquery.min.js',
					'Admin/jquery.geocomplete.js',
					
				));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		
	?>
  </head>

  <body style="background:#F7F7F7;">
    <?php echo $this->fetch('content');  ?>
  </body>
</html>
