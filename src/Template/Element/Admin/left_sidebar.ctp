<style>
.site_title .img_logo {
    width: 70%;
    margin-left: 7%;
    z-index: 1000;
    position: inherit;
    margin-top: 0px;
    border: 1px solid rgba(52, 73, 94, 0.44);
    padding: 2px;
    border-radius: 10%;
}
.minileft_dasimg img {
    width: 30px;
}

</style>
<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
		<div class="navbar nav_title" style="border: 0;">
			<a href="<?php echo $this->Url->build(['controller'=>'pages','action'=>'dashboard']); ?>" class="site_title"><strong class="minileft_dasimg"><img src="<?php echo $this->request->webroot.'img/logom.png'; ?>" alt="..."></strong> <span><img src="<?php echo $this->request->webroot.'img/logo.png'; ?>" alt="..." class="img_logo"></span></a>
        </div>

        <div class="clearfix"></div>
			<!-- menu profile quick info -->
            <div class="profile">
				<div class="profile_pic">
					<img src="<?php echo $this->request->webroot.'uploads/user/'.$authUser['image']; ?>" alt="..." class="img-circle profile_img">
				</div>
				<div class="profile_info">
					<span>Welcome,</span>
					<h2><?php echo $authUser['username'];?></h2>
				</div>
            </div>
            <!-- /menu profile quick info -->
			<br />
 
			<!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
				<div class="menu_section">
					<h3><?php echo $project_title ;?></h3>
					<ul class="nav side-menu">
						<li>
							<a href="<?php echo $this->Url->build(["controller" => "pages","action" => "dashboard"]); ?>"><i class="fa fa-home"></i> Summary </a>
						</li>
						<li>
							<a href="<?php echo $this->Url->build(["controller" => "pages","action" => "manageTheme"]); ?>"><i class="fa fa-fort-awesome"></i> Manage Themes </a>
						</li>
										 
						<!----- subscription menu------- -->
						<li>
							<a><i class="fa fa-strikethrough"></i>Subscription Plans <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								 <li><a href="<?php echo $this->Url->build(['controller'=>'premiums' ,'action' =>'add']); ?>">Add premium</a></li>
								<li><a href="<?php echo $this->Url->build(['controller'=>'premiums' ,'action' =>'manage']); ?>">Manage premium</a></li>
							</ul>
						</li>
						
						<!----- banner menu------- -->
						<li>
							<a><i class="fa fa-tag"></i>Banners <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'banners' ,'action' =>'add']);  ?>">Add</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'banners' ,'action' =>'manage']); ?>">Manage</a>
								</li>
							</ul>
						</li>
						
						<!----- User menu------- -->
						<li>
							<a><i class="fa fa-users"></i>Users <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'users' ,'action' =>'add']);  ?>">Add</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'users' ,'action' =>'manage']); ?>">Manage</a>
								</li>
								<!--li><a href="<?php echo $this->Url->build(['controller'=>'premiums' ,'action' =>'users']); ?>">Packages</a></li -->
							</ul>
						</li>
						
						<!----- category menu------- -->
						<li><a><i class="fa fa-sitemap"></i> Categories <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<!--<li><a href="<?php echo $this->Url->build(['controller'=>'parent_categories' ,'action' =>	'manage']);  ?>">Parent Category</a></li> -->
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'categories' ,'action' =>'add']);  ?>">Add</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'categories' ,'action' =>'manage']);  
								?>">Manage</a>
								</li>
							</ul>
						</li>
						
						<!----- Post menu------- -->
						<li><a><i class="fa fa-sticky-note"></i> Posts <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'posts' ,'action' =>'pendingManage']);  
								   ?>">Pending Post Lists</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'posts' ,'action' =>'manage']);  
								   ?>">Public  Post Lists</a>
								</li>
							</ul>
						</li>
						
						<!----- Post menu------- -->
						<li><a><i class="fa fa-trophy"></i> Challenges <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'challenges' ,'action' =>'manage']);  
								   ?>">All Challenges</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'challenges' ,'action' =>'winnerChallenge']);  
								   ?>">Top Winner Challenges</a>
								</li>
							</ul>
						</li>
						
						<!----- request singer & DJ menu------- -->
						<li><a><i class="fa fa-send-o"></i> Request for Singer / DJ <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'users' ,'action' =>'requestList']);  
								   ?>">Approve Request List</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'users' ,'action' =>'requestnotApprovedList']);  
								   ?>">Not Approved Request List</a>
								</li>
							</ul>
						</li>
						
						<!----- Song menu------- -->
						<li>
							<a><i class="fa fa-music"></i> Songs List <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'songUploads' ,'action' =>'singerManage']);  
								   ?>">Singer Songs List</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'songUploads' ,'action' =>'djManage']);  
								   ?>">Dj Songs List</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'songUploads' ,'action' =>'manage']);  
								   ?>">All Songs List</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'songUploads' ,'action' =>'add']);  
								   ?>">Add Song</a>
								</li>
								
							</ul>
						</li>
						
						<!----- Like menu------- -->
						<li>
							<a href="<?php echo $this->Url->build(["controller" => "songUploads","action" => "like"]); ?>"><i class="fa fa-thumbs-up"></i> Likes </a>
						</li>
						
						<!----- Like menu------- -->
						<li>
							<a href="<?php echo $this->Url->build(["controller" => "songUploads","action" => "unlike"]); ?>"><i class="fa fa-thumbs-down"></i> Unlikes </a>
						</li>
						
						
						<!----- Comment menu------- -->
						<li>
							<a href="<?php echo $this->Url->build(["controller" => "songUploads","action" => "comments"]); ?>"><i class="fa fa-comments"></i> Comments </a>
						</li>
						
						<!----- Download song list menu------- -->
						<li>
							<a><i class="fa fa-download"></i>Downloads Songs List <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'downloads' ,'action' =>'manage']);  
								   ?>">Manage</a>
								</li>
							</ul>
						</li>
						
						<!----- User Rewards list menu------- -->
						<li>
							<a><i class="fa fa-gift"></i>Rewards List <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'user_rewards' ,'action' =>'top_singer_songs']);  
								   ?>">Top 10 Singer Songs</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'user_rewards' ,'action' =>'top_dj_songs']);  
								   ?>">Top 10 DJ Songs</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'user_rewards' ,'action' =>'top_task_rewards']);  
								   ?>">Last 10 Days Task Rewards</a>
								</li>
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'user_rewards' ,'action' =>'manage']);  
								   ?>">Manage Rewards</a>
								</li>	
								
							</ul>
						</li>
						
						<!----- User Rewards list menu------- -->
						<li>
							<a><i class="fa fa-money"></i>Payment Details <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'payment_details' ,'action' =>'manage']);  
								   ?>">Manage</a>
								</li>
							</ul>
						</li>
						
						<!----- Send Bulk Email menu------- -->
						<li>
							<a><i class="fa fa-comments"></i> Send Bulk Email <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'sms' ,'action' =>'index']);  ?>">manage</a>
								</li>
							</ul>
						</li> 
						
						
						<!----- Transactions List menu------- -->
						<li>
							<a><i class="fa fa-rupee"></i> Transactions List <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'transactions' ,'action' =>'manage']);  ?>">manage</a>
								</li>
							</ul>
						</li> 
						
						
						
						<!----- Contact list menu------- -->
						<!--li>
							<a><i class="fa fa-question-circle-o"></i> Contact Us <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li>
									<a href="<?php echo $this->Url->build(['controller'=>'contact_us' ,'action' =>'manage']);  ?>">manage</a>
								</li>
							</ul>
						</li --> 
						
						<!----- CMS pages menu------- -->
						<li>
							<a><i class="fa fa-cog"></i>Cms pages<span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu"> 
								<li><a href="<?php echo $this->Url->build(['controller'=>'pages','action'=>'manage']);  ?>">Cms pages</a></li> 
							</ul>
						</li>
						
						<!----- admin menu------- -->
						<li>
							<a><i class="fa fa-cog"></i>Admin Settings<span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu"> 
								<li><a href="<?php echo $this->Url->build(['controller'=>'pages','action'=>'manage_reward_top_singer']);  ?>">Manage Rewards of top singer</a></li>
								<li><a href="<?php echo $this->Url->build(['controller'=>'pages','action'=>'manage_reward_top_dj']);  ?>">Manage Rewards of top dj</a></li>  
								<li><a href="<?php echo $this->Url->build(['controller'=>'users','action'=>'manage_reward_time']);  ?>">Manage Rewards Time</a></li> 
								<li>
								  <a href="<?php echo $this->url->build(['controller' => 'users' , 'action' => 'minimumrewards']); ?>">Change minimum reward points</a>
								</li>
								<li>
								  <a href="<?php echo $this->url->build(['controller' => 'users' , 'action' => 'freesubscription']); ?>">Change free subscription duration</a>
								</li>
								 <li>
								  <a href="<?php echo $this->url->build(['controller' => 'users' , 'action' => 'appusetime']); ?>">Change app use time</a>
								</li>
								<li><a href="<?php echo $this->url->build(['controller'=>'users','action'=>'logout']); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
								</li> 
							</ul>
						</li>
					</ul>
				</div>
             
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
				<a target="_blank" data-toggle="tooltip" data-placement="top" title="Website" href = "<?php echo $this->Url->build(['controller'=>'pages','action'=>'home','prefix'=>false]); ?>">
					<span class="fa fa-desktop" aria-hidden="true"></span>
				</a>
				<a data-toggle="tooltip" data-placement="top" title="Settings" href = "<?php echo $this->Url->build(['controller'=>'settings','action'=>'manage']); ?>">
					<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
				</a>
				<a data-toggle="tooltip" data-placement="top" title="FullScreen" href = "javascript:;">
					<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
				</a>
				<a href = "<?php echo $this->Url->build(['controller'=>'users','action'=>'logout']); ?>" data-toggle="tooltip" data-placement="top" title="Logout">
					<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
				</a>
            </div>
            <!-- /menu footer buttons -->
	</div>
</div>  
