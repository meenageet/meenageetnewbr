<div class="top_nav">
		<div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $this->request->webroot.'uploads/user/'.$authUser['image']; ?>" alt=""><?php echo $authUser['username'];?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
					<li>
                      <a href="<?php echo $this->url->build(['controller' => 'users' , 'action' => 'changeProfile']); ?>">Change Profile Image</a>
                    </li>
                    <li>
                      <a href="<?php echo $this->url->build(['controller' => 'users' , 'action' => 'changepassword']); ?>">Change password</a>
                    </li>
                    
                    
                  </ul>
                </li>
				<li style="margin-top:20px"><?php echo  date('D jS\, M Y g:i A');?></li>
              </ul>
            </nav>
          </div>

        </div>
        <!-- /top navigation -->
