<footer>
  <div class="pull-right">
	<p>©2016 All Rights Reserved.  <?=$project_title?>! </p>
  </div>
  <div class="clearfix"></div>
</footer>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCd_Tujq99uHzmtznB4SqOWvjy6KHmDpzQ&libraries=places"></script>
<?php 
		echo $this->Html->script(
		array(
			'Admin/bootstrap.min.js',
			'Admin/jquery.geocomplete.js',
			'Admin/icheck.min.js',
			'Admin/validator.min.js',
			'Admin/pnotify.js',
			'Admin/bootbox.min.js',
			'Admin/moment.min.js',
			'Admin/daterangepicker.js',
		//	'Admin/jquery.multiselect.js',
			'Admin/jquery.flot.js',
			'Admin/jquery.flot.time.js',
			'Admin/jquery.flot.tooltip.js',  
			'Admin/custom.js'
		));
		echo $this->fetch('script');
				
				
	?>

<!-- validator -->
    <script>
	
	$('.numeric').keydown(function(event) {
	// Allow special chars + arrows 
	if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
		|| event.keyCode == 27 || event.keyCode == 13 
		|| (event.keyCode == 65 && event.ctrlKey === true) 
		|| (event.keyCode >= 35 && event.keyCode <= 39)){
			return;
	}else {
		// If it's not a number stop the keypress
		if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
			event.preventDefault(); 
		}   
	}
});
    
	jQuery(document).ready(function(){
		  // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
		 jQuery('form').on('blur', 'input[required], input.optional, select.required', validator.checkField);
		 jQuery('form').on('change', 'select.required', validator.checkField);
		 //jQuery('form').on('keypress', 'input[required][pattern]', validator.keypress);
		 

		  jQuery('.multi.required').on('keyup blur', 'input', function() {
			validator.checkField.apply($(this).siblings().last()[0]);
		  });

		  jQuery('form').submit(function(e) {
			e.preventDefault();
			var submit = true;

			// evaluate the form using generic validaing
			if (!validator.checkAll($(this))) {
			  submit = false;
			}

			if (submit)
			  this.submit();

			return false;
		  });
		  
		  //full screen call
		  
				$('.glyphicon-fullscreen').click(function(){

						if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
						   if (document.documentElement.requestFullScreen) {
								document.documentElement.requestFullScreen();
							} else if (document.documentElement.mozRequestFullScreen) {
								document.documentElement.mozRequestFullScreen();
							} else if (document.documentElement.webkitRequestFullScreen) {
								document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
							}
						} else {
							if (document.cancelFullScreen) {
								document.cancelFullScreen();
							} else if (document.mozCancelFullScreen) {
								document.mozCancelFullScreen();
							} else if (document.webkitCancelFullScreen) {
								document.webkitCancelFullScreen();
							}
						}
				});
							
				$('select[multiple]').multiselect({
					columns: 1,
					selectAll     : true, 
					placeholder: ' Select '
				});

					  
	});
    </script>
    <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>	
<script>

/* $('[data-toggle="confirmation"]').confirmation()
$('[data-toggle="confirmation-singleton"]').confirmation({singleton:true});
$('[data-toggle="confirmation-popout"]').confirmation({popout: true}); */

<!---- tinymce editor  ----->

tinymce.init({
    selector: "textarea.editor",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
 
 <!---- /tinymce editor  ----->
 /*
 $('input[type=text],textarea').keyup(function(){
			
			if(this.value.match(/^[a-z]/)){
				// replace the first letter
				this.value = this.value.replace(/^./,function(letter){
					// with the uppercase version
					return letter.toUpperCase();
				});
			}
			
		}); */
</script>
