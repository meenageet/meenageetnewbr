<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

class UtilityHelper extends Helper
{
	// function to create dynamic treeview menus
	function user_name($fname,$lname){
		
		$name = ucfirst($fname);
		if($lname !='') $name .= " ".ucfirst(substr($lname,0,1));
		return $name;
	}
	
	function user_image($image){
		
		if($image != ''){
			$path = WWW_ROOT."uploads/user/".$image;
			if(file_exists($path)) $image = $this->request->webroot.'uploads/user/'.$image;
			else $image = $this->request->webroot.'images/no-image.jpg';
			
		 }else $image = $this->request->webroot.'images/no-image.jpg';
		 return $image;
	}
	function image($images)
	{
		if(!empty($images)){
			$path = WWW_ROOT."uploads/deal_logo/".$images[0]['image_name'];
			if(file_exists($path)) $image = $this->request->webroot.'uploads/deal_logo/'.$images[0]['image_name'];
			else $image = $this->request->webroot.'images/no-image.jpg';
		}else $image = $this->request->webroot.'images/no-image.jpg'; 
		return $image;
		
	}
	
	function days($date)
	{
		$day = round(abs(strtotime(date('Y-m-d'))-strtotime($date))/86400);
		if($day >0) return $day." days";
		else return $day." day";
	
	}
	
	function category_div($categories,$partners,$authUser)
	{
		//pr($authUser);die;	
		
		$html= '';
		foreach($categories as $cat)
		{
             $html.='<li class="hvr-bob"> <a href="'.Router::url(['controller'=>'categories','action'=>false,urlencode($cat['category_name'])]).'">
                    <div><center><img src="'.$this->request->webroot.'uploads/category_web/'.$cat['cat_logo_web'].'" alt="'.$cat['category_name'].'"></center></div>
                    <p>'.$cat['category_name'].'</p>
                   </a> 
                   </li>';
         }
			
		 foreach($partners as $cat)
		 {
			 $html.= '<li class="hvr-bob">';
			 if (!empty($authUser)){
				if($authUser['single_registered'] == 'N') $html.='<a href="'.Router::url(["controller"=>"users","action"=>"single-register"]).'">';
				else{
					if($cat['category_name'] == 'Male Singles') $param ="M";
					else  $param ="F";
					$html.='<a href="'.Router::url(["controller"=>"users","action"=>"single",$param]).'">';
				}
			 } 
			 else
			 { 
				
				 $html .='<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">';
				
			  }
			$html.= '<div><center><img src="'.$this->request->webroot.'uploads/category_web/'.$cat['cat_logo_web'].'" alt="'.$cat['category_name'].'"></center></div>
				<p> '.$cat['category_name'].'</p>
				</a> 
			   </li>';
		  }
	
		return $html;
		
	}
	
	function profileHeader($page)
	{
		$notification_model=TableRegistry::get('Notifications');
		$user_id  = $this->request->session()->read('Auth.User.id');
		$unread_count  =  $notification_model->find('all' ,['conditions'=>['user_id'=>$user_id,'is_read'=>'N']])->count();
		$class1=$class2=$class3=$class4='';
		if($page=='profile') $class1 = 'active';
		else if($page=='news') $class2 = 'active';
		else if($page=='selling') $class3 = 'active';
		else if($page=='buying') $class4 = 'active';
		
		$html='<ul>
				<li class="'.$class1.'"><a href="'.Router::url(["controller"=>"users","action"=>"profile"]).'">Profile</a></li>
				<li class="'.$class2.'"><a href="'.Router::url(["controller"=>"users","action"=>"news"]).'">News<span class="badge">'.$unread_count.'</span></a></li>
				<li class="'.$class3.'"><a href="'.Router::url(["controller"=>"posts","action"=>"selling"]).'">Selling</a></li>
				<li class="'.$class4.'"><a href="'.Router::url(["controller"=>"followings","action"=>"buying"]).'">Buying</a></li>
                </ul>';
		return $html;
	}
	
	
	function extraHeader($page)
	{
		$class1=$class2=$class3=$class4=$class5= '';
		if($page=='about-us') $class1 = 'active';
		else if($page=='contact-us') $class2 = 'active';
		else if($page=='privacy-policy') $class3 = 'active';
		else if($page=='terms-conditions') $class4 = 'active';
		else if($page=='help-center') $class5 = 'active';
		$html =  '<div class="static_nav_bg clearfix">
					<div class="container">
						<div class="static_nav">
							<ul>
								<li class="'.$class1.'" ><a href="'.Router::url(['controller'=>'Pages','action'=>'content','about-us']).'">About Us</a></li>
								<li class="'.$class2.'"><a href="'.Router::url(['controller' => 'pages','action' => 'contact-us']).'">Contact Us</a></li>
								<li class="'.$class3.'"><a href="'.Router::url(['controller'=>'Pages','action'=>'content','privacy-policy']).'">Policies</a></li>
								<li class="'.$class4.'"><a href="'.Router::url(['controller'=>'Pages','action'=>'content','terms-conditions']).'">Terms & Conditions</a></li>
								<li class="'.$class5.'"><a href="'.Router::url(['controller'=>'Pages','action'=>'content','help-center']).'">Help Center</a></li>
							</ul>
						</div>
						
					</div>
				</div>';
				
		return $html;
	}
	
	function timeFunc($dateTime)
	{
		
		$difference = strtotime(date('Y-m-d h:i:s A')) -  $dateTime;
		if($difference < 3570) $output = round($difference / 60).' minutes ago ';
		elseif ($difference < 86370) $output = round($difference / 3600).' hours ago';
		elseif ($difference < 604770) $output = round($difference / 86400).' days ago';
		elseif ($difference < 31535970) $output = round($difference / 604770).' week ago';
		else $output = round($difference / 31536000).' years ago';
		return $output;
	}
	
	function get_min($dateTime){
		$difference = strtotime(date('Y-m-d h:i:s A')) -  $dateTime;
		$output = round($difference / 60);
		return $output;
	
	
	}
	
	function distance($lat1, $lon1, $lat2, $lon2, $unit) {

	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
		return ($miles * 1.609344);
	  } else if ($unit == "N") {
		  return ($miles * 0.8684);
		} else {
			return $miles;
		  }
	}
}
?>
