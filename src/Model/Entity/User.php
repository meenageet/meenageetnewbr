<?php
// src/Model/Entity/User.php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\I18n\Time;
class User extends Entity
{

    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

	
   
    // ...

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }


	protected $_virtual = ['age','full_name'];   
	
	//protected function _getFullName() {
      //  return $this->_properties['full_name'];
  //  }
	protected function _getAge()
    {
		// return is_null($this->_properties['dob'])?null:$this->_properties['dob']->diff(Time::now())->format('%y');
       
    }
    // ...
}
