<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\ORM\Rule\IsUnique;

class ForgetPasswordsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('Users', [
            'className' => 'Users',
			'foreignKey' => 'user_id'
        ]);
		
	}
	public function validationDefault(Validator $validator)
    {
		$validator
		->notEmpty('user_id', 'Please select user')
		->notEmpty('password', 'Please enter password');
		
				
		$validator->add('value', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'This value exist'
		]);
	    return $validator;
	}

	

}
?>
