<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class ChallengesTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('user', [
            'className' => 'Users',
			'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('user1', [
            'className' => 'Users',
			'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('user2', [
            'className' => 'Users',
			'foreignKey' => 'accept_user_id'
        ]);
         $this->belongsTo('song', [
            'className' => 'SongUploads',
			'foreignKey' => 'song_id'
        ]);
        $this->belongsTo('acceptusersong', [
            'className' => 'SongUploads',
			'foreignKey' => 'accepted_user_song_id'
        ]);
	}
	
	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('type')
			->requirePresence('song_id')
			->requirePresence('challenge_time')
			->requirePresence('challenge_amount')
			->requirePresence('image')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('type', 'Please select Type.')
			->notEmpty('song_id', 'Please select Any Song.')
			->notEmpty('challenge_time', 'Please enter Challenge Time.')
			->notEmpty('challenge_amount', 'Please enter Challenge Amount.')
			->notEmpty('image', 'Please enter Challenge image.');
		return $validator;
	}
	
	
	/*************
	 * Add Challenge validation of for API
	 * 
	 * */
	 public function validationApiChallenge(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('type')
			->requirePresence('song_id')
			->requirePresence('challenge_time')
			->requirePresence('challenge_amount')
			->requirePresence('image')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('type', 'Please select Type.')
			->notEmpty('song_id', 'Please select Any Song.')
			->notEmpty('challenge_time', 'Please enter Challenge Time.')
			->notEmpty('challenge_amount', 'Please enter Challenge Amount.')
			->notEmpty('image', 'Please enter Challenge image.');
		$validator
			->add('image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .gif ,.jpeg ,.png ,.jpg')
                ]
			]);

		return $validator;
		
	}
	
	/*************
	 * Api Edit Challenge validation of for API
	 * 
	 * */
	 public function validationApiEditChallenge(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('type')
			->requirePresence('song_id')
			->requirePresence('challenge_time')
			->requirePresence('challenge_amount')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('type', 'Please select Type.')
			->notEmpty('song_id', 'Please select Any Song.')
			->notEmpty('challenge_time', 'Please enter Challenge Time.')
			->notEmpty('challenge_amount', 'Please enter Challenge Amount.');
		$validator
			->add('image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .gif ,.jpeg ,.png ,.jpg')
                ]
			]);
			
		$validator
			->add('audio', [
                'validExtension' => [
                    'rule' => ['extension',['mp3']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .mp3')
                ]
			]);


		return $validator;
		
	}
}
?>
