<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class UserRegistersTable extends Table
{
	   
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
             
	}
	

	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('phone_number')
			->notEmpty('phone_number', 'Please enter Phone Number'); 
		
		
		$validator->add('phone_number', 'validFormat', [
			'rule' => 'numeric',
			'message' => 'Phone Number should be numeric'
		])
		->add('phone_number', [
			'length' => [
				'rule' => ['maxLength', 15],
				'message' => 'Maximum 15 digits required',
			]
		]);
		
		return $validator;
	}
	

}
?>
