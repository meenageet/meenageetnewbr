<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class ManagerTopSingerDjRewardsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
	}

	public function validationDefault(Validator $validator)
    {
		
		$validator
				->notEmpty('title', 'Please enter Page name')
				->notEmpty('slug', 'Please enter Slug')
				->notEmpty('point', 'Please enter Point');
				
		$validator->add('title', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'This name already exist'
		]);	
		
		$validator->add('slug', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'This slug already exist'
		]);	
	    return $validator;
	}
	
}
?>
