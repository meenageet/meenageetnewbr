<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class RequestDjSingersTable extends Table
{
	   
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
        $this->belongsTo('user', [
            'className' => 'Users',
			'foreignKey' => 'user_id',
			'propertyType' => 'user',
        ]);  
         $this->hasMany('song', [
            'className' => 'SongUploads',
			'foreignKey' => 'user_id',
			'bindingKey' => 'user_id'
        ]);       
	}
	

	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('type')
			->requirePresence('address')
			->requirePresence('id_proof')
			->notEmpty('type', 'Please select Singer & DJ')
			->notEmpty('address', 'Please enter Address')
			->notEmpty('id_proof', 'Please select ID Proof'); 
		
		$validator
			->add('id_proof', [
				'uploadError' => [
                'rule' => 'uploadError',
                'message' => 'The image upload failed.',
                'last' => true
				],
				'mimeType' => [
					'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
					'message' => 'Please only upload images (gif, png, jpg).',
				],
			]);
		return $validator;
	}
	

	/********* Become a singer & dj validation for Api ***********/
	public function validationApiAdd(Validator $validator)
    {
		
		$validator
			->requirePresence('type')
			->requirePresence('address')
			->requirePresence('id_proof')
			->notEmpty('type', 'Please select Singer & DJ')
			->notEmpty('address', 'Please enter Address')
			->notEmpty('id_proof', 'Please select ID Proof'); 
		
		$validator
			->add('id_proof', [
				'uploadError' => [
                'rule' => 'uploadError',
                'message' => 'The image upload failed.',
                'last' => true
				],
				'mimeType' => [
					'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
					'message' => 'Please only upload images (gif, png, jpg).',
				],
			]);
		return $validator;
	}
	
	
}
?>
