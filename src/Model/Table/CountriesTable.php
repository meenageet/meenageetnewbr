<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class CountriesTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->hasMany('Business', [
            'className' => 'Business',
            'foreignKey' => 'iso_code_2'
        ]);
        $this->hasMany('Cities', [
            'className' => 'PostCities',
			'foreignKey' => 'country_id',
		]);
    }	
    
    public function validationDefault(Validator $validator)
    {
		
				
		$validator
		->notEmpty('country_name', 'Please enter Country name')
		->notEmpty('iso_code', 'Please enter iso_code_2')
		->notEmpty('currency_code', 'Please enter currency_code')
		->notEmpty('currency_name', 'Please enter currency_name')
		->notEmpty('currency_symbol', 'Please enter currency_symbol')
		->notEmpty('phone_code', 'Please enter country code');
	    $validator->add('country_name', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'Country name already exist'
		]);
		 $validator->add('iso_code_2', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'iso_code_2 already exist'
		]);
		
		$validator
			->add('iso_code', [
				'minLength' => [
					'rule' => ['minLength', 2],
					'last' => true,
					'message' => 'ISO CODE is of minimum 2 characters.'
				],
				'maxLength' => [
					'rule' => ['maxLength', 2],
					'message' => 'ISO CODE is of minimum 2 characters.'
				], 
				
			]);
			$validator
			->add('iso_code_3', [
				'minLength' => [
					'rule' => ['minLength', 3],
					'last' => true,
					'message' => 'ISO CODE is of minimum 2 characters.'
				],
				'maxLength' => [
					'rule' => ['maxLength', 3],
					'message' => 'ISO CODE is of minimum 2 characters.'
				], 
				
			]);
			$validator
			->add('currency_code', [
				
				'maxLength' => [
					'rule' => ['maxLength', 3],
					'message' => 'Currency code is of minimum 2 characters.'
				], 
				
			]);
			$validator
			->add('currency_symbol', [
				
				'maxLength' => [
					'rule' => ['maxLength', 3],
					'message' => 'Currency symbol is of minimum 2 characters.'
				], 
				
			]);
		  $validator->add('phone_code', 'validFormat', [
			'rule' => 'isInteger',
			'message' => 'country code should be numeric'
		]);
		return $validator;

          
	}
	
}
?>
