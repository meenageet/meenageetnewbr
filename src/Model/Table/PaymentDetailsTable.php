<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class PaymentDetailsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('user', [
            'className' => 'Users',
			'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Userrewards', [
            'className' => 'UserRewards',
			'foreignKey' => 'reward_id'
        ]);
	}
	
	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('reward_id')
			->requirePresence('type')
			->requirePresence('point')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('reward_id', 'Required reward id.')
			->notEmpty('point', 'Required point.')
			->notEmpty('type', 'Required type.');
		return $validator;
	}
}
?>
