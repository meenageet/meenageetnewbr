<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class NotificationStatusTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('Users', [
            'className' => 'Users',
			'foreignKey' => 'user_id',
		]);
		$this->belongsTo('OtherUsers', [
            'className' => 'Users',
			'foreignKey' => 'other_user_id',
		]);
		
	}	
	
	
}
?>
