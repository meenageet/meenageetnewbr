<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class UserRewardsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('user', [
            'className' => 'Users',
			'foreignKey' => 'user_id'
        ]);
	}
	
	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			//->requirePresence('time')
			->notEmpty('user_id', 'Required user id.');
		return $validator;
	}
	
	
	/*************
	 * Add reward validation of for API
	 * 
	 * */
	public function validationApiReward(Validator $validator)
	{
		$validator
			->requirePresence('user_id')
			//->requirePresence('time')
			->notEmpty('user_id', 'Required user id.');
		return $validator;
		
	}
	
	
}
?>
