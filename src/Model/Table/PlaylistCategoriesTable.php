<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class PlaylistCategoriesTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('user', [
            'className' => 'Users',
			'foreignKey' => 'user_id'
        ]);
	}
	
	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('category_name')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('category_name', 'Please enter Category Name.');
		return $validator;
	}
	
}
?>
