<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class PostsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('user', [
            'className' => 'Users',
			'foreignKey' => 'user_id'
        ]);
        
	}
	
	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('title')
			->requirePresence('description')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('title', 'Please enter Product title.')
			->notEmpty('description', 'Please enter description.');
		return $validator;
	}
	
	
	/*************
	 * Add post validation of for API
	 * 
	 * */
	 public function validationApiPost(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('title')
			->requirePresence('description')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('title', 'Please enter Product title.')
			->notEmpty('description', 'Please enter description.');
		return $validator;
	}
}
?>
