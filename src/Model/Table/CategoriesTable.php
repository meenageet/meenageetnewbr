<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class CategoriesTable extends Table
{
	public function initialize(array $config)
	{
		$this->belongsTo('ParentCategories', [
            'className' => 'Categories',
			'foreignKey' => 'parent_category_id'
        ]);
        $this->hasMany('PCategories', [
            'className' => 'Categories',
			'foreignKey' => 'parent_category_id'
        ]);
       $this->hasMany('Posts');
       $this->hasMany('PartnerPosts');
	   $this->addBehavior('Timestamp');
		    
	}
	public function validationDefault(Validator $validator)
    {
		
		
		$validator
			//->notEmpty('parent_category_id', 'Please enter parent category')
			->notEmpty('category_name', 'Please enter category name');
				
	  $validator->add('category_name', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'This name already exist'
		]);
	 $validator
		->allowEmpty('cat_logo_web','update')
		->allowEmpty('cat_logo_app1','update')
		->allowEmpty('cat_logo_app2','update')
		->allowEmpty('cat_logo_app3','update');

            
		$validator->add('cat_logo_web', 'file', [
				'rule' => array('mimeType',array('image/gif','image/png','image/jpeg','image/svg','image/svg+xml')),
				'message' => 'Please only upload images (gif, png, jpg, svg).',
				
			]);
		$validator->add('cat_logo_app', 'file', [
			'rule' => array('mimeType',array('image/gif','image/png','image/jpeg','image/svg','image/svg+xml')),
			'message' => 'Please only upload images (gif, png, jpg, svg).',
			
		]);

		$validator
			->add('cat_logo_web','custom',[
				'rule'=>  function($value, $context){
					//pr($context);die;

					
					$error= 0;
					if($context['data']['cat_logo_web'] != ''){
						$img_size = explode("*",$context['data']['cat_logo_web']['size']);
						$size = getimagesize($value['tmp_name']);
						if($size[0] > 116 && $size[1] > 116){
							$error = 1;
						}
					}
					if($error == 1)
					{
						
						return false;
					}
					return true;
					
				},
				'message'=>'Image dimension is not appropriate,max size(116*116)',
			]);
		$validator
			->add('cat_logo_app','custom',[
				'rule'=>  function($value, $context){
					pr($context);die;
					
					
					$error= 0;
					if($context['data']['image_validation_app'] != ''){
						$img_size = explode("*",$context['data']['image_validation_app']);
						
						$size = getimagesize($value['tmp_name']);
						//pr($size);die;
						if($size[0] < $img_size[0] || $size[1] < $img_size[1]){
							$error = 1;
							//$msg ='Minimum dimension require is '.$img_size[0].'*'.$img_size[1];
						}
					}
					if($error == 1)
					{
						
						return false;
					}
					return true;
					
				},
				'message'=>'Image dimension is not appropriate',
			]);
		
		return $validator;

          
	}
	
	public function validationImport(Validator $validator)
    {
		
		
		$validator
				->notEmpty('bus_cat_name', 'Please enter business category name')
				->notEmpty('merchant_condition', 'Please enter merchant condition')
				->notEmpty('cat_logo', 'Image missing');
	  
		$validator->add('bus_cat_name', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'This name already exist'
		]);	
		return $validator;

          
	}
	
	
	
}
?>
