<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class PlaylistsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('user', [
            'className' => 'Users',
			'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('songUploads', [
            'className' => 'SongUploads',
			'foreignKey' => 'song_id'
        ]);
	}
	
	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('song_id')
			->requirePresence('playlist_category_id')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('song_id', 'Required song id.')
			->notEmpty('playlist_category_id', 'Required playlist category id.');
		return $validator;
	}
	
}
?>
