<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class StatesTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('Countries', [
            'className' => 'Countries',
			'foreignKey' => 'country_id',
		]);
		$this->hasMany('Cities', [
            'className' => 'Cities',
			'foreignKey' => 'state_id',
		]);
		
	}	
	public function validationDefault(Validator $validator)
    {
		$validator
			->notEmpty('country_id', 'Please select country')
			->notEmpty('state_name', 'Please enter state_name');
	   	$validator
	   		->add('state_name', [
				'unique' => [
				'rule' => ['validateUnique', ['scope' => 'country_id']],
				'provider' => 'table',
				'message'=>'State already exist for this country'
				]
			]);
		
		$validator
			->add('import', 'file', [
				'rule' => array('mimeType',array('application/vnd.ms-excel','text/plain','text/csv','text/tsv')),
				'message' => 'Please only upload files (csv).',
			]);				
		return $validator;
	}
	
}
?>
