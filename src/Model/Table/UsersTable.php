<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class UsersTable extends Table
{
	   
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('AccessLevel', [
            'className' => 'AccessLevel',
			'foreignKey' => 'access_level_id'
        ]);
        $this->belongsTo('country', [
            'className' => 'Countries',
			'foreignKey' => 'country_id',
			'propertyType' => 'country',
        ]);
        $this->belongsTo('state', [
            'className' => 'States',
			'foreignKey' => 'state_id',
			'propertyType' => 'state',
        ]);
        $this->belongsTo('city', [
            'className' => 'Cities',
			'foreignKey' => 'city_id',
			'propertyType' => 'city',
        ]);    
        $this->hasOne('requestdjsinger', [
            'className' => 'RequestDjSingers',
			'foreignKey' => 'user_id',
			'bindingKey' => 'id'
        ]);      
	}
	

	public function validationDefault(Validator $validator)
    {
		$validator
			
			->requirePresence('full_name')
			->requirePresence('email')
			->notEmpty('full_name', 'Please enter Full name')
			->notEmpty('dob', 'Please select date of birth')
			->notEmpty('gender', 'Please select gender')
			->notEmpty('email', 'Please enter Email')
			
			/*->notEmpty('old_password', 'Please enter Old Password')
			->notEmpty('new_password', 'Please enter New Password')
			->notEmpty('confirm_password', 'Please enter Confirm Password')*/
			
			->notEmpty('phone_number', 'Please enter Phone Number')
			->notEmpty('address', 'Please enter Address')
			/*->notEmpty('password', 'Please enter Password')*/
			->notEmpty('access_level_id', 'Please select access level') 
			->allowEmpty('image'); 
		
		/*$validator->add('full_name','custom',[
			'rule'=>  function($value, $context){
				$error= 0;
				if(preg_match("/[a-z]/i",$value)) return true;
				else return false;
			}
		]);	*/
		
		
		$validator->add('phone_number', 'validFormat', [
			'rule' => 'numeric',
			'message' => 'Phone Number should be numeric'
		])
		->add('phone_number', [
			'length' => [
				'rule' => ['minLength', 6],
				'message' => 'Minimum 6 digits required',
			]
		]);
		$validator->add('email', 'validFormat', [
			'rule' => 'email',
			'message' => 'E-mail must be a valid email'
		]);
		$validator->add('email', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'email already exist'
		]); 
		$validator->add('phone_number', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'Phone number already exist'
		]);     
		
		/*$validator->add('confirm_password', [
			'equalToPassword' => [
				'rule' => function ($value, $context) {
					return $value === $context['data']['password'];
				},
				'message' => __("Your confirm password must match with your password.")
			]
		]);
		  
		
		$validator->add('password', [
			'minLength' => [
			'rule' => ['minLength', 8],
			'last' => true,
			'message' => 'Password should be minimum 8 characters.'
			]
		]);*/
		
		$validator
        ->add('image', [
            'uploadError' => [
                'rule' => 'uploadError',
                'message' => 'The image upload failed.',
                'last' => true
            ],
            'mimeType' => [
                'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                'message' => 'Please only upload images (gif, png, jpg).',
            ],
          
        ]);
     
			
		return $validator;
	}
	

	
	public function validationEdituser(Validator $validator)
    {
		
		 $validator
			
			->requirePresence('full_name')
			->notEmpty('full_name', 'Please enter Full name')
			->notEmpty('phone_number', 'Please enter Phone Number')
			->notEmpty('access_level_id', 'Please select access level') 
			; 
		
		/*$validator->add('full_name','custom',[
			'rule'=>  function($value, $context){
				$error= 0;
				if(preg_match("/[a-z]/i",$value)) return true;
				else return false;
			}
		]);	*/
		
		
		$validator->add('phone_number', 'validFormat', [
			'rule' => 'numeric',
			'message' => 'Phone Number should be numeric'
		])
		->add('phone_number', [
			'length' => [
				'rule' => ['minLength', 6],
				'message' => 'Minimum 6 digits required',
			]
		]);
		
		
		$validator->add('phone_number', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'Phone Number already exist'
		])->add('phone_number', [
			'length' => [
				'rule' => ['minLength', 6],
				'message' => 'Minimum 6 digits required',
			]
		]);
		
		
		return $validator;
	}
	
	public function validationPassword(Validator $validator )
	{
		$validator
		->notEmpty('old_password', 'Please enter old password')
		->notEmpty('new_password', 'Please enter new password')
		->notEmpty('confirm_password', 'Please enter confirm password');
		
		$validator
			->add('old_password','custom',[
				'rule'=>  function($value, $context){
					//pr($context);die;
					$user = $this->get($context['data']['id']);
					if ($user) {

						if ((new DefaultPasswordHasher)->check($value, $user->password)) {
							return true;
						}
					}
					return false;
				},
				'message'=>'The old password is not correct!',
			])
			->notEmpty('old_password');

		$validator
			->add('new_password', [
				'length' => [
					'rule' => ['minLength', 8],
					'message' => 'New password length should be min 8'
				]
			])
			->add('new_password',[
				'match'=>[
					'rule'=> ['compareWith','confirm_password'],
					'message'=>"fields don't match",
				]
			])
			->notEmpty('new_password');
		$validator
			->add('confirm_password', [
				'length' => [
					'rule' => ['minLength', 8],
					'message' => 'Confirm password length should be min 8',
				]
			])
			->add('confirm_password',[
				'match'=>[
					'rule'=> ['compareWith','new_password'],
					'message'=>"fields don't match",
				]
			])
			->notEmpty('confirm_password');

		return $validator;
	}	
	
	
	
	
	/********* Register user validation for Api ***********/
	public function validationApiOtp(Validator $validator)
    {
		
		 $validator
			->requirePresence('otp_number')
			->notEmpty('otp_number', 'Please enter otp')
			->notEmpty('phone_number', 'Please enter Phone Number'); 
		 $validator->add('phone_number', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'Phone number already exist.'
		 ]);  
		return $validator;
	}
	
	
	
	public function validationApiEditProfile(Validator $validator)
    {
		
		  $validator
			->requirePresence('email')
			->requirePresence('full_name')
			->requirePresence('address')
			->requirePresence('dob')
			->notEmpty('email', 'Please enter Email')
			->notEmpty('full_name', 'Please enter Full name')
			->notEmpty('address', 'Please enter Full name')
			->notEmpty('dob', 'Please enter address'); 
				
		
		/*$validator->add('full_name','custom',[
			'rule'=>  function($value, $context){
				$error= 0;
				if(preg_match("/[a-z]/i",$value)) return true;
				else return false;
			}
		]);	*/
		$validator->add('email', 'validFormat', [
			'rule' => 'email',
			'message' => 'E-mail must be a valid email'
		]);
		
		
		return $validator;
	}
	
	/********* Minimum rewards validation ***********/
	public function validationMinRewards(Validator $validator)
    {
		
		 $validator
			->requirePresence('minimum_rewards')
			->notEmpty('minimum_rewards', 'Please enter minimum reward points'); 
		return $validator;
	}
	
	/********* Minimum app use time validation ***********/
	public function validationAppUseTime(Validator $validator)
    {
		
		 $validator
			->requirePresence('app_use_time')
			->notEmpty('app_use_time', 'Please enter minimum app use time'); 
		return $validator;
	}
	
	/********* Free Subscription Duration validation ***********/
	public function validationSubscriptionDuration(Validator $validator)
    {
		
		 $validator
			->requirePresence('subscription_duration_month')
			->notEmpty('subscription_duration_month', 'Please enter subscription duration in months'); 
		return $validator;
	}
	/********* Minimum rewards validation ***********/
	public function validationManageRewardstime(Validator $validator)
    {
		
		 $validator; 
		return $validator;
	}
	
}
?>
