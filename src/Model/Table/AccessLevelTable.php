<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\ORM\Rule\IsUnique;

class AccessLevelTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		
		
	}

	public function validationDefault(Validator $validator)
    {
		
		 $validator
				->notEmpty('title', 'Please enter Access title');
				
		$validator->add('title', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'Title already exist'
		]); 
	    
		return $validator;

          
	}
	

}
?>
