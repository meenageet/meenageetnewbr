<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class BannersTable extends Table
{
	public function initialize(array $config)
	{
	   $this->addBehavior('Timestamp');
		    
	}
	public function validationDefault(Validator $validator)
    {
		
		
		$validator
				->notEmpty('text', 'Please enter banner text');
		
		$validator
				->allowEmpty('image','update');	
		$validator->add('image', 'file', [
				'rule' => array('mimeType',array('image/gif','image/png','image/jpeg','image/svg','image/svg+xml')),
				'message' => 'Please only upload images (gif, png, jpg, svg).',
				
			]);
		
		$validator
			->add('image','custom',[
				'rule'=>  function($value, $context){
					$error= 0;
					if($context['data']['image'] != ''){
						$img_size = explode("*",$context['data']['image']['size']);
						$size = getimagesize($value['tmp_name']);
						
						$ratio = $size[1]/$size[0];
						$formattedNum = number_format($ratio, 2);
						if($size[0] > 1920 || $size[1] > 787){
							if($formattedNum < 0.40 || $formattedNum > 0.42){
								$error = 1;
							}
						}else if($formattedNum < 0.40 || $formattedNum > 0.42){
								$error = 1;
							}
					}
					//print_r($error);die;
					if($error == 1)
					{
						
						return false;
					}
					return true;
					
				},
				'message'=>'Banner image should be height/width=.40 to .42 ratio.',
			]);
		return $validator;

          
	}
	
}
?>
