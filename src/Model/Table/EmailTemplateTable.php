<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class EmailTemplateTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
	}

	public function validationDefault(Validator $validator)
    {
		$validator
				->notEmpty('title', 'Please Enter Title')
				->notEmpty('from_email', 'Please Enter Sender Email')
				->notEmpty('from_name', 'Please Enter Sender Name')
				->notEmpty('subject', 'Please Enter Subject')
				->notEmpty('from_name', 'Please Enter Sender Name')
				->notEmpty('description', 'Please Enter Description');
				
		$validator->add('from_email', 'validFormat', [
			'rule' => 'email',
			'message' => 'Sender Email must be a valid email'
		]);
		
		$validator->add('title', 'unique', [
			'rule' => 'validateUnique',
			'provider' => 'table',
			'message'=>'Title already exist'
		]);     		
	    return $validator;
	}
	
}
?>
