<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;


class CitiesTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('States', [
            'className' => 'States',
			'foreignKey' => 'state_id',
			]);
	}	
	public function validationDefault(Validator $validator)
    {
		
				
		$validator
		->notEmpty('state_id', 'Please select state')
		->notEmpty('city_name', 'Please enter City name')
		->notEmpty('latitude', 'Please enter latitude')
		->notEmpty('longitude', 'Please enter longitude');
		  $validator->add('latitude', 'validFormat', [
			'rule' => 'latitude',
			'message' => 'Invalid latitude'
		]);
		  $validator->add('longitude', 'validFormat', [
			'rule' => 'longitude',
			'message' => 'Invalid longitude'
		]);
	   	$validator->add('city_name', [
		'unique' => [
		'rule' => ['validateUnique', ['scope' => 'state_id']],
		'provider' => 'table',
		'message'=>'city already exist for this state'
		]
		]);
		
		$validator->add('import', 'file', [
			'rule' => array('mimeType',array('application/vnd.ms-excel','text/plain','text/csv','text/tsv')),
			'message' => 'Please only upload files (csv).',

		]);	
			
		return $validator;   
	}
	
	
}
?>
