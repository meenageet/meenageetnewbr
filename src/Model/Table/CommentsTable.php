<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class CommentsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('Users', [
            'className' => 'Users',
			'foreignKey' => 'user_id',
		]);
		$this->belongsTo('SongUploads', [
            'className' => 'SongUploads',
			'foreignKey' => 'song_id',
		]);
		
	}	
	
	
		public function validationDefault(Validator $validator)
    {
		$validator->requirePresence('comment')
			->notEmpty('comment', 'Please enter comment');
	
			
		return $validator;
	}
	
	
	
	
	
	
}
?>
