<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;


class AreasTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('Cities', [
            'className' => 'Cities',
			'foreignKey' => 'city_id',
			]);
	}	
	public function validationDefault(Validator $validator)
    {
		
				
		$validator
		->notEmpty('city_id', 'Please select city')
		->notEmpty('area_name', 'Please enter Area name');
		 
	   	$validator->add('area_name', [
		'unique' => [
		'rule' => ['validateUnique', ['scope' => 'city_id']],
		'provider' => 'table',
		'message'=>'area already exist for this city'
		]
		]);
		
		$validator->add('import', 'file', [
			'rule' => array('mimeType',array('application/vnd.ms-excel','text/plain','text/csv','text/tsv')),
			'message' => 'Please only upload files (csv).',

		]);	
			
		return $validator;   
	}
	
	
}
?>
