<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SettingsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
	
	}
	public function validationDefault(Validator $validator)
    {
		
		$validator
				->notEmpty('module_name', 'Module name cannot be blank')
				->notEmpty('value', 'Please define value');
	    $validator
			->add('value','custom',[
				'rule'=>  function($value, $context){
					$error = 1;
					if($context['data']['type'] == 'limit'){
						//echo $value;
						if( (int)$value == $value && (int)$value >= 0 ){
							$error = 0;
						}

					}else if($context['data']['type'] == 'price'){
						if( is_numeric($value) && (int)$value >= 0 ){
							$error = 0;
						}
					
					}else $error = 0;
				
					if($error == 1)
					{
						return false;
					}
					return true;
					
				},
				'message'=>'Incorrect value',
			]);
		return $validator;

          
	}
	
	
	
}
?>
