<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class PremiumsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		
    }
	
	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('plan_name')
			->requirePresence('plan_desc')
			->requirePresence('duration')
			->requirePresence('duration_type')
			->requirePresence('amount')
			; 
	
		return $validator;
	}
	
}
?>
