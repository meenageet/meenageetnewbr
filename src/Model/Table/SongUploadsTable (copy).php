<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class SongUploadsTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('user', [
            'className' => 'Users',
			'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('category', [
            'className' => 'Categories',
			'foreignKey' => 'cat_id'
        ]);
        $this->belongsTo('subcategory', [
            'className' => 'Categories',
			'foreignKey' => 'sub_cat_id'
        ]);
        $this->hasMany('likes', [
            'className' => 'Likes',
			'foreignKey' => 'song_id'
        ]);
        $this->hasMany('comment', [
            'className' => 'Comments',
			'foreignKey' => 'song_id'
        ]);
	}
	
	public function validationDefault(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('title')
			->requirePresence('cat_id')
			->requirePresence('sub_cat_id')
			->requirePresence('image')
			->requirePresence('audio')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('title', 'Please enter Product title.')
			->notEmpty('cat_id', 'Please enter Category Id.')
			->notEmpty('sub_cat_id', 'Please enter Subcategory Id.')
			->notEmpty('image', 'Please enter Product title.')
			->notEmpty('audio', 'Please enter audio.');
		return $validator;
	}
	
	
	/*************
	 * Add song validation of for API
	 * 
	 * */
	 public function validationApiPost(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('title')
			->requirePresence('cat_id')
			->requirePresence('sub_cat_id')
			->requirePresence('image')
			->requirePresence('audio')
			->requirePresence('duration')
			->requirePresence('size')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('title', 'Please enter Product title.')
			->notEmpty('cat_id', 'Please enter Category Id.')
			->notEmpty('sub_cat_id', 'Please enter Subcategory Id.')
			->notEmpty('image', 'Please enter Product Image.')
			->notEmpty('audio', 'Please enter audio.')
			->notEmpty('duration', 'Please enter duration.')
			->notEmpty('size', 'Please enter size.');
		
		$validator
			->add('image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .gif ,.jpeg ,.png ,.jpg')
                ]
			]);
			
		$validator
			->add('audio', [
                'validExtension' => [
                    'rule' => ['extension',['mp3']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .mp3')
                ]
			]);


		return $validator;
		
	}
	
	/*************
	 * Api Edit song validation of for API
	 * 
	 * */
	 public function validationApiEditPost(Validator $validator)
    {
		$validator
			->requirePresence('user_id')
			->requirePresence('title')
			->requirePresence('cat_id')
			->requirePresence('sub_cat_id')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('title', 'Please enter Product title.')
			->notEmpty('cat_id', 'Please enter Category Id.')
			->notEmpty('sub_cat_id', 'Please enter Subcategory Id.');
		
		$validator
			->add('image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .gif ,.jpeg ,.png ,.jpg')
                ]
			]);
			
		$validator
			->add('audio', [
                'validExtension' => [
                    'rule' => ['extension',['mp3']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .mp3')
                ]
			]);


		return $validator;
		
	}
	
	/*************
	 * Add song validation of for Admin
	 * 
	 * */
	 public function validationAddSong(Validator $validator)
	{
		$validator
			->requirePresence('user_id')
			->requirePresence('title')
			->requirePresence('cat_id')
			->requirePresence('sub_cat_id')
			->requirePresence('image')
			->requirePresence('audio')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('title', 'Please enter Product title.')
			->notEmpty('cat_id', 'Please enter Category Id.')
			->notEmpty('sub_cat_id', 'Please enter Subcategory Id.')
			->notEmpty('image', 'Please enter Product Image.')
			->notEmpty('audio', 'Please enter audio.');
		
		$validator
			->add('image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .gif ,.jpeg ,.png ,.jpg')
                ]
			]);
			
		$validator
			->add('audio', [
                'validExtension' => [
                    'rule' => ['extension',['mp3']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .mp3')
                ]
			]);


		return $validator;
		
	}
	
	/*************
	 * Add song validation of for Admin
	 * 
	 * */
	 public function validationEditSong(Validator $validator)
	{
		$validator
			->requirePresence('user_id')
			->requirePresence('title')
			->requirePresence('cat_id')
			->requirePresence('sub_cat_id')
			->notEmpty('user_id', 'Required user id.')
			->notEmpty('title', 'Please enter Product title.')
			->notEmpty('cat_id', 'Please enter Category Id.')
			->notEmpty('sub_cat_id', 'Please enter Subcategory Id.')
			->allowEmpty('image','update')
			->allowEmpty('audio','update');
		
		$validator
			->add('image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .gif ,.jpeg ,.png ,.jpg')
                ]
			]);
			
		$validator
			->add('audio', [
                'validExtension' => [
                    'rule' => ['extension',['mp3']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => __('These files extension are allowed: .mp3')
                ]
			]);


		return $validator;
		
	}
	
}
?>
