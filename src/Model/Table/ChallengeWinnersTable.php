<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class ChallengeWinnersTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('Users', [
            'className' => 'Users',
			'foreignKey' => 'user_id',
		]);
		$this->belongsTo('Users2', [
            'className' => 'Users',
			'foreignKey' => 'other_user_id',
		]);
		$this->belongsTo('Challenges', [
            'className' => 'Challenges',
			'foreignKey' => 'challenge_id',
		]);
		
	}	
		
}
?>
