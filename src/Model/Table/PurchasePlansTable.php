<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class PurchasePlansTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('Users', [
            'className' => 'Users',
			'foreignKey' => 'user_id',
		]);
		$this->belongsTo('Premiums', [
            'className' => 'Premiums',
			'foreignKey' => 'plan_id',
		]);
		
	}	
	
	
}
?>
