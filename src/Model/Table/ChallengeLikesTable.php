<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class ChallengeLikesTable extends Table
{
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
		$this->belongsTo('Users', [
            'className' => 'Users',
			'foreignKey' => 'user_id',
		]);
		$this->belongsTo('challenge_user_id', [
            'className' => 'Users',
			'foreignKey' => 'ch_user_id',
		]);
		$this->belongsTo('Challenges', [
            'className' => 'Challenges',
			'foreignKey' => 'challenge_id',
		]);
		
	}	
		
}
?>
