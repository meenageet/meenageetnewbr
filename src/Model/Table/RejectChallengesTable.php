<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Rule\IsUnique;

class RejectChallengesTable extends Table
{
	   
	public function initialize(array $config)
	{
		$this->addBehavior('Timestamp');
        $this->belongsTo('user', [
            'className' => 'Users',
			'foreignKey' => 'user_id',
			'propertyType' => 'user',
        ]);  
         $this->belongsTo('challenge', [
            'className' => 'Challenges',
			'foreignKey' => 'challenge_id',
			'propertyType' => 'challenge',
        ]);       
	}
	

	public function validationDefault(Validator $validator)
    {
		$validator; 
		return $validator;
	}
	

}
?>
