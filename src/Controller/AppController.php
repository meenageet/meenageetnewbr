<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use ip2location_lite;
use App\View\Helper\DateHelper;
use Cake\Routing\Router;
use App\Controller\Component\AwsComponent;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
		$this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');
		$this->loadComponent('Aws');
		
		if (isset($this->request->params['prefix'])  && $this->request->params['prefix']== 'admin') {
			$this->loadComponent('Auth', [
				'authorize' => ['Controller'],
				'loginRedirect' => [
					'controller' => 'pages',
					'action' => 'dashboard'
				],
				'logoutRedirect' => [
					'controller' => 'users',
					'action' => 'login',
				],
				'authenticate' => [
					'Form' => [
						'fields' => ['username' => 'username']
					]
				],
				
			]);
		
		}else{
			$this->loadComponent('Auth', [
				'authorize' => ['Controller'],
				'loginRedirect' => [
					'controller' => 'users',
					'action' => 'login',
				],
				'logoutRedirect' => [
					'controller' => 'users',
					'action' => 'login',
				],
				'authenticate' => [
					'Form' => [
						'fields' => ['username' => 'username']
					]
				],
				
			]);
			
		}
    }


	public function isAuthorized($user)
	{
		 if(isset($this->request->params['prefix']) && $this->request->params['prefix'] === 'admin' ){
		
			if($this->Auth->user('access_level_id') == '1'){
				return true;
			}
		}else{
			return true;
		}
		return false;
	}
	
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
		if($this->Auth->user('id'))
		{
			$this->set('logged','Yes');
			$this->set('authUser',$this->Auth->user());
		}else{
			$this->set('logged','No');
		}
		
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
        
		
		if (isset($this->request->params['prefix'])  && $this->request->params['prefix']== 'admin') 
		{
			if(isset($this->request->params['action'])  && $this->request->params['action']== 'login'){
				$this->viewBuilder()->layout('login_admin');
			}else if(isset($this->request->params['action'])  && $this->request->params['action']== 'forbidden'){
				$this->viewBuilder()->layout('403');
			}else{
				
				if($this->request->is('ajax')){
					$this->viewBuilder()->layout('ajax');
				}else{
					$this->viewBuilder()->layout('admin');
				}
			}
		}else{
			if($this->request->is('ajax')) $this->viewBuilder()->layout('ajax');
			
		}
	 
	 }
    
    public function beforeFilter(Event $event)
    {
		
		
		if (isset($this->request->params['prefix'])  && $this->request->params['prefix']== 'admin') 
		{
			// pagination 
			$this->loadModel('settings');
			$query = $this->settings->find()
			->select(['value'])
			->where(['module_name =' => 'pagination'])->first();
			
			if(!empty($query) )
			{
				$this->pagination_limit=$query->value;
			}else{
				$this->pagination_limit=10;
			}
			
			
		}else{
			if($this->request->session()->read('user_location') == '') 
			{
				require_once(ROOT .  DS. 'vendor' . DS . 'ip2locationlite.class.php');
				$ipLite = new ip2location_lite;
				$ipLite->setKey('7bdd43620202c5e332da0369ae5ddd09053537a61aa281d5cd9a7aa4478d06eb');
				$locations = $ipLite->getCity($_SERVER['REMOTE_ADDR']);
				if($locations['countryName']!='-') $userLocation = $locations;
				else $userLocation = array('countryName'=>'India','latitude'=>'26.9167','longitude'=>'75.8167');
				$this->request->session()->write('user_location',$userLocation);
			}
			
		}
		
		
		
		$this->project_title='Meena Geet';
		$this->userLocation = $this->request->session()->read('user_location');
		$this->set('project_title','Meena Geet');
		$this->Auth->allow(['index', 'view', 'home']);
    }
  
	/***********************************************************************************
	 * Upload  only video/audio on AWS server
	 * *********************************************************************************/
	 
	 public function uploadFile($files,$type) {  
		if($type == 'post'){
			$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $files['name']);
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
			
			$uploadimg = $this->Aws->bucketUpload($files['tmp_name'], $filename, path_post_image_folder);
			if($uploadimg){
				$postimages = $filename;
				return $postimages;
			}else{
				$postimages = '';
				return $postimages;
				return '';
			}
		}else if($type == 'idproof'){
			$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $files['name']);
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
			
			$uploadimg = $this->Aws->bucketUpload($files['tmp_name'], $filename, path_IDProof_image_folder);
			if($uploadimg){
				$postimages = $filename;
				return $postimages;
			}else{
				$postimages = '';
				return $postimages;
				return '';
			}
			
		}else if($type == 'song'){
			$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $files['name']);
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
				
			$uploadimg = $this->Aws->bucketUpload($files['tmp_name'], $filename, path_song_image_folder);
			if($uploadimg){
				$songimages = $filename;
				return $songimages;
			}else{
				$songimages = '';
				return $songimages;
				return '';
			}
			
		}else if($type == 'audio'){
			$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $files['name']);
			$uploadimg = $this->Aws->bucketUpload($files['tmp_name'], $filename, path_song_audio_folder);
			if($uploadimg){
				$songaudio = $filename;
				return $songaudio;
			}else{
				$songaudio = '';
				return $songaudio;
				return '';
			}
		}
	 }
	 /***********************************************************************************
	 * Upload  only video/audio on AWS server
	 * *********************************************************************************/
	 
	 public function uploadFileAudioAdmin($name,$tmp_name,$type) {  
		if($type == 'audio'){
			$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $name);
			$uploadimg = $this->Aws->bucketUpload($tmp_name, $filename, path_song_audio_folder);
			if($uploadimg){
				$songaudio = $filename;
				return $songaudio;
			}else{
				$songaudio = '';
				return $songaudio;
				return '';
			}
		}
	 }
	 public function uploadImage($pfile,$ptfile,$directory,$filename) {  
		$fileTemp = $pfile;
        $image_name = $filename;        
        $imageType = $ptfile;
        $allowed = array('image/jpeg', 'image/png', 'image/gif', 'image/JPG', 'image/jpg', 'image/pjpeg','text/csv','application/csv');
        //To check if the file are image file
        if (!in_array($imageType, $allowed)) {
            return false;
        } else { 
		if (!file_exists($directory)){
			mkdir($directory, 0777, true);
			$directory=$directory;
		}
	    	if(move_uploaded_file($fileTemp,WWW_ROOT.$directory."/".basename($image_name))) {
	        	return true;
	    	}
	    	else {
	        	return false;
	    	}
         }
     }
     
     /***********************************************************************************
	 * Upload  only documents
	 * *********************************************************************************/
	 
	 public function uploadDocument($pfile,$ptfile,$directory,$filename) {  
		$fileTemp = $pfile;
        $image_name = $filename;        
        $imageType = $ptfile;
        $allowed = array('application/pdf', 'text/plain', 'text/html', 'application/xls', 'application/vnd.ms-excel', 'application/powerpoint','text/csv','application/csv', 'application/msword');
        //To check if the file are image file
        if (!in_array($imageType, $allowed)) {
            return false;
        } else { 
		if (!file_exists($directory)){
			mkdir($directory, 0777, true);
			$directory=$directory;
		}
	    	if(move_uploaded_file($fileTemp,WWW_ROOT.$directory."/".basename($image_name))) {
	        	return true;
	    	}
	    	else {
	        	return false;
	    	}
         }
     }
	
	/***********************************************************************************
	 * Create Thumbnail only image
	 * *********************************************************************************/
	 public function createThumbnail($filename,$imageDirName, $dirName, $nx, $ny) {
        $path_to_thumbs_directory = WWW_ROOT . $dirName . "/";
        
        $path_to_image_directory = WWW_ROOT .  $imageDirName. "/";
        $filename = $filename;
		$final_width_of_image = 193;
        if (preg_match('/[.](jpg)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        } else if (preg_match('/[.](jpeg)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        } else if (preg_match('/[.](gif)$/', $filename)) {
            $im = imagecreatefromgif($path_to_image_directory . $filename);
        } else if (preg_match('/[.](png)$/', $filename)) {
            $im = imagecreatefrompng($path_to_image_directory . $filename);
        } else if (preg_match('/[.](JPG)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        }else if (preg_match('/[.](mp4)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        }
        $ox = imagesx($im);
        $oy = imagesy($im);
        $nm = imagecreatetruecolor($nx, $ny);
        imagesavealpha($nm, true);
        $trans_colour = imagecolorallocatealpha($nm, 0, 0, 0, 127);
        imagefill($nm, 0, 0, $trans_colour);
        imagecopyresampled($nm, $im, 0, 0, 0, 0, $nx, $ny, $ox, $oy);
        if (!file_exists($path_to_thumbs_directory)) {
            if (!mkdir($path_to_thumbs_directory)) {
                return false;
            }
        }
		if (preg_match('/[.](jpg)$/', $filename)) {
            imagejpeg($nm, $path_to_thumbs_directory . $filename);
        } else if (preg_match('/[.](jpeg)$/', $filename)) {
            imagejpeg($nm, $path_to_thumbs_directory . $filename);
        } else if (preg_match('/[.](gif)$/', $filename)) {
            imagegif($nm, $path_to_thumbs_directory . $filename);
        } else if (preg_match('/[.](png)$/', $filename)) {
            imagepng($nm, $path_to_thumbs_directory . $filename);
        } else if (preg_match('/[.](JPG)$/', $filename)) {
            imagejpeg($nm, $path_to_thumbs_directory . $filename);
        } else if (preg_match('/[.](mp4)$/', $filename)) {
            imagejpeg($nm, $path_to_thumbs_directory . $filename);
        }
		return true;
    }
    
   function userfullname($fname,$lname){
	 
		
		$name = ucfirst($fname);
		if($lname !='') $name .= " ".ucfirst(substr($lname,0,1));
		return $name;
	}
	
	
	
	public function user_image($image){
		
		if($image != ''){
			$path = WWW_ROOT."uploads/user/".$image;
			if(file_exists($path)) $image = $this->request->webroot.'uploads/user/'.$image;
			else $image = $this->request->webroot.'images/no-image.jpg';
			
		 }else $image = $this->request->webroot.'images/no-image.jpg';
		 return $image;
	}
	
	public function getAddress($lat,$lng)
	{
		$file_contents =file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$this->request->data['latitude'].','.$this->request->data['longitude'].'&sensor=false');
		//$file_contents = file_get_contents($request);
		$json_decode = json_decode($file_contents);
		$address= '';
		if(array_key_exists(0,$json_decode->results)) $address =  $json_decode->results[0]->formatted_address;
		else if(array_key_exists(1,$json_decode->results)) $address =  $json_decode->results[1]->formatted_address;
		
		return $address;
	}
	
	public function getCountry($lat,$lng)
	{
		$country = '';
		//$geocode=file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$this->request->data['latitude'].",".$lon1."&destinations=".$this->request->data['latitude'].",".$this->request->data['longitude']."&key=".GOOGLE_API_KEY_RIDER);
		
		$ch = curl_init();
		$Url='https://maps.googleapis.com/maps/api/geocode/json?latlng='.$this->request->data['latitude'].','.$this->request->data['longitude'].'&sensor=false&key='.GOOGLE_API_KEY_CLASSIFIED;
	
		curl_setopt($ch, CURLOPT_URL, $Url); 
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		$output = curl_exec($ch);
		$output = json_decode($output);
		
		
		//$geocode= file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$this->request->data['latitude'].' ,'.$this->request->data['longitude'].' &sensor=false  &key=AIzaSyBsoQOb5d2gnkd5eJ076CYhAYz8dJuJAi4');
		
			//$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$this->request->data['latitude'].','.$this->request->data['longitude'].'&sensor=false &key='.GOOGLE_API_KEY_RIDER);
		//$output= json_decode($geocode);
		/*if ($output->status==OK && $output->rows[0]->elements[0]->status==OK) {
			$distance=$output->rows[0]->elements[0]->distance->value; // IN meters
			$duration=$output->rows[0]->elements[0]->duration->value; // IN seconds
			return $distance . , . $duration;
		} else {
			return ;
		}*/
		for($j=0;$j<count($output->results[0]->address_components);$j++){
			$cn=array($output->results[0]->address_components[$j]->types[0]);
			if(in_array("country", $cn)){
				$country= $output->results[0]->address_components[$j]->long_name;
			}
		}
		return $country;
		$this->request->data['country'] = $country; 
	}
   
   
   public function userImage($image)
   {
	   if($image != ''){
			$image = $image;
		 }else $image = BASEURL.'images/no-image.jpg';
		 return $image;
	  
	  
   }
   
   public function userImageold($image)
   {
	   if($image != ''){
			$path = WWW_ROOT."uploads/user/".$image;
			if(file_exists($path)) $image = BASEURL.'uploads/user/'.$image;
			else $image = BASEURL.'images/no-image.jpg';
			
		 }else $image = BASEURL.'images/no-image.jpg';
		 return $image;
	  
	  
   }
   
   
    public function profileCompletion($user)
    {
		$percentage = 0;
		if($user['email'] != '') $percentage = $percentage+15;
		if($user['image'] != '') $percentage = $percentage+15;
		if($user['gender'] != '') $percentage = $percentage+10;
		if($user['dob'] != '') $percentage = $percentage+10;
		if($user['profile_type'] != 'N') $percentage = $percentage+10;
		if($user['single_registered'] == 'Y') $percentage = $percentage+30;
		if($user['fb_id'] == '' || $user['google_id'] == '' ) $percentage = $percentage+10;
		return $percentage;
	}
	
	public function duration($created)
	{
		$now  = strtotime(date('Y-m-d h:i:s A'));
		$difference = $now - strtotime($created);
		if($difference < 3570) $output = round($difference / 60).' minutes ago ';
		elseif ($difference < 86370) $output = round($difference / 3600).' hours ago';
		elseif ($difference < 604770) $output = round($difference / 86400).' days ago';
		elseif ($difference < 31535970) $output = round($difference / 604770).' week ago';
		else $output = round($difference / 31536000).' years ago';
		return $output;
	}
	
 
   public function postImage($images)
	{
		if(!empty($images)){
			$path = WWW_ROOT."uploads/deal_logo_thumbnail/".$images[0]['image_name'];
			if(file_exists($path)) $image =BASEURL.'uploads/deal_logo_thumbnail/'.$images[0]['image_name'];
			else $image = BASEURL.'images/no-image.jpg';
		}else $image = BASEURL.'images/no-image.jpg'; 
		return $image;
	}
	
	 public function postOrginalImage($images)
	{
		if(!empty($images)){
			$path = WWW_ROOT."uploads/deal_logo/".$images[0]['image_name'];
			if(file_exists($path)) $image =BASEURL.'uploads/deal_logo/'.$images[0]['image_name'];
			else $image = BASEURL.'images/no-image.jpg';
		}else $image = BASEURL.'images/no-image.jpg'; 
		return $image;
	}
	
	
	public function userName($firstname)
	{
		$firstname = str_replace(' ', '', $firstname);
		$exist_username = $this->Users->find()->select(['id'])->where(['username' => $firstname])->hydrate(false)->first();
		if(empty($exist_username))
		{
			return  $firstname;
		}else{
			$user_name =  $firstname.rand(1111,9999);
			$this->loadModel('Users');
			$exist_username = $this->Users->find()->select(['id'])->where(['username' => $user_name])->hydrate(false)->first();
			if(!empty($exist_username))
			{
				$this->userName($firstname);
			}else{
				return  $user_name;
				die;
			}
		}
		
	}
	
	
	//create sulg function
	public static function slugify($text)
	{
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  // trim
	  $text = trim($text, '-');

	  // remove duplicate -
	  $text = preg_replace('~-+~', '-', $text);

	  // lowercase
	  $text = strtolower($text);

	  if (empty($text)) {
		return 'n-a';
	  }

	  return $text;
	}
	
	
	 /******************** Quickbox Token Function *****************/
	public function quickboxToken(){
        // server
        $application_id = QUICKBLOX_APPLICATION_ID;
        $auth_key = QUICKBLOX_AUTH_KEY;
        $authSecret = QUICKBLOX_AUTHSECRET;
        $nonce = rand();
        $timestamp = time();

        if($this->Auth->user()){
            $stringForSignature = "application_id=".$application_id."&auth_key=".$auth_key."&nonce=".$nonce."&timestamp=".$timestamp."&user[login]=".$this->Auth->user('quickblox_username')."&user[password]=".$this->Auth->user('quickblox_password');

            $signature = hash_hmac('sha1', $stringForSignature , $authSecret);

            $post_body = http_build_query(array(
                'application_id' => $application_id,
                'auth_key' => $auth_key,
                'timestamp' => $timestamp,
                'nonce' => $nonce,
                'signature' => $signature,
                'user[login]' => $this->Auth->user('quickblox_username'),
                'user[password]' => $this->Auth->user('quickblox_password')
            ));
        }else{
            $stringForSignature = "application_id=".$application_id."&auth_key=".$auth_key."&nonce=".$nonce."&timestamp=".$timestamp;
            $signature = hash_hmac( 'sha1', $stringForSignature , $authSecret);

            $post_body = http_build_query(

                array(
                    'application_id' => $application_id,
                    'auth_key' => $auth_key,
                    'nonce' => $nonce,
                    'signature' => $signature,
                    'timestamp' => $timestamp,
                )

            );
        }
        $tokenCurl = curl_init();
        curl_setopt($tokenCurl, CURLOPT_URL, QUICKBLOX_API_URL.'session.json');
        curl_setopt($tokenCurl, CURLOPT_POST, true);
        curl_setopt($tokenCurl, CURLOPT_POSTFIELDS, $post_body);
        curl_setopt($tokenCurl, CURLOPT_RETURNTRANSFER, true);
        $responce = curl_exec($tokenCurl);
        // Check errors
        if ($responce) {
            $responceDecode  =json_decode($responce);
            $token = $responceDecode->session->token;
            return $token;
        } else {
            $error = curl_error($tokenCurl). '(' .curl_errno($tokenCurl). ')';
            echo $error . "\n";die;
        }
        // Close connection
        curl_close($tokenCurl);

    }
    
	public function time_elapsed_string ($time)
	{
		
		date_default_timezone_set('Asia/Kolkata');
		$time = time() - $time; // to get the time since that moment
		$time = ($time<1)? 1 : $time;
		$tokens = array (
			31536000 => 'year',
			2592000 => 'month',
			604800 => 'week',
			86400 => 'day',
			3600 => 'hour',
			60 => 'minute',
			1 => 'second'
		);

		foreach ($tokens as $unit => $text) {
			if ($time < $unit) continue;
			$numberOfUnits = floor($time / $unit);
			return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
		}

	}

	
	public function sendPushNotification($notification_type, $song_id=null, $songname=null, $challenge_id=null, $user_id=null, $message=null)
	{
		$this->loadModel('Users');
		$url = 'https://fcm.googleapis.com/fcm/send';
		$server_key = 'AIzaSyAdC4iSaZt2KbwOlFvR7lza_QsV5u_m_e4'; 
		
		$keyDataArr = array('title'=>'Meena Geet', 'body'=>$message, 'sound'=>'mySound', 'badge' => '1', 'notification_type' => $notification_type, 'song_id' => $song_id, 'songname' => $songname, 'challenge_id' => $challenge_id, 'user_id' => $user_id);
		//print_r($keyDataArr); die;
		$user = $this->Users->find('all',array('fields'=>array('device_type','device_token'),'conditions'=>array('id'=>$user_id,'device_token IS NOT'=>NULL)))->hydrate(false)->first();	
		$arrayToSend = array
			(
				'priority' => "high",
				'data'  => $keyDataArr
			);
		$arrayToSend['to'] = $user['device_token'];		
		
		$headers = array(
			'Content-Type:application/json',
			'Authorization:key='.$server_key
		);	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));
		$result = curl_exec($ch);
		curl_close($ch);
		if(isset($return) && $return == 'success'){
			$arr =  json_decode($result);
			return  $arr->success;
		}			
 		
	}
	
	public function sendPushNotificationAdmin($notification_type, $song_id=null, $songname=null, $challenge_id=null, $user_id=null, $message=null,$type)
	{
		$this->loadModel('Users');
		$url = 'https://fcm.googleapis.com/fcm/send';
		$server_key = 'AIzaSyAdC4iSaZt2KbwOlFvR7lza_QsV5u_m_e4'; 
		
		if($type == 1){
			$typetime = 'true';
		}else{
			$typetime = 'false';
		}
		$keyDataArr = array('title'=>'Meena Geet','rewards_time'=> $typetime, 'body'=>$message, 'sound'=>'mySound', 'badge' => '1', 'notification_type' => $notification_type, 'song_id' => $song_id, 'songname' => $songname, 'challenge_id' => $challenge_id, 'user_id' => $user_id);
		$userdata = $this->Users->find('all',array('fields'=>array('device_type','device_token'),'conditions'=>array('device_token IS NOT'=>NULL)))->hydrate(false)->all();	
			
			foreach(array_chunk($userdata, 100) as  $key => $userval ) { 
				$deviceTknArr = [];
				foreach($userval as $notify_user_id) {
					$userDeviceTkn = $this->Users->find('all', array('conditions' => array('id' => $notify_user_id['id'],'access_level_id'=>2, 'enabled' => 'Y', 'is_deleted' => 'N')))->first();
					$deviceTknArr[] = $userDeviceTkn['device_token'];
				}
				$arrayToSend = array
					(
						'registration_ids' =>$deviceTknArr,
						'priority' => "high",
						'data'  => $keyDataArr
					);
				$headers = array(
					'Content-Type:application/json',
					'Authorization:key='.$server_key
				);	
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));
				$result = curl_exec($ch);
				curl_close($ch);
				if(isset($return) && $return == 'success'){
					$arr =  json_decode($result);
					return  $arr->success;
				}
			}
	}
	
	public function sendPushNotificationPayment($notification_type, $user_id=null,$message=null)
	{
		//print_r($user_id);die;
		$this->loadModel('Users');
		$url = 'https://fcm.googleapis.com/fcm/send';
		$server_key = 'AIzaSyAdC4iSaZt2KbwOlFvR7lza_QsV5u_m_e4'; 
		$keyDataArr = array('title'=>'Meena Geet', 'body'=>$message, 'sound'=>'mySound', 'badge' => '1', 'notification_type' => $notification_type, 'song_id' => '', 'songname' => '', 'challenge_id' => '', 'user_id' => $user_id);
		$user = $this->Users->find('all',array('fields'=>array('device_type','device_token'),'conditions'=>array('id'=>$user_id,'device_token IS NOT'=>NULL)))->hydrate(false)->first();	
		//print_r($user_id);die;
		if(isset($user)){
			$arrayToSend = array
				(
					'priority' => "high",
					'data'  => $keyDataArr
				);
			$arrayToSend['to'] = $user['device_token'];		
			
			$headers = array(
				'Content-Type:application/json',
				'Authorization:key='.$server_key
			);	
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));
			$result = curl_exec($ch);
			curl_close($ch);
			//print_r(json_decode($result));die;
			if(isset($return) && $return == 'success'){
				$arr =  json_decode($result);
				return  $arr->success;
			}	
		}		
	}
	
	/**** Audio size convert *******/
	public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}
	
	public function secondsToTime($seconds)
	{
		$ret = "";

		/*** get the days ***/
		$days = intval(intval($seconds) / (3600*24));
		if($days> 0)
		{
			$ret .= "$days days ";
		}

		/*** get the hours ***/
		$hours = (intval($seconds) / 3600) % 24;
		if($hours > 0)
		{
			$ret .= "$hours hours ";
		}

		/*** get the minutes ***/
		$minutes = (intval($seconds) / 60) % 60;
		if($minutes > 0)
		{
			$ret .= "$minutes minutes ";
		}

		/*** get the seconds ***/
		$seconds = intval($seconds) % 60;
		if ($seconds > 0) {
			$ret .= "$seconds seconds";
		}

		return $ret;

	}
	
	
	//get otp for mobil 
	public function getOtpMobile($number,$otp)
	{
		
		$curl = curl_init();

		  curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://alerts.prioritysms.com/api/web2sms.php?workingkey=A135fa3c5b3012a051b2b53c4d3685e2c&to=".$number."&sender=MeenaG&message=Your otp is ".$otp."",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo null;
		}
	}
	
	//get otp for mobil 
	public function getOtpMobileAdmin($number,$message)
	{
		
		$curl = curl_init();

		  curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://alerts.prioritysms.com/api/web2sms.php?workingkey=A135fa3c5b3012a051b2b53c4d3685e2c&to=".$number."&sender=MeenaG&message= ".$message."",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo null;
		}
	}


	
} 
