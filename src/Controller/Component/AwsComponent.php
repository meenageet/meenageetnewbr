<?php
namespace App\Controller\Component;

#use vendor\autoload;
use Cake\Controller\Component;

use Aws\Sdk;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;

class AwsComponent extends Component {
	
	
	function bucketUpload ($tmp , $actual_image_name,$directory){
        $s3Client = new S3Client([
            'version'     => 'latest',
            'region'      => AMAZON_BUCKET_REGION,
            'credentials' => [
                'key'    => AMAZON_BUCKET_ACCESSKEY,
                'secret' => AMAZON_BUCKET_SECRETKEY,
            ],
             'http' => [ 'verify' => false ]
        ]);

        $result = $s3Client->putObject(array(
                'SourceFile'   => $tmp,
                //'ContentType'  => $type,
                'Bucket' => AMAZON_BUCKET_NAME,
                'Key'    => $directory.$actual_image_name,
                'ACL'    => 'public-read',

            ));


        if($result){

            return $actual_image_name;
        }
        return false;
    }
	
	public function bucketFileDownload() {
        $s3Client = new S3Client([
            'version'     => 'latest',
            'region'      => AMAZON_BUCKET_REGION,
            'credentials' => [
                'key'    => AMAZON_BUCKET_ACCESSKEY,
                'secret' => AMAZON_BUCKET_SECRETKEY,
            ],
             'http' => [ 'verify' => false ]
        ]);

        $result = $s3Client->getObject(array(
            'Bucket' => AMAZON_BUCKET_NAME,
            'Key'    => 'images/add-icon.png'
        ));
		if($result){

            return $result;
        }
        return false;
    }
    
	/*public function awsconfig() {
		
		
		  $s3Client = new Aws\S3\S3Client([
            'version'     => 'latest',
            'region'      => AMAZON_BUCKET_REGION,
            'credentials' => [
                'key'    => AMAZON_BUCKET_ACCESSKEY,
                'secret' => AMAZON_BUCKET_SECRETKEY,
            ],
             'http' => [ 'verify' => false ]
        ]);

        $result = $s3Client->putObject(array(
                'SourceFile'   => $tmp,
                //'ContentType'  => $type,
                'Bucket' => AMAZON_BUCKET_NAME,
                'Key'    => $directory.$actual_image_name,
                'ACL'    => 'public-read',

            ));


        if($result){

            return $actual_image_name;
        }
        
		// Use the us-east-2 region and latest version of each client.
		$sharedConfig = [
			'profile' => 'default',
			'region' => 'us-east-2',
			'version' => 'latest'
		];
		//print_r($sharedConfig);die;
		// Create an SDK class used to share configuration across clients.
		$sdk = new Sdk($sharedConfig);
		
		
		// Use an Aws\Sdk class to create the S3Client object.
		$s3Client = $sdk->createS3();
		
		// Send a PutObject request and get the result object.
		$result = $s3Client->putObject([
			'Bucket' => 'meenageet',
			'Key' => 'AKIAIVQDG4VN7ADOMWBQ',
			'Body' => 'this is the body!'
		]);
		
		
		
		// Download the contents of the object.
		$result = $s3Client->getObject([
			'Bucket' => 'meenageet',
			'Key' => 'AKIAIVQDG4VN7ADOMWBQ'
		]);

		// Print the body of the result by indexing into the result object.
		echo $result['Body'];

	}*/
	 
}
