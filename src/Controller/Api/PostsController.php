<?php
namespace App\Controller\Api;
use App\Controller\AppController; // HAVE TO USE App\Controller\AppController
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\Validation\Validation;
use Cake\I18n\Time;

class PostsController extends AppController
{
	
	public function beforeFilter(Event $event)
    {
		 $this->Auth->allow();
		 $this->postPagination = 10;
	}
	
	/****************** Add post Api *******************************/
	public function addPost()
	{
		if($this->request->is(['post','put']))
    	{
			$error =true;$code=0;
			$message= '';$response = [];
				if(isset($this->request->data['user_id']))
			{ 	
				$image_base_url = path_post_image;
					
				if(isset($this->request->data['post_id']) && $this->request->data['post_id'] !=''){
					$post = $this->Posts->get($this->request->data['post_id']);
					$this->request->data['slug'] = trim($this->slugify($this->request->data['title'])); 
					$post = $this->Posts->patchEntity($post, $this->request->data,['validate'=>'ApiPost']);
					$before_image = $post->images;
				}else{
					$post = $this->Posts->newEntity();
					$this->request->data['slug'] = trim($this->slugify($this->request->data['title'])); 
					$post = $this->Posts->patchEntity($post, $this->request->data,['validate'=>'ApiPost']);
					$before_image = $post->images;
				}
					
				
			//	print_r($post);die;
				if($saved_post = $this->Posts->save($post))
				{
					$post_id = $saved_post->id;
					$post = $this->Posts->get($post_id);
					if(isset($_FILES['images']) && $_FILES['images']['tmp_name'] !='')
					{
						
						$postimages = $this->uploadFile($_FILES['images'],'post');
						if(isset($postimages) && $postimages!=''){
							$post['images'] = $postimages;
						}else{
							$post->images = $before_image;
						}
					} else  $post->images = $before_image;
					$this->Posts->save($post);
					$responsepost  = $this->Posts->find('all',array('conditions'=>array('id'=>$post_id)))->hydrate(false)->first();
						$response['image_base_url'] = $image_base_url;
						$error = false;
						$message = 'Congratulations!! Your post have Submitted successfully.';
						$response['post'] = $post;
				}
				else
				{
					$error = true;
						
					foreach($post->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
					}
				}
			}else{
				 $message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** Delete post Api ****************************/
	public function deletePost()
	{
		if($this->request->is(['post','put']))
    	{
			$error =true;$code=0;
			$message= $response = '';
			if( isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['post_id']) && $this->request->data['post_id'] !='')
			{
				$user_id = $this->request->data['user_id'];
				$post_id = $this->request->data['post_id'];
				$post = $this->Posts->get($post_id);
				$post->is_deleted = 'Y';
				$post->enabled = 'N';
				$this->Posts->save($post);
				$error = false;
				$message = 'Your post deleted successfully.';		
				
			}
			else $message = 'Incomplete Data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));			
		}
	}
	
	/****************** User my post Api ***************************/
	public function myPost()
	{
		if($this->request->is(['post','put']))
    	{
			$error = true; $code = 0;
			$message = '';
			$response = array(); $post_data = [];
			if(isset($this->request->data['page']) && $this->request->data['page'] !='') {
				$page = $this->request->data['page'];
			} else {
				$page = '1';
			}
			$image_base_url = path_post_image;
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] != '') {
			
				
				$searchDataPast = array();
				$searchDataPast['AND'][] = array("Posts.status"=>'1',"Posts.enabled !="=>'N' , "Posts.is_deleted !=" => 'Y');
				$searchDataPast['AND'][] = array("Posts.user_id" => $this->request->data['user_id']);
				
				$past_posts =  $this->Posts->find('all')->select()
								->contain([
									'user'=>['fields' => ['full_name']]
								])
								->where($searchDataPast)
								->order(['Posts.id'=>'DESC'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
			
			
				
				$past_post_data = array();
				if (!empty($past_posts)) {
					foreach ($past_posts as $key => $value) {
						$past_post_data[$key] = $value;
					}
					$error = false;
					$message = 'My Post Inquiries';
					$response['image_base_url'] = $image_base_url;
					$response['post'] = $past_post_data;
				} else {
					$error = true;
					$message = "No record available";	
				}
				
			} else {
				$error = true;
				$message= 'User id required field.';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** all post Api *******************************/
	public function allPost()
	{
		if($this->request->is(['post','put']))
    	{
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			$searchData = array();
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !=''){
				$searchData['AND'][] = array("Posts.user_id "=>$this->request->data['user_id'] ,"Posts.status"=>'1',"Posts.enabled"=>'Y' , "Posts.is_deleted !=" => 'Y');
			}else{
				$searchData['AND'][] = array("Posts.status"=>'1',"Posts.enabled"=>'Y' , "Posts.is_deleted !=" => 'Y');
			}
			$image_base_url = path_post_image;
			
			$posts =  $this->Posts->find('all')->select()
								->contain([
									'user'=>['fields' => ['full_name']]
								])
								->where($searchData)
								->order(['Posts.id'=>'DESC'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
			
			
		
			$past_post_data = array();
			if (isset($posts) &&  $posts !='' && !empty($posts)) {
				foreach ($posts as $key => $value) {
					$past_post_data[$key] = $value;
				}
				$error = false;
				$message = 'All Post';
				$response['image_base_url'] = $image_base_url;
				$response['post'] = $past_post_data;
			} else {
				$error = true;
				$message = "No record available";	
			}
			
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Post details Api ***************************/
	public function detailPost()
	{
		if($this->request->is(['post','put']))
    	{
			$error =true;$code=0;
			$message= $response = '';
			$this->loadModel('PostImages');
			$image_base_url = path_post_image;
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] != '' || isset($this->request->data['post_id']) && $this->request->data['post_id'] != ''){
				$searchData = array();
				$searchData['AND'][] = array("Posts.status"=>'1',"Posts.enabled "=>'Y' , "Posts.is_deleted !=" => 'Y', "Posts.id" => $this->request->data['post_id']);
				$posts = $this->Posts->find('all',array(
											'conditions'=>$searchData,
											))->contain(['user'=>['fields' => ['full_name']]])->first();
				$error = false;
				$message= 'Post Detail';
				$response = $posts;
				$response['image_base_url'] = $image_base_url;
				
			}else{
				$error = true;
				$message= 'User id required field.';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
}
