<?php
namespace App\Controller\Api;
use App\Controller\AppController; // HAVE TO USE App\Controller\AppController
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\Validation\Validation;
use Cake\I18n\Time;
use Stripe\Stripe;
use Paytm;

class UsersController extends AppController
{
	
	public function beforeFilter(Event $event)
    {
		 $this->Auth->allow();
		 $this->postPagination = 10;
	}
	
	/****************** Register function for api **********************/
    public function register()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('UserRegisters');
			$error = true; $code = 0;
			$message = $response = ''; 
			if( isset( $this->request->data['phone_number'] ) && isset( $this->request->data['device_type'] ) && isset( $this->request->data['device_token'] ) )
			{
				$otpNumber = rand(1111,9999);
				$userRegister = $this->UserRegisters->newEntity();
				$this->request->data['otp_number'] = $otpNumber;
				$this->request->data['phone_number'] = $this->request->data['phone_number'];
				$userRegister = $this->UserRegisters->patchEntity($userRegister, $this->request->data);
				if ($usrDetail = $this->UserRegisters->save($userRegister))
				{
					
					$userDataArr = [];
					$user_id = $usrDetail->id;
					$userData = $this->UserRegisters->find('all', array('conditions' => array('id' => $user_id)))->first();		
					$userDataArr['phone_number'] = $userData['phone_number'];
					$userDataArr['otp'] = $userData['otp_number'];
					
					//otp send on your mobile number
					$this->getOtpMobile($userData['phone_number'],$userData['otp_number']);
					
					$userMainData = $this->Users->find('all', array('conditions' => array('phone_number' => $userData['phone_number'])))->first();
					if(isset($userMainData) && $userMainData !=''){
						$userDataArr['user_id'] = $userMainData['id'];
					}else{
						$userDataArr['user_id'] = '0';
					}
					
					$response = $userDataArr;
					$error = false;
					$message = 'Congratulations!! You have registered successfully.';
						
					
				} else {
					$error = true;
					foreach($userRegister->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Login function for api *************************/
	public function otpLogin()
	{ 
 		if ($this->request->is(['post','put']))
    	{ 
			$this->loadModel('UserRegisters');
			$this->loadModel('PurchasePlans');
			$error = true; $code = 0;
			$message = $response = ''; 
			
			if (isset($this->request->data['phone_number']) && isset($this->request->data['otp_number']) && isset($this->request->data['user_id'])  && isset($this->request->data['device_token']) && isset($this->request->data['device_type']))
			{
				if($this->request->data['user_id'] == '0' ){
					
					$userregister_data = $this->UserRegisters->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
					/********* check otp exists in table are not **************/
					if($this->request->data['otp_number'] == $userregister_data['otp_number']){
						/******************** user register code ************/
						$user = $this->Users->newEntity();
						$this->request->data['username'] = 'user';
						$this->request->data['otp_number'] = $this->request->data['otp_number'];
						$this->request->data['phone_number'] = $this->request->data['phone_number'];	
						
						/*********** refer code assign to user code ************/
					
						if(isset($this->request->data['used_referal_code']) && $this->request->data['used_referal_code'] !='') {
							
							$user_refer = $this->Users->find('all', array(
									'fields' => array('Users.id', 'Users.refer_code', 'Users.free_subscription_time'),
									'conditions' => array('Users.refer_code' => $this->request->data['used_referal_code']),
							))->first();
							
							if(!empty($user_refer)) {
							
								$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
								$refer_code = "";
								for ($i = 0; $i < 10; $i++) {
									$refer_code .= $chars[mt_rand(0, strlen($chars)-1)];
								}
								$this->request->data['refer_code'] = $refer_code;	
								/*********** end refer code generate*********/			
								
								$user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'ApiOtp']);
								if ($usrDetail = $this->Users->save($user))
								{
										$user_id = $usrDetail->id;
									/************** get user detials after register ********************/
										$error = false;
										$message =  'Logged in successfully.';
										$user_data = $this->Users->find('all', array('conditions' => array('id' => $user_id)))->first();
										$user_data['device_token'] = $this->request->data['device_token']; 
										$user_data['device_type'] = $this->request->data['device_type'];
										$user_data['last_login'] = date('Y-m-d');
										$user_data['is_logged'] = 1;
										
										/*********** add free subscription time to user ************/
										$freeSubsDays = $this->Users->find('all', array(
												'fields' => array('Users.subscription_duration_month'),
												'conditions' => array('Users.access_level_id' => 1),
										))->first();
										//$chkRewards['start_time']->i18nFormat('yyyy-MM-dd HH:mm:ss')
										if(!empty($freeSubsDays)) {
											$user_data['free_subscription_time'] = date('Y-m-d h:i:s', strtotime("+".$freeSubsDays['subscription_duration_month'].' '."months", strtotime($user_data['created']->i18nFormat('yyyy-MM-dd HH:mm:ss'))));
											
											$user_free_time = $this->Users->get($user_id);
											$user_free_time->free_subscription_time = $user_data['free_subscription_time'];
											$this->Users->save($user_free_time);
										}
										$this->Users->save($user_data);
										/*********** End add free subscription time to user ************/
										
										$image_base_url = path_user;
										$user = $this->Users->find('all',array('conditions'=>array('id'=>$user['id'])))->first();
										if ($user['image'] != '')
										{
											$user['image'] = $image_base_url.$user['image'];
										} else {
											$user['image'] = '';
										}
										
										$response = $user;
										$this->set(array('user_id'=>$user['id'],'data'=>$response,'code'=>$code,'error'=>false,'message'=> $message,'_serialize'=>array('error','code','message','data','user_id')));
									
										/************** end get user detials after register ********************/
										if($user_refer['free_subscription_time'] == ''){
											
											$purchasePlanData = $this->PurchasePlans->find('all', array('conditions' => array('user_id' => $user_refer['id'],'status'=>'C','is_deleted'=>'N')))->first();  
											$purchase_subscription_time = date('Y-m-d h:i:s', strtotime("+10 day", strtotime($purchasePlanData['created']->i18nFormat('yyyy-MM-dd HH:mm:ss'))));
											$update_purchase_time = $this->PurchasePlans->get($purchasePlanData['id']);
											$update_purchase_time->created = $purchase_subscription_time;
											$this->PurchasePlans->save($update_purchase_time);
											
										}else{
											$free_subscription_time = date('Y-m-d h:i:s', strtotime("+10 day", strtotime($user_refer['free_subscription_time']->i18nFormat('yyyy-MM-dd HH:mm:ss'))));
											$update_free_time = $this->Users->get($user_refer['id']);
											$update_free_time->free_subscription_time = $free_subscription_time;
											$this->Users->save($update_free_time);
										}
										
									
										
									
									
								} else {
									$error = true;
									foreach($user->errors() as $field_key =>  $error_data)
									{
										foreach($error_data as $error_text)
										{
											$message = $field_key.", ".$error_text;
											break 2;
										} 
									}
								}
								
							} else {
								$response = [];
								$message = 'Your referal code is incorrect.';
								$this->set(array('data'=>$response,'code'=>$code,'error'=>true,'message'=> $message,'_serialize'=>array('error','code','message','data')));
							}
						} else {
							
						/*********** refer code assign to user code ************/
						
							/*********** refer code generate*********/
							$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
							$refer_code = "";
							for ($i = 0; $i < 10; $i++) {
								$refer_code .= $chars[mt_rand(0, strlen($chars)-1)];
							}
							$this->request->data['refer_code'] = $refer_code;	
							/*********** end refer code generate*********/			
							
							$user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'ApiOtp']);
							
							if ($usrDetail = $this->Users->save($user))
							{
									$user_id = $usrDetail->id;
								/************** get user detials after register ********************/
									$error = false;
									$message =  'Logged in successfully.';
									$user_data = $this->Users->find('all', array('conditions' => array('id' => $user_id)))->first();
									$user_data['device_token'] = $this->request->data['device_token']; 
									$user_data['device_type'] = $this->request->data['device_type'];
									$user_data['last_login'] = date('Y-m-d');
									$user_data['is_logged'] = 1;
									
									/*********** add free subscription time to user ************/
									$freeSubsDays = $this->Users->find('all', array(
											'fields' => array('Users.subscription_duration_month'),
											'conditions' => array('Users.access_level_id' => 1),
									))->first();
									if(!empty($freeSubsDays)) {
										$user_data['free_subscription_time'] = date('Y-m-d h:i:s', strtotime("+".$freeSubsDays['subscription_duration_month'].' '."months", strtotime($user_data['created']->i18nFormat('yyyy-MM-dd HH:mm:ss'))));
										
										$user_free_time = $this->Users->get($user_id);
										$user_free_time->free_subscription_time = $user_data['free_subscription_time'];
										$this->Users->save($user_free_time);
									}
									$this->Users->save($user_data);
									/*********** End add free subscription time to user ************/
									
									$image_base_url = path_user;
									$user = $this->Users->find('all',array('conditions'=>array('id'=>$user['id'])))->first();
									if ($user['image'] != '')
									{
										$user['image'] = $image_base_url.$user['image'];
									} else {
										$user['image'] = '';
									}
									$response = $user;
									$this->set(array('user_id'=>$user['id'],'data'=>$response,'code'=>$code,'error'=>false,'message'=> $message,'_serialize'=>array('error','code','message','data','user_id')));
								
								/************** end get user detials after register ********************/
								
							} else {
								$error = true;
								foreach($user->errors() as $field_key =>  $error_data)
								{
									foreach($error_data as $error_text)
									{
										$message = $field_key.", ".$error_text;
										break 2;
									} 
								}
							}
						}
						/******************** end user register code ************/
					}else{
						$response = [];
						$message = 'Please enter correct otp.';
						$this->set(array('data'=>$response,'code'=>$code,'error'=>true,'message'=> $message,'_serialize'=>array('error','code','message','data')));
					}
					
					/************** end otp exists in table ************************/
				}else{
					$userregister_data = $this->UserRegisters->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
					//echo "<pre>";print_r($userregister_data);exit;
					/********* check otp exists in table are not **************/
					if($this->request->data['otp_number'] == $userregister_data['otp_number']){
						/*********************** get user detsils *********************/
						$user = $this->Users->find('all',array('conditions'=>array('id'=>$this->request->data['user_id'])))->first();
						//print_r($user);die;
						if ($user['is_deleted'] == 'Y' || $user['enabled'] == 'N') {
							$message =  'Your account is blocked';
							$this->set(array('user_id'=>$user['id'], 'code'=>1, 'error'=>true, 'message'=> $message, '_serialize'=>array('error','code','message','user_id')));
						}
						else
						{
							$error = false;
							$message =  'Logged in successfully.';
							$user_data = $this->Users->find('all',array('conditions'=>array('id'=>$user['id'])))->first();
							$user_data['device_token'] = $this->request->data['device_token']; 
							$user_data['device_type'] = $this->request->data['device_type'];
							$user_data['last_login'] = date('Y-m-d');
							$user_data['is_logged'] = 1;
							$this->Users->save($user_data);
							$user = $this->Users->find('all',array('conditions'=>array('id'=>$user['id'])))->first();
							if ($user['image'] != '')
							{
								$user['image'] = $user['image'];
							} else {
								$user['image'] = '';
							}
							
							$response = $user;
							$this->set(array('user_id'=>$user['id'],'data'=>$response,'code'=>$code,'error'=>false,'message'=> $message,'_serialize'=>array('error','code','message','data','user_id')));
						}
						/*********************** end get user detsils *********************/
					}else{
						$response = [];
						$message = 'Please enter correct otp.';
						$this->set(array('data'=>$response, 'code'=>1,'error'=>true,'message'=> $message,'_serialize'=>array('error','code','message','data','user_id')));
					}
				}
			} else {
				$this->set(array('data' => array(), 'code' => 1,'error'=>true,'message'=> 'Incomplete data','_serialize'=>array('error','code','message','data')));
			} 						
		}
	}
	
	/****************** resend otp function for api ********************/
	public function resendOtp()
	{
		if ($this->request->is(array('post','put')))
		{
			$this->loadModel('UserRegisters');
			$error = true; $code=0;
			$message = $response = ''; 
			if(isset($this->request->data['phone_number']) && isset($this->request->data['user_id']))
			{
				
				if($this->request->data['user_id'] == '0'){
					$user = $this->UserRegisters->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
					$otpNumber = rand(1111,9999);
					if(!empty($user))
					{
						$user->otp_number = $otpNumber;
						$usrDetail = $this->UserRegisters->save($user);
						$userDataArr = [];
						$user_id = $usrDetail->id;
						$userData = $this->UserRegisters->find('all', array('conditions' => array('id' => $user_id)))->first();
						$userDataArr['phone_number'] = $userData['phone_number'];
						$userDataArr['otp'] = $userData['otp_number'];
						$userDataArr['user_id'] = '0';
						$response = $userDataArr;
						$message = "New OTP generate successfully.";
						$error = false;
						$this->getOtpMobile($userData['phone_number'],$userData['otp_number']);
						
					}
				}else{
					$userRegister = $this->UserRegisters->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
					$otpNumber = rand(1111,9999);
					
					if(!empty($userRegister))
					{
						$userRegister->otp_number = $otpNumber;
						$this->UserRegisters->save($userRegister);
						$user = $this->Users->find('all', array('conditions' => array('phone_number' => $this->request->data['phone_number']),'order' => array('id' => 'DESC')))->first();
						if(!empty($user))
						{
							$user->otp_number = $otpNumber;
							$usrDetail = $this->Users->save($user);
							$userDataArr = [];
							$user_id = $usrDetail->id;
							$userData = $this->Users->find('all', array('conditions' => array('id' => $user_id)))->first();
							$userDataArr['phone_number'] = $userData['phone_number'];
							$userDataArr['otp'] = $userData['otp_number'];
							$userDataArr['user_id'] = '0';
							$response = $userDataArr;
							$message = "New OTP generate successfully.";
							$error = false;
							$this->getOtpMobile($userData['phone_number'],$userData['otp_number']);
							
						}
					}
				}
				
			}
			else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
		
	}
	
	/****************** logout function for api ************************/
	public function logout()
	{
		if($this->request->is(['post','put']))
    	{
			$error = true; $code = 0;
			$message = $response = ''; 
			if (isset( $this->request->data['user_id']))
			{
				$user_data = $this->Users->find('all',array('conditions'=>array('id'=>$this->request->data['user_id'])))->first();
				if ($user_data['device_token'] ==  $this->request->data['device_token'])
				{
					$user_data['last_login'] = date('Y-m-d');
					$user_data['is_logged'] = 0;
					$user_data['device_token'] = '';
					$this->Users->save($user_data);
					$error = false;
					$message =  'Logged out successfully.';
				} else {
					if(!empty($user_data))
					{
						$user_data['last_login'] = date('Y-m-d');
						$user_data['is_logged'] = 0;
						$this->Users->save($user_data);
						$error = false;
						$message =  'Logged out successfully.';
					} else $message =  'User not found.';
				}
			}
			else $message =  'Incomplete data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('error','code','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('error','code','message','data')));
			
		}		
	}

	/****************** My profile function for api ********************/
	public function myProfile()
	{
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = ''; 
			$image_base_user_url = path_user;
			
			if(isset($this->request->data['user_id']))
			{
				$id = $this->request->data['user_id'];
				$users = $this->Users->find('all', array(
					'conditions' => array('Users.id' => $id, 'Users.access_level_id' => 2)
				))
				->hydrate(false)->first();
				
				if(!empty($users)) {
					if($users['image'] != ''){
						$image_base_url = path_user;
						$users['image'] = $users['image'];
					} else {
							$users['image']= '';
					}
					$error = false;
					$users['dob'] = date('Y-m-d', strtotime($users['dob']));
					$response =  $users;
					$response['image_base_user_url'] = $image_base_user_url;
				} 
				else $message = 'No Record Found';
			
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** edit user function for api *********************/
	public function editUser()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = ''; 
			
			 if(isset($this->request->data['user_id']))
			{
				$id = $this->request->data['user_id'];
				$users  = $this->Users->find('all',array('conditions'=>array('id'=>$id,'access_level_id'=>2)))->hydrate(false)->first();
				if(!empty($users))
				{
					$users  = $this->Users->get($id);
					
					
					$user = $this->Users->patchEntity($users, $this->request->data, ['validate' => 'ApiEditProfile']);
					$before_image = $user->image;
				
					if($saveUser = $this->Users->save($user))
					{ 
						$user = $this->Users->get($id);
						if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
						{
							$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $_FILES['image']['name']);
							$ext = pathinfo($filename, PATHINFO_EXTENSION);
							$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
							
							if ($this->uploadImage($_FILES['image']['tmp_name'], $_FILES['image']['type'], path_user_folder.'/', $filename)) {
								$url = path_user.$filename;
								$info = getimagesize($url);
								$width = 100;
								$aspectRatio = $info[1] / $info[0];
								$height = (int)($aspectRatio * $width);
								$this->createThumbnail($filename, path_user_folder, path_user_images_folder, $width,$height); 
								$user['image'] = $filename;
							}
							else  $user->image = $before_image;
						} else  $user->image = $before_image;
						$this->Users->save($user);
						$response  = $this->Users->find('all',array('conditions'=>array('id'=>$id)))->hydrate(false)->first();
						$response['dob'] = date('Y-m-d', strtotime($response['dob']));
						if($response['image'] != ''){
							$path = WWW_ROOT.path_user_folder."/".$response['image'];
							if(file_exists($path )) $response['image'] = $response['image'];
							else $response['image'] = '';
						} else $response['image'] = '';
						$image_base_user_url = path_user;
						$response['image_base_user_url'] = $image_base_user_url;
						$message = "Profile updated successfully";
						$error = false;
					}
					else
					{
						foreach($user->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$message = $field_key.", ".$error_text;
								break 2;
							} 
							
						}
					}
				} else $message = 'No Record Found';
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data'))); 
		}	
	}
	
	/****************** contact us function for api ********************/
	public function contactUs()
    {
		if ($this->request->is(array('post','put')))
		{
			if( isset( $this->request->data['subject'] ) && isset( $this->request->data['message'] ) )
			{
				$error = true; $code = 0;
				$message = $response = ''; 
				$this->loadModel('ContactUs');
				
				$contact = $this->ContactUs->newEntity();
				$contact = $this->ContactUs->patchEntity($contact, $this->request->data);
					
				if ($contactDetail = $this->ContactUs->save($contact))
				{ 
					$response = $contactDetail;
					$error = false;
					$message = 'Congratulations!! Your message has been submitted successfully.';
				} else {
					$error = true;
					foreach($contact->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}	
	}
	
	/****************** cms page function for api **********************/
	public function cmsPages()
    {
		if ($this->request->is(array('post','put')))
		{
			if ( isset( $this->request->data['id'] ) )
			{
				$error = true; $code = 0;
				$message = $response = ''; 
				$this->loadModel('Pages');
				$pages = $this->Pages->find('all',array('conditions'=>array('id'=>$this->request->data['id'], 'enabled'=>'1')))->hydrate(false)->toArray();
				if(!empty($pages))
				{ 
					$error = false;
					$response = $pages;
				} else { 
					$error = true;
					$message = "No record available";
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** rule for DJ / Singer page function for api **********************/
	public function rulePages()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = ''; 
			$response = []; 
			$this->loadModel('Pages');
			$pagesDJ = $this->Pages->find('all', array('fields' => array('Pages.title', 'Pages.description'),'conditions' => array('Pages.id' => 22, 'Pages.enabled' => 1)))->first();
			if(!empty($pagesDJ))
			{ 
				$response['rule_for_dj'] = $pagesDJ['description'];
			} else { 
				$response['rule_for_dj'] = '';
			}
			
			$pagesSinger = $this->Pages->find('all', array('fields' => array('Pages.title', 'Pages.description'),'conditions' => array('Pages.id' => 21, 'Pages.enabled' => 1)))->first();
			if(!empty($pagesSinger))
			{ 
				$response['rule_for_singer'] = $pagesSinger['description'];
			} else { 
				$response['rule_for_singer'] = '';
			}
			$error = false;
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** get category list function fo*******************/
	public function getCategories()
	{		
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = ''; 
			$this->loadModel('Categories');
			$category_data = array();
			if(isset($this->request->data['category_id']) && $this->request->data['category_id']!='') {
				$categories = $this->Categories->find('all', ['conditions' => array('enabled' => 'Y', 'is_deleted' => 'N', 'parent_category_id' => $this->request->data['category_id']), 'order' => array('category_name' => 'ASC')])->toArray();				
				foreach($categories as $key => $value) {
					$category_data[$key] = $value;
					$category_data[$key]['cat_logo_web'] = path_category_web_image.$value['cat_logo_web'];
				}
			} else {
				$categories = $this->Categories->find('all', ['conditions' => array('enabled' => 'Y', 'is_deleted' => 'N', 'parent_category_id' => 0), 'order' => array('category_name' => 'ASC')])->toArray();			
				foreach($categories as $key => $value) {
					$category_data[$key] = $value;
					$category_data[$key]['cat_logo_web'] = path_category_web_image.$value['cat_logo_web'];
				}
							
			}
			if(!empty($categories)) {
				$error = false;
				$response = $category_data;
			} else {
				$error = true;
				$message = "No record available";	
			}
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** upload song / edit song by user function for api ***************************/
    public function songUpload()
    {
		
		if($this->request->is(['post','put']))
    	{
			
			$this->loadModel('SongUploads');
			$this->loadModel('NotifyUsers');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				if(isset($this->request->data['song_upload_id']) && $this->request->data['song_upload_id'] !=''){
					$songUpload = $this->SongUploads->get($this->request->data['song_upload_id']);
					$songUpload = $this->SongUploads->patchEntity($songUpload, $this->request->data,['validate'=>'ApiEditPost']);
					$before_image = $songUpload->image;
					$before_audio = $songUpload->audio;
				}else{
					$songUpload = $this->SongUploads->newEntity();
					$songUpload = $this->SongUploads->patchEntity($songUpload, $this->request->data,['validate'=>'ApiPost']);
					$before_image = $songUpload->image;
					$before_audio = $songUpload->audio;
				}
					
			
				if($saved_songUpload = $this->SongUploads->save($songUpload))
				{
					$songUpload_id = $saved_songUpload->id;
					$songUpload = $this->SongUploads->get($songUpload_id);
					/******************* upload image **************************/
					if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
					{
						$image_base_url = path_song_upload_image;
						$songimages = $this->uploadFile($_FILES['image'],'song');
						
						if(isset($songimages) && $songimages!=''){
							$songUpload['image'] = $songimages;
						}else{
							$songUpload->image = $before_image;
						}
						
					} else  $songUpload->image = $before_image;
					/******************* end upload image **************************/
					
					/******************* upload audio **************************/
					if(isset($_FILES['audio']) && $_FILES['audio']['tmp_name'] !='')
					{
						
						$fileName = $_FILES['audio']['name'];
						$songaudio = $this->uploadFile($_FILES['audio'],'audio');
						//print_r($songaudio);die;
						if(isset($songaudio) && $songaudio!=''){
							$songUpload['audio'] = $songaudio;
						}else{
							$songUpload->audio = $before_audio;
						}
						
						
					} else  $songUpload->audio = $before_audio;
					/******************* end upload audio **************************/
					
					$this->SongUploads->save($songUpload);
					$responsepost  = $this->SongUploads->find('all',array('conditions'=>array('id'=>$songUpload_id)))->hydrate(false)->first();
					if($responsepost['image'] != '' OR $responsepost['image'] != null){
						$response['image'] = path_song_upload_image.$responsepost['image'];
					} else $response['image'] = path_song_upload_image.'no_image1564997469.png';
					
					if($responsepost['audio'] != ''){
						$response['audio'] = path_song_upload_audio.$responsepost['audio'];
					} else $response['audio'] = '';
						
					
					$error = false;
					$message = 'Congratulations!! Your song uploaded successfully.';
					$response['song'] = $songUpload;
					
					//save data for push notification 
					$NotifyUser = $this->NotifyUsers->newEntity();
					$notifyData['user_id'] = $this->request->data['user_id'];
					$notifyData['song_id'] = $responsepost['id'];
					$notifyData['type'] = 'song';
					$NotifyUser = $this->NotifyUsers->patchEntity($NotifyUser, $notifyData);
					$this->NotifyUsers->save($NotifyUser);
					//Send Push Notification to users
					//$this->sendNotification($this->request->data['user_id'], $responsepost['id'], $responsepost['title'],'song');
					
				}
				else
				{
					$error = true;
						
					foreach($songUpload->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
					}
				}
			}else{
				 $message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** upload song delete function for api ***************************/
	public function deleteSong()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$error =true;$code=0;
			$message= $response = '';
			if( isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['song_upload_id']) && $this->request->data['song_upload_id'] !='')
			{
				$user_id = $this->request->data['user_id'];
				$song_upload_id = $this->request->data['song_upload_id'];
				$songUpload = $this->SongUploads->get($song_upload_id);
				$songUpload->is_deleted = 'Y';
				$songUpload->enabled = 'N';
				$this->SongUploads->save($songUpload);
				$error = false;
				$message = 'Your song deleted successfully.';		
				
			}
			else $message = 'Incomplete Data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));			
		}
	}
	
	/****************** all song / My song list function for api ***************************/
	public function allSong()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			$image_base_url = path_song_upload_image;
			$audio_base_url = path_song_upload_audio;
			
			$searchData = array();
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !=''){
				$searchData['AND'][] = array("SongUploads.user_id "=>$this->request->data['user_id'] , "SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y');
			}else{
				$searchData['AND'][] = array("SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y');
			}
			
			$songUploads =  $this->SongUploads->find('all')->select()
								->contain([
									'user'=>['fields' => ['full_name']],
									'category'=>['fields' => ['category_name']],
									'subcategory'=>['fields' => ['category_name']]
								])
								->where($searchData)
								->order(['SongUploads.id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
								
	
			$song_post_data = array();
			if (isset($songUploads) &&  $songUploads !='' && !empty($songUploads)) {
				foreach ($songUploads as $key => $value) {
					$song_post_data['id'] = $value['id'];
					$song_post_data['user_id'] = $value['user_id'];
					$song_post_data['title'] = $value['title'];
					$song_post_data['cat_id'] = $value['cat_id'];
					$song_post_data['sub_cat_id'] = $value['sub_cat_id'];
					$song_post_data['image'] = $value['image'];
					$song_post_data['audio'] = $value['audio'];
					$song_post_data['size'] = $value['size'];
					$song_post_data['duration'] = $value['duration'];
					$song_post_data['enabled'] = $value['enabled'];
					$song_post_data['is_deleted'] = $value['is_deleted'];
					$song_post_data['created'] = date('Y m d h:i:s', strtotime($value['created']));
					$song_post_data['cat_title'] = $value['category']['category_name'];
					$song_post_data['sub_cat_title'] = $value['subcategory']['category_name'];
					$song_post_data['user_name'] = $value['user']['full_name'];
					
					$songDetail = $this->showLike($value['id'], $value['user_id']);
					$song_post_data['likes'] = $songDetail['likes'];
					$song_post_data['total_likes'] = $songDetail['total_likes'];
					$song_post_data['unlikes'] = $songDetail['unlikes'];
					$song_post_data['total_unlikes'] = $songDetail['total_unlikes'];
					$song_post_data['is_comment'] = $songDetail['is_comment'];
					$song_post_data['total_comments'] = $songDetail['total_comments'];
					
					//print_r($likeUnlikeArr); die;
					
					$song_post_dataArr[] = $song_post_data;
				}
				$error = false;
				$message = 'All Post';
				$response['image_base_url'] = $image_base_url;
				$response['audio_base_url'] = $audio_base_url;
				if(isset($song_post_dataArr) &&  $song_post_dataArr !='' && !empty($song_post_dataArr)) {
					$response['song'] = $song_post_dataArr;
				}else{
					$response['song'] = [];
				}
				
			} else {
				$error = true;
				$message = "No record available";	
				
			}
			
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Download song by user function for api ***************************/
    public function downloadSongUpload()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Downloads');
			$this->loadModel('SongUploads');
			$this->loadModel('PurchasePlans');
			$this->loadModel('RequestDjSingers');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				
				/*********** Check if user has bought a plan or not ************/
				$userPlan = $this->PurchasePlans->find('all', array(
					'fields' => array('PurchasePlans.id', 'PurchasePlans.status', 'PurchasePlans.created'),
					'conditions' => array('PurchasePlans.user_id' => $this->request->data['user_id'], 'PurchasePlans.status' => 'C', 'PurchasePlans.is_deleted' => 'N'),
					'contain'=>[
						'Premiums'=>['fields' => ['id','duration_type','duration']]
					]
				))->first();
				
				if($userPlan['premium']['duration_type'] == 'Monthly') { 
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."months", strtotime($userPlan['created'])));
				} elseif($userPlan['Premiums']['duration_type'] == 'Yearly') {	
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."years", strtotime($userPlan['created'])));
				}
				
				//calulation current time
				date_default_timezone_set('Asia/Kolkata');
				$currrenttime = time();
				$current_date = date('Y-m-d', $currrenttime);
				
				$chkuser = $this->RequestDjSingers->find('all', array(
							'fields' => array('id'),
							'conditions' => array('user_id' => $this->request->data['user_id'], 'is_approved' => 'Y', 'is_deleted' => 'N'),
					))->first();
					
				if(!empty($chkuser)){
					
						$download = $this->Downloads->newEntity();
						$download = $this->Downloads->patchEntity($download, $this->request->data);
						
						if($saved_download = $this->Downloads->save($download))
						{
							$download_id = $saved_download->id;
							$download = $this->Downloads->get($download_id);
							
							$responsepost  = $this->SongUploads->find('all',array('conditions'=>array('id'=>$download->song_id)))->hydrate(false)->first();
							if($responsepost['image'] != ''){
								$path = WWW_ROOT.path_song_image_folder."/".$responsepost['image'];
								if(file_exists($path )) $response['image'] = path_song_upload_image.$responsepost['image'];
								else $response['image'] = '';
							} else $response['image'] = '';
							
							if($responsepost['audio'] != ''){
								$path = WWW_ROOT.path_song_audio_folder."/".$responsepost['audio'];
								if(file_exists($path )) $response['audio'] = path_song_upload_audio.$responsepost['audio'];
								else $response['audio'] = '';
							} else $response['audio'] = '';
								
							
								$error = false;
								$message = 'Congratulations!! Your song downloaded successfully.';
								$response['download'] = $download;
						}
						else
						{
							$error = true;
								
							foreach($download->errors() as $field_key =>  $error_data)
							{
								foreach($error_data as $error_text)
								{
									$message = $field_key.", ".$error_text;
									break 2;
								} 
							}
						}
						
				}else{
					if ($userPlan =='' && empty($userPlan)) { 
						
						$free_subscription = $this->Users->find('all', array(
								'fields' => array('Users.id', 'Users.free_subscription_time'),
								'conditions' => array('Users.id' => $this->request->data['user_id']),
						))->first();
						
						if (isset($free_subscription['free_subscription_time']) && $free_subscription['free_subscription_time'] !='' && !empty($free_subscription['free_subscription_time'])) {
							$subsExpireDate =  date('Y-m-d', strtotime($free_subscription['free_subscription_time']));
							$previousSubsExpireDate = date('Y-m-d', strtotime('-1 day', strtotime($subsExpireDate)));
							//print_r($previousSubsExpireDate); die;
							if($current_date >= $subsExpireDate) {
								$free_plan = $this->Users->get($free_subscription['id']);
								$free_plan->free_subscription_time = null;
								$this->Users->save($free_plan);
								
								// push notification code here
								$message = 'Your free subscription plan has been expired. you have you purchse a subscription plan to download the song.';
								$this->sendPushNotification('Song_download', null, null, null, $this->request->data['user_id'], $message);
								
							} else {
								if ($current_date == $previousSubsExpireDate) {
									// push notification code here
									$message = 'Your free subscription plan is going to expire today.';
									$this->sendPushNotification('Song_download', null, null, null, $this->request->data['user_id'], $message);
								} 
								$download = $this->Downloads->newEntity();
								$download = $this->Downloads->patchEntity($download, $this->request->data);
								
								if($saved_download = $this->Downloads->save($download))
								{
									$download_id = $saved_download->id;
									$download = $this->Downloads->get($download_id);
									
									$responsepost  = $this->SongUploads->find('all',array('conditions'=>array('id'=>$download->song_id)))->hydrate(false)->first();
									if($responsepost['image'] != ''){
										$path = WWW_ROOT.path_song_image_folder."/".$responsepost['image'];
										if(file_exists($path )) $response['image'] = path_song_upload_image.$responsepost['image'];
										else $response['image'] = '';
									} else $response['image'] = '';
									
									if($responsepost['audio'] != ''){
										$path = WWW_ROOT.path_song_audio_folder."/".$responsepost['audio'];
										if(file_exists($path )) $response['audio'] = path_song_upload_audio.$responsepost['audio'];
										else $response['audio'] = '';
									} else $response['audio'] = '';
										
									
										$error = false;
										$message = 'Congratulations!! Your song downloaded successfully.';
										$response['download'] = $download;
								}
								else
								{
									$error = true;
										
									foreach($download->errors() as $field_key =>  $error_data)
									{
										foreach($error_data as $error_text)
										{
											$message = $field_key.", ".$error_text;
											break 2;
										} 
									}
								} 
							} 
						} else {
							// push notification code here
							$message = 'Your plan has been expired. Please upgrade your plan to download the song.';
							$this->sendPushNotification('Song_download', null, null, null, $this->request->data['user_id'], $message);
						}
					} elseif($current_date >= $planExpireDate) {
						$purchase_plan = $this->PurchasePlans->get($userPlan['id']);
						$purchase_plan->status = 'E';
						$this->PurchasePlans->save($purchase_plan);
						// push notification code here
						$message = 'Your plan has been expired. Please upgrade your plan to download the song.';
						$this->sendPushNotification('Song_download', null, null, null, $this->request->data['user_id'], $message);
					} else {
						
						$download = $this->Downloads->newEntity();
						$download = $this->Downloads->patchEntity($download, $this->request->data);
						
						if($saved_download = $this->Downloads->save($download))
						{
							$download_id = $saved_download->id;
							$download = $this->Downloads->get($download_id);
							
							$responsepost  = $this->SongUploads->find('all',array('conditions'=>array('id'=>$download->song_id)))->hydrate(false)->first();
							if($responsepost['image'] != ''){
								$path = WWW_ROOT.path_song_image_folder."/".$responsepost['image'];
								if(file_exists($path )) $response['image'] = path_song_upload_image.$responsepost['image'];
								else $response['image'] = '';
							} else $response['image'] = '';
							
							if($responsepost['audio'] != ''){
								$path = WWW_ROOT.path_song_audio_folder."/".$responsepost['audio'];
								if(file_exists($path )) $response['audio'] = path_song_upload_audio.$responsepost['audio'];
								else $response['audio'] = '';
							} else $response['audio'] = '';
								
							
								$error = false;
								$message = 'Congratulations!! Your song downloaded successfully.';
								$response['download'] = $download;
						}
						else
						{
							$error = true;
								
							foreach($download->errors() as $field_key =>  $error_data)
							{
								foreach($error_data as $error_text)
								{
									$message = $field_key.", ".$error_text;
									break 2;
								} 
							}
						} 
					}
					/********/
				}
				
			} else {
				$message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Download song delete function for api ***************************/
	public function deleteDownloadSong()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Downloads');
			$error =true;$code=0;
			$message= $response = '';
			if( isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['download_id']) && $this->request->data['download_id'] !='')
			{
				$user_id = $this->request->data['user_id'];
				$download_id = $this->request->data['download_id'];
				$download = $this->Downloads->get($download_id);
				$download->is_deleted = 'Y';
				$this->Downloads->save($download);
				$error = false;
				$message = 'Your download song deleted successfully.';		
				
			}
			else $message = 'Incomplete Data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));			
		}
	}
	
	/****************** all song list function for api ***************************/
	public function allDownloadSong()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Downloads');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['user_id']))
			{
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$image_base_url = path_song_upload_image;
				$audio_base_url = path_song_upload_audio;
				
				$searchData = array();
				$searchData['AND'][] = array("Downloads.is_deleted !=" => 'Y',"Downloads.user_id" => $this->request->data['user_id']);
				
				$download =  $this->Downloads->find('all')->select()
								->contain([
									'user'=>['fields' => ['full_name']],'songUploads'=>function($q){ 
										return $q->find('all', ['conditions' => array('songUploads.is_deleted'=>'N')])->contain(['category','subcategory']);
									}
								])
								->where($searchData)
								->order(['Downloads.id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
				
				
				
				$download_data = array();
				$download_dataArr = array();
				//print_r($download);die;
				if (isset($download) &&  $download !='' && !empty($download)) {
					foreach ($download as $key => $value) {
						$download_data['id'] = $value['id'];
						$download_data['song_id'] = $value['song_upload']['id'];
						$download_data['title'] = $value['song_upload']['title'];
						$download_data['category_name'] = $value['song_upload']['category']['category_name'];
						$download_data['sub_category_name'] = $value['song_upload']['subcategory']['category_name'];
						$download_data['image'] = $image_base_url.$value['song_upload']['image'];
						$download_data['audio'] = $audio_base_url.$value['song_upload']['audio'];
						$download_data['size'] = $value['song_upload']['size'];
						$download_data['duration'] = $value['song_upload']['duration'];
						
						$songDetail = $this->showLike($value['song_id'], $value['user_id']);
						$download_data['likes'] = $songDetail['likes'];
						$download_data['total_likes'] = $songDetail['total_likes'];
						$download_data['unlikes'] = $songDetail['unlikes'];
						$download_data['total_unlikes'] = $songDetail['total_unlikes'];
						$download_data['is_comment'] = $songDetail['is_comment'];
						$download_data['total_comments'] = $songDetail['total_comments'];
						
						$download_dataArr[] = $download_data;
						
					}
					$error = false;
					$message = 'All Song';
					$response['image_base_url'] = $image_base_url;
					$response['audio_base_url'] = $audio_base_url;
					
					if(isset($download_dataArr) &&  $download_dataArr !='' && !empty($download_dataArr)) {
						$response['song'] = $download_dataArr;
					}else{
						$response['song'] = [];
					}
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Send Request for Become a singer / Dj function for api ***************************/
	public function requestSingerDj()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('RequestDjSingers');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				$requestDjSinger = $this->RequestDjSingers->newEntity();
				$requestDjSinger = $this->RequestDjSingers->patchEntity($requestDjSinger, $this->request->data,['validate'=>'ApiAdd']);					
				if($saved_request = $this->RequestDjSingers->save($requestDjSinger))
				{
					$image_base_url = path_IDProof_image;
					$request_id = $saved_request->id;
					$requestDjSinger = $this->RequestDjSingers->get($request_id);
					if(isset($_FILES['id_proof']) && $_FILES['id_proof']['tmp_name'] !='')
					{
						
						$id_proofimages = $this->uploadFile($_FILES['id_proof'],'idproof');
						if(isset($id_proofimages) && $id_proofimages!=''){
							$requestDjSinger['id_proof'] = $id_proofimages;
						}else{
							$requestDjSinger['id_proof'] = '';
						}
						
					}
					$this->RequestDjSingers->save($requestDjSinger);
					$responseRequest  = $this->RequestDjSingers->find('all',array('conditions'=>array('id'=>$request_id)))->hydrate(false)->first();
					if($responseRequest['id_proof'] != ''){
						$path = WWW_ROOT.path_IDProof_image_folder."/".$responseRequest['id_proof'];
						$response['id_proof'] = path_IDProof_image.$responseRequest['id_proof'];
					} else $response['id_proof'] = '';
						$error = false;
						$message = 'Congratulations!! Your form have Submitted successfully.';
						$response = $requestDjSinger;
				}
				else
				{
					$error = true;
					foreach($requestDjSinger->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
					}
				}
			}else{
				 $message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** Create Challenge function for api ***************************/
	public function createChallenge()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$this->loadModel('Challenges');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				if(isset($this->request->data['challenge_id']) && $this->request->data['challenge_id'] !=''){
					$challenges = $this->Challenges->get($this->request->data['challenge_id']);
					date_default_timezone_set('Asia/Kolkata');
					$currrenttime = time();
					$challenge_time = $challenges->challenge_time;
					$hours = floor($challenge_time / 3600);
					$total_time = date("Y-m-d H:i:s", strtotime('+'.$hours.' hours'));
					$this->request->data['remaining_time'] =  $total_time;
					$challenges = $this->Challenges->patchEntity($challenges, $this->request->data,['validate'=>'ApiEditChallenge']);
					$before_image = $challenges->image;
					
				}else{
					$challenges = $this->Challenges->newEntity();
					date_default_timezone_set('Asia/Kolkata');
					$currrenttime = time();
					$challenge_time = $this->request->data['challenge_time'];
					$hours = floor($challenge_time / 3600);
					$total_time = date("Y-m-d H:i:s", strtotime('+'.$hours.' hours'));
					$this->request->data['remaining_time'] =  $total_time;
					$challenges = $this->Challenges->patchEntity($challenges, $this->request->data,['validate'=>'ApiChallenge']);
					$before_image = $challenges->image;
				}
				
				
				if($saved_challenges = $this->Challenges->save($challenges))
				{
					$image_base_url = path_song_upload_image;
					$challenges_id = $saved_challenges->id;
					$challenge = $this->Challenges->get($challenges_id);
					/******************* upload image **************************/
					if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
					{
						
						$challengeimages = $this->uploadFile($_FILES['image'],'song');
						if(isset($challengeimages) && $challengeimages!=''){
							$challenge['image'] = $challengeimages;
						}else{
							$challenge->image = $before_image;
						}
						
					} else  $challenge->image = $before_image;
					/******************* end upload image **************************/
					 
					$this->Challenges->save($challenge);
					$responsechallenges  = $this->Challenges->find('all',array('conditions'=>array('id'=>$challenges_id)))->hydrate(false)->first();
					if($responsechallenges['image'] != ''){
						$responsechallenges['image'] = $responsechallenges['image'];
					} else $responsechallenges['image'] = '';
						
						$challenge['id'] = $challenges_id;
						$challenge['created'] = date('Y-m-d h:i:s', strtotime($challenge['created']));
						$error = false;
						$message = 'Congratulations!! Your challenge created successfully.';
						$response['challenge'] = $challenge;
						
						$songsdata = $this->SongUploads->find()->where(['id'=>$responsechallenges['song_id'],"is_deleted !=" => 'Y', "enabled" => 'Y'])->first();
						
						$this->loadModel('NotifyUsers');
						$NotifyUser = $this->NotifyUsers->newEntity();
						$notifyData['user_id'] = $this->request->data['user_id'];
						$notifyData['song_id'] = $responsechallenges['song_id'];
						$notifyData['type'] = 'challenge';
						$NotifyUser = $this->NotifyUsers->patchEntity($NotifyUser, $notifyData);
						$this->NotifyUsers->save($NotifyUser);
						
						
				}
				else
				{
					$error = true;
						
					foreach($challenges->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
					}
				}
			}else{
				 $message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	
	/****************** comment List function for api ***************************/
	public function comments()
	{
		if($this->request->is(['post','put']))
    	{
			
			$this->loadModel('Comments');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			
			$searchData = array();
			if(isset($this->request->data['song_id']) && $this->request->data['song_id'] !='' ){
				$searchData['AND'][] = array("Comments.is_deleted !=" => 'Y',"Comments.song_id"=>$this->request->data['song_id']);
				
				$commentLists =  $this->Comments->find('all')->select()
								->contain([
									'Users'=>['fields' => ['full_name']],
									'SongUploads'=>['fields' => ['title']],
								])
								->where($searchData)
								->order(['Comments.id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
				
				$song_comment_data = array();
				if (isset($commentLists) &&  $commentLists !='' && !empty($commentLists)) {
					foreach ($commentLists as $key => $value) {
						$song_comment_data['id'] = $value['id'];
						$song_comment_data['user_id'] = $value['user_id'];
						$song_comment_data['title'] = $value['song_upload']['title'];
						$song_comment_data['song_id'] = $value['song_id'];
						$song_comment_data['is_deleted'] = $value['is_deleted'];
						$song_comment_data['comments'] = $value['comment'];
						$song_comment_data['created'] = date('Y m d h:i:s', strtotime($value['created']));
						$song_comment_data['user_name'] = $value['user']['full_name'];
						$song_comment_dataArr[] = $song_comment_data;
					}
					$error = false;
					$message = 'All Comments';
					
						
					if(isset($song_comment_dataArr) &&  $song_comment_dataArr !='' && !empty($song_comment_dataArr)) {
						$response['comment'] = $song_comment_dataArr;
					}else{
						$response['comment'] = [];
					}
					
				} else {
					$error = true;
					$message = "No record available";
					$response['comment'] = [];	
				}
				
			}else{
				 $message = 'Incomplete Data';
			}
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	
	/****************** Add comment api **********************/
    public function addComment()
    {
		if($this->request->is(['post','put']))
    	{
    	
			$this->loadModel('Comments');
			$this->loadModel('PurchasePlans');
			$this->loadModel('RequestDjSingers');
			$error = true; $code = 0;
			$message = $response = []; 
			if( isset( $this->request->data['user_id'] ) && isset( $this->request->data['song_id'] ) && isset( $this->request->data['comment'] )) 
			{
				$chkuser = $this->RequestDjSingers->find('all', array(
							'fields' => array('id'),
							'conditions' => array('user_id' => $this->request->data['user_id'], 'is_approved' => 'Y', 'is_deleted' => 'N'),
					))->first();
				if(!empty($chkuser)){
					// add comment code
					$addcomment = $this->Comments->newEntity();
					$addcomments = $this->Comments->patchEntity($addcomment, $this->request->data);
					if ($CommentsDetail = $this->Comments->save($addcomments))
					{
						$error = false;
						$message = 'Comment Added Successfuly';
					} else {
						$error = true;
						foreach($addcomments->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$message = $field_key.", ".$error_text;
								break 2;
							} 
							
						}
					}
				}else{
					/*********** Check if user has bought a plan or not ************/
					$userPlan = $this->PurchasePlans->find('all', array(
						'fields' => array('PurchasePlans.id', 'PurchasePlans.status', 'PurchasePlans.created'),
						'conditions' => array('PurchasePlans.user_id' => $this->request->data['user_id'], 'PurchasePlans.status' => 'C', 'PurchasePlans.is_deleted' => 'N'),
						'contain'=>[
							'Premiums'=>['fields' => ['id','duration_type','duration']]
						]
					))->first();
					
					if($userPlan['premium']['duration_type'] == 'Monthly') { 
						$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."months", strtotime($userPlan['created'])));
					} elseif($userPlan['premium']['duration_type'] == 'Yearly') {	
						$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."years", strtotime($userPlan['created'])));
					}
					
					//calulation current time
					date_default_timezone_set('Asia/Kolkata');
					$currrenttime = time();
					$current_date = date('Y-m-d', $currrenttime);
				
					if ($userPlan =='' && empty($userPlan)) { 
						
						$free_subscription = $this->Users->find('all', array(
								'fields' => array('Users.id', 'Users.free_subscription_time'),
								'conditions' => array('Users.id' => $this->request->data['user_id']),
						))->first();
						
						if (isset($free_subscription['free_subscription_time']) && $free_subscription['free_subscription_time'] !='' && !empty($free_subscription['free_subscription_time'])) {
							$subsExpireDate =  date('Y-m-d', strtotime($free_subscription['free_subscription_time']));
							$previousSubsExpireDate = date('Y-m-d', strtotime('-1 day', strtotime($subsExpireDate)));
							//print_r($previousSubsExpireDate); die;
							if($current_date >= $subsExpireDate) {
								$free_plan = $this->Users->get($free_subscription['id']);
								$free_plan->free_subscription_time = null;
								$this->Users->save($free_plan);
								
								// push notification code here
								$message = 'Your free subscription plan has been expired. you have you purchse a subscription plan to comment.';
								$this->sendPushNotification('add_comment', $this->request->data['song_id'], null, null, $this->request->data['user_id'], $message);
							} else {
								if ($current_date == $previousSubsExpireDate) {
									// push notification code here
									$message = 'Your free subscription plan is going to expire today.';
									$this->sendPushNotification('add_comment', $this->request->data['song_id'], null, null, $this->request->data['user_id'], $message);
								} 
								// add comment code
								$addcomment = $this->Comments->newEntity();
								$addcomments = $this->Comments->patchEntity($addcomment, $this->request->data);
								if ($CommentsDetail = $this->Comments->save($addcomments))
								{
									$error = false;
									$message = 'Comment Added Successfuly';
								} else {
									$error = true;
									foreach($addcomments->errors() as $field_key =>  $error_data)
									{
										foreach($error_data as $error_text)
										{
											$message = $field_key.", ".$error_text;
											break 2;
										} 
										
									}
								} 
							} 
						} else {
							// push notification code here
							$message = 'Your plan has been expired. Please upgrade your plan to comment.';
							$this->sendPushNotification('add_comment', $this->request->data['song_id'], null, null, $this->request->data['user_id'], $message);
						}
					} elseif($current_date >= $planExpireDate) {
						$purchase_plan = $this->PurchasePlans->get($userPlan['id']);
						$purchase_plan->status = 'E';
						$this->PurchasePlans->save($purchase_plan);
						// push notification code here
						$message = 'Your plan has been expired. Please upgrade your plan to comment.';
						$this->sendPushNotification('add_comment', $this->request->data['song_id'], null, null, $this->request->data['user_id'], $message);
					} else {
						
						// add comment code
						$addcomment = $this->Comments->newEntity();
						$addcomments = $this->Comments->patchEntity($addcomment, $this->request->data);
						if ($CommentsDetail = $this->Comments->save($addcomments))
						{
							$error = false;
							$message = 'Comment Added Successfuly';
						} else {
							$error = true;
							foreach($addcomments->errors() as $field_key =>  $error_data)
							{
								foreach($error_data as $error_text)
								{
									$message = $field_key.", ".$error_text;
									break 2;
								} 
								
							}
						}
					}
				}
				
				
				$songDetail = $this->showLike($this->request->data['song_id'], $this->request->data['user_id']);
				$response['total_comments'] = $songDetail['total_comments'];
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
		
	/****************** Like/Dislike api **********************/
    public function likeDislike()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Likes');
			$error = true; $code = 0;
			$message = $response = array(); 
			
			if( isset( $this->request->data['user_id'] ) && isset( 
			$this->request->data['song_id'] ) && isset( $this->request->data['like'] )) 
			{
				$likedata = $this->Likes->find()->where(['user_id'=>$this->request->data['user_id'],
				'song_id'=>$this->request->data['song_id']])->toArray();
				if(!empty($likedata)){
					$likesdetail  = $this->Likes->get($likedata[0]['id']);
					if($this->request->data['like']==0){
						$this->request->data['status']=0;
						$message = 'You dislike this song';
					}else{
						$this->request->data['status']=1;
						$message = 'Thanks for like this song';
					}
					$likeadd = $this->Likes->patchEntity($likesdetail, $this->request->data);
					$saveLikes = $this->Likes->save($likeadd);
					$error = false;
					$response['total_likes'] = $this->Likes->find()->where(['song_id'=>$this->request->data['song_id'],'status'=>1])->count();
					$response['total_unlikes'] = $this->Likes->find()->where(['song_id'=>$this->request->data['song_id'],'status'=>0])->count();
				}else{
					$addLikes = $this->Likes->newEntity();
					if($this->request->data['like']==0){
						$this->request->data['status']=0;
						$message = 'You dislike this song';
					}else{
						$this->request->data['status']=1;
						$message = 'Thanks for like this song';
					}
					$addLike = $this->Likes->patchEntity($addLikes, $this->request->data);
					if ($LikesDetail = $this->Likes->save($addLike))
					{
						$response['total_likes'] = $this->Likes->find()->where(['song_id'=>$this->request->data['song_id'],'status'=>1])->count();
						$response['total_unlikes'] = $this->Likes->find()->where(['song_id'=>$this->request->data['song_id'],'status'=>0])->count();
						$error = false;
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data'=>array(),'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Search songs api **********************/
	public function searchSongs()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['page']) && $this->request->data['page'] !='' ){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			$image_base_url = path_song_upload_image;
			$audio_base_url = path_song_upload_audio;
			
			$searchData = array();
			/*********** Search using by text **********************/
			if(isset($this->request->data['text']) || isset($this->request->data['user_id']) || isset($this->request->data['cat_id']) || isset($this->request->data['sub_cat_id'])){
				
				if($this->request->data['cat_id']!='' && $this->request->data['sub_cat_id'] !='' && $this->request->data['text']!='' && $this->request->data['user_id']!=''){
					$searchData['AND'][] = ["SongUploads.cat_id" => $this->request->data['cat_id'],"SongUploads.sub_cat_id" => $this->request->data['sub_cat_id'],'SongUploads.title LIKE' => '%'.$this->request->data['text'].'%',"SongUploads.user_id" => $this->request->data['user_id']];
				}if($this->request->data['cat_id']!='' && $this->request->data['sub_cat_id'] !='' && $this->request->data['text']!='' && $this->request->data['user_id']==''){
					$searchData['AND'][] = ["SongUploads.cat_id" => $this->request->data['cat_id'],"SongUploads.sub_cat_id" => $this->request->data['sub_cat_id'],'SongUploads.title LIKE' => '%'.$this->request->data['text'].'%'];
				}else if($this->request->data['cat_id']!='' && $this->request->data['sub_cat_id'] !='' && $this->request->data['user_id']!='' && $this->request->data['text'] == ''){
					$searchData['AND'][] = ["SongUploads.cat_id" => $this->request->data['cat_id'],"SongUploads.sub_cat_id" => $this->request->data['sub_cat_id'],"SongUploads.user_id" => $this->request->data['user_id']];
				}else if($this->request->data['cat_id']!='' && $this->request->data['sub_cat_id'] !='' && $this->request->data['user_id']=='' && $this->request->data['text'] == ''){
					$searchData['AND'][] = ["SongUploads.cat_id" => $this->request->data['cat_id'],"SongUploads.sub_cat_id" => $this->request->data['sub_cat_id']];
				}else if($this->request->data['user_id']!='' && $this->request->data['text'] !=''){
					$searchData['AND'][] = ['SongUploads.title LIKE' => '%'.$this->request->data['text'].'%',"SongUploads.user_id" => $this->request->data['user_id']];
				}else if($this->request->data['user_id']!='' && $this->request->data['text'] ==''){
					$searchData['AND'][] = ["SongUploads.user_id" => $this->request->data['user_id']];
				}else{
					$searchData['AND'][] = ['SongUploads.title LIKE' => '%'.$this->request->data['text'].'%'];
				}
				$searchData['AND'][] = array("SongUploads.is_deleted !=" => 'Y');
			}
			
			
				$searchList =  $this->SongUploads->find('all')->select()
								->contain([
									'user'=>['fields' => ['full_name']],
									'category'=>['fields' => ['category_name']],
									'subcategory'=>['fields' => ['category_name']]
								])
								->where($searchData)
								->order(['SongUploads.id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
								
			
				$searchList_data = array();
				$searchList_dataArr = array();
				if (isset($searchList) &&  $searchList !='' && !empty($searchList)) {
					foreach ($searchList as $key => $value) {
						$searchList_data['id'] = $value['id'];
						$searchList_data['user_id'] = $value['user_id'];
						$searchList_data['title'] = $value['title'];
						$searchList_data['cat_id'] = $value['cat_id'];
						$searchList_data['sub_cat_id'] = $value['sub_cat_id'];
						$searchList_data['image'] = $value['image'];
						$searchList_data['audio'] = $value['audio'];
						
						$searchList_data['size'] = $value['size'];
						$searchList_data['duration'] = $value['duration'];
						
						$searchList_data['enabled'] = $value['enabled'];
						$searchList_data['is_deleted'] = $value['is_deleted'];
						$searchList_data['created'] = date('Y m d h:i:s', strtotime($value['created']));
						$searchList_data['cat_title'] = $value['category']['category_name'];
						$searchList_data['sub_cat_title'] = $value['subcategory']['category_name'];
						$searchList_data['user_name'] = $value['user']['full_name'];
						
						$songDetail = $this->showLike($value['id'], $value['user_id']);
						$searchList_data['likes'] = $songDetail['likes'];
						$searchList_data['total_likes'] = $songDetail['total_likes'];
						$searchList_data['unlikes'] = $songDetail['unlikes'];
						$searchList_data['total_unlikes'] = $songDetail['total_unlikes'];
						$searchList_data['is_comment'] = $songDetail['is_comment'];
						$searchList_data['total_comments'] = $songDetail['total_comments'];
						
						$searchList_dataArr[] = $searchList_data;
					}
					$error = false;
					$message = 'All Song';
					$response['image_base_url'] = $image_base_url;
					$response['audio_base_url'] = $audio_base_url;
					
					if(isset($searchList_dataArr) &&  $searchList_dataArr !='' && !empty($searchList_dataArr)) {
						$response['song'] = $searchList_dataArr;
					}else{
						$response['song'] = [];
					}
					
				}else {
					$response['song'] = [];
					$error = false;
					$message = "all songs";	
				}
		
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
			
	/****************** Search song Detail function for api ***************************/
	public function songDetail()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('SongUploads');
			$this->loadModel('Likes');
			$this->loadModel('Comments');
			$error =true;$code=0;
			$message= ""; $response = [];
		
			$image_base_url = path_song_upload_image;
			$audio_base_url = path_song_upload_audio;
			
			$songDetails=$this->SongUploads->get($this->request->data['song_id'], ['contain'=> [
							'user'=>['fields' => ['full_name']],
							'category'=>['fields' => ['category_name']],
							'subcategory'=>['fields' => ['category_name']]
							]])->toArray();
			$songDetails_dataArr = array();
			if (isset($songDetails) &&  $songDetails !='' && !empty($songDetails)) {
				
				
				
				
				$songDetails_data['id'] = $songDetails['id'];
				$songDetails_data['user_id'] = $songDetails['user_id'];
				$songDetails_data['title'] = $songDetails['title'];
				$songDetails_data['cat_id'] = $songDetails['cat_id'];
				$songDetails_data['sub_cat_id'] = $songDetails['sub_cat_id'];
				$songDetails_data['image'] = $songDetails['image'];
				$songDetails_data['audio'] = $songDetails['audio'];
				$songDetails_data['size'] = $songDetails['size'];
				$songDetails_data['duration'] = $songDetails['duration'];
				$songDetails_data['enabled'] = $songDetails['enabled'];
				$songDetails_data['is_deleted'] = $songDetails['is_deleted'];
				$songDetails_data['created'] = date('Y m d h:i:s', strtotime($songDetails['created']));
				$songDetails_data['cat_title'] = $songDetails['category']['category_name'];
				$songDetails_data['sub_cat_title'] = $songDetails['subcategory']['category_name'];
				$songDetails_data['user_name'] = $songDetails['user']['full_name'];
				
				$songDetail = $this->showLike($songDetails['id'], $songDetails['user_id']);
				$songDetails_data['likes'] = $songDetail['likes'];
				$songDetails_data['total_likes'] = $songDetail['total_likes'];
				$songDetails_data['unlikes'] = $songDetail['unlikes'];
				$songDetails_data['total_unlikes'] = $songDetail['total_unlikes'];
				$songDetails_data['is_comment'] = $songDetail['is_comment'];
				$songDetails_data['total_comments'] = $songDetail['total_comments'];
				
				
				
				$songDetails_data['image_base_url'] = $image_base_url;
				$songDetails_data['audio_base_url'] = $audio_base_url;
				
				$error = false;
				$message = 'Song Details';
				$response = $songDetails_data;
			} else {
				$error = true;
				$message = "No record available";	
				$response = [];
			}
			
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** My Challenge Other user challange list function for api ***************************/
	public function myChallengeList()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Challenges');
			$error =true;$code=0;
				$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$searchData = array();
				if(isset($this->request->data['status']) && $this->request->data['status'] =='1'){
					$searchData['AND'][] = array("Challenges.user_id"=>$this->request->data['user_id'], "Challenges.is_deleted !=" => 'Y', "Challenges.status" => '1');
				}else{
					$searchData['AND'][] = array("Challenges.user_id"=>$this->request->data['user_id'], "Challenges.is_deleted !=" => 'Y');
				}
				
				
				$challengesData =  $this->Challenges->find('all')->select()
								->contain([
									'user'=>['fields' => ['full_name','image']],
									'song'=>['fields' => ['title']]
								])
								->where($searchData)
								->order(['Challenges.id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
								
				
				$challengesDataArr = array();
				$image_base_user_url = path_user;
				$image_base_url = path_song_upload_image;
				if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
					foreach ($challengesData as $key => $value) {
						
						$challenges['id'] = $value['id'];
						$challenges['user_id'] = $value['user_id'];
						$challenges['accept_user_id'] = $value['accept_user_id'];
						$challenges['type'] = $value['type'];
						$challenges['song_id'] = $value['song_id'];
						$challenges['challenge_time'] = $value['challenge_time'];
						$challenges['challenge_amount'] = $value['challenge_amount'];
						$challenges['challenge_image'] = $value['image'];
						$challenges['song_name'] = $value['song']['title'];
						$challenges['full_name'] = $value['user']['full_name'];
						$challenges['userimage'] = $value['user']['image'];
						$challenges['status'] = $value['status'];
						if($value['status'] == 1){
							$challenges['challenge_live'] = true;
						}else{
							$challenges['challenge_live'] = false;
						}
						
						if($value['status'] == 3){
							$challenges['challenge_winner'] = true;
						}else{
							$challenges['challenge_winner'] = false;
						}
						
						if($value['status'] == 0){
							$challenges['is_pending'] = true;
						}else{
							$challenges['is_pending'] = false;
						}
							
						
						$this->loadModel('ChallengeLikes');
						$total_challenge_likes = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user_id'],'status'=>1])->count();

						$total_challenge_unlikes = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user_id'],'status'=>0])->count();
						
						$challenges['total_challenge_likes'] = $total_challenge_likes;
						$challenges['total_challenge_unlikes'] = $total_challenge_unlikes;
						
						$challengesDataArr[] = $challenges;
					}
					$error = false;
					$message = 'My Challenge';
					$response['image_base_user_url'] = $image_base_user_url;
					$response['image_base_url'] = $image_base_url;
					
					if(isset($challengesDataArr) &&  $challengesDataArr !='' && !empty($challengesDataArr)) {
						$response['challenge'] = $challengesDataArr;
					}else{
						$response['challenge'] = [];
					}
					
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	
	/****************** Add Song in Recently list function for api ***************************/
	public function recentlyPlayed()
	{
		if($this->request->is(['post','put']))
    	{
    	
			$this->loadModel('RecentlySongs');
			$error = true; $code = 0;
			$message = $response = ''; 
			if( isset( $this->request->data['user_id'] ) && $this->request->data['user_id']  !='' && isset( $this->request->data['song_id'] ) && $this->request->data['song_id'] !='') 
			{
				$recentlySong = $this->RecentlySongs->newEntity();
				$recentlySongs = $this->RecentlySongs->patchEntity($recentlySong, $this->request->data);
				if ($recentlyDetail = $this->RecentlySongs->save($recentlySongs))
				{
					$error = false;
					$message = 'Song Added in Recently list.';
				} else {
					$error = true;
					foreach($recentlySongs->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	
	
	/****************** Singer / DJ lsit function for api ***************************/
	public function singerDj()
	{
		if($this->request->is(['post','put']))
    	{
			$error =true;$code=0;
			$message= ""; $response = [];
			$this->loadModel('RequestDjSingers');
			$this->loadModel('Likes');
			$this->loadModel('NotificationStatus');
			if(isset($this->request->data['type']) && isset($this->request->data['user_id']))
			{ 
				
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$searchData = array();
				if($this->request->data['type'] =='S'){
					$typeRequest = 'Singer';
					$searchData['AND'][] = array("RequestDjSingers.type"=>'S',"RequestDjSingers.user_id !="=>$this->request->data['user_id'],"RequestDjSingers.is_approved"=>'Y', "RequestDjSingers.is_deleted !=" => 'Y');
				}else{
					$typeRequest = 'D';
					$searchData['AND'][] = array("RequestDjSingers.type"=>'D',"RequestDjSingers.user_id !="=>$this->request->data['user_id'],"RequestDjSingers.is_approved"=>'Y', "RequestDjSingers.is_deleted !=" => 'Y');
				}
				
				
				$requestDjSingers =  $this->RequestDjSingers->find('all')->select()
								->contain([
									'user'=>['fields' => ['full_name','image','email','dob','address']]
								])
								->where($searchData)
								->order(['RequestDjSingers.id'=>'asc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
								
				
				$requestDjSingersdata = array();
				
				$image_base_url = path_user;
				if (isset($requestDjSingers) &&  $requestDjSingers !='' && !empty($requestDjSingers)) {
					foreach ($requestDjSingers as $key => $value) {
						
						$total_likes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>1])->count();
						$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['user_id'],'status'=>0])->count();
						
						$requestDj['id'] = $value['id'];
						$requestDj['user_id'] = $value['user_id'];
						$requestDj['full_name'] = $value['user']['full_name'];
						$requestDj['email'] = $value['user']['email'];
						$requestDj['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$requestDj['address'] = $value['user']['address'];
						$requestDj['image'] = $value['user']['image'];
						$requestDj['like_count'] = $total_likes;
						$requestDj['unlike_count'] = $total_unlikes;
						
						$isNotificationStatus  = $this->NotificationStatus->find('all',array('conditions'=>array('user_id'=>$this->request->data['user_id'],'other_user_id'=>$value['user_id'])))->hydrate(false)->first();
						if(isset($isNotificationStatus) && $isNotificationStatus !='' && !empty($isNotificationStatus)){
							$requestDj['notification'] = false;
						}else{
							$requestDj['notification'] = true;
						}
						
						$requestDjSingersdata[] = $requestDj;
					}
					$error = false;
					$message = 'All '.$typeRequest;
					$response['image_base_url'] = $image_base_url;
					
					if(isset($requestDjSingersdata) &&  $requestDjSingersdata !='' && !empty($requestDjSingersdata)) {
						$response['user'] = $requestDjSingersdata;
					}else{
						$response['user'] = [];
					}
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	
	/******************Accept Challenge function for api ***************************/
	public function acceptChallenge()
	{
		
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Challenges');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']) && isset($this->request->data['challenge_id']) && $this->request->data['challenge_id'] !='' && isset($this->request->data['status']) && $this->request->data['status'] !='')
			{ 		
				
				//calculate remaining time
				$challenges = $this->Challenges->get($this->request->data['challenge_id']);
				
				if($this->request->data['status'] =='1'){
					$action = 'Accepted';
				}else{
					
					$action = 'Declined';
				}
				//echo "<pre>"; print_r($challenges);exit;
				$isChkchallenges  = $this->Challenges->find('all',array('conditions'=>array('id'=>$challenges->id,'song_id'=>$challenges->song_id,'status'=>'1')))->hydrate(false)->first();
				
				if(!isset($isChkchallenges) && empty($isChkchallenges)){
						//calulation time
						date_default_timezone_set('Asia/Kolkata');
						$currrenttime = time();
						$date = strtotime(date('Y-m-d h:i:s',$currrenttime));
						$date2 = strtotime(date('Y-m-d h:i:s',strtotime($challenges->remaining_time)));
						$diff = $date2 - $date;
					$challenges->remaining_second =  abs($diff);
					if($this->request->data['status'] =='1'){
						$challenges->status =  '1';
						$challenges->accept_user_id =  $this->request->data['user_id'];
						$challenges->accepted_user_song_id =  $this->request->data['accepted_user_song_id'];
					}else{
						//$challenges->accept_user_id =  $this->request->data['user_id'];
						$challenges->status =  '0';
					}
					
					
					//echo "<pre>"; print_r($challenges);exit;
					
					if($saved_challenges = $this->Challenges->save($challenges))
					{
						$challenges_id = $saved_challenges->id;
						
						//RejectChallenges save data in table
						$this->loadModel('RejectChallenges');
						$RejectChallenge = $this->RejectChallenges->newEntity();
						$RejectChallengeData['user_id'] = $this->request->data['user_id'];
						$RejectChallengeData['challenge_id'] = $challenges_id;
						$RejectChallenge = $this->RejectChallenges->patchEntity($RejectChallenge, $RejectChallengeData);
						$this->RejectChallenges->save($RejectChallenge);
						
						/*************accept user upload image functionlity ****************/
						$image_base_url = path_song_upload_image;
						$challenge = $this->Challenges->get($challenges_id);
						/******************* upload image **************************/
						if(isset($_FILES['accepted_user_image']) && $_FILES['accepted_user_image']['tmp_name'] !='')
						{
							
							$challengeimages = $this->uploadFile($_FILES['accepted_user_image'],'song');
							if(isset($challengeimages) && $challengeimages!=''){
								$challenge['accepted_user_image'] = $challengeimages;
							}else{
								$challenge->image = $before_image;
							}
							
						} else  $challenge->image = $before_image;
						/******************* end upload image **************************/
						
						$this->Challenges->save($challenge);
						$responsechallenges  = $this->Challenges->find('all',array('conditions'=>array('id'=>$challenges_id)))->hydrate(false)->first();
						if($responsechallenges['accepted_user_image'] != ''){
							$responsechallenges['accepted_user_image'] = $responsechallenges['accepted_user_image'];
						} else $responsechallenges['accepted_user_image'] = '';
							
						/*************accept user upload image functionlity ****************/
						$responsechallenges['created'] = date('Y-m-d h:i:s', strtotime($responsechallenges['created']));
						$error = false;
						$message = 'Congratulations!! Your challenge '.$action.' successfully.';
						$response['challenge'] = $responsechallenges;
					}
					else
					{
						$error = true;
							
						foreach($challenges->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$message = $field_key.", ".$error_text;
								break 2;
							} 
						}
					}
				}else{
					$image_base_url = path_song_upload_image;
					$challenges_id = $this->request->data['challenge_id'];
					$responsechallenges  = $this->Challenges->find('all',array('conditions'=>array('id'=>$challenges_id)))->hydrate(false)->first();
					if($responsechallenges['image'] != ''){
						$responsechallenges['image'] = $responsechallenges['image'];
					} else $responsechallenges['image'] = '';
						
						$responsechallenges['created'] = date('Y-m-d h:i:s', strtotime($responsechallenges['created']));
						$error = false;
						$message = 'Sorry!! This challenge allready '.$action.'.';
						$response['challenge'] = $responsechallenges;
				}
				
			}else{
				 $message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	}
	
		/****************** Live Challenge for accept / decline list function for api ***************************/
	public function liveChallengeList()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Challenges');
			$this->loadModel('RejectChallenges');
			$error =true;$code=0;
				$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				
				
				$searchData = array();
				
				$rejectChellIds = 	$this->RejectChallenges->find('list', ['keyField' => 'id','valueField' => 'challenge_id','conditions'=>array('user_id'=>$this->request->data['user_id'])])->toArray();
				//print_r($rejectChellIds);die;
				if(isset($rejectChellIds) && !empty($rejectChellIds)){
					$searchData['AND'][] = array("Challenges.id NOT IN"=>$rejectChellIds);
				}
				$searchData['AND'][] = array("Challenges.user_id !="=>$this->request->data['user_id']);
				$searchData['AND'][] = array("Challenges.is_deleted !=" => 'Y', "Challenges.status" => '0');
				
				//print_r($searchData);die;
				$challengesData = $this->Paginator->paginate(
					$this->Challenges, [
						'limit' => '10',
						'page' => $page,
						'order'=>['id'=>'asc'],
						'contain'=>[
							'user'=>['fields' => ['full_name','image']],
							'song'=>['fields' => ['title']]
						],
						'conditions'=>$searchData,
				]);
				$challengesDataArr = array();
				$image_base_user_url = path_user;
				$image_base_url = path_song_upload_image;
				if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
					foreach ($challengesData as $key => $value) {
						
						$challenges['id'] = $value['id'];
						$challenges['user_id'] = $value['user_id'];
						$challenges['type'] = $value['type'];
						$challenges['song_id'] = $value['song_id'];
						$challenges['challenge_time'] = $value['challenge_time'];
						$challenges['challenge_amount'] = $value['challenge_amount'];
						$challenges['challenge_image'] = $value['image'];
						$challenges['song_name'] = $value['song']['title'];
						$challenges['full_name'] = $value['user']['full_name'];
						$challenges['userimage'] = $value['user']['image'];
						$challenges['status'] = $value['status'];
						
						//second calculation ago time
						date_default_timezone_set('Asia/Kolkata');
						$createdTime = strtotime(date('Y-m-d h:i:s',strtotime($value['created'])));
						$challenges['created'] = $this->time_elapsed_string($createdTime);
						
						$songDetail = $this->showLike($value['song_id'], $value['user_id']);
						$challenges['likes'] = $songDetail['likes'];
						$challenges['total_likes'] = $songDetail['total_likes'];
						$challenges['unlikes'] = $songDetail['unlikes'];
						$challenges['total_unlikes'] = $songDetail['total_unlikes'];
						$challenges['is_comment'] = $songDetail['is_comment'];
						$challenges['total_comments'] = $songDetail['total_comments'];
						
						$challengesDataArr[] = $challenges;
					}
					$error = false;
					$message = 'My Challenge';
					$response['image_base_user_url'] = $image_base_user_url;
					$response['image_base_url'] = $image_base_url;
					
					if(isset($challengesDataArr) &&  $challengesDataArr !='' && !empty($challengesDataArr)) {
						$response['challenge'] = $challengesDataArr;
					}else{
						$response['challenge'] = [];
					}
						
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Create New Playlist api **********************/
    public function createNewPlaylist()
    {
		if($this->request->is(['post','put']))
    	{   	
			$this->loadModel('PlaylistCategories');
			$error = true; $code = 0;
			$message = $response = []; 
			if( isset( $this->request->data['user_id'] ) && isset( $this->request->data['category_name'] )) 
			{
				$addplaylist = $this->PlaylistCategories->newEntity();
				$addplaylists = $this->PlaylistCategories->patchEntity($addplaylist, $this->request->data);
				if ($PlaylistsDetail = $this->PlaylistCategories->save($addplaylists))
				{
					$error = false;
					$message = 'Playlist Added Successfuly';
				} else {
					$error = true;
					foreach($addplaylists->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Delete Playlist api ***************************/
	public function deletePlaylist()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('PlaylistCategories');
			$error = true; $code = 0;
			$message = $response = '';
			if( isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['playlist_id']) && $this->request->data['playlist_id'] !='')
			{
				$user_id = $this->request->data['user_id'];
				$playlist_id = $this->request->data['playlist_id'];
				$playlist = $this->PlaylistCategories->get($playlist_id);
				$playlist->is_deleted = 'Y';
				$this->PlaylistCategories->save($playlist);
				$error = false;
				$message = 'Your Playlist category has been deleted successfully.';						
			}
			else $message = 'Incomplete Data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));			
		}
	}
	
	/****************** All playlist categories list function for api ***************************/
	public function playlistCategoriesList()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('PlaylistCategories');
			$this->loadModel('Playlists');
			$error = true; $code = 0;
			$message = ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$searchData = array();
				
				$searchData['AND'][] = array("PlaylistCategories.user_id"=>$this->request->data['user_id'], "PlaylistCategories.is_deleted !=" => 'Y');
				
				
				$playlistsData =  $this->PlaylistCategories->find('all')->select()
							->contain([
								'user'=>['fields' => ['full_name','image']]
								//'playlist'
							])
							->where($searchData)
							->order(['PlaylistCategories.id'=>'desc'])
							->limit($this->postPagination)
							->page($page)
							->hydrate(false)->toArray();
				//print_r($playlistsData);die;
				$songsDataArr = array();
				$playlistsDataArr = array();
				$image_base_user_url = path_user;
				$image_base_url = path_song_upload_image;
				if (isset($playlistsData) &&  $playlistsData !='' && !empty($playlistsData)) {
					foreach ($playlistsData as $key => $value) {
						
						$playdata = $this->Playlists->find('all', array(
							'fields' => array('Playlists.id', 'Playlists.song_id', 'Playlists.user_id'),
							'conditions' => array('Playlists.user_id' => $value['user_id'],'Playlists.playlist_category_id' => $value['id'],'Playlists.enabled'=>'Y','Playlists.is_deleted !='=>'Y'),
							'contain'=>[
								'user'=>['fields' => ['full_name','image']],
								'songUploads'=> function(\Cake\ORM\Query $q) {
										return $q->find('all', array(
											'fields' => ['id','user_id','cat_id','sub_cat_id','title','image','audio','size','duration','enabled','is_deleted','created'],
											'conditions' => array("songUploads.is_deleted !=" => 'Y', "songUploads.enabled" => 'Y'),
											'contain'=>[
												'user'=>['fields' => ['id','full_name']],
												'category'=>['fields' => ['id','category_name']],
												'subcategory'=>['fields' => ['id','category_name']]
											],
											'order'=>['songUploads.id'=>'desc'],	
										));
									}
							]	
						))->hydrate(false)->toArray();
							$songsDataArr = array();
							foreach($playdata as $playlistsval){
								if(isset($playlistsval['song_upload']) && $playlistsval['song_upload'] !=''){
									$song_data['id'] = $playlistsval['song_upload']['id'];
									$song_data['user_id'] = $playlistsval['song_upload']['user_id'];
									$song_data['title'] = $playlistsval['song_upload']['title'];
									$song_data['cat_id'] = $playlistsval['song_upload']['cat_id'];
									$song_data['sub_cat_id'] = $playlistsval['song_upload']['sub_cat_id'];
									$song_data['image'] = $playlistsval['song_upload']['image'];
									$song_data['audio'] = $playlistsval['song_upload']['audio'];
									$song_data['size'] = $playlistsval['song_upload']['size'];
									$song_data['duration'] = $playlistsval['song_upload']['duration'];
									$song_data['enabled'] = $playlistsval['song_upload']['enabled'];
									$song_data['is_deleted'] = $playlistsval['song_upload']['is_deleted'];
									$song_data['created'] = date('Y m d h:i:s', strtotime($playlistsval['song_upload']['created']));
									$song_data['cat_title'] = $playlistsval['song_upload']['category']['category_name'];
									$song_data['sub_cat_title'] = $playlistsval['song_upload']['subcategory']['category_name'];
									$song_data['user_name'] = $playlistsval['song_upload']['user']['full_name'];
									
									$songDetail = $this->showLike($playlistsval['song_upload']['id'], $playlistsval['song_upload']['user_id']);
									$song_data['likes'] = $songDetail['likes'];
									$song_data['total_likes'] = $songDetail['total_likes'];
									$song_data['unlikes'] = $songDetail['unlikes'];
									$song_data['total_unlikes'] = $songDetail['total_unlikes'];
									$song_data['is_comment'] = $songDetail['is_comment'];
									$song_data['total_comments'] = $songDetail['total_comments'];
									$songsDataArr[] = $song_data;
								}
								
							}
						$playlists['id'] = $value['id'];
						$playlists['user_id'] = $value['user_id'];
						$playlists['category_name'] = $value['category_name'];
						$playlists['full_name'] = $value['user']['full_name'];
						$playlists['userimage'] = $value['user']['image'];
						$playlists['songs'] = $songsDataArr;
						
						$playlistsDataArr[] = $playlists;
					}
					$error = false;
					$message = 'Playlist Categories';
					$response['image_base_user_url'] = $image_base_user_url;
					$response['image_base_url'] = $image_base_url;
					if(isset($playlistsDataArr) &&  $playlistsDataArr !='' && !empty($playlistsDataArr)) {
						$response['playlist_categories'] = $playlistsDataArr;
					}else{
						$response['playlist_categories'] = [];
					}
					
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Playlist songs listing by category function for api ***************************/
	public function songListingByPlaylistCategory()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Playlists');
			$error = true; $code = 0;
			$message = ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['playlist_category_id']) && $this->request->data['playlist_category_id'] !='')
			{ 
				$playlistsData = $this->Playlists->find('all', array(
					'fields' => array('Playlists.id', 'Playlists.song_id', 'Playlists.user_id'),
					'conditions' => array('Playlists.user_id' => $this->request->data['user_id'], 'Playlists.playlist_category_id' => $this->request->data['playlist_category_id']),
					'contain'=>[
						'user'=>['fields' => ['full_name','image']],
						'songUploads'=>
							function(\Cake\ORM\Query $q) {
									return  $q->find('all', array(
										'fields' => ['id','user_id','cat_id','sub_cat_id','title','image','audio','size','duration','enabled','is_deleted','created'],
										'conditions' => array("songUploads.is_deleted !=" => 'Y', "songUploads.enabled" => 'Y'),
										'contain'=>[
											'user'=>['fields' => ['id','full_name']],
											'category'=>['fields' => ['id','category_name']],
											'subcategory'=>['fields' => ['id','category_name']]
										],
										'order'=>['songUploads.id'=>'DESC'],	
									));
								}
					]	
				));
			
				$playlistsDataArr = array();
				$image_base_user_url = path_user;
				$image_base_url = path_song_upload_image;
				$audio_base_url = path_song_upload_audio;
				
				if (isset($playlistsData) &&  $playlistsData !='' && !empty($playlistsData)) {
					$req_song_Arr = [];
					foreach ($playlistsData as $key => $value) {
							$req_songA['id'] = $value['song_upload']['id'];
							$req_songA['user_id'] = $value['song_upload']['user_id'];
							$req_songA['title'] = $value['song_upload']['title'];
							$req_songA['cat_id'] = $value['song_upload']['cat_id'];
							$req_songA['sub_cat_id'] = $value['song_upload']['sub_cat_id'];
							$req_songA['image'] = $value['song_upload']['image'];
							$req_songA['audio'] = $value['song_upload']['audio'];
							$req_songA['size'] = $value['song_upload']['size'];
							$req_songA['duration'] = $value['song_upload']['duration'];
							$req_songA['enabled'] = $value['song_upload']['enabled'];
							$req_songA['is_deleted'] = $value['song_upload']['is_deleted'];
							$req_songA['created'] = date('Y m d h:i:s', strtotime($value['song_upload']['created']));
							$req_songA['cat_title'] = $value['song_upload']['category']['category_name'];
							$req_songA['sub_cat_title'] = $value['song_upload']['subcategory']['category_name'];
							$req_songA['user_name'] = $value['song_upload']['user']['full_name'];
							
							$songDetail = $this->showLike($value['song_upload']['id'], $value['song_upload']['user_id']);
							$req_songA['likes'] = $songDetail['likes'];
							$req_songA['total_likes'] = $songDetail['total_likes'];
							$req_songA['unlikes'] = $songDetail['unlikes'];
							$req_songA['total_unlikes'] = $songDetail['total_unlikes'];
							$req_songA['is_comment'] = $songDetail['is_comment'];
							$req_songA['total_comments'] = $songDetail['total_comments'];
							$req_song_Arr[] = $req_songA;
					}
					$error = false;
					$message = 'Playlist Category Songs';
					$response['image_base_user_url'] = $image_base_user_url;
					$response['image_base_url'] = $image_base_url;
					$response['audio_base_url'] = $audio_base_url;
					
					if(isset($req_song_Arr) &&  $req_song_Arr !='' && !empty($req_song_Arr)) {
						$response['playlist_category_songs'] = $req_song_Arr;
					}else{
						$response['playlist_category_songs'] = [];
					}
						
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			} else {
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Delete song from playlist function for api ***************************/
	public function deleteSongFromPlaylist()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Playlists');
			$error = true; $code = 0;
			$message = $response = '';
			if( isset($this->request->data['user_id']) && $this->request->data['user_id'] !='' && isset($this->request->data['playlist_id']) && $this->request->data['playlist_id'] !='')
			{
				$user_id = $this->request->data['user_id'];
				$playlist_id = $this->request->data['playlist_id'];
				$playlist = $this->Playlists->get($playlist_id);
				$playlist->is_deleted = 'Y';
				$this->Playlists->save($playlist);
				$error = false;
				$message = 'Song from Playlist category has been deleted successfully.';						
			}
			else $message = 'Incomplete Data.';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));			
		}
	}
	
	/****************** Add Songs to Playlist function for api ***************************/
	public function addSongsToPlaylist()
	{
		if($this->request->is(['post','put']))
    	{   	
			$this->loadModel('Playlists');
			$error = true; $code = 0;
			$message = $response = ''; 
			
			if( isset( $this->request->data['user_id'] ) && isset( $this->request->data['song_ids']) && $this->request->data['song_ids'] != "" && isset( $this->request->data['playlist_category_id'] ) )
			{
				$song_ids =  $this->request->data['song_ids'];
				$song_ids = json_decode($song_ids);
				//print_r($song_ids); die;
				if(is_array($song_ids) && !empty($song_ids))
				{ 
					foreach($song_ids as $song_id)
					{
						$prev_user_playlist = $this->Playlists->find('all', array(
							'fields' => array('Playlists.id'),
							'conditions' => array('Playlists.user_id' => $this->request->data['user_id'], 'Playlists.song_id' => $song_id, 'Playlists.playlist_category_id' => $this->request->data['playlist_category_id'])
						))->first();
						
						if(!empty($prev_user_playlist)) {
							$entity = $this->Playlists->get($prev_user_playlist['id']);
							$this->Playlists->delete($entity);
						}
						
						$this->request->data['song_id'] = $song_id;
						//print_r($this->request->data); die;
						$addplaylist = $this->Playlists->newEntity();
						$addplaylists = $this->Playlists->patchEntity($addplaylist, $this->request->data);
						if ($PlaylistsDetail = $this->Playlists->save($addplaylists))
						{
							$error = false;
							$message = 'Songs are Added to Playlist Successfuly';
						} else {
							$error = true;
							foreach($addplaylists->errors() as $field_key => $error_data)
							{
								foreach($error_data as $error_text)
								{
									$message = $field_key.", ".$error_text;
									break 2;
								} 
							}
						}
						
					} 
				}
			}
			
			else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** Home page function for api ***************************/
	public function homePage()
	{
		if($this->request->is(['post','put']))
    	{
									
			//$this->sendPushNotification('24', 'abc', 'hello');
			$this->loadModel('RecentlySongs');
			$this->loadModel('Challenges');
			$this->loadModel('SongUploads');
			$this->loadModel('Posts');
			$this->loadModel('Categories');
			$this->loadModel('RequestDjSingers');
			$this->loadModel('Likes');
			$this->loadModel('ChallengeLikes');
			$this->loadModel('ChallengeWinners');
			$this->loadModel('Users');
			$this->loadModel('Themes');
			$this->loadModel('Pages');
			$this->loadModel('HistoryRewards');
			$error = true; $code = 0;
			$message = ""; $response = array();
			$responseA = array();
			$response_base = array();
			$allresponse = array();
			$allresponseArr = array(); 
			
			$image_base_user_url = path_user;
			$post_image_base_url = path_post_image;
			$song_image_base_url = path_song_upload_image;
			$song_audio_base_url = path_song_upload_audio;
			$image_banner_url = path_banner_image;
			
			$response_base['image_base_user_url'] = $image_base_user_url;
			$response_base['post_image_base_url'] = $post_image_base_url;
			$response_base['song_image_base_url'] = $song_image_base_url;
			$response_base['song_audio_base_url'] = $song_audio_base_url;
			$response_base['image_banner_url'] = $image_banner_url;
			
			$chkTheme =  $this->Themes->find('all', array('conditions' => array('status' => 'Y')))->first();
			if(isset($chkTheme) && !empty($chkTheme)){
				$response_base['theme']['type'] = $chkTheme['type'];
				$response_base['theme']['image'] = $image_base_user_url.$chkTheme['image'];
				$response_base['theme']['name'] = $chkTheme['name'];
			}else{
				
			}
			$response_base['image_banner_url'] = $image_banner_url;
			$terms = $this->Pages->find('all', array(
					'fields' => array('Pages.title', 'Pages.description'),
					'conditions' => array('Pages.id' => 24, 'Pages.enabled' => 1),
				))->first();
				if (isset($terms) &&  $terms !='' && !empty($terms)) {
					$response_base['daily_task_page'] = $terms['description'];
				}
			
			/************ list of siner and dj request ****************/
			$singeruserlist = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('type' => 'S', 'is_approved' => 'Y', 'is_deleted' => 'N')])->toArray();
			
			if(isset($singeruserlist) && !empty($singeruserlist)){
				$singeruserislist = $singeruserlist;
			}else{
				$singeruserislist = '';
			}
			
			$djuserlist = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('type' => 'D', 'is_approved' => 'Y', 'is_deleted' => 'N')])->toArray();
				if(isset($djuserlist) && !empty($djuserlist)){
					$djuserislist = $djuserlist;
				}else{
					$djuserislist = '';
				}
			
			
			if (isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
				
				
				/*********** Check if user is requested as Singer/Dj or not ************/
				$checkSingerDj = $this->RequestDjSingers->find('all', array(
					'fields' => array('RequestDjSingers.id','RequestDjSingers.is_approved'),
					'conditions' => array('RequestDjSingers.user_id' => $this->request->data['user_id'], 'RequestDjSingers.is_deleted' => 'N')
				))->first();
			
				if (isset($checkSingerDj) &&  $checkSingerDj !='' && !empty($checkSingerDj)) {
					if($checkSingerDj['is_approved'] == 'Y'){
						$response_base['is_approved'] = true;
					}else{
						$response_base['is_approved'] = false;
					}
					$response_base['singerDjCreated'] = true;
				} else {
					$response_base['is_approved'] = false;
					$response_base['singerDjCreated'] = false;
				}
			
			} else {
				$response_base['is_approved'] = false;
				$response_base['singerDjCreated'] = false;
			}
			
			// check rewards are disabled are not
			$rewards_timeuser = $this->Users->find('all', array(
					'fields' => array('Users.rewards_time'),
					'conditions' => array('Users.access_level_id' => 1),
				))->first();
			if(isset($rewards_timeuser) && $rewards_timeuser['rewards_time'] =='1'){
				$response_base['rewards_time'] = true;
			}else{
				$response_base['rewards_time'] = false;
			}
			
			$allresponseArr = $response_base;
			
			if (isset($this->request->data['type']) && !empty($this->request->data['type'])) {
				
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				} else {
					$page = '1';
				}
				
				
				if ($this->request->data['type'] == 'recently_played') {
					/*********** Recently Played Songs List ************/
					if (isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
						
						$recentlySongsData =  $this->RecentlySongs->find('all')->select(['RecentlySongs.id', 'RecentlySongs.song_id', 'RecentlySongs.user_id'])
							->contain([
								'user'=>['fields' => ['full_name','image']],
									'song'=> function(\Cake\ORM\Query $q) {
										return $q->find('all', array(
											'fields' => ['id','user_id','cat_id','sub_cat_id','title','image','audio','size','duration','created'],
											'conditions' => array("song.is_deleted !=" => 'Y', "song.enabled" => 'Y'),
											'contain'=>[
												'user'=>['fields' => ['id','full_name']],
												'category'=>['fields' => ['id','category_name']],
												'subcategory'=>['fields' => ['id','category_name']]
											],
											'order'=>['song.id'=>'desc'],	
										));
									}
							])
							->where(['RecentlySongs.user_id' => $this->request->data['user_id'], 'RecentlySongs.is_deleted' => 'N', 'song.is_deleted !=' => 'Y', 'song.enabled' => 'Y'])
							->order(['song.id'=>'desc'])
							->group(['RecentlySongs.song_id'])
							->limit($this->postPagination)
							->page($page)
							->hydrate(false)->toArray();
					
						
						$recentlySongsDataArr = array();
						
						if (isset($recentlySongsData) &&  $recentlySongsData !='' && !empty($recentlySongsData)) {
							foreach ($recentlySongsData as $key => $value) {
								
								$recentlysongs['song']['id'] = $value['song']['id'];
								$recentlysongs['song']['user_id'] = $value['song']['user_id'];
								$recentlysongs['song']['title'] = $value['song']['title'];
								$recentlysongs['song']['cat_id'] = $value['song']['cat_id'];
								$recentlysongs['song']['sub_cat_id'] = $value['song']['sub_cat_id'];
								$recentlysongs['song']['image'] = $value['song']['image'];
								$recentlysongs['song']['audio'] = $value['song']['audio'];
								$recentlysongs['song']['size'] = $value['song']['size'];
								$recentlysongs['song']['duration'] = $value['song']['duration'];
								$recentlysongs['song']['created'] = date('Y m d h:i:s', strtotime( $value['song']['created']));
								$recentlysongs['song']['cat_title'] = $value['song']['category']['category_name'];
								$recentlysongs['song']['sub_cat_title'] = $value['song']['subcategory']['category_name'];
								$recentlysongs['song']['user_name'] = $value['song']['user']['full_name'];
								
								$songDetail = $this->showLike($value['song_id'], $value['user_id']);
								$recentlysongs['song']['likes'] = $songDetail['likes'];
								$recentlysongs['song']['total_likes'] = $songDetail['total_likes'];
								$recentlysongs['song']['unlikes'] = $songDetail['unlikes'];
								$recentlysongs['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$recentlysongs['song']['is_comment'] = $songDetail['is_comment'];
								$recentlysongs['song']['total_comments'] = $songDetail['total_comments'];
								
								$recentlySongsDataArr[] = $recentlysongs;
							}
							$error = false;
							$message = 'Recently Played';
							$response['recently_played'] = $recentlySongsDataArr;
							
						} else {
							$error = true;
							$message = 'No data';
							$recentlySongsDataArr = array();
							$response['recently_played'] = array();
						}
					} else { $error = true; $response['recently_played'] = array(); }
					
				} else if ($this->request->data['type'] == 'challenge') {
					/*********** Challenge List ************/
					
					
					$challengesData =  $this->Challenges->find('all')->select(['Challenges.id', 'Challenges.remaining_time', 'Challenges.challenge_time', 'Challenges.image', 'Challenges.accepted_user_image'])
						->contain([
							'user1'=>['fields' => ['id','full_name','image']],
							'user2'=>['fields' => ['id','full_name','image']],
							'song'=>['fields' => ['title','image','audio']],
							'acceptusersong'=>['fields' => ['title','image','audio']]
						])
						->where(['Challenges.accept_user_id !=' => '0', 'Challenges.status' => '1', 'Challenges.is_deleted' => 'N'])
						->order(['Challenges.id'=>'desc'])
						->limit($this->postPagination)
						->page($page)
						->hydrate(false)->toArray();
				
					$challengesDataArr = array();
				
					if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
						foreach ($challengesData as $key => $value) {
							
							
							$challengesA['id'] = $value['id'];
							$challengesA['user1']['id'] = $value['user1']['id'];
							$challengesA['user1']['full_name'] = $value['user1']['full_name'];
							$challengesA['user1']['userimage'] = $value['user1']['image'];
							$challengesA['user1']['challenge_image'] = $value['image'];
							
							$challengesA['user2']['id'] = $value['user2']['id'];
							$challengesA['user2']['full_name'] = $value['user2']['full_name'];
							$challengesA['user2']['userimage'] = $value['user2']['image'];
							$challengesA['user2']['challenge_image'] = $value['accepted_user_image'];
							
							$challenges['challenge'] = $challengesA;
							
							//$challenges['challenge']['total_time'] = $value['challenge_time'];
							/******* update challenge time ************/
							date_default_timezone_set('Asia/Kolkata');
							$currrenttime = time();
							$date = strtotime(date('Y-m-d h:i:s',$currrenttime));
							$date2 = strtotime(date('Y-m-d h:i:s',strtotime($value['remaining_time'])));
							$diff = $date2 - $date;
							
							if($diff < 0) {
								$challenge = $this->Challenges->get($value['id']);
								$challenge->remaining_second = 0;
								$challenge->status = '3';
								$this->Challenges->save($challenge);
								
								/******* challenge winner like count ************/
								$total_ch_likes_user1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>1])->count();

								$total_ch_likes_user2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>1])->count();
							
								if($total_ch_likes_user1 > $total_ch_likes_user2) {
									$winner = $this->ChallengeWinners->newEntity();
									$this->request->data['challenge_id'] = $value['id'];
									$this->request->data['user_id'] = $value['user1']['id'];
									$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
									$this->ChallengeWinners->save($winners);
								} elseif($total_ch_likes_user2 > $total_ch_likes_user1) {
									$winner = $this->ChallengeWinners->newEntity();
									$this->request->data['challenge_id'] = $value['id'];
									$this->request->data['user_id'] = $value['user2']['id'];
									$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
									$this->ChallengeWinners->save($winners);
								} elseif($total_ch_likes_user2 == $total_ch_likes_user1) {
									
									$total_ch_unlikes_user1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>0])->count();

									$total_ch_unlikes_user2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>0])->count();
									
									if($total_ch_unlikes_user1 < $total_ch_unlikes_user2) {
										$winner = $this->ChallengeWinners->newEntity();
										$this->request->data['challenge_id'] = $value['id'];
										$this->request->data['user_id'] = $value['user1']['id'];
										$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
										$this->ChallengeWinners->save($winners);
									} elseif($total_ch_unlikes_user2 < $total_ch_unlikes_user1) {
										$winner = $this->ChallengeWinners->newEntity();
										$this->request->data['challenge_id'] = $value['id'];
										$this->request->data['user_id'] = $value['user2']['id'];
										$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
										$this->ChallengeWinners->save($winners);
									}
								}
								/******* end challenge winner like count ************/
								
							}
						
							/******* end update challenge time ************/
							
							$challengesDataArr[] = $challenges;
						}
						$error = false;
						$message = 'Challenge';
						$response['challenge'] = $challengesDataArr;
						
					} else {
						$error = true;
						$message = 'No Data';
						$challengesDataArr = array();
						$response['challenge'] = [];
					}
				} else if ($this->request->data['type'] == 'new_added_by_singer') {
					/*********** New Added Songs List (By Singer) ************/
					
					$req_song =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.title','SongUploads.image','SongUploads.audio','SongUploads.size','SongUploads.duration','SongUploads.enabled','SongUploads.is_deleted','SongUploads.created','SongUploads.total_likes'])
								->contain([
									'user'=>['fields' => ['id','full_name']],
									'category'=>['fields' => ['id','category_name']],
									'subcategory'=>['fields' => ['id','category_name']]
								])
								->where(["SongUploads.user_id IN" => $singeruserislist,"SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'])
								->order(['SongUploads.id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
					
					
					if (isset($req_song) &&  $req_song !='' && !empty($req_song)) {
						$req_song_Arr = array();
							
						foreach($req_song as $songsDataVal) {
							
								$req_songA['song']['id'] = $songsDataVal['id'];
								$req_songA['song']['user_id'] = $songsDataVal['user_id'];
								$req_songA['song']['title'] = $songsDataVal['title'];
								$req_songA['song']['cat_id'] = $songsDataVal['cat_id'];
								$req_songA['song']['sub_cat_id'] = $songsDataVal['sub_cat_id'];
								$req_songA['song']['image'] = $songsDataVal['image'];
								$req_songA['song']['audio'] = $songsDataVal['audio'];
								$req_songA['song']['size'] = $songsDataVal['size'];
								$req_songA['song']['duration'] = $songsDataVal['duration'];
								$req_songA['song']['enabled'] = $songsDataVal['enabled'];
								$req_songA['song']['is_deleted'] = $songsDataVal['is_deleted'];
								$req_songA['song']['created'] = date('Y m d h:i:s', strtotime($songsDataVal['created']));
								$req_songA['song']['cat_title'] = $songsDataVal['category']['category_name'];
								$req_songA['song']['sub_cat_title'] = $songsDataVal['subcategory']['category_name'];
								$req_songA['song']['user_name'] = $songsDataVal['user']['full_name'];
								
								$songDetail = $this->showLike($songsDataVal['id'], $songsDataVal['user_id']);
								$req_songA['song']['likes'] = $songDetail['likes'];
								$req_songA['song']['total_likes'] = $songDetail['total_likes'];
								$req_songA['song']['unlikes'] = $songDetail['unlikes'];
								$req_songA['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$req_songA['song']['is_comment'] = $songDetail['is_comment'];
								$req_songA['song']['total_comments'] = $songDetail['total_comments'];
								
								$req_song_Arr[] = $req_songA;
							
							
						}
						$error = false;
						$message = 'New Added By Singer';
						$response['new_added_by_singer'] = $req_song_Arr;
					} else {
						$error = true;
						$message = 'No data';
						$response['new_added_by_singer'] = [];
					}
				} else if ($this->request->data['type'] == 'new_added_by_dj') {
					/*********** New Added Songs List (By Dj) ************/
					
					$newdj_song =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.title','SongUploads.image','SongUploads.audio','SongUploads.size','SongUploads.duration','SongUploads.enabled','SongUploads.is_deleted','SongUploads.created','SongUploads.total_likes'])
								->contain([
									'user'=>['fields' => ['id','full_name']],
									'category'=>['fields' => ['id','category_name']],
									'subcategory'=>['fields' => ['id','category_name']]
								])
								->where(["SongUploads.user_id IN" => $djuserislist,"SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'])
								->order(['SongUploads.id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
					if (isset($newdj_song) &&  $newdj_song !='' && !empty($newdj_song)) {
						$newdj_song_Arr = array();
						foreach($newdj_song as $songsDataVal) {
							
								$new_dj_songA['song']['id'] = $songsDataVal['id'];
								$new_dj_songA['song']['user_id'] = $songsDataVal['user_id'];
								$new_dj_songA['song']['title'] = $songsDataVal['title'];
								$new_dj_songA['song']['cat_id'] = $songsDataVal['cat_id'];
								$new_dj_songA['song']['sub_cat_id'] = $songsDataVal['sub_cat_id'];
								$new_dj_songA['song']['image'] = $songsDataVal['image'];
								$new_dj_songA['song']['audio'] = $songsDataVal['audio'];
								$new_dj_songA['song']['size'] = $songsDataVal['size'];
								$new_dj_songA['song']['duration'] = $songsDataVal['duration'];
								$new_dj_songA['song']['enabled'] = $songsDataVal['enabled'];
								$new_dj_songA['song']['is_deleted'] = $songsDataVal['is_deleted'];
								$new_dj_songA['song']['created'] = date('Y m d h:i:s', strtotime($songsDataVal['created']));
								$new_dj_songA['song']['cat_title'] = $songsDataVal['category']['category_name'];
								$new_dj_songA['song']['sub_cat_title'] = $songsDataVal['subcategory']['category_name'];
								$new_dj_songA['song']['user_name'] = $songsDataVal['user']['full_name'];
								
								$songDetail = $this->showLike($songsDataVal['id'], $songsDataVal['user_id']);
								$new_dj_songA['song']['likes'] = $songDetail['likes'];
								$new_dj_songA['song']['total_likes'] = $songDetail['total_likes'];
								$new_dj_songA['song']['unlikes'] = $songDetail['unlikes'];
								$new_dj_songA['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$new_dj_songA['song']['is_comment'] = $songDetail['is_comment'];
								$new_dj_songA['song']['total_comments'] = $songDetail['total_comments'];
								
								$newdj_song_Arr[] = $new_dj_songA;
							
								
						}
						$error = false;
						$message = 'New Added By Dj';
						$response['new_added_by_dj'] = $newdj_song_Arr;
					} else {
						$error = true;
						$message = 'No Data';
						$response['new_added_by_dj'] = [];
					}
				} else if ($this->request->data['type'] == 'top_10_dj_songs') {
					/*********** Top 10 Dj Songs List ************/
				
					$req_dj =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.title','SongUploads.image','SongUploads.audio','SongUploads.size','SongUploads.duration','SongUploads.enabled','SongUploads.is_deleted','SongUploads.created','SongUploads.total_likes'])
								->contain([
									'user'=>['fields' => ['id','full_name']],
									'category'=>['fields' => ['id','category_name']],
									'subcategory'=>['fields' => ['id','category_name']]
								])
								->where(["SongUploads.user_id IN" => $djuserislist,"SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'])
								->order(['SongUploads.total_month_likes'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
					
					if (isset($req_dj) &&  $req_dj !='' && !empty($req_dj)) {
						$djsongsDataArr = array();
						foreach($req_dj as $djsongsDataVal) {
							
								$req_djsongA['song']['id'] = $djsongsDataVal['id'];
								$req_djsongA['song']['user_id'] = $djsongsDataVal['user_id'];
								$req_djsongA['song']['title'] = $djsongsDataVal['title'];
								$req_djsongA['song']['cat_id'] = $djsongsDataVal['cat_id'];
								$req_djsongA['song']['sub_cat_id'] = $djsongsDataVal['sub_cat_id'];
								$req_djsongA['song']['image'] = $djsongsDataVal['image'];
								$req_djsongA['song']['audio'] = $djsongsDataVal['audio'];
								$req_djsongA['song']['size'] = $djsongsDataVal['size'];
								$req_djsongA['song']['duration'] = $djsongsDataVal['duration'];
								$req_djsongA['song']['enabled'] = $djsongsDataVal['enabled'];
								$req_djsongA['song']['is_deleted'] = $djsongsDataVal['is_deleted'];
								$req_djsongA['song']['created'] = date('Y m d h:i:s', strtotime($djsongsDataVal['created']));
								$req_djsongA['song']['cat_title'] = $djsongsDataVal['category']['category_name'];
								$req_djsongA['song']['sub_cat_title'] = $djsongsDataVal['subcategory']['category_name'];
								$req_djsongA['song']['user_name'] = $djsongsDataVal['user']['full_name'];
								
								$songDetail = $this->showLike($djsongsDataVal['id'], $djsongsDataVal['user_id']);
								$req_djsongA['song']['likes'] = $songDetail['likes'];
								$req_djsongA['song']['total_likes'] = $songDetail['total_likes'];
								$req_djsongA['song']['unlikes'] = $songDetail['unlikes'];
								$req_djsongA['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$req_djsongA['song']['is_comment'] = $songDetail['is_comment'];
								$req_djsongA['song']['total_comments'] = $songDetail['total_comments'];
								
								$djsongsDataArr[] = $req_djsongA;
							
							
						}
						$error = false;
						$message = 'Top 10 Songs Dj Songs (Monthly)';
						$response['top_10_dj_songs'] = $djsongsDataArr;
					} else {
						$error = true;
						$message = 'No Data';
						$response['top_10_dj_songs'] = [];
					}
				} else if ($this->request->data['type'] == 'top_10_Singer_songs') {
					/*********** Top 10 Singer Songs List ************/
					
					$req_singer =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.title','SongUploads.image','SongUploads.audio','SongUploads.size','SongUploads.duration','SongUploads.enabled','SongUploads.is_deleted','SongUploads.created','SongUploads.total_likes'])
							->contain([
								'user'=>['fields' => ['id','full_name']],
								'category'=>['fields' => ['id','category_name']],
								'subcategory'=>['fields' => ['id','category_name']]
							])
							->where(["SongUploads.user_id IN" => $singeruserislist,"SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'])
							->order(['SongUploads.total_month_likes'=>'desc'])
							->limit($this->postPagination)
							->page($page)
							->hydrate(false)->toArray();
					
					if (isset($req_singer) &&  $req_singer !='' && !empty($req_singer)) {
						$singersongsDataArr = array();
						foreach($req_singer as $singersongsDataVal) {
							
								$req_singersongA['song']['id'] = $singersongsDataVal['id'];
								$req_singersongA['song']['user_id'] = $singersongsDataVal['user_id'];
								$req_singersongA['song']['title'] = $singersongsDataVal['title'];
								$req_singersongA['song']['cat_id'] = $singersongsDataVal['cat_id'];
								$req_singersongA['song']['sub_cat_id'] = $singersongsDataVal['sub_cat_id'];
								$req_singersongA['song']['image'] = $singersongsDataVal['image'];
								$req_singersongA['song']['audio'] = $singersongsDataVal['audio'];
								$req_singersongA['song']['size'] = $singersongsDataVal['size'];
								$req_singersongA['song']['duration'] = $singersongsDataVal['duration'];
								$req_singersongA['song']['enabled'] = $singersongsDataVal['enabled'];
								$req_singersongA['song']['is_deleted'] = $singersongsDataVal['is_deleted'];
								$req_singersongA['song']['created'] = date('Y m d h:i:s', strtotime($singersongsDataVal['created']));
								$req_singersongA['song']['cat_title'] = $singersongsDataVal['category']['category_name'];
								$req_singersongA['song']['sub_cat_title'] = $singersongsDataVal['subcategory']['category_name'];
								$req_singersongA['song']['user_name'] = $singersongsDataVal['user']['full_name'];
								
								$songDetail = $this->showLike($singersongsDataVal['id'], $singersongsDataVal['user_id']);
								$req_singersongA['song']['likes'] = $songDetail['likes'];
								$req_singersongA['song']['total_likes'] = $songDetail['total_likes'];
								$req_singersongA['song']['unlikes'] = $songDetail['unlikes'];
								$req_singersongA['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$req_singersongA['song']['is_comment'] = $songDetail['is_comment'];
								$req_singersongA['song']['total_comments'] = $songDetail['total_comments'];
								
								$singersongsDataArr[] = $req_singersongA;
							
							
						}
						$error = false;
						$message = 'Top 10 Songs Singer Songs (Monthly)';
						$response['top_10_Singer_songs'] = $singersongsDataArr;
					} else {
						$error = true;
						$message = 'No Data';
						$response['top_10_Singer_songs'] = [];
					}
				} else if ($this->request->data['type'] == 'top_post') {
					/*********** Top Posts ************/
					
					$topsongsData =  $this->Posts->find('all')->select(['Posts.id', 'Posts.title', 'Posts.images', 'Posts.description'])
								->contain([
									'user'=>['fields' => ['full_name']]
								])
								->where(["Posts.status"=>'1',"Posts.enabled "=>'Y' , "Posts.is_deleted !=" => 'Y'])
								->order(['Posts.id'=>'DESC'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
					
					$topsongsDataArr = array();
				
					if (isset($topsongsData) &&  $topsongsData !='' && !empty($topsongsData)) {
						foreach ($topsongsData as $key => $value) {
							$topsongs['post'] = $value;
							$topsongsDataArr[] = $topsongs;
						}
						$error = false;
						$message = 'Top Post';
						$response['top_post'] = $topsongsDataArr;
						
					} else {
						$error = true;
						$topsongsDataArr = array();
						$message = 'No Data';
						$response['top_post'] = [];
					}
				} else if ($this->request->data['type'] == 'top_singer_profile') {
					/*********** Top Singer Profile List ************/
					
					$singerProfileData =  $this->Users->find('all')->select(['id','full_name','image','email','dob','address'])
					->where(["id IN" => $singeruserislist,'is_deleted' => 'N', 'enabled' => 'Y'])
					->order(['total_like'=>'desc'])
					->limit($this->postPagination)
					->page($page)
					->hydrate(false)->toArray();
					
					$singerProfileDataArr = array();
				
					if (isset($singerProfileData) &&  $singerProfileData !='' && !empty($singerProfileData)) {
						foreach ($singerProfileData as $key => $value) {
							$total_likes = $this->Likes->find()->where(['profile_id'=>$value['id'],'status'=>1])->count();
							$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['id'],'status'=>0])->count();
												
							
							$singer_profile['profile']['user_id'] = $value['id'];
							$singer_profile['profile']['full_name'] = $value['full_name'];
							$singer_profile['profile']['email'] = $value['email'];
							$singer_profile['profile']['dob'] = isset($value['dob']) ? date('Y-m-d',strtotime($value['dob'])) : null;
							$singer_profile['profile']['address'] = $value['address'];
							$singer_profile['profile']['image'] = $value['image'];
							$singer_profile['profile']['like_count'] = $total_likes;
							$singer_profile['profile']['unlike_count'] = $total_unlikes;
							
							$singerProfileDataArr[] = $singer_profile;
						}
						$error = false;
						$message = 'Top Singer Profile';
						$response['top_singer_profile'] = $singerProfileDataArr;
					} else {
						$error = true;
						$response['top_singer_profile'] = [];
					}
				} else if ($this->request->data['type'] == 'top_dj_profile') {
					/*********** Top dj Profile List ************/
					
					$djProfileData =  $this->Users->find('all')->select(['id','full_name','image','email','dob','address'])
					->where(["id IN" => $djuserislist,'is_deleted' => 'N', 'enabled' => 'Y'])
					->order(['total_like'=>'desc'])
					->limit($this->postPagination)
					->page($page)
					->hydrate(false)->toArray();
					
					$djProfileDataArr = array();
				
					if (isset($djProfileData) &&  $djProfileData !='' && !empty($djProfileData)) {
						foreach ($djProfileData as $key => $value) {							
							$total_likes = $this->Likes->find()->where(['profile_id'=>$value['id'],'status'=>1])->count();
							$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['id'],'status'=>0])->count();	
							
							$dj_profile['profile']['user_id'] = $value['id'];
							$dj_profile['profile']['full_name'] = $value['full_name'];
							$dj_profile['profile']['email'] = $value['email'];
							$dj_profile['profile']['dob'] = isset($value['dob']) ? date('Y-m-d',strtotime($value['dob'])) : null;
							$dj_profile['profile']['address'] = $value['address'];
							$dj_profile['profile']['image'] = $value['image'];
							$dj_profile['profile']['like_count'] = $total_likes;
							$dj_profile['profile']['unlike_count'] = $total_unlikes;
							
							$djProfileDataArr[] = $dj_profile;
						}
						$error = false;
						$message = 'Top DJ Profile';
						$response['top_dj_profile'] = $djProfileDataArr;
					} else {
						$error = true;
						$response['top_dj_profile'] = [];
					}
				} else if ($this->request->data['type'] == 'last_challenge_winner') {
					/*********** Last challenge winner List ************/
					$lastChWinnerData =  $this->ChallengeWinners->find('all')->select(['ChallengeWinners.user_id','ChallengeWinners.other_user_id','ChallengeWinners.user_like','ChallengeWinners.user_unlike','ChallengeWinners.other_user_like','ChallengeWinners.other_user_unlike'])
									->contain([
										'Users'=>['fields' => ['id','full_name','image','email','dob','address']],
										'Users2'=>['fields' => ['id','full_name','image','email','dob','address']]
									])
									->where(['ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N'])
									->order(['ChallengeWinners.id'=>'desc'])
									->limit($this->postPagination)
									->page($page)
									->hydrate(false)->toArray();
					
					$lastChWinnerDataArr = array();
				
					if (isset($lastChWinnerData) &&  $lastChWinnerData !='' && !empty($lastChWinnerData)) {
						foreach ($lastChWinnerData as $key => $value) { //print_r($value); die;
							
							$last_challenge_winner['profile']['user_id'] = $value['user']['id'];
							$last_challenge_winner['profile']['full_name'] = $value['user']['full_name'];
							$last_challenge_winner['profile']['email'] = $value['user']['email'];
							$last_challenge_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
							$last_challenge_winner['profile']['address'] = $value['user']['address'];
							$last_challenge_winner['profile']['image'] = $value['user']['image'];
							
							$last_challenge_winner['profile']['total_like'] = $value['user_like'];
							$last_challenge_winner['profile']['total_unlike'] = $value['user_unlike'];
							
							$last_challenge_winner['profile2']['user_id'] = $value['users2']['id'];
							$last_challenge_winner['profile2']['full_name'] = $value['users2']['full_name'];
							$last_challenge_winner['profile2']['email'] = $value['users2']['email'];
							$last_challenge_winner['profile2']['dob'] = isset($value['users2']['dob']) ? date('Y-m-d',strtotime($value['users2']['dob'])) : null;
							$last_challenge_winner['profile2']['address'] = $value['users2']['address'];
							$last_challenge_winner['profile2']['image'] = $value['users2']['image'];
							
							$last_challenge_winner['profile2']['total_like'] = $value['other_user_like'];
							$last_challenge_winner['profile2']['total_unlike'] = $value['other_user_unlike'];
							
							$lastChWinnerDataArr[] = $last_challenge_winner;
						}
						$error = false;
						$message = 'Last Challenge Winner';
						$response['last_challenge_winner'] = $lastChWinnerDataArr;
					} else {
						$error = true;
						$response['last_challenge_winner'] = [];
					}
				} else if ($this->request->data['type'] == 'last_10_day_winner') {
					/*********** Last 10 day winners List ************/
					$last_10_day = date("Y-n-j", strtotime("10 days ago"));
					
					$lastWinnerData =  $this->ChallengeWinners->find('all')->select(['ChallengeWinners.user_id','ChallengeWinners.other_user_id','ChallengeWinners.user_like','ChallengeWinners.user_unlike','ChallengeWinners.other_user_like','ChallengeWinners.other_user_unlike'])
									->contain([
										'Users'=>['fields' => ['id','full_name','image','email','dob','address']],
										'Users2'=>['fields' => ['id','full_name','image','email','dob','address']]
									])
									->where(['ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N', 'ChallengeWinners.created >=' => $last_10_day])
									->order(['ChallengeWinners.id'=>'desc'])
									->limit($this->postPagination)
									->page($page)
									->hydrate(false)->toArray();
					
					$lastWinnerDataArr = array();
				
					if (isset($lastWinnerData) &&  $lastWinnerData !='' && !empty($lastWinnerData)) {
						foreach ($lastWinnerData as $key => $value) { //print_r($value); die;
							
							$last_winner['profile']['user_id'] = $value['user']['id'];
							$last_winner['profile']['full_name'] = $value['user']['full_name'];
							$last_winner['profile']['email'] = $value['user']['email'];
							$last_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
							$last_winner['profile']['address'] = $value['user']['address'];
							$last_winner['profile']['image'] = $value['user']['image'];
							
							$last_winner['profile']['total_like'] = $value['user_like'];
							$last_winner['profile']['total_unlike'] = $value['user_unlike'];
							
							$last_winner['profile2']['user_id'] = $value['users2']['id'];
							$last_winner['profile2']['full_name'] = $value['users2']['full_name'];
							$last_winner['profile2']['email'] = $value['users2']['email'];
							$last_winner['profile2']['dob'] = isset($value['users2']['dob']) ? date('Y-m-d',strtotime($value['users2']['dob'])) : null;
							$last_winner['profile2']['address'] = $value['users2']['address'];
							$last_winner['profile2']['image'] = $value['users2']['image'];
							
							$last_winner['profile2']['total_like'] = $value['other_user_like'];
							$last_winner['profile2']['total_unlike'] = $value['other_user_unlike'];
							
							$lastWinnerDataArr[] = $last_winner;
						}
						$error = false;
						$message = 'Last 10 day daily task winner';
						$response['last_10_day_winner'] = $lastWinnerDataArr;
					} else {
						$error = true;
						$response['last_10_day_winner'] = [];
					}
				} else if ($this->request->data['type'] == 'last_month_winner_singer_dj') {
					/*********** Last month winners List ************/
					$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
					$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
					
					$lastMonthWinnerData =  $this->ChallengeWinners->find('all')->select(['ChallengeWinners.user_id','ChallengeWinners.other_user_id','ChallengeWinners.user_like','ChallengeWinners.user_unlike','ChallengeWinners.other_user_like','ChallengeWinners.other_user_unlike'])
									->contain([
										'Users'=>['fields' => ['id','full_name','image','email','dob','address']],
										'Users2'=>['fields' => ['id','full_name','image','email','dob','address']]
									])
									->where(['ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N', 'ChallengeWinners.created >=' => $last_month_start, 'ChallengeWinners.created <=' => $last_month_end])
									->order(['ChallengeWinners.id'=>'desc'])
									->limit($this->postPagination)
									->page($page)
									->hydrate(false)->toArray();
					
					$lastMonthWinnerDataArr = array();
				
					if (isset($lastMonthWinnerData) &&  $lastMonthWinnerData !='' && !empty($lastMonthWinnerData)) {
						foreach ($lastMonthWinnerData as $key => $value) { //print_r($value); die;
							
							$last_month_winner['profile']['user_id'] = $value['user']['id'];
							$last_month_winner['profile']['full_name'] = $value['user']['full_name'];
							$last_month_winner['profile']['email'] = $value['user']['email'];
							$last_month_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
							$last_month_winner['profile']['address'] = $value['user']['address'];
							$last_month_winner['profile']['image'] = $value['user']['image'];
							
							$last_month_winner['profile']['total_like'] = $value['user_like'];
							$last_month_winner['profile']['total_unlike'] = $value['user_unlike'];
							
							$last_month_winner['profile2']['user_id'] = $value['users2']['id'];
							$last_month_winner['profile2']['full_name'] = $value['users2']['full_name'];
							$last_month_winner['profile2']['email'] = $value['users2']['email'];
							$last_month_winner['profile2']['dob'] = isset($value['users2']['dob']) ? date('Y-m-d',strtotime($value['users2']['dob'])) : null;
							$last_month_winner['profile2']['address'] = $value['users2']['address'];
							$last_month_winner['profile2']['image'] = $value['users2']['image'];
							
							$last_month_winner['profile2']['total_like'] = $value['other_user_like'];
							$last_month_winner['profile2']['total_unlike'] = $value['other_user_unlike'];
							
							$lastMonthWinnerDataArr[] = $last_month_winner;
						}
						$error = false;
						$message = 'Last Month Winner Singer/DJ';
						$response['last_month_winner_singer_dj'] = $lastMonthWinnerDataArr;
					} else {
						$error = true;
						$response['last_month_winner_singer_dj'] = [];
					}
				} else if ($this->request->data['type'] == 'singer_list_whos_birthday_today') {
					/*********** Singer List Who's Birthday Today ************/
					$singerBdyData =  $this->RequestDjSingers->find('all')->select(['RequestDjSingers.user_id'])
									->contain([
										'Users'=>['fields' => ['id','full_name','image','email','dob','address']]
									])
									->where(['RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N'])
									->order(['RequestDjSingers.id'=>'desc'])
									->limit($this->postPagination)
									->page($page)
									->hydrate(false)->toArray();
					
					if (isset($singerBdyData) &&  $singerBdyData !='' && !empty($singerBdyData)) {
						$singerBdyDataArr = array();
						foreach ($singerBdyData as $key1 => $value1) {
							$userBdyData = $this->Paginator->paginate(
								$this->Users, [
									'fields' =>['id','full_name','image','email','dob','address'],
									'page' => $page,
									'order'=>['Users.id'=>'desc'],	
									'conditions' => array('Users.id' => $value1['user_id'], 'Users.enabled' => 'Y', 'Users.is_deleted' => 'N')
							]);
							if (isset($userBdyData) &&  $userBdyData !='' && !empty($userBdyData)) {
								foreach($userBdyData as $userBdyDataVal) {
									$old_date = $userBdyDataVal['dob'];  
									$old_date_timestamp = strtotime($old_date);
									$new_date = date('m-d', $old_date_timestamp);   
									if($new_date == date("m-d")) {
										$singerBdyARR['profile'] = $userBdyDataVal;
										$singerBdyDataArr[] = $singerBdyARR;
									}
								}
							}
						}
						$error = false;
						$message = "Singer List who's Birthday Today";
						$response['singer_list_whos_birthday_today'] = $singerBdyDataArr;
					} else {
						$error = true;
						$response['singer_list_whos_birthday_today'] = [];
					}
				}
				foreach($response as $keydata=>$valdata){
					
					if($keydata == 'recently_played'){
						$responserecord['title'] = 'Recently Played' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'challenge'){
						$responserecord['title'] = 'Challenge' ;
						$responserecord['type'] = 'challenge' ;
					}elseif($keydata == 'new_added_by_singer'){
						$responserecord['title'] = 'New Added (By Singer)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'new_added_by_dj'){
						$responserecord['title'] = 'New Added (By Dj)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_10_dj_songs'){
						$responserecord['title'] = 'Top 10 Songs Dj Songs (Monthly)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_10_Singer_songs'){
						$responserecord['title'] = 'Top 10 Songs Singer Songs (Monthly)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_post'){
						$responserecord['title'] = 'Top Post' ;
						$responserecord['type'] = 'post' ;
					}elseif($keydata == 'top_singer_profile'){
						$responserecord['title'] = 'Top Singer Profile' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'top_dj_profile'){
						$responserecord['title'] = 'Top Dj Profile' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'singer_list_whos_birthday_today'){
						$responserecord['title'] = "Singer List Who's Birthday Today" ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_10_day_winner'){
						$responserecord['title'] = 'Last 10 day daily task winner' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_month_winner_singer_dj'){
						$responserecord['title'] = 'Last Month Winner Singer/DJ' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_challenge_winner'){
						$responserecord['title'] = 'Last Challenge Winner' ;
						$responserecord['type'] = 'profile' ;
					}
					$responserecord['name'] =  $keydata;
					$responserecord['data'] =  $valdata;
					$allresponse[] = $responserecord;
				}
				$allresponseArr['category'] = $allresponse;
				
				
				
			} else {
				
				/*********** Recently Played Songs List ************/
				if (isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
				
					$recentlySongsData = $this->RecentlySongs->find('all', array(
						'fields' => array('RecentlySongs.id', 'RecentlySongs.song_id', 'RecentlySongs.user_id'),
						'conditions' => array('RecentlySongs.user_id' => $this->request->data['user_id'], 'RecentlySongs.is_deleted' => 'N', 'song.is_deleted !=' => 'Y', 'song.enabled' => 'Y'),
						'contain'=>[
							'user'=>['fields' => ['full_name','image']],
							'song'=> function(\Cake\ORM\Query $q) {
										return $q->find('all', array(
											'fields' => ['id','user_id','cat_id','sub_cat_id','title','image','audio','size','duration','created'],
											'conditions' => array('song.is_deleted !=' => 'Y', 'song.enabled' => 'Y'),
											'contain'=>[
												'user'=>['fields' => ['id','full_name']],
												'category'=>['fields' => ['id','category_name']],
												'subcategory'=>['fields' => ['id','category_name']]
											],
											'order'=>['song.id'=>'asc'],	
										));
									}
						],
						'limit' => '10',
						'order'=>['RecentlySongs.id'=>'desc'],	
						'group'=>['RecentlySongs.song_id']
					));
					$recentlySongsDataArr = array();
					if (isset($recentlySongsData) &&  $recentlySongsData !='' && !empty($recentlySongsData)) {
						foreach ($recentlySongsData as $key => $value) {
							$recentlysongs['song']['id'] = $value['song']['id'];
							$recentlysongs['song']['user_id'] = $value['song']['user_id'];
							$recentlysongs['song']['title'] = $value['song']['title'];
							$recentlysongs['song']['cat_id'] = $value['song']['cat_id'];
							$recentlysongs['song']['sub_cat_id'] = $value['song']['sub_cat_id'];
							$recentlysongs['song']['image'] = $value['song']['image'];
							$recentlysongs['song']['audio'] = $value['song']['audio'];
							$recentlysongs['song']['size'] = $value['song']['size'];
							$recentlysongs['song']['duration'] = $value['song']['duration'];
							$recentlysongs['song']['created'] = date('Y m d h:i:s', strtotime( $value['song']['created']));
							$recentlysongs['song']['cat_title'] = $value['song']['category']['category_name'];
							$recentlysongs['song']['sub_cat_title'] = $value['song']['subcategory']['category_name'];
							$recentlysongs['song']['user_name'] = $value['song']['user']['full_name'];
							
							$songDetail = $this->showLike($value['song_id'], $value['user_id']);
							$recentlysongs['song']['likes'] = $songDetail['likes'];
							$recentlysongs['song']['total_likes'] = $songDetail['total_likes'];
							$recentlysongs['song']['unlikes'] = $songDetail['unlikes'];
							$recentlysongs['song']['total_unlikes'] = $songDetail['total_unlikes'];
							$recentlysongs['song']['is_comment'] = $songDetail['is_comment'];
							$recentlysongs['song']['total_comments'] = $songDetail['total_comments'];
							
							$recentlySongsDataArr[] = $recentlysongs;
						}
						$error = false;
						$message = 'Recently Played Songs';
						$response['recently_played'] = $recentlySongsDataArr;
						
					} else {
						$error = true;
						$recentlySongsDataArr = array();
						$response['recently_played'] = array();
					}
				
				}else{
					$error = true;
					$recentlySongsDataArr = array();
					$response['recently_played'] = array();
				}
				/*********** Challenge List ************/
				$challengesData = $this->Challenges->find('all', array(
					'fields' => array('Challenges.id', 'Challenges.remaining_time', 'Challenges.challenge_time', 'Challenges.image', 'Challenges.accepted_user_image'),
					'conditions' => array('Challenges.accept_user_id !=' => '0', 'Challenges.status' => '1', 'Challenges.is_deleted' => 'N'),
					'contain'=>[
						'user1'=>['fields' => ['id','full_name','image']],
						'user2'=>['fields' => ['id','full_name','image']],
						'song'=>['fields' => ['title','image','audio']],
						'acceptusersong'=>['fields' => ['title','image','audio']]
					],
					'limit' => '10',
					'order'=>['Challenges.id'=>'desc'],	
				));
				//print_r($challengesData->toArray()); die;
				$challengesDataArr = array();
			
				if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
					foreach ($challengesData as $key => $value) {
						
							$challengesA['id'] = $value['id'];
							$challengesA['user1']['user_id'] = $value['user1']['id'];
							$challengesA['user1']['full_name'] = $value['user1']['full_name'];
							$challengesA['user1']['userimage'] = $value['user1']['image'];
							$challengesA['user1']['challenge_image'] = $value['image'];
							
							$challengesA['user2']['user_id'] = $value['user2']['id'];
							$challengesA['user2']['full_name'] = $value['user2']['full_name'];
							$challengesA['user2']['userimage'] = $value['user2']['image'];
							$challengesA['user2']['challenge_image'] = $value['accepted_user_image'];
							
							$challenges['challenge'] = $challengesA;
							//$challenges['challenge']['total_time'] = $value['challenge_time'];
						
							/******* update challenge time ************/
							date_default_timezone_set('Asia/Kolkata');
							$currrenttime = time();
							$date = strtotime(date('Y-m-d h:i:s',$currrenttime));
							$date2 = strtotime(date('Y-m-d h:i:s',strtotime($value['remaining_time'])));
							$diff = $date2 - $date;
								
							if($diff < 0) {
								$challenge = $this->Challenges->get($value['id']);
								$challenge->remaining_second = 0;
								$challenge->status = '3';
								$this->Challenges->save($challenge);
							
							
								/******* challenge winner like count ************/
								$total_ch_likes_user1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>1])->count();

								$total_ch_likes_user2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>1])->count();
								
								$total_ch_unlikes_user1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>0])->count();

								$total_ch_unlikes_user2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>0])->count();
								
								if($total_ch_likes_user1 > $total_ch_likes_user2) {
									$winner = $this->ChallengeWinners->newEntity();
									$this->request->data['challenge_id'] = $value['id'];
									$this->request->data['user_id'] = $value['user1']['id'];
									$this->request->data['other_user_id'] = $value['user2']['id'];
									$this->request->data['user_like'] = $total_ch_likes_user1;
									$this->request->data['other_user_like'] = $total_ch_likes_user2;
									$this->request->data['user_unlike'] = $total_ch_unlikes_user1;
									$this->request->data['other_user_unlike'] = $total_ch_unlikes_user2;
									$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
									$this->ChallengeWinners->save($winners);
								} elseif($total_ch_likes_user2 > $total_ch_likes_user1) {
									$winner = $this->ChallengeWinners->newEntity();
									$this->request->data['challenge_id'] = $value['id'];
									$this->request->data['user_id'] = $value['user2']['id'];
									$this->request->data['other_user_id'] = $value['user1']['id'];
									$this->request->data['user_like'] = $total_ch_likes_user2;
									$this->request->data['other_user_like'] = $total_ch_likes_user1;
									$this->request->data['user_unlike'] = $total_ch_unlikes_user2;
									$this->request->data['other_user_unlike'] = $total_ch_unlikes_user1;
									$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
									$this->ChallengeWinners->save($winners);
								} elseif($total_ch_likes_user2 == $total_ch_likes_user1) {
									
								
									
									if($total_ch_unlikes_user1 < $total_ch_unlikes_user2) {
										$winner = $this->ChallengeWinners->newEntity();
										$this->request->data['challenge_id'] = $value['id'];
										$this->request->data['user_id'] = $value['user1']['id'];
										$this->request->data['other_user_id'] = $value['user2']['id'];
										$this->request->data['user_like'] = $total_ch_likes_user1;
										$this->request->data['other_user_like'] = $total_ch_likes_user2;
										$this->request->data['user_unlike'] = $total_ch_unlikes_user1;
										$this->request->data['other_user_unlike'] = $total_ch_unlikes_user2;
										$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
										$this->ChallengeWinners->save($winners);
									} elseif($total_ch_unlikes_user2 < $total_ch_unlikes_user1) {
										$winner = $this->ChallengeWinners->newEntity();
										$this->request->data['challenge_id'] = $value['id'];
										$this->request->data['user_id'] = $value['user2']['id'];
										$this->request->data['other_user_id'] = $value['user1']['id'];
										$this->request->data['user_like'] = $total_ch_likes_user2;
										$this->request->data['other_user_like'] = $total_ch_likes_user1;
										$this->request->data['user_unlike'] = $total_ch_unlikes_user2;
										$this->request->data['other_user_unlike'] = $total_ch_unlikes_user1;
										$winners = $this->ChallengeWinners->patchEntity($winner, $this->request->data);
										$this->ChallengeWinners->save($winners);
									}
								}
								/******* end challenge winner like count ************/
							}
						
							/******* end update challenge time ************/
						
						
						$challengesDataArr[] = $challenges;
					}
					$error = false;
					$message = 'Recently Played Songs';
					$response['challenge'] = $challengesDataArr;
					
				} else {
					$error = true;
					$challengesDataArr = array();
					$response['challenge'] = array();
				}
				
				/*********** New Added Songs List (By Singer) ************/
				
				$req_song =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.title','SongUploads.image','SongUploads.audio','SongUploads.size','SongUploads.duration','SongUploads.enabled','SongUploads.is_deleted','SongUploads.created','SongUploads.total_likes'])
							->contain([
								'user'=>['fields' => ['id','full_name']],
								'category'=>['fields' => ['id','category_name']],
								'subcategory'=>['fields' => ['id','category_name']]
							])
							->where(["SongUploads.user_id IN" => $singeruserislist,"SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'])
							->order(['SongUploads.id'=>'desc'])
							->limit(10)
							->hydrate(false)->toArray();
			
				if (isset($req_song) &&  $req_song !='' && !empty($req_song)) {
					$req_song_Arr = array();
					foreach($req_song as $songsDataVal) {
						
						$req_songA['song']['id'] = $songsDataVal['id'];
						$req_songA['song']['user_id'] = $songsDataVal['user_id'];
						$req_songA['song']['title'] = $songsDataVal['title'];
						$req_songA['song']['cat_id'] = $songsDataVal['cat_id'];
						$req_songA['song']['sub_cat_id'] = $songsDataVal['sub_cat_id'];
						$req_songA['song']['image'] = $songsDataVal['image'];
						$req_songA['song']['audio'] = $songsDataVal['audio'];
						$req_songA['song']['size'] = $songsDataVal['size'];
						$req_songA['song']['duration'] = $songsDataVal['duration'];
						$req_songA['song']['enabled'] = $songsDataVal['enabled'];
						$req_songA['song']['is_deleted'] = $songsDataVal['is_deleted'];
						$req_songA['song']['created'] = date('Y m d h:i:s', strtotime($songsDataVal['created']));
						$req_songA['song']['cat_title'] = $songsDataVal['category']['category_name'];
						$req_songA['song']['sub_cat_title'] = $songsDataVal['subcategory']['category_name'];
						$req_songA['song']['user_name'] = $songsDataVal['user']['full_name'];
						
						$songDetail = $this->showLike($songsDataVal['id'], $songsDataVal['user_id']);
						$req_songA['song']['likes'] = $songDetail['likes'];
						$req_songA['song']['total_likes'] = $songDetail['total_likes'];
						$req_songA['song']['unlikes'] = $songDetail['unlikes'];
						$req_songA['song']['total_unlikes'] = $songDetail['total_unlikes'];
						$req_songA['song']['is_comment'] = $songDetail['is_comment'];
						$req_songA['song']['total_comments'] = $songDetail['total_comments'];
						
						$req_song_Arr[] = $req_songA;
					}
					$error = false;
					$message = 'New Added Songs By Singer';
					$response['new_added_by_singer'] = $req_song_Arr;
				} else {
					$error = true;
					$response['new_added_by_singer'] = [];
				}
				
				/*********** New Added Songs List (By Dj) ************/
					
					
					$newdj_song =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.title','SongUploads.image','SongUploads.audio','SongUploads.size','SongUploads.duration','SongUploads.enabled','SongUploads.is_deleted','SongUploads.created','SongUploads.total_likes'])
									->contain([
										'user'=>['fields' => ['id','full_name']],
										'category'=>['fields' => ['id','category_name']],
										'subcategory'=>['fields' => ['id','category_name']]
									])
									->where(["SongUploads.user_id IN" => $djuserislist,"SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'])
									->order(['SongUploads.id'=>'desc'])
									->limit(10)
									->hydrate(false)->toArray();
					if (isset($newdj_song) &&  $newdj_song !='' && !empty($newdj_song)) {
						$newdj_song_Arr = array();
						foreach($newdj_song as $songsDataVal) {
							
								$new_dj_songA['song']['id'] = $songsDataVal['id'];
								$new_dj_songA['song']['user_id'] = $songsDataVal['user_id'];
								$new_dj_songA['song']['title'] = $songsDataVal['title'];
								$new_dj_songA['song']['cat_id'] = $songsDataVal['cat_id'];
								$new_dj_songA['song']['sub_cat_id'] = $songsDataVal['sub_cat_id'];
								$new_dj_songA['song']['image'] = $songsDataVal['image'];
								$new_dj_songA['song']['audio'] = $songsDataVal['audio'];
								$new_dj_songA['song']['size'] = $songsDataVal['size'];
								$new_dj_songA['song']['duration'] = $songsDataVal['duration'];
								$new_dj_songA['song']['enabled'] = $songsDataVal['enabled'];
								$new_dj_songA['song']['is_deleted'] = $songsDataVal['is_deleted'];
								$new_dj_songA['song']['created'] = date('Y m d h:i:s', strtotime($songsDataVal['created']));
								$new_dj_songA['song']['cat_title'] = $songsDataVal['category']['category_name'];
								$new_dj_songA['song']['sub_cat_title'] = $songsDataVal['subcategory']['category_name'];
								$new_dj_songA['song']['user_name'] = $songsDataVal['user']['full_name'];
								
								$songDetail = $this->showLike($songsDataVal['id'], $songsDataVal['user_id']);
								$new_dj_songA['song']['likes'] = $songDetail['likes'];
								$new_dj_songA['song']['total_likes'] = $songDetail['total_likes'];
								$new_dj_songA['song']['unlikes'] = $songDetail['unlikes'];
								$new_dj_songA['song']['total_unlikes'] = $songDetail['total_unlikes'];
								$new_dj_songA['song']['is_comment'] = $songDetail['is_comment'];
								$new_dj_songA['song']['total_comments'] = $songDetail['total_comments'];
								
								$newdj_song_Arr[] = $new_dj_songA;
						}
						$error = false;
						$message = 'New Added By Dj';
						$response['new_added_by_dj'] = $newdj_song_Arr;
					} else {
						$error = true;
						$response['new_added_by_dj'] = [];
					}
				
				
				/*********** Top 10 Dj Songs List ************/
				
				$req_dj =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.title','SongUploads.image','SongUploads.audio','SongUploads.size','SongUploads.duration','SongUploads.enabled','SongUploads.is_deleted','SongUploads.created','SongUploads.total_likes'])
								->contain([
									'user'=>['fields' => ['id','full_name']],
									'category'=>['fields' => ['id','category_name']],
									'subcategory'=>['fields' => ['id','category_name']]
								])
								->where(["SongUploads.user_id IN" => $djuserislist,"SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'])
								->order(['SongUploads.total_month_likes'=>'desc'])
								->limit(10)
								->hydrate(false)->toArray();
				if (isset($req_dj) &&  $req_dj !='' && !empty($req_dj)) {
					$djsongsDataArr = array();
						foreach($req_dj as $djsongsDataVal) {
						
							$req_djsongA['song']['id'] = $djsongsDataVal['id'];
							$req_djsongA['song']['user_id'] = $djsongsDataVal['user_id'];
							$req_djsongA['song']['title'] = $djsongsDataVal['title'];
							$req_djsongA['song']['cat_id'] = $djsongsDataVal['cat_id'];
							$req_djsongA['song']['sub_cat_id'] = $djsongsDataVal['sub_cat_id'];
							$req_djsongA['song']['image'] = $djsongsDataVal['image'];
							$req_djsongA['song']['audio'] = $djsongsDataVal['audio'];
							$req_djsongA['song']['size'] = $djsongsDataVal['size'];
							$req_djsongA['song']['duration'] = $djsongsDataVal['duration'];
							$req_djsongA['song']['enabled'] = $djsongsDataVal['enabled'];
							$req_djsongA['song']['is_deleted'] = $djsongsDataVal['is_deleted'];
							$req_djsongA['song']['created'] = date('Y m d h:i:s', strtotime($djsongsDataVal['created']));
							$req_djsongA['song']['cat_title'] = $djsongsDataVal['category']['category_name'];
							$req_djsongA['song']['sub_cat_title'] = $djsongsDataVal['subcategory']['category_name'];
							$req_djsongA['song']['user_name'] = $djsongsDataVal['user']['full_name'];
							
							$songDetail = $this->showLike($djsongsDataVal['id'], $djsongsDataVal['user_id']);
							$req_djsongA['song']['likes'] = $songDetail['likes'];
							$req_djsongA['song']['total_likes'] = $songDetail['total_likes'];
							$req_djsongA['song']['unlikes'] = $songDetail['unlikes'];
							$req_djsongA['song']['total_unlikes'] = $songDetail['total_unlikes'];
							$req_djsongA['song']['is_comment'] = $songDetail['is_comment'];
							$req_djsongA['song']['total_comments'] = $songDetail['total_comments'];
							
							$djsongsDataArr[] = $req_djsongA;
						
					}
					$error = false;
					$message = 'Top 10 Songs Dj Songs (Monthly)';
					$response['top_10_dj_songs'] = $djsongsDataArr;
				} else {
					$error = true;
					$response['top_10_dj_songs'] = [];
				}
				
				/*********** Top 10 Singer Songs List ************/
				
				$req_singer =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.user_id','SongUploads.cat_id','SongUploads.sub_cat_id','SongUploads.title','SongUploads.image','SongUploads.audio','SongUploads.size','SongUploads.duration','SongUploads.enabled','SongUploads.is_deleted','SongUploads.created','SongUploads.total_likes'])
							->contain([
								'user'=>['fields' => ['id','full_name']],
								'category'=>['fields' => ['id','category_name']],
								'subcategory'=>['fields' => ['id','category_name']]
							])
							->where(["SongUploads.user_id IN" => $singeruserislist,"SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'])
							->order(['SongUploads.total_month_likes'=>'desc'])
							->limit(10)
							->hydrate(false)->toArray();
				
				if (isset($req_singer) &&  $req_singer !='' && !empty($req_singer)) {
					$singersongsDataArr = array();
					foreach($req_singer as $singersongsDataVal) {
						
						
						$req_singersongA['song']['id'] = $singersongsDataVal['id'];
						$req_singersongA['song']['user_id'] = $singersongsDataVal['user_id'];
						$req_singersongA['song']['title'] = $singersongsDataVal['title'];
						$req_singersongA['song']['cat_id'] = $singersongsDataVal['cat_id'];
						$req_singersongA['song']['sub_cat_id'] = $singersongsDataVal['sub_cat_id'];
						$req_singersongA['song']['image'] = $singersongsDataVal['image'];
						$req_singersongA['song']['audio'] = $singersongsDataVal['audio'];
						$req_singersongA['song']['size'] = $singersongsDataVal['size'];
						$req_singersongA['song']['duration'] = $singersongsDataVal['duration'];
						$req_singersongA['song']['enabled'] = $singersongsDataVal['enabled'];
						$req_singersongA['song']['is_deleted'] = $singersongsDataVal['is_deleted'];
						$req_singersongA['song']['created'] = date('Y m d h:i:s', strtotime($singersongsDataVal['created']));
						$req_singersongA['song']['cat_title'] = $singersongsDataVal['category']['category_name'];
						$req_singersongA['song']['sub_cat_title'] = $singersongsDataVal['subcategory']['category_name'];
						$req_singersongA['song']['user_name'] = $singersongsDataVal['user']['full_name'];
						
						$songDetail = $this->showLike($singersongsDataVal['id'], $singersongsDataVal['user_id']);
						$req_singersongA['song']['likes'] = $songDetail['likes'];
						$req_singersongA['song']['total_likes'] = $songDetail['total_likes'];
						$req_singersongA['song']['unlikes'] = $songDetail['unlikes'];
						$req_singersongA['song']['total_unlikes'] = $songDetail['total_unlikes'];
						$req_singersongA['song']['is_comment'] = $songDetail['is_comment'];
						$req_singersongA['song']['total_comments'] = $songDetail['total_comments'];
						
						$singersongsDataArr[] = $req_singersongA;
						
						
					}
					$error = false;
					$message = 'Top 10 Songs Singer Songs (Monthly)';
					$response['top_10_Singer_songs'] = $singersongsDataArr;
				} else {
					$error = true;
					$response['top_10_Singer_songs'] = [];
				}
				
				/*********** Top Posts ************/
				$toppostsData =  $this->Posts->find('all')->select(['Posts.id', 'Posts.title', 'Posts.images', 'Posts.description'])
								->contain([
									'user'=>['fields' => ['full_name']]
								])
								->where(["Posts.status"=>'1',"Posts.enabled "=>'Y' , "Posts.is_deleted !=" => 'Y'])
								->order(['Posts.id'=>'DESC'])
								->limit(10)
								->hydrate(false)->toArray();
				
				$toppostsDataArr = array();
			
				if (isset($toppostsData) &&  $toppostsData !='' && !empty($toppostsData)) {
					foreach ($toppostsData as $key => $value) {
						$topposts['post'] = $value;
						$toppostsDataArr[] = $topposts;
					}
					$error = false;
					$message = 'Top Posts';
					$response['top_post'] = $toppostsDataArr;
					
				} else {
					$error = true;
					$toppostsDataArr = array();
					$response['top_post'] = [];
				}
				
				/*********** Top Singer Profile List ************/
			
				$singerProfileData = $this->Users->find('all', array(
					'fields' => array('id','full_name','image','email','dob','address'),
					'conditions' => array("id IN" => $singeruserislist,'is_deleted' => 'N', 'enabled' => 'Y'),
					'limit' => '10',
					'order'=>['total_like'=>'desc'],	
				));
				
				$singerProfileDataArr = array();
			
				if (isset($singerProfileData) &&  $singerProfileData !='' && !empty($singerProfileData)) {
					foreach ($singerProfileData as $key => $value) { //print_r($value); die;
						
						$total_likes = $this->Likes->find()->where(['profile_id'=>$value['id'],'status'=>1])->count();
						$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['id'],'status'=>0])->count();
											
						
						$singer_profile['profile']['user_id'] = $value['id'];
						$singer_profile['profile']['full_name'] = $value['full_name'];
						$singer_profile['profile']['email'] = $value['email'];
						$singer_profile['profile']['dob'] = isset($value['dob']) ? date('Y-m-d',strtotime($value['dob'])) : null;
						$singer_profile['profile']['address'] = $value['address'];
						$singer_profile['profile']['image'] = $value['image'];
						$singer_profile['profile']['like_count'] = $total_likes;
						$singer_profile['profile']['unlike_count'] = $total_unlikes;
						
						
						$singerProfileDataArr[] = $singer_profile;
					}
					$error = false;
					$message = 'Singers Profile';
					$response['top_singer_profile'] = $singerProfileDataArr;
				} else {
					$error = true;
					$response['top_singer_profile'] = [];
				}
				
				/*********** Top dj Profile List ************/
			
				$djProfileData = $this->Users->find('all', array(
					'fields' => array('id','full_name','image','email','dob','address'),
					'conditions' => array("id IN" => $djuserislist,'is_deleted' => 'N', 'enabled' => 'Y'),
					'limit' => '10',
					'order'=>['total_like'=>'desc'],	
				));
				
				$djProfileDataArr = array();
			
				if (isset($djProfileData) &&  $djProfileData !='' && !empty($djProfileData)) {
					foreach ($djProfileData as $key => $value) {
						
						$total_likes = $this->Likes->find()->where(['profile_id'=>$value['id'],'status'=>1])->count();
						$total_unlikes = $this->Likes->find()->where(['profile_id'=>$value['id'],'status'=>0])->count();
											
						
						$dj_profile['profile']['user_id'] = $value['id'];
						$dj_profile['profile']['full_name'] = $value['full_name'];
						$dj_profile['profile']['email'] = $value['email'];
						$dj_profile['profile']['dob'] = isset($value['dob']) ? date('Y-m-d',strtotime($value['dob'])) : null;
						$dj_profile['profile']['address'] = $value['address'];
						$dj_profile['profile']['image'] = $value['image'];
						$dj_profile['profile']['like_count'] = $total_likes;
						$dj_profile['profile']['unlike_count'] = $total_unlikes;
						
						
						$djProfileDataArr[] = $dj_profile;
					}
					$error = false;
					$message = 'DJ Profile';
					$response['top_dj_profile'] = $djProfileDataArr;
				} else {
					$error = true;
					$response['top_dj_profile'] = [];
				}
				
				
				/*********** Last 10 day winners List ************/
				$last_10_day = date("Y-n-j", strtotime("10 days ago"));
				
				$UserRewardsDatas  = $this->HistoryRewards->find('all',array('conditions'=>array('type'=>'T','is_deleted'=>'N','DATE(created) >=' => $last_10_day)))->hydrate(false)->toArray();
					
				$last_month_top_dj_song_rewards = array();
				//echo "<pre>";print_r($UserRewardsDatas);exit;
			
				if (isset($UserRewardsDatas) &&  $UserRewardsDatas !='' && !empty($UserRewardsDatas)) {
					foreach ($UserRewardsDatas as $key => $value) { //print_r($value); 
						$UsersData  = $this->Users->find('all',array('conditions'=>array('id'=>$value['user_id'])))->hydrate(false)->first();
						 //print_r($UsersData); die; 
						$last_month_winner['profile']['user_id'] = $value['user_id'];
						$last_month_winner['profile']['full_name'] = $UsersData['full_name'];
						$last_month_winner['profile']['email'] = $UsersData['email'];
						$last_month_winner['profile']['dob'] = isset($UsersData['dob']) ? date('Y-m-d',strtotime($UsersData['dob'])) : null;
						$last_month_winner['profile']['address'] = $UsersData['address'];
						$last_month_winner['profile']['image'] = $UsersData['image'];
						
						
						
						$last_month_top_dj_song_rewards[] = $last_month_winner;
					}
					$error = false;
					$message = 'Last 10 day daily task winner';					
					$response['last_10_day_winner'] = $last_month_top_dj_song_rewards;
				} else {
					$error = true;
					$response['last_10_day_winner'] = [];
				}
				
				
				/*$lastWinnerData = $this->ChallengeWinners->find('all', array(
					'fields' => array('ChallengeWinners.user_id','ChallengeWinners.other_user_id','ChallengeWinners.user_like','ChallengeWinners.user_unlike','ChallengeWinners.other_user_like','ChallengeWinners.other_user_unlike'),
					'conditions' => array('ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N', 'ChallengeWinners.created >=' => $last_10_day),
					'contain'=>[
						'Users'=>['fields' => ['id','full_name','image','email','dob','address']],
						'Users2'=>['fields' => ['id','full_name','image','email','dob','address']]
					],
					'limit' => '10',
					'order'=>['ChallengeWinners.id'=>'desc'],	
				));
				
				$lastWinnerDataArr = array();
			
				if (isset($lastWinnerData) &&  $lastWinnerData !='' && !empty($lastWinnerData)) {
					foreach ($lastWinnerData as $key => $value) { //print_r($value); die;
						
						$last_winner['profile']['user_id'] = $value['user']['id'];
						$last_winner['profile']['full_name'] = $value['user']['full_name'];
						$last_winner['profile']['email'] = $value['user']['email'];
						$last_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$last_winner['profile']['address'] = $value['user']['address'];
						$last_winner['profile']['image'] = $value['user']['image'];
						
						$last_winner['profile']['total_like'] = $value['user_like'];
						$last_winner['profile']['total_unlike'] = $value['user_unlike'];
						
						$last_winner['profile2']['user_id'] = $value['users2']['id'];
						$last_winner['profile2']['full_name'] = $value['users2']['full_name'];
						$last_winner['profile2']['email'] = $value['users2']['email'];
						$last_winner['profile2']['dob'] = isset($value['users2']['dob']) ? date('Y-m-d',strtotime($value['users2']['dob'])) : null;
						$last_winner['profile2']['address'] = $value['users2']['address'];
						$last_winner['profile2']['image'] = $value['users2']['image'];
						
						$last_winner['profile2']['total_like'] = $value['other_user_like'];
						$last_winner['profile2']['total_unlike'] = $value['other_user_unlike'];
						
						$lastWinnerDataArr[] = $last_winner;
					}
					$error = false;
					$message = 'Last 10 Day Winner';
					$response['last_10_day_winner'] = $lastWinnerDataArr;
				} else {
					$error = true;
					$response['last_10_day_winner'] = [];
				}*/
				
				
				/*********** Last month winners List ************/
				$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
				$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
				//$last_month_start = '2019-5-1';
				//$last_month_end = '2019-5-31';
				
				$lastMonthWinnerData = $this->ChallengeWinners->find('all', array(
					'fields' => array('ChallengeWinners.user_id','ChallengeWinners.other_user_id','ChallengeWinners.user_like','ChallengeWinners.user_unlike','ChallengeWinners.other_user_like','ChallengeWinners.other_user_unlike'),
					'conditions' => array('ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N', 'ChallengeWinners.created >=' => $last_month_start, 'ChallengeWinners.created <=' => $last_month_end),
					'contain'=>[
						'Users'=>['fields' => ['id','full_name','image','email','dob','address']],
						'Users2'=>['fields' => ['id','full_name','image','email','dob','address']]
					],
					'limit' => '10',
					'order'=>['ChallengeWinners.id'=>'desc'],	
				));
				//echo "<pre>";print_r($lastMonthWinnerData);exit;
				$lastMonthWinnerDataArr = array();
			
				if (isset($lastMonthWinnerData) &&  $lastMonthWinnerData !='' && !empty($lastMonthWinnerData)) {
					foreach ($lastMonthWinnerData as $key => $value) { //print_r($value); die;
						
						$last_month_winner['profile']['user_id'] = $value['user']['id'];
						$last_month_winner['profile']['full_name'] = $value['user']['full_name'];
						$last_month_winner['profile']['email'] = $value['user']['email'];
						$last_month_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$last_month_winner['profile']['address'] = $value['user']['address'];
						$last_month_winner['profile']['image'] = $value['user']['image'];
						
						$last_month_winner['profile']['total_like'] = $value['user_like'];
						$last_month_winner['profile']['total_unlike'] = $value['user_unlike'];
						
						$last_month_winner['profile2']['user_id'] = $value['users2']['id'];
						$last_month_winner['profile2']['full_name'] = $value['users2']['full_name'];
						$last_month_winner['profile2']['email'] = $value['users2']['email'];
						$last_month_winner['profile2']['dob'] = isset($value['users2']['dob']) ? date('Y-m-d',strtotime($value['users2']['dob'])) : null;
						$last_month_winner['profile2']['address'] = $value['users2']['address'];
						$last_month_winner['profile2']['image'] = $value['users2']['image'];
						
						$last_month_winner['profile2']['total_like'] = $value['other_user_like'];
						$last_month_winner['profile2']['total_unlike'] = $value['other_user_unlike'];
						
						$lastMonthWinnerDataArr[] = $last_month_winner;
					}
					$error = false;
					$message = 'Last Month Winner Singer/DJ';
					$response['last_month_winner_singer_dj'] = $lastMonthWinnerDataArr;
				} else {
					$error = true;
					$response['last_month_winner_singer_dj'] = [];
				}
				
				
				
				/*********** Last month top singer songs according monthly rewards ************/
				$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
				$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
				
				$UserRewardsData  = $this->HistoryRewards->find('all',array('conditions'=>array('type'=>'S','is_deleted'=>'N','DATE(created) >=' => $last_month_start,'DATE(created) <=' => $last_month_end)))->hydrate(false)->toArray();
				
				$last_month_top_singer_song_rewards = array();
			
				if (isset($UserRewardsData) &&  $UserRewardsData !='' && !empty($UserRewardsData)) {
					foreach ($UserRewardsData as $key => $value) { //print_r($value); 
						$UsersData  = $this->Users->find('all',array('conditions'=>array('id'=>$value['user_id'])))->hydrate(false)->first();
						 //print_r($UsersData); die; 
						$last_month_winner['profile']['user_id'] = $value['user_id'];
						$last_month_winner['profile']['full_name'] = $UsersData['full_name'];
						$last_month_winner['profile']['email'] = $UsersData['email'];
						$last_month_winner['profile']['dob'] = isset($UsersData['dob']) ? date('Y-m-d',strtotime($UsersData['dob'])) : null;
						$last_month_winner['profile']['address'] = $UsersData['address'];
						$last_month_winner['profile']['image'] = $UsersData['image'];
						
						/*$last_month_winner['profile']['total_like'] = $value['user_like'];
						$last_month_winner['profile']['total_unlike'] = $value['user_unlike'];
						
						$last_month_winner['profile2']['user_id'] = $value['users2']['id'];
						$last_month_winner['profile2']['full_name'] = $value['users2']['full_name'];
						$last_month_winner['profile2']['email'] = $value['users2']['email'];
						$last_month_winner['profile2']['dob'] = isset($value['users2']['dob']) ? date('Y-m-d',strtotime($value['users2']['dob'])) : null;
						$last_month_winner['profile2']['address'] = $value['users2']['address'];
						$last_month_winner['profile2']['image'] = $value['users2']['image'];
						
						$last_month_winner['profile2']['total_like'] = $value['other_user_like'];
						$last_month_winner['profile2']['total_unlike'] = $value['other_user_unlike'];*/
						
						$last_month_top_singer_song_rewards[] = $last_month_winner;
					}
					$error = false;
					$message = 'Last Month Rewarded Singer';					
					$response['last_month_top_singer_song_rewards'] = $last_month_top_singer_song_rewards;
				} else {
					$error = true;
					$response['last_month_top_singer_song_rewards'] = [];
				}
				
				
				/*********** Last month top dj songs according monthly rewards ************/
				
				$UserRewardsDatas  = $this->HistoryRewards->find('all',array('conditions'=>array('type'=>'D','is_deleted'=>'N','DATE(created) >=' => $last_month_start,'DATE(created) <=' => $last_month_end)))->hydrate(false)->toArray();
				//$UserRewardsDatas  = $this->HistoryRewards->find('all',array('conditions'=>array('type'=>'D','is_deleted'=>'N','created BETWEEN ? and ?' => array($last_month_start1, $last_month_end1)')))->hydrate(false)->toArray();
				
				$last_month_top_dj_song_rewards = array();
				//echo "<pre>";print_r($UserRewardsDatas);exit;
			
				if (isset($UserRewardsDatas) &&  $UserRewardsDatas !='' && !empty($UserRewardsDatas)) {
					foreach ($UserRewardsDatas as $key => $value) { //print_r($value); 
						$UsersData  = $this->Users->find('all',array('conditions'=>array('id'=>$value['user_id'])))->hydrate(false)->first();
						 //print_r($UsersData); die; 
						$last_month_winner['profile']['user_id'] = $value['user_id'];
						$last_month_winner['profile']['full_name'] = $UsersData['full_name'];
						$last_month_winner['profile']['email'] = $UsersData['email'];
						$last_month_winner['profile']['dob'] = isset($UsersData['dob']) ? date('Y-m-d',strtotime($UsersData['dob'])) : null;
						$last_month_winner['profile']['address'] = $UsersData['address'];
						$last_month_winner['profile']['image'] = $UsersData['image'];
						
						/*$last_month_winner['profile']['total_like'] = $value['user_like'];
						$last_month_winner['profile']['total_unlike'] = $value['user_unlike'];
						
						$last_month_winner['profile2']['user_id'] = $value['users2']['id'];
						$last_month_winner['profile2']['full_name'] = $value['users2']['full_name'];
						$last_month_winner['profile2']['email'] = $value['users2']['email'];
						$last_month_winner['profile2']['dob'] = isset($value['users2']['dob']) ? date('Y-m-d',strtotime($value['users2']['dob'])) : null;
						$last_month_winner['profile2']['address'] = $value['users2']['address'];
						$last_month_winner['profile2']['image'] = $value['users2']['image'];
						
						$last_month_winner['profile2']['total_like'] = $value['other_user_like'];
						$last_month_winner['profile2']['total_unlike'] = $value['other_user_unlike'];*/
						
						$last_month_top_dj_song_rewards[] = $last_month_winner;
					}
					$error = false;
					$message = 'Last Month Rewarded Dj';					
					$response['last_month_top_dj_song_rewards'] = $last_month_top_dj_song_rewards;
				} else {
					$error = true;
					$response['last_month_top_dj_song_rewards'] = [];
				}
								
				
				/*********** Last challenge winner List ************/
				$lastChWinnerData = $this->ChallengeWinners->find('all', array(
					'fields' => array('ChallengeWinners.user_id','ChallengeWinners.other_user_id','ChallengeWinners.user_like','ChallengeWinners.user_unlike','ChallengeWinners.other_user_like','ChallengeWinners.other_user_unlike'),
					'conditions' => array('ChallengeWinners.status' => '1', 'ChallengeWinners.is_deleted' => 'N'),
					'contain'=>[
						'Users'=>['fields' => ['id','full_name','image','email','dob','address']],
						'Users2'=>['fields' => ['id','full_name','image','email','dob','address']]
					],
					'limit' => '10',
					'order'=>['ChallengeWinners.id'=>'desc'],	
				));
				
				$lastChWinnerDataArr = array();
			
				if (isset($lastChWinnerData) &&  $lastChWinnerData !='' && !empty($lastChWinnerData)) {
					foreach ($lastChWinnerData as $key => $value) { //print_r($value); die;
						
						$last_challenge_winner['profile']['user_id'] = $value['user']['id'];
						$last_challenge_winner['profile']['full_name'] = $value['user']['full_name'];
						$last_challenge_winner['profile']['email'] = $value['user']['email'];
						$last_challenge_winner['profile']['dob'] = isset($value['user']['dob']) ? date('Y-m-d',strtotime($value['user']['dob'])) : null;
						$last_challenge_winner['profile']['address'] = $value['user']['address'];
						$last_challenge_winner['profile']['image'] = $value['user']['image'];
						
						$last_challenge_winner['profile']['total_like'] = $value['user_like'];
						$last_challenge_winner['profile']['total_unlike'] = $value['user_unlike'];
						
						$last_challenge_winner['profile2']['user_id'] = $value['users2']['id'];
						$last_challenge_winner['profile2']['full_name'] = $value['users2']['full_name'];
						$last_challenge_winner['profile2']['email'] = $value['users2']['email'];
						$last_challenge_winner['profile2']['dob'] = isset($value['users2']['dob']) ? date('Y-m-d',strtotime($value['users2']['dob'])) : null;
						$last_challenge_winner['profile2']['address'] = $value['users2']['address'];
						$last_challenge_winner['profile2']['image'] = $value['users2']['image'];
						
						$last_challenge_winner['profile2']['total_like'] = $value['other_user_like'];
						$last_challenge_winner['profile2']['total_unlike'] = $value['other_user_unlike'];
						
						$lastChWinnerDataArr[] = $last_challenge_winner;
					}
					$error = false;
					$message = 'Last Challenge Winner';
					$response['last_challenge_winner'] = $lastChWinnerDataArr;
				} else {
					$error = true;
					$response['last_challenge_winner'] = [];
				}
				
				/*********** Singer List Who's Birthday Today ************/
				$singerBdyData = $this->RequestDjSingers->find('all', array(
					'fields' => array('RequestDjSingers.user_id'),
					'conditions' => array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N')	
				)); 
				if (isset($singerBdyData) &&  $singerBdyData !='' && !empty($singerBdyData)) {
					$singerBdyDataArr = array();
					foreach ($singerBdyData as $key1 => $value1) {
						$userBdyData = $this->Users->find('all', array(
							'fields' =>['id','full_name','image','email','dob','address'],
							'conditions' => array('Users.id' => $value1['user_id'],  'Users.enabled' => 'Y', 'Users.is_deleted' => 'N'),
							'limit' => '10',
							'order'=>['Users.id'=>'desc'],	
						));
						if (isset($userBdyData) &&  $userBdyData !='' && !empty($userBdyData)) {
							foreach($userBdyData as $userBdyDataVal) {
								$old_date = $userBdyDataVal['dob'];  
								$old_date_timestamp = strtotime($old_date);
								$new_date = date('m-d', $old_date_timestamp);   
								
								if($new_date == date("m-d")) {
									$singerBdyAr['profile'] = $userBdyDataVal;
									$singerBdyDataArr[] = $singerBdyAr;
								}
							}
						}
					}
					$error = false;
					$message = "Singer List who's Birthday Today";
					$response['singer_list_whos_birthday_today'] = $singerBdyDataArr;
				} else {
					$error = true;
					$response['singer_list_whos_birthday_today'] = [];
				}
			
				//$response['last_10_day_winner'] = [];
				//$response['last_month_winner_singer_dj'] = [];
				//$response['last_challenge_winner'] = [];
				
				
				$responseA['cat'] = $response;
				foreach($responseA['cat'] as $keydata=>$valdata){
					
					if($keydata == 'recently_played'){
						$responserecord['title'] = 'Recently Played' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'challenge'){
						$responserecord['title'] = 'Challenge' ;
						$responserecord['type'] = 'challenge' ;
					}elseif($keydata == 'new_added_by_singer'){
						$responserecord['title'] = 'New Added (By Singer)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'new_added_by_dj'){
						$responserecord['title'] = 'New Added (By Dj)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_10_dj_songs'){
						$responserecord['title'] = 'Top 10 Songs Dj Songs (Monthly)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_10_Singer_songs'){
						$responserecord['title'] = 'Top 10 Songs Singer Songs (Monthly)' ;
						$responserecord['type'] = 'song' ;
					}elseif($keydata == 'top_post'){
						$responserecord['title'] = 'Top Post' ;
						$responserecord['type'] = 'post' ;
					}elseif($keydata == 'top_singer_profile'){
						$responserecord['title'] = 'Top Singer Profile' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'top_dj_profile'){
						$responserecord['title'] = 'Top Dj Profile' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'singer_list_whos_birthday_today'){
						$responserecord['title'] = "Singer List Who's Birthday Today" ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_10_day_winner'){
						$responserecord['title'] = 'Last 10 day daily task winner' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_month_winner_singer_dj'){
						$responserecord['title'] = 'Last Month Winner Singer/DJ' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_month_top_singer_song_rewards'){
						$responserecord['title'] = 'Last Month Rewarded Singer' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_month_top_dj_song_rewards'){
						$responserecord['title'] = 'Last Month Rewarded Dj' ;
						$responserecord['type'] = 'profile' ;
					}elseif($keydata == 'last_challenge_winner'){
						$responserecord['title'] = 'Last Challenge Winner' ;
						$responserecord['type'] = 'profile' ;
					}
					$responserecord['name'] =  $keydata;
					$responserecord['data'] =  $valdata;
					$allresponse[] = $responserecord;
				}
				$allresponseArr['category'] = $allresponse;
				
				
			}
	
			//pr(json_encode($response));die;
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $allresponseArr,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Profile Like/Dislike api **********************/
    public function profileLikeDislike()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Likes');
			$error = true; $code = 0;
			$message = $response = array(); 
			
			if( isset( $this->request->data['user_id'] ) && isset( 
			$this->request->data['profile_id'] ) && isset( $this->request->data['like'] )) 
			{
				$likedata = $this->Likes->find()->where(['user_id'=>$this->request->data['user_id'],
				'profile_id'=>$this->request->data['profile_id']])->toArray();
				if(!empty($likedata)){
					$likesdetail  = $this->Likes->get($likedata[0]['id']);
					if($this->request->data['like']==0){
						$this->request->data['status']=0;
						$message = 'You dislike this profile';
					}else{
						$this->request->data['status']=1;
						$message = 'Thanks for like this profile';
					}
					$likeadd = $this->Likes->patchEntity($likesdetail, $this->request->data);
					$saveLikes = $this->Likes->save($likeadd);
					$error = false;
					$response['total_likes'] = $this->Likes->find()->where(['profile_id'=>$this->request->data['profile_id'],'status'=>1])->count();
					$response['total_unlikes'] = $this->Likes->find()->where(['profile_id'=>$this->request->data['profile_id'],'status'=>0])->count();
				}else{
					$addLikes = $this->Likes->newEntity();
					if($this->request->data['like']==0){
						$this->request->data['status']=0;
						$message = 'You dislike this song';
					}else{
						$this->request->data['status']=1;
						$message = 'Thanks for like this song';
					}
					
					$addLike = $this->Likes->patchEntity($addLikes, $this->request->data);
					if ($LikesDetail = $this->Likes->save($addLike))
					{
						$response['total_likes'] = $this->Likes->find()->where(['profile_id'=>$this->request->data['profile_id'],'status'=>1])->count();
						$response['total_unlikes'] = $this->Likes->find()->where(['profile_id'=>$this->request->data['profile_id'],'status'=>0])->count();
						$error = false;
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data'=>array(),'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Challenge function for api ***************************/
	public function challengeDetail()
	{
		if($this->request->is(['post','put']))
    	{			
			$this->loadModel('Challenges');
			$this->loadModel('SongUploads');
			$this->loadModel('Likes');
			$this->loadModel('Pages');
			$this->loadModel('ChallengeLikes');
			$error = true; $code = 0;
			$message = ""; $response = [];
			
			$image_base_user_url = path_user;
			$song_image_base_url = path_song_upload_image;
			$song_audio_base_url = path_song_upload_audio;
			
			$response['image_base_user_url'] = $image_base_user_url;
			$response['song_image_base_url'] = $song_image_base_url;
			$response['song_audio_base_url'] = $song_audio_base_url;
			
			if (isset($this->request->data['challenge_id']) && !empty($this->request->data['challenge_id'])) {
			
				/*********** Challenge List ************/
				$challengesData = $this->Challenges->find('all', array(
					'fields' => array('Challenges.id','Challenges.remaining_second', 'Challenges.remaining_time', 'Challenges.challenge_time', 'Challenges.image', 'Challenges.accepted_user_image'),
					'conditions' => array(
						'Challenges.id' => $this->request->data['challenge_id'], 
						'Challenges.status' => '1', 
						'Challenges.is_deleted' => 'N'
					),
					'contain'=>[
						'user1'=>['fields' => ['id','full_name','image']],
						'user2'=>['fields' => ['id','full_name','image']],
						'song'=>['fields' => ['title','image','audio']],
						'acceptusersong'=>['fields' => ['title','image','audio']]
					],
					'limit' => '10',
					'order'=>['Challenges.id'=>'desc']
				));
				//print_r($challengesData->toArray()); die;
				$challengesDataArr = array();
			
				if (isset($challengesData) &&  $challengesData !='' && !empty($challengesData)) {
					foreach ($challengesData as $key => $value) { //print_r($value); die;
						
						/*********** Challenge like and unlike count ************/
						$total_challenge_likes1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>1])->count();
						$total_challenge_unlikes1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user1']['id'],'status'=>0])->count();
						
						$total_challenge_likes2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>1])->count();
						$total_challenge_unlikes2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'ch_user_id'=>$value['user2']['id'],'status'=>0])->count();
						
						/**** Is like user1 *****/
						$datalike1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'user_id'=>$value['user1']['id'],'status'=>1])->toArray();
						if(empty($datalike1)){
							$is_like1 = false;
						}else{
							$is_like1 = true;
						}
						$dataunlike1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'user_id'=>$value['user1']['id'],'status'=>0])->toArray();
						if(empty($dataunlike1)){
							$is_unlike1 = false;
						}else{
							$is_unlike1 = true;
						}
						
						/**** Is like user2 *****/
						$datalike2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'user_id'=>$value['user2']['id'],'status'=>1])->toArray();
						if(empty($datalike2)){
							$is_like2 = false;
						}else{
							$is_like2 = true;
						}
						$dataunlike2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$value['id'],'user_id'=>$value['user2']['id'],'status'=>0])->toArray();
						if(empty($dataunlike2)){
							$is_unlike2 = false;
						}else{
							$is_unlike2 = true;
						}
						
						$challenges['challenge_id'] = $value['id'];
						
						//calulation time
						date_default_timezone_set('Asia/Kolkata');
						$currrenttime = time();
						$date = strtotime(date('Y-m-d H:i:s'));
						$date2 = strtotime($value['remaining_time']->i18nFormat('yyyy-MM-dd HH:mm:ss'));
						$diff = $date2 - $date;
						if($diff < 0) {
							$challenge = $this->Challenges->get($value['id']);
							$challenge->remaining_second = 0;
							//$challenge->status = '3';
							$this->Challenges->save($challenge);
						}
						
						if($value['remaining_second'] == 0) {
							$challenges['remaining_time'] = 0;
						} else {
							$challenges['remaining_time'] = $diff;
						}
						$challenges['total_time'] = $value['challenge_time'];
						$challenges['user1']['id'] = $value['user1']['id'];
						$challenges['user1']['full_name'] = $value['user1']['full_name'];
						$challenges['user1']['userimage'] = $value['user1']['image'];
						$challenges['user1']['challenge_image'] = $value['image'];
						$challenges['user1']['total_challenge_likes'] = $total_challenge_likes1;
						$challenges['user1']['total_challenge_unlikes'] = $total_challenge_unlikes1;
						$challenges['user1']['is_like'] = $is_like1;
						$challenges['user1']['is_unlike'] = $is_unlike1;
						$challenges['user1']['song']['title'] = $value['song']['title'];
						$challenges['user1']['song']['image'] = $value['song']['image'];
						$challenges['user1']['song']['audio'] = $value['song']['audio'];
						
						$challenges['user2']['id'] = $value['user2']['id'];
						$challenges['user2']['full_name'] = $value['user2']['full_name'];
						$challenges['user2']['userimage'] = $value['user2']['image'];
						$challenges['user2']['challenge_image'] = $value['accepted_user_image'];
						$challenges['user2']['total_challenge_likes'] = $total_challenge_likes2;
						$challenges['user2']['total_challenge_unlikes'] = $total_challenge_unlikes2;
						$challenges['user2']['is_like'] = $is_like2;
						$challenges['user2']['is_unlike'] = $is_unlike2;
						$challenges['user2']['song']['title'] = $value['acceptusersong']['title'];
						$challenges['user2']['song']['image'] = $value['acceptusersong']['image'];
						$challenges['user2']['song']['audio'] = $value['acceptusersong']['audio'];
						
					    // $challenges[$key] = $value;
						
						
						$challengesDataArr[] = $challenges;
					}
					$terms = $this->Pages->find('all', array(
						'fields' => array('Pages.title', 'Pages.description'),
						'conditions' => array('Pages.id' => 10, 'Pages.enabled' => 1),
					))->first();
					if (isset($terms) &&  $terms !='' && !empty($terms)) {
						$response['challenge_rules_and_conditions'] = $terms['description'];
					}
					$error = false;
					$message = 'Challenge';
					$response['challenge'] = $challengesDataArr;
					
				} else {
					$error = true;
					$challengesDataArr = array();
					$response['challenge'] = array();
				}
				
			} else $message = 'Incomplete Data';
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	private function showLike($song_id, $user_id) {
				
		$this->loadModel('Likes');
		$this->loadModel('Comments');
		$this->loadModel('SongUploads');
		
		$songDetails_data = array();
		$songDetails=$this->SongUploads->get($song_id, ['contain'=> ['likes','comment']])->toArray();
		//print_r($songDetails); die;
		/**** total like *****/
		$datalike = $this->Likes->find()->where(['song_id'=>$song_id,'user_id'=>$user_id,'status'=>1])->toArray();
		if(empty($datalike)){
			$songDetails_data['likes'] = false;
		}else{
			$songDetails_data['likes'] = true;
		}
		
		$dataunlike = $this->Likes->find()->where(['song_id'=>$song_id,'user_id'=>$user_id,'status'=>0])->toArray();
		if(empty($dataunlike)){
			$songDetails_data['unlikes'] = false;
		}else{
			$songDetails_data['unlikes'] = true;
		}
		
		$like = $this->Likes->find()->where(['song_id'=>$song_id,'status'=>1])->count();
		$songDetails_data['total_likes'] = $like;
		$unlike = $this->Likes->find()->where(['song_id'=>$song_id,'status'=>0])->count();
		$songDetails_data['total_unlikes'] = $unlike;	
		
		/**** end total like *****/
		
		/**** total comment *****/
		$data_comment=$this->Comments->find()->where(['song_id'=>$song_id,'user_id'=>$user_id,'is_deleted !='=>'Y'])->toArray();
		if(empty($data_comment)){
			$songDetails_data['is_comment'] = false;
		}else{
			$songDetails_data['is_comment'] = true;
		}
		$songDetails_data['total_comments'] = count($songDetails['comment']);
		/**** end total comment *****/
		return $songDetails_data;
				
	}
	
	/****************** Banner list function for api **********************/
	public function banner()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = []; 
			$this->loadModel('Banners');
			
			
			if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			
			$allbanners =  $this->Banners->find('all')->select()
								->where(['enabled'=>'Y'])
								->order(['id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
								
			
			$banner_dataArr = array();
			if (isset($allbanners) &&  $allbanners !='' && !empty($allbanners)) {
				foreach ($allbanners as $key => $value) {
					$banner_data['id'] = $value['id'];
					$banner_data['text'] = $value['text'];
					$banner_data['image'] = $value['image'];
					$banner_dataArr[] = $banner_data;
				}
				$error = false;
				$message = 'All Banners';
				$image_banner_url = path_banner_image;
				$response['image_banner_url'] = $image_banner_url;
				$response['banner'] = $banner_dataArr;	
				
			} else {
				$error = true;
				$message = "No record available";
				$response['banner'] = [];	
			}
		
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	
	/****************** song paly or stop rewards api **********************/
    public function songPlayStop()
    {
		if($this->request->is(['post','put']))
    	{
			date_default_timezone_set('Asia/Kolkata');
			$this->loadModel('Users');
			$this->loadModel('UserRewards');
			$this->loadModel('HistoryRewards');
			
			$error = true; $code = 0;
			$message = $response = []; 
			if( isset( $this->request->data['user_id'] ) &&  $this->request->data['user_id'] !='' && isset( $this->request->data['video_type'] )  &&  $this->request->data['video_type'] !='') 
			{
				$minRewards = $this->Users->find('all', array(
					'fields' => array('Users.app_use_time'),
					'conditions' => array('Users.access_level_id' => 1),
				))->first();
				//echo $this->request->data['video_type'];exit;
				if(trim($this->request->data['video_type']) == 'P'){ //echo "adf";exit;
					$chkRewards = $this->UserRewards->find()->where(['user_id'=>$this->request->data['user_id'],'is_deleted'=>'N','enabled'=>'Y'])->first();
					//print_r($chkRewards);exit;
					if(!empty($chkRewards)){
						$currentdate = strtotime(date('Y-m-d'));
						$cdate = strtotime(date('Y-m-d H:i:s'));
						$start_time = strtotime($chkRewards['start_time']->i18nFormat('yyyy-MM-dd HH:mm:ss'));
						
						$start_timeoneday = strtotime(date('Y-m-d',strtotime("+1 days",strtotime($chkRewards['start_time']->i18nFormat('yyyy-MM-dd')))));
						 $diff = $cdate - $start_time;
						
						if($currentdate >= $start_timeoneday){ 
							$userrewards = $this->UserRewards->get($chkRewards['id']);
							$userrewards->time = 0;
							$userrewards->play_status = 1;
							$userrewards->start_time = date('Y-m-d H:i:s');
							$this->UserRewards->save($userrewards);
							$error = false;
							$user_rewardsArr['total_reward_time'] = (int)$userrewards->time;
							$user_rewardsArr['app_use_time'] = (int)$minRewards['app_use_time'];
							$response = $user_rewardsArr;
							$message = 'Rewards Added Successfuly';
						}else{ 
							if($diff >= 0) { 
								$userrewards = $this->UserRewards->get($chkRewards['id']);
								$userrewards->start_time = date('Y-m-d H:i:s');
								$userrewards->play_status = 1;
								$this->UserRewards->save($userrewards);
								$error = false;
								$user_rewardsArr['total_reward_time'] = (int)$userrewards->time;
								$user_rewardsArr['app_use_time'] = (int)$minRewards['app_use_time'];
								$response = $user_rewardsArr;
								$message = 'Rewards Added Successfuly';
							}
						}
						
						
						
					}else{
						$addReward = $this->UserRewards->newEntity();
						$this->request->data['start_time'] = date('Y-m-d H:i:s');
						$this->request->data['play_status'] = 1;
						$addRewards = $this->UserRewards->patchEntity($addReward, $this->request->data);
						if ($rewardsDetail = $this->UserRewards->save($addRewards))
						{
							$error = false;
							$user_rewardsArr['total_reward_time'] = (int)$rewardsDetail->time;
							$user_rewardsArr['app_use_time'] = (int)$minRewards['app_use_time'];
							$response = $user_rewardsArr;
							$message = 'Rewards Added Successfuly';
						} else {
							$error = true;
							foreach($addRewards->errors() as $field_key =>  $error_data)
							{
								foreach($error_data as $error_text)
								{
									$message = $field_key.", ".$error_text;
									break 2;
								} 
							}
						}
					}
				}else if(trim($this->request->data['video_type']) =='C'){
					$chkRewards = $this->UserRewards->find()->where(['user_id'=>$this->request->data['user_id'],'is_deleted'=>'N','enabled'=>'Y'])->first();
					if(!empty($chkRewards)){
						
						$cdate = strtotime(date('Y-m-d H:i:s'));
						$start_time = strtotime($chkRewards['start_time']->i18nFormat('yyyy-MM-dd HH:mm:ss'));
						$diff = $cdate - $start_time;
						if($diff >= 0) {
							$totaltime = $diff + $chkRewards['time'];
							$userrewards = $this->UserRewards->get($chkRewards['id']);
							$userrewards->time = $totaltime;
							$userrewards->start_time = date('Y-m-d H:i:s');
							$userrewards->play_status = 0;
							//echo "<pre>";print_r($userrewards);exit;
							$userrewardsdetails = $this->UserRewards->save($userrewards);
							$error = false;
							$user_rewardsArr['total_reward_time'] = (int)$userrewardsdetails['time'];
							$user_rewardsArr['app_use_time'] = (int)$minRewards['app_use_time'];
							$response = $user_rewardsArr;
							$message = 'Rewards Added Successfuly';
						}
						
						/**************** assign rewards to the perticular user ************/
						//above assign rewards time to add reward table
						$admin_app_use_time = $this->Users->find('all', array(
							'fields' => array('Users.app_use_time','Users.app_use_reward_point','Users.minimum_rewards','Users.rewards_time'),
							'conditions' => array('Users.access_level_id' => 1),
						))->first();
						
						if($admin_app_use_time['rewards_time']  == 1){
							if($userrewardsdetails['time'] > $admin_app_use_time['app_use_time']){
								//add reward point in reward table 
								//echo date('Y-m-d');
								$chkHistoryReward =  $this->HistoryRewards->find('all')->select()
									->where(['type'=>'T','DATE(created)'=> date('Y-m-d'),'user_id'=>$this->request->data['user_id']])
									->hydrate(false)->first();
							//echo "<pre>";print_r($chkHistoryReward);exit;
								if(empty($chkHistoryReward)){
									$addHistoryReward = $this->HistoryRewards->newEntity();
									$this->request->data['user_id'] = $this->request->data['user_id'];
									$this->request->data['point'] = $admin_app_use_time['app_use_reward_point'];
									$this->request->data['type'] = 'T';
									$addHistoryRewards = $this->HistoryRewards->patchEntity($addHistoryReward, $this->request->data);
									$this->HistoryRewards->save($addHistoryRewards);
								}
							}
						}
						 
						 
					}
				}else{
					$chkRewards = $this->UserRewards->find()->where(['user_id'=>$this->request->data['user_id'],'is_deleted'=>'N','enabled'=>'Y'])->first();
					if(!empty($chkRewards)){
						$userrewards = $this->UserRewards->get($chkRewards['id']);
						$userrewards->start_time = date('Y-m-d H:i:s');
						$userrewardsdetails = $this->UserRewards->save($userrewards);
						$error = false;
						$user_rewardsArr['total_reward_time'] = (int)$userrewardsdetails['time'];
						$user_rewardsArr['app_use_time'] = (int)$minRewards['app_use_time'];
						$response = $user_rewardsArr;
						$message = 'Rewards Added Successfuly';
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** Add Payment Details api **********************/
    public function addPaymentDetail()
    {
		if($this->request->is(['post','put']))
    	{
    	
			$this->loadModel('PaymentDetails');
			$this->loadModel('HistoryRewards');
			$error = true; $code = 0;
			$message = $response = []; 
			if( isset( $this->request->data['user_id'] )) 
			{
				$addPaymentDetail = $this->PaymentDetails->newEntity();
				$addPaymentDetails = $this->PaymentDetails->patchEntity($addPaymentDetail, $this->request->data);
				//print_r($addRewards);die;

				if ($paymentDetail = $this->PaymentDetails->save($addPaymentDetails))
				{
					$error = false;					
					$userrewards =  $this->HistoryRewards->find('all')->select()
								->where(['user_id'=>$this->request->data['user_id'],'is_deleted'=>'N'])
								->hydrate(false)->toArray();
					foreach($userrewards as $userrewardsval){
						$userrewarddata = $this->HistoryRewards->get($userrewardsval['id']); // Return article with id 12
						$userrewarddata->is_deleted = 'Y';
						$this->HistoryRewards->save($userrewarddata);
					}
					$response['remaining_reward_point '] = '0';
					$message = 'Submitted Successfuly';
				} else {
					$error = true;
					foreach($addPaymentDetails->errors() as $field_key =>  $error_data)
					{
						foreach($error_data as $error_text)
						{
							$message = $field_key.", ".$error_text;
							break 2;
						} 
						
					}
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}	
	}
	
	/****************** challenge Like/Dislike api **********************/
    public function challengeLikeDislike()
    {
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('ChallengeLikes');
			$error = true; $code = 0;
			$message = $response = array(); 
			
			if( isset( $this->request->data['challenge_id'] ) && isset( $this->request->data['user_id'] ) && isset( 
			$this->request->data['ch_user_id'] ) && isset( $this->request->data['like'] )) 
			{
				$likedata = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'user_id'=>$this->request->data['user_id'],'ch_user_id'=>$this->request->data['ch_user_id']])->toArray();
				if(!empty($likedata)){
					$likesdetail  = $this->ChallengeLikes->get($likedata[0]['id']);
					if($this->request->data['like']==0){
						$this->request->data['status']=0;
						$message = 'You dislike this challenge';
					}else{
						$this->request->data['status']=1;
						$message = 'Thanks for like this challenge';
					}
					$likeadd = $this->ChallengeLikes->patchEntity($likesdetail, $this->request->data);
					$saveLikes = $this->ChallengeLikes->save($likeadd);
					$error = false;
					$response['total_challenge_likes'] = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'ch_user_id'=>$this->request->data['ch_user_id'],'status'=>1])->count();
					$response['total_challenge_unlikes'] = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'ch_user_id'=>$this->request->data['ch_user_id'],'status'=>0])->count();
				}else{
					$addLikes = $this->ChallengeLikes->newEntity();
					if($this->request->data['like']==0){
						$this->request->data['status']=0;
						$message = 'You dislike this song';
					}else{
						$this->request->data['status']=1;
						$message = 'Thanks for like this song';
					}
					$addLike = $this->ChallengeLikes->patchEntity($addLikes, $this->request->data);
					//print_r($addLike); die;
					if ($LikesDetail = $this->ChallengeLikes->save($addLike))
					{
						$response['total_challenge_likes'] = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'ch_user_id'=>$this->request->data['ch_user_id'],'status'=>1])->count();
						$response['total_challenge_unlikes'] = $this->ChallengeLikes->find()->where(['challenge_id'=>$this->request->data['challenge_id'],'ch_user_id'=>$this->request->data['ch_user_id'],'status'=>0])->count();
						$error = false;
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data'=>array(),'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/****************** My rewards list function for api ***************************/
	public function myRewards()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('UserRewards');
			$this->loadModel('Users');
			$this->loadModel('HistoryRewards');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				
				$userRewards = $this->UserRewards->find('all', array(
					'fields' => array('UserRewards.id', 'UserRewards.user_id', 'UserRewards.point'),
					'conditions' => array('UserRewards.user_id' => $this->request->data['user_id'], 'UserRewards.enabled' => 'Y', 'UserRewards.is_deleted' => 'N'),
				))->first();
				$minRewards = $this->Users->find('all', array(
					'fields' => array('Users.minimum_rewards'),
					'conditions' => array('Users.access_level_id' => 1),
				))->first();
				
				
				$queryHistoryRewards = $this->HistoryRewards->find();
				$totalRewards =$queryHistoryRewards->select(['sum' => $queryHistoryRewards->func()->sum('HistoryRewards.point')])->where(['HistoryRewards.user_id' =>$userRewards['user_id'],'HistoryRewards.is_deleted' =>'N'])->first();
				if (isset($userRewards) &&  $userRewards !='' && !empty($userRewards)) {
					
					$user_rewards['id'] = $userRewards['id'];
					$user_rewards['user_id'] = $userRewards['user_id'];
					$user_rewards['total_reward_points'] = isset($totalRewards->sum) ? (int)$totalRewards->sum:'0';
					$user_rewards['minimum_reward_points'] = (int)$minRewards['minimum_rewards'];
						
					$response = $user_rewards;
					$error = false;
					$message = 'My Reward';
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Daily Task calculate your remaining rewards time function for api ***************************/
	public function dailyTask()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('UserRewards');
			$this->loadModel('Users');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				
				$userRewards = $this->UserRewards->find('all', array(
					'fields' => array('UserRewards.id', 'UserRewards.user_id', 'UserRewards.time'),
					'conditions' => array('UserRewards.user_id' => $this->request->data['user_id'], 'UserRewards.enabled' => 'Y', 'UserRewards.is_deleted' => 'N'),
				))->first();
				$minRewards = $this->Users->find('all', array(
					'fields' => array('Users.app_use_time'),
					'conditions' => array('Users.access_level_id' => 1),
				))->first();
				
				if (isset($userRewards) &&  $userRewards !='' && !empty($userRewards)) {
					
					$user_rewards['id'] = $userRewards['id'];
					$user_rewards['user_id'] = $userRewards['user_id'];
					$user_rewards['total_reward_time'] = (int)$userRewards['time'];
					$user_rewards['app_use_time'] = (int)$minRewards['app_use_time'];
						
					$response = $user_rewards;
					$error = false;
					$message = 'My Daily task Time';
					
				} else {
					$error = true;
					$message = "No record available";	
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Subscriptions list function for api **********************/
	public function subscriptionsList()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = $response = []; 
			$this->loadModel('Premiums');
			
			
			if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
				$page = $this->request->data['page'];
			}else{
				$page = '1';
			}
			
			$allsubscriptions =  $this->Premiums->find('all')->select()
								->where(['status'=>'Y'])
								->order(['id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
		
			$subscriptions_dataArr = array();
			if (isset($allsubscriptions) &&  $allsubscriptions !='' && !empty($allsubscriptions)) {
				foreach ($allsubscriptions as $key => $value) {
					$subscriptions_data['id'] = $value['id'];
					$subscriptions_data['plan_name'] = $value['plan_name'];
					$subscriptions_data['plan_desc'] = $value['plan_desc'];
					$subscriptions_data['duration'] = $value['duration'];
					$subscriptions_data['duration_type'] = $value['duration_type'];
					$subscriptions_data['amount'] = $value['amount'];
					$subscriptions_dataArr[] = $subscriptions_data;
				}
				$error = false;
				$message = 'All Subscriptions';
				$response['subscriptions'] = $subscriptions_dataArr;	
				
			} else {
				$error = true;
				$message = "No record available";
				$response['subscriptions'] = [];	
			}
		
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** Refer friend function for api **********************/
	public function referFriend()
	{
		if($this->request->is(['post','put']))
    	{
			
			$this->loadModel('Users');
			$this->loadModel('Pages');
			$error =true;$code=0;
			$message= ""; $response = [];
			if(isset($this->request->data['user_id']) && $this->request->data['user_id'] !='')
			{ 
				$referal_code = $this->Users->find('all', array(
					'fields' => array('Users.refer_code'),
					'conditions' => array('Users.id' => $this->request->data['user_id']),
				))->first();
				if (isset($referal_code) &&  $referal_code !='' && !empty($referal_code)) {
					$user_data['referal_code'] = $referal_code['refer_code'];
				}
				
				$terms = $this->Pages->find('all', array(
					'fields' => array('Pages.title', 'Pages.description'),
					'conditions' => array('Pages.id' => 20, 'Pages.enabled' => 1),
				))->first();
				if (isset($terms) &&  $terms !='' && !empty($terms)) {
					$user_data['terms_and_conditions'] = $terms['description'];
				}
				
				$refer_earn = $this->Pages->find('all', array(
					'fields' => array('Pages.title', 'Pages.description'),
					'conditions' => array('Pages.id' => 19, 'Pages.enabled' => 1),
				))->first();
				if (isset($refer_earn) &&  $refer_earn !='' && !empty($refer_earn)) {
					$user_data['refer_and_earn'] = $refer_earn['description'];
				}
				
				$response = $user_data;
				$error = false;
				$message = 'Refer Friend';
				
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	/****************** Check subscriber plan function for api ***************************/
    public function checkSubscriberPlan()
    {
		if($this->request->is(['post','put']))
    	{
			
			$this->loadModel('RequestDjSingers');
			$this->loadModel('PurchasePlans');
			$error =true;$code=0;
			$message= '';$response = [];
			if(isset($this->request->data['user_id']))
			{ 		
				
				$freeSubsDays = $this->Users->find('all', array(
					'fields' => array('Users.subscription_duration_month'),
					'conditions' => array('Users.access_level_id' => 1),
				))->first();
				
				/*********** Check if user has bought a plan or not ************/
				$userPlan = $this->PurchasePlans->find('all', array(
					'fields' => array('PurchasePlans.id', 'PurchasePlans.status', 'PurchasePlans.created'),
					'conditions' => array('PurchasePlans.user_id' => $this->request->data['user_id'], 'PurchasePlans.status' => 'C', 'PurchasePlans.is_deleted' => 'N'),
					'contain'=>[
						'Premiums'=>['fields' => ['id','duration_type','duration']]
					]
				))->first();
				
				if($userPlan['premium']['duration_type'] == 'Monthly') { 
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."months", strtotime($userPlan['created'])));
				} elseif($userPlan['premium']['duration_type'] == 'Yearly') {	
					$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."years", strtotime($userPlan['created'])));
				}
				
				//calulation current time
				date_default_timezone_set('Asia/Kolkata');
				$currrenttime = time();
				$current_date = date('Y-m-d', $currrenttime);
				
				$chkuser = $this->RequestDjSingers->find('all', array(
							'fields' => array('id'),
							'conditions' => array('user_id' => $this->request->data['user_id'], 'is_approved' => 'Y', 'is_deleted' => 'N'),
					))->first();
					
				if(!empty($chkuser)){
					$free_subscription = $this->Users->find('all', array(
								'fields' => array('Users.id', 'Users.free_subscription_time', 'Users.created'),
								'conditions' => array('Users.id' => $this->request->data['user_id']),
						))->first();
						
					$error = false;
					$response['plan_type'] = '';
					$response['plan_purchase_date'] = date('Y-m-d', strtotime($free_subscription['created']));
					$response['plan_expire_date'] = '';
					$response['plan_duration_type'] = 'Free';
					$response['plan_duration'] = 'Free';
					$response['is_running'] = false; 
					$response['singerdjuser'] = true; 
					$response['used_time'] = '0 days';
					$response['remaining_time'] = '0 days';
					$response['message'] = '';
				}else{
					if ($userPlan =='' && empty($userPlan)) { 
						$free_subscription = $this->Users->find('all', array(
								'fields' => array('Users.id', 'Users.free_subscription_time', 'Users.created'),
								'conditions' => array('Users.id' => $this->request->data['user_id']),
						))->first();
						
						if (isset($free_subscription['free_subscription_time']) && $free_subscription['free_subscription_time'] !='' && !empty($free_subscription['free_subscription_time'])) {
							$subsExpireDate =  date('Y-m-d', strtotime($free_subscription['free_subscription_time']));
							$previousSubsExpireDate = date('Y-m-d', strtotime('-1 day', strtotime($subsExpireDate)));
							
							if($current_date >= $subsExpireDate) {
								$free_plan = $this->Users->get($free_subscription['id']);
								$free_plan->free_subscription_time = null;
								$this->Users->save($free_plan);
								
								// user plan details
								$error = false;
								$response['plan_type'] = 'Free Subscription';
								$response['plan_purchase_date'] = date('Y-m-d', strtotime($free_subscription['created']));
								$response['plan_expire_date'] = $subsExpireDate;
								$response['plan_duration_type'] = 'monthly';
								$response['plan_duration'] = $freeSubsDays['subscription_duration_month'];
								$response['is_running'] = false;
								$response['singerdjuser'] = false; 
								$response['used_time'] = ((strtotime($response['plan_expire_date'])-strtotime($response['plan_purchase_date']))/86400).' days';
								$response['remaining_time'] = '0 days';
								$response['message'] = 'Your free subscription plan has been expired.';
								
							} else {
								if ($current_date == $previousSubsExpireDate) {
									$response['message'] = 'Your free subscription plan is going to expire today.';
								} 
								// user plan details
								$error = false;
								$response['plan_type'] = 'Free Subscription';
								$response['plan_purchase_date'] = date('Y-m-d', strtotime($free_subscription['created']));
								$response['plan_expire_date'] = $subsExpireDate;
								$response['plan_duration_type'] = 'monthly';
								$response['plan_duration'] = $freeSubsDays['subscription_duration_month'];
								$response['is_running'] = true;
								$response['singerdjuser'] = false; 
								$response['used_time'] = ((strtotime($current_date)-strtotime($response['plan_purchase_date']))/86400).' days';
								$response['remaining_time'] = ((strtotime($response['plan_expire_date'])-strtotime($current_date))/86400).' days';
							} 
						} else {
							// user plan details
							$error = false;
							$response['plan_type'] = 'premium';
							$response['is_running'] = false;
							$response['singerdjuser'] = false; 
							$response['message'] = 'Your plan has been expired.';
						}
					} elseif($current_date >= $planExpireDate) {
						$purchase_plan = $this->PurchasePlans->get($userPlan['id']);
						$purchase_plan->status = 'E';
						$this->PurchasePlans->save($purchase_plan);
						
						// user plan details
						$error = false;
						$response['plan_type'] = 'premium';
						$response['plan_purchase_date'] = date('Y-m-d', strtotime($userPlan['created']));
						$response['plan_expire_date'] = $planExpireDate;
						$response['plan_duration_type'] = $userPlan['premium']['duration_type'];
						$response['plan_duration'] = $userPlan['premium']['duration'];
						$response['is_running'] = false;
						$response['singerdjuser'] = false; 
						$response['used_time'] = ((strtotime($response['plan_expire_date'])-strtotime($response['plan_purchase_date']))/86400).' days';
						$response['remaining_time'] = '0 days';
						$response['message'] = 'Your plan has been expired.';
						
					} else {
						// user plan details
						$error = false;
						$response['plan_type'] = 'premium';
						$response['plan_purchase_date'] = date('Y-m-d', strtotime($userPlan['created']));
						$response['plan_expire_date'] = $planExpireDate;
						$response['plan_duration_type'] = $userPlan['premium']['duration_type'];
						$response['plan_duration'] = $userPlan['premium']['duration'];
						$response['is_running'] = true;
						$response['singerdjuser'] = false; 
						$response['used_time'] = ((strtotime($current_date)-strtotime($response['plan_purchase_date']))/86400).' days';
						$response['remaining_time'] = ((strtotime($response['plan_expire_date'])-strtotime($current_date))/86400).' days';
						$response['message'] = '';
						
					}
				}
					
				
				
			} else {
				$message = 'Incomplete Data';
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
	
	}
	
	/******** On off notification for a user function for Api *******/
	public function notificationOnOff()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('NotificationStatus');
			$this->loadModel('Users');
			$error = true; $code = 0;
			$message = $response = array(); 
			
			if( isset( $this->request->data['user_id'] ) && isset( $this->request->data['other_user_id'] )) 
			{
				$statusdata = $this->NotificationStatus->find()->where(['user_id'=>$this->request->data['user_id'],
				'other_user_id'=>$this->request->data['other_user_id']])->first();
				
				$user_data = $this->Users->find('all', array('conditions' => array('id' => $this->request->data['other_user_id'])))->first();
				if(!empty($statusdata)) {
					$statusdetail  = $this->NotificationStatus->get($statusdata['id']);
					$this->NotificationStatus->delete($statusdetail);
					$error = false;
					$message = 'Notification is on.';
					$response['notification'] = true;
				} else {
					$addStatus = $this->NotificationStatus->newEntity();
					$off_noti = $this->NotificationStatus->patchEntity($addStatus, $this->request->data);
					if ($StatusDetail = $this->NotificationStatus->save($off_noti))
					{
						
						$error = false;
						$message = 'Notification is off .';
						$response['notification'] = false;
					}
				}
				
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data'=>array(),'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}	
	}
	
	/****************** Rewards history list function for api ***************************/
	public function rewardsHistory()
	{
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('HistoryRewards');
			$error =true;$code=0;
			$message= ""; $response = [];
			
			if(isset($this->request->data['user_id']))
			{
					
				if(isset($this->request->data['page']) && $this->request->data['page'] !=''){
					$page = $this->request->data['page'];
				}else{
					$page = '1';
				}
				$searchData = array();
				$searchData['AND'][] = array("HistoryRewards.is_deleted !=" => 'Y',"HistoryRewards.user_id" =>$this->request->data['user_id']);
				
				$historyRewards =  $this->HistoryRewards->find('all')->select()
								->contain([
									'user'=>['fields' => ['full_name']]
								])
								->where($searchData)
								->order(['HistoryRewards.id'=>'desc'])
								->limit($this->postPagination)
								->page($page)
								->hydrate(false)->toArray();
				
				
				$rewardsHistory_data = array();
				if (isset($historyRewards) &&  $historyRewards !='' && !empty($historyRewards)) {
					foreach ($historyRewards as $key => $value) {
						$rewards_data['id'] = $value['id'];
						$rewards_data['user_id'] = $value['user_id'];
						if($value['type'] == 'T'){
							$rewards_data['title'] = 'Rewards For Task Complete';
						}else if($value['type'] == 'S'){
							$rewards_data['title'] = 'Rewards For Top Singer';
						}else if($value['type'] == 'D'){
							$rewards_data['title'] = 'Rewards For Top DJ';
						}else{
						$rewards_data['title'] = 'Rewards Title';
						}
						$rewards_data['point'] = $value['point'];
						$rewards_data['created'] = date('Y-m-d',strtotime($value['created']));
						$rewardsHistory_data[] = $rewards_data;
					}
					$error = false;
					$message = 'All Rewards History';
					if(isset($rewardsHistory_data) &&  $rewardsHistory_data !='' && !empty($rewardsHistory_data)) {
						$response = $rewardsHistory_data;
					}else{
						$response = [];
					}
					
				} else {
					$error = true;
					$message = "No record available";	
					$response = [];
					
				}
			}else{
				 $message = 'Incomplete Data';
			}	
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
			
		}
	}
	
	
	public function paytm($user_id, $amount ,$challangeId=null,$type=null)
	{	
		require_once(ROOT . '/vendor' . DS  . 'Paytm' . DS . 'lib' . DS . 'encdec_paytm.php');
		require_once(ROOT . '/vendor' . DS  . 'Paytm' . DS . 'lib' . DS . 'config_paytm.php');
		
		
			$this->loadModel('Users');
			$user_detail = $this->Users->find('all', array('conditions' => array('id' => $user_id)))->first();

			$checkSum = "";
			$paramList = array();

			
			if(isset($type) && $type =='P'){
				$ORDER_ID = $user_id."_P_".$challangeId."_".time();
				
			}else{
				$ORDER_ID = $user_id."_C_".$challangeId."_".time();
			}
			
			$CUST_ID = "customer_".$user_id;
			
			
			$TXN_AMOUNT = $amount;

			// Create an array having all required parameters for creating checksum.
			$paramList["MID"] = PAYTM_MERCHANT_MID;
			$paramList["ORDER_ID"] = $ORDER_ID;
			$paramList["CUST_ID"] = $CUST_ID;
			$paramList["INDUSTRY_TYPE_ID"] = INDUSTRY_TYPE_ID;
			$paramList["CHANNEL_ID"] = CHANNEL_ID;
			$paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
			$paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
			if(isset($type) && $type =='C'){
				$paramList["CALLBACK_URL"] = PAYTM_USER_WALLET_CALLBACK2;
			}else{
				$paramList["CALLBACK_URL"] = PAYTM_USER_WALLET_CALLBACK;
			}
			
			if(!empty($user_detail['email'])){
				 $paramList["EMAIL"] =$user_detail['email'];
				
			}
			 if(!empty($user_detail['phone_number'])){
				  $paramList["MOBILE_NO"] =$user_detail['phone_number'];
				
			}
			$checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
			$paramList["CHECKSUMHASH"] = $checkSum;
			
			//echo $amount.$challangeId.$type;
			//echo "<pre>";
			//print_r($paramList);exit;
			//file_put_contents('paytm_txn_parm.txt', print_r($paramList, true),FILE_APPEND);

			$html='<html><head>
			<title>Meena Geet</title></head><body>
			<center><h1>Please do not refresh this page...</h1></center>';
			$html.='<form method="post" action="'.PAYTM_TXN_URL.'" name="f1">
			<table border="1">
				<tbody>';
					   
			foreach($paramList as $name => $value) {
				$html.='<input type="hidden" name="' . $name .'" value="' . $value . '">';
			}                    
		   
			$html.='</tbody></table><script type="text/javascript"> document.f1.submit(); </script></form></body></html>';
			echo $html;        
			exit;
		
	}
	
	
	 public function paytmCallback() {
		//file_put_contents('paytm_txn_parm.txt', print_r($data, true),FILE_APPEND);
		if($this->request->is(['post','put']))
    	{
			$this->loadModel('Transactions');
			$this->loadModel('PurchasePlans');
			$data = $this->request->data;
			$status=$data['STATUS'];
			$error = true; $code = 0;
			$message = '';
			$response = array(); 
			if($status=="TXN_SUCCESS"){
				
				$data=$this->paytm_status_api($data);
				
				$userid=explode('_P_',$data['ORDERID']);
				$user_id=$userid[0];
				
				$cuser_id=explode('_',$userid[1]);
				$plan_id= $cuser_id[0];
				
			
				$txnid=$data['TXNID'];
				$amount=$data['TXNAMOUNT'];
				$status=$data['STATUS'];
				//$CUST_ID=$data['CUST_ID'];
				 if($status=="TXN_SUCCESS"){
					$error = false;
					$message = 'Your payment successfully done.';
				
				///purchase plan save data in table 
				$addPurchasePlans = $this->PurchasePlans->newEntity();
				$this->request->data['user_id'] = $user_id;
				$this->request->data['plan_id'] = $plan_id;
				$this->request->data['status'] = 'C';
				$addPurchasePlan = $this->PurchasePlans->patchEntity($addPurchasePlans, $this->request->data);
				$this->PurchasePlans->save($addPurchasePlan);
				
				$addTransactions = $this->Transactions->newEntity();
				$this->request->data['user_id'] = $user_id;
				//$this->request->data['challenge_id'] = $data['ORDERID'];
				$this->request->data['type'] = 'U';
				$this->request->data['txnid'] = $txnid;
				$this->request->data['orderid'] = $data['ORDERID'];
				$this->request->data['txnamount'] = $data['TXNAMOUNT'];
				$this->request->data['gatewayname'] = $data['GATEWAYNAME'];
				$this->request->data['txndate'] = $data['TXNDATE'];
				$this->request->data['status'] = 'S';
				$addaddTransaction = $this->Transactions->patchEntity($addTransactions, $this->request->data);
				$this->Transactions->save($addaddTransaction);
					
				}else{
					$message = 'Your payment failed.';
					$addTransactions = $this->Transactions->newEntity();
					$this->request->data['user_id'] = $user_iddata[0];
					//$this->request->data['challenge_id'] = $data['ORDERID'];
					$this->request->data['type'] = 'U';
					$this->request->data['txnid'] = $txnid;
					$this->request->data['orderid'] = $data['ORDERID'];
					$this->request->data['txnamount'] = $data['TXNAMOUNT'];
					$this->request->data['gatewayname'] = $data['GATEWAYNAME'];
					$this->request->data['txndate'] = $data['TXNDATE'];
					$this->request->data['status'] = 'F';
					$addaddTransaction = $this->Transactions->patchEntity($addTransactions, $this->request->data);
					$this->Transactions->save($addaddTransaction);
				}
				
			}
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
    }
    
    public function paytmCallback2() {
		error_reporting(E_ALL);
		//echo "adf";
		//echo "<pre>";print_r($this->request->data);exit;
		//file_put_contents('paytm_txn_parm.txt', print_r($data, true),FILE_APPEND);
		//if($this->request->is(['post','put']))
		if($this->request->data)
    	{		//echo "dfdfa";
			//$data = $this->request->data;
			
			//echo $data['ORDERID'];echo "adf";exit;
			$this->loadModel('Challenges');
			$this->loadModel('Transactions');
			$data = $this->request->data;
			$status=$data['STATUS'];
			$error = true;
			$code = 0;
			$message = '';
			$response = array(); 
			if($status=="TXN_SUCCESS"){
				//echo "<pre>";print_r($data);echo "ad";
				$data=$this->paytm_status_api($data);
				//echo "<pre>";print_r($data);
				$userid=explode('_C_',$data['ORDERID']);
				$user_id=$userid[0];
				
				$cuser_id=explode('_',$userid[1]);
				$challenge_id= $cuser_id[0];
				
				$txnid=$data['TXNID'];
				$amount=$data['TXNAMOUNT'];
				 $status=$data['STATUS'];
				
				//$CUST_ID=$data['CUST_ID'];exit;
				 if($status=="TXN_SUCCESS"){
					$error = false;
					$message = 'Your payment successfully done.';
					$addTransactions = $this->Transactions->newEntity();
					$this->request->data['user_id'] = $user_id;
					$this->request->data['challenge_id'] = $challenge_id;
					$this->request->data['type'] = 'C';
					$this->request->data['txnid'] = $txnid;
					$this->request->data['orderid'] = $data['ORDERID'];
					$this->request->data['txnamount'] = $data['TXNAMOUNT'];
					$this->request->data['gatewayname'] = $data['GATEWAYNAME'];
					$this->request->data['txndate'] = $data['TXNDATE'];
					$this->request->data['status'] = 'S';
					$addaddTransaction = $this->Transactions->patchEntity($addTransactions, $this->request->data);
					$this->Transactions->save($addaddTransaction);
					
					//echo $challenge_id;
					//challange update status of Y an dsend notifications to others users 
					$challengesData = $this->Challenges->find('all', array(
								'fields' => array('Challenges.id','Challenges.user_id','Challenges.accept_user_id','Challenges.song_id', 'Challenges.accepted_user_song_id'),
								'conditions' => array(
									'Challenges.id' => $challenge_id, 
									'Challenges.status' => '0', 
									'Challenges.is_deleted' => 'Y'
								),
								'contain'=>[
									'user1'=>['fields' => ['id','full_name','image']],
									'user2'=>['fields' => ['id','full_name','image']],
									'song'=>['fields' => ['id', 'title','image','audio']],
									'acceptusersong'=>['fields' => ['title','image','audio']]
								],
							))->first();
							
						
					if(isset($challengesData) && $challengesData['accepted_user_song_id'] ==''){
						
						$this->loadModel('NotifyUsers');
						$NotifyUser = $this->NotifyUsers->newEntity();
						$notifyData['user_id'] = $user_id;
						$notifyData['song_id'] = $challengesData['song']['id'];
						$notifyData['type'] = 'challenge';
						$NotifyUser = $this->NotifyUsers->patchEntity($NotifyUser, $notifyData);
						$this->NotifyUsers->save($NotifyUser);
						
						$challengesData['is_deleted'] = 'N';
						$this->Challenges->save($challengesData);
					}else{
						$this->loadModel('NotifyUsers');
						$NotifyUser = $this->NotifyUsers->newEntity();
						$notifyData['user_id'] = $user_id;
						//$notifyData['song_id'] = $challengesData['acceptusersong']['id'];
						$notifyData['song_id'] = 0;
						$notifyData['type'] = 'challengeaccept';
						$NotifyUser = $this->NotifyUsers->patchEntity($NotifyUser, $notifyData);
						$this->NotifyUsers->save($NotifyUser);
						
						//$challengesData['is_deleted'] = 'N';
						
						//$this->Challenges->save($challengesData);
					}
					
					
				}else{
					$message = 'Your payment failed.';
					$addTransactions = $this->Transactions->newEntity();
					$this->request->data['user_id'] = $user_id;
					$this->request->data['challenge_id'] = $challenge_id;
					$this->request->data['type'] = 'C';
					$this->request->data['txnid'] = $txnid;
					$this->request->data['orderid'] = $data['ORDERID'];
					$this->request->data['txnamount'] = $data['TXNAMOUNT'];
					$this->request->data['gatewayname'] = $data['GATEWAYNAME'];
					$this->request->data['txndate'] = $data['TXNDATE'];
					$this->request->data['status'] = 'F';
					$addaddTransaction = $this->Transactions->patchEntity($addTransactions, $this->request->data);
					$this->Transactions->save($addaddTransaction);
					
					$challenge = $this->Challenges->get($challenge_id);
					$this->Challenges->delete($challenge);
				}
				
			}
			
			if($error == true){ 
			$this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}else { 
			$this->set(array('code'=>$code,'error'=>$error,'message'=> $message,'data'=> $response,'_serialize'=>array('code','error','message','data')));
		}
		}
    }
    
    
    public function paytm_status_api($data){
		
		require_once(ROOT . '/vendor' . DS  . 'Paytm' . DS . 'lib' . DS . 'encdec_paytm.php');
		require_once(ROOT . '/vendor' . DS  . 'Paytm' . DS . 'lib' . DS . 'config_paytm.php');
		
			header("Pragma: no-cache");
			header("Cache-Control: no-cache");
			header("Expires: 0");
                $ORDER_ID = $data['ORDERID'];
                $requestParamList = array();
                $responseParamList = array();
                
                $requestParamList = array("MID" => PAYTM_MERCHANT_MID , "ORDERID" => $ORDER_ID);  
                
                $checkSum = getChecksumFromArray($requestParamList,PAYTM_MERCHANT_KEY);
                $requestParamList['CHECKSUMHASH'] = urlencode($checkSum);
                
                $data_string = "JsonData=".json_encode($requestParamList);
               //echo $data_string;
                
                $ch = curl_init();                    // initiate curl
                $url = PAYTM_STATUS_QUERY_URL; //Paytm server where you want to post data
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
                curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string); // define what you want to post
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
                $headers = array();
                $headers[] = 'Content-Type: application/json';
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $output = curl_exec($ch); // execute
                $info = curl_getinfo($ch);
                $data = json_decode($output, true);
		        return $data;
		
		}
		
	public function refundPoliciesPages()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = ''; 
			$response = []; 
			$this->loadModel('Pages');
			$pagesDJ = $this->Pages->find('all', array('fields' => array('Pages.title', 'Pages.description'),'conditions' => array('Pages.id' => 23, 'Pages.enabled' => 1)))->first();
			if(!empty($pagesDJ))
			{ 
				$error = false;
				$response['refund_policies_pages'] = $pagesDJ['description'];
			} else { 
				$response['refund_policies_pages'] = '';
				$error = true;
				$message = "No record available";
			}
			
			
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
		}
	}
	
	/****************** edit user function for api *********************/
	public function updateDeviceToken()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = '';$response = []; 
			
			 if(isset($this->request->data['user_id']) && isset($this->request->data['device_token']))
			{
				$id = $this->request->data['user_id'];
				$users  = $this->Users->find('all',array('conditions'=>array('id'=>$id,'access_level_id'=>2)))->hydrate(false)->first();
				if(!empty($users))
				{
					$userData = $this->Users->get($id);
					$userData->device_token = $this->request->data['device_token'];
					$this->Users->save($userData);
					$message = "Device Token update successfully";
					$error = false;
					
				} else $message = 'No Record Found';
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data'))); 
		}	
	}
	
	
	/****************** edit user function for api *********************/
	public function userChk()
    {
		if ($this->request->is(array('post','put')))
		{
			$error = true; $code = 0;
			$message = '';$response = []; 
			
			 if(isset($this->request->data['user_id']))
			{
				$id = $this->request->data['user_id'];
				$users  = $this->Users->find('all',array('conditions'=>array('id'=>$id,'access_level_id'=>2)))->hydrate(false)->first();
				if(!empty($users))
				{
					if($users['enabled']=='N'){
						$response['user_delete'] = true;
						$message = "User has been deactivated";
						$this->sendPushNotificationPayment('user_delete', $users['id'], $message);
						$error = false;
					}elseif($users['is_deleted']=='Y'){
						$response['user_delete'] = true;
						$message = "User has been deleted";
						$this->sendPushNotificationPayment('user_delete', $users['id'], $message);
						$error = false;
					}else{
						$response['user_delete'] = false;
						$message = "";
						$error = false;
					}
				} else {
					$message = 'No Record Found';
				}
			} else $message = 'Incomplete Data';
			if($error == true) $this->set(array('data' => array(), 'code' => 1,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data')));
			else $this->set(array('data'=>$response,'code'=>$code,'error'=>$error,'message'=> $message,'_serialize'=>array('code','error','message','data'))); 
		}	
	}



	
}
