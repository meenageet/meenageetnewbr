<?php
namespace App\Controller;
use App\Controller\AppController; // HAVE TO USE App\Controller\AppController
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\Validation\Validation;
use Cake\I18n\Time;

class CronesController extends AppController
{
	
	public function beforeFilter(Event $event)
    {
		 $this->Auth->allow(['cronMonthlyPointAssignTest','croneCheckSubscriptionPlan','cronMonthlyPointAssign','cronPushNotification','cronSongPlayStopDaily']);
	}
	
	public function croneCheckSubscriptionPlan()
    {
					
		$this->loadModel('Users');
		$this->loadModel('PurchasePlans');
		$this->loadModel('RequestDjSingers');						
		$freeSubsDays = $this->Users->find('all', array(
			'fields' => array('Users.subscription_duration_month'),
			'conditions' => array('Users.access_level_id' => 1),
		))->first();
		$allUsersData =  $this->Users->find('all')->select()->where(["enabled" => 'Y',"is_deleted !=" => 'Y'])->order(['id'=>'desc'])->hydrate(false)->toArray();
		foreach($allUsersData as $allUsersDataval){
			$user_id = $allUsersDataval['id'];
			$chkuser = $this->RequestDjSingers->find('all', array(
							'fields' => array('id'),
							'conditions' => array('user_id' => $user_id, 'is_approved' => 'Y', 'is_deleted' => 'N'),
					))->first();
			if(empty($chkuser)){
					/*********** Check if user has bought a plan or not ************/
					$userPlan = $this->PurchasePlans->find('all', array(
						'fields' => array('PurchasePlans.id', 'PurchasePlans.status', 'PurchasePlans.created'),
						'conditions' => array('PurchasePlans.user_id' => $user_id, 'PurchasePlans.status' => 'C', 'PurchasePlans.is_deleted' => 'N'),
						'contain'=>[
							'Premiums'=>['fields' => ['id','duration_type','duration']]
						]
					))->first();
					if(!empty($userPlan)){
						if($userPlan['premium']['duration_type'] == 'Monthly') { 
							$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."months", strtotime($userPlan['created'])));
						} elseif($userPlan['premium']['duration_type'] == 'Yearly') {	
							$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."years", strtotime($userPlan['created'])));
						}
						$previousPlanExpireDate = date('Y-m-d', strtotime('-1 day', strtotime($planExpireDate)));
						//calulation current time
						date_default_timezone_set('Asia/Kolkata');
						$currrenttime = time();
						$current_date = date('Y-m-d', $currrenttime);
					
						if ($userPlan =='' && empty($userPlan)) { 
							
							$free_subscription = $this->Users->find('all', array(
									'fields' => array('Users.id', 'Users.free_subscription_time', 'Users.created'),
									'conditions' => array('Users.id' => $user_id),
							))->first();
							
							if (isset($free_subscription['free_subscription_time']) && $free_subscription['free_subscription_time'] !='' && !empty($free_subscription['free_subscription_time'])) {
								$subsExpireDate =  date('Y-m-d', strtotime($free_subscription['free_subscription_time']));
								$previousSubsExpireDate = date('Y-m-d', strtotime('-1 day', strtotime($subsExpireDate)));
								
								if($current_date >= $subsExpireDate) {
									$free_plan = $this->Users->get($free_subscription['id']);
									$free_plan->free_subscription_time = null;
									$this->Users->save($free_plan);
									
									// push notification code here
									$message = 'Your free subscription plan has been expired.';
									$this->sendPushNotification('Song_download', null, null, null, $user_id, $message);
								} elseif ($current_date == $previousSubsExpireDate) {
									
									// push notification code here
									$message = 'Your free subscription plan is going to expire today.';
									$this->sendPushNotification('Song_download', null, null, null, $user_id, $message);
								} 
							} else {
								// push notification code here
								$message = 'Your plan has been expired.';
								$this->sendPushNotification('Song_download', null, null, null, $user_id, $message);
							}
						} elseif ($current_date == $previousPlanExpireDate) {
								
							//push notification code here
							$message = 'Your subscription plan is going to expire today.';
							$this->sendPushNotification('Song_download', null, null, null, $user_id, $message);
						} elseif($current_date >= $planExpireDate) {
							$purchase_plan = $this->PurchasePlans->get($userPlan['id']);
							$purchase_plan->status = 'E';
							$this->PurchasePlans->save($purchase_plan);
							
							// push notification code here
							$message = 'Your plan has been expired.';
							$this->sendPushNotification('Song_download', null, null, null, $user_id, $message);
						}	
					}
			}
			
		}
		echo 'Done';die;
	}
	
	public function cronMonthlyPointAssign()
    {
		
		$monthlast_date = date('Y-m-t');
		$current_date = date('Y-m-d');
		//if($current_date == $monthlast_date){
			$this->loadModel('ManagerTopSingerDjRewards');
			$this->loadModel('SongUploads');
			$this->loadModel('RequestDjSingers');
			$this->loadModel('HistoryRewards');
			$managerewardsingerlist = $this->ManagerTopSingerDjRewards->find('list', ['keyField' => 'id','valueField' => 'point','conditions'=>array('enabled'=>'Y','is_deleted'=>'N','type'=>'S')])->toArray();
			$managerewarddjlist = $this->ManagerTopSingerDjRewards->find('list', ['keyField' => 'id','valueField' => 'point','conditions'=>array('enabled'=>'Y','is_deleted'=>'N','type'=>'D')])->toArray();
			$i = 0;
			$j = 0;
			$dataARR = [];
			foreach($managerewardsingerlist as $rewardsingerkey => $rewardsingervalue){
				
				/***************** add rewards according to singer user. *********************/
				$requestDjSingerUserids = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('RequestDjSingers.type' => 'S', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N')])->toArray();
				$songUploads =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.title', 'SongUploads.total_month_likes', 'SongUploads.user_id'])->where(["SongUploads.user_id IN" => $requestDjSingerUserids,"SongUploads.is_deleted !=" => 'Y',"SongUploads.enabled" => 'Y'])
									->order(['SongUploads.total_month_likes'=>'desc'])
									->limit(1)
									->offset($i)
									->hydrate(false)->first();
					//add reward point in reward table 
					/*$chkHistoryReward =  $this->HistoryRewards->find('all')->select()
						->where(['type'=>'S','DAY(created)'=> date('d'),'user_id'=>$songUploads['user_id']])
						->hydrate(false)->first();
				
					if(empty($chkHistoryReward)){*/
						$addHistoryReward = $this->HistoryRewards->newEntity();
						$this->request->data['user_id'] = $songUploads['user_id'];
						$this->request->data['point'] = $rewardsingervalue;
						$this->request->data['type'] = 'S';
						$addHistoryRewards = $this->HistoryRewards->patchEntity($addHistoryReward, $this->request->data);
						$this->HistoryRewards->save($addHistoryRewards);
					//}
				$i++;
				/***************** end add rewards according to singer user. *********************/
			}
			
			foreach($managerewarddjlist as $rewarddjkey => $rewarddjvalue){
				/***************** add rewards according to dj user. *********************/
				$requestDjUserids = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('RequestDjSingers.type' => 'D', 'RequestDjSingers.is_approved' => 'Y', 'RequestDjSingers.is_deleted' => 'N')])->toArray();
				$djsongUploads =  $this->SongUploads->find('all')->select(['SongUploads.id','SongUploads.title', 'SongUploads.total_month_likes', 'SongUploads.user_id'])->where(["SongUploads.user_id IN" => $requestDjUserids,"SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'])
									->order(['SongUploads.total_month_likes'=>'desc'])
									->limit(1)
									->offset($j)
									->hydrate(false)->first();
					//add reward point in reward table
					/*$chkdjHistoryReward =  $this->HistoryRewards->find('all')->select()
						->where(['type'=>'D','DAY(created)'=> date('d'),'user_id'=>$djsongUploads['user_id']])
						->hydrate(false)->first();
					if(empty($chkdjHistoryReward)){*/
						$adddjHistoryReward = $this->HistoryRewards->newEntity();
						$this->request->data['user_id'] = $djsongUploads['user_id'];
						$this->request->data['point'] = $rewarddjvalue;
						$this->request->data['type'] = 'D';
						$adddjHistoryRewards = $this->HistoryRewards->patchEntity($adddjHistoryReward, $this->request->data);
						$this->HistoryRewards->save($adddjHistoryRewards);
					//}
				$j++;
			} 
		//}
		echo 'Done';die;
	}
	
	
	
	
	
	
	
	
	
	public function cronPushNotification()
    {
		$this->loadModel('Users');
		$this->loadModel('SongUploads');
		$this->loadModel('RequestDjSingers');
		$this->loadModel('NotificationStatus');
		$this->loadModel('NotifyUsers');
		
	    /* $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
		$txt = "John Doe\n";
		fwrite($myfile, $txt);
		$txt = "Jane Doe\n";
		fwrite($myfile, $txt);
		fclose($myfile);
		die('sss'); 
		*/
		$notifyUsers =  $this->NotifyUsers->find('all')->select()->order(['id'=>'desc'])->hydrate(false)->toArray();
		if (isset($notifyUsers) &&  $notifyUsers !='' && !empty($notifyUsers)) {
			
			foreach($notifyUsers as $notifyUsersData){
				$user_data_song = $this->SongUploads->find('all', array('conditions' => array("SongUploads.id "=>$notifyUsersData['song_id'], "SongUploads.is_deleted !=" => 'Y', "SongUploads.enabled" => 'Y'),'contain'=>['user'=>['fields' => ['full_name','phone_number']]]))->first();
				$song_id = $user_data_song['id'];
				$song_name = $user_data_song['title'];
				
				$user_data = $this->Users->find('list', array('keyField' => 'id','valueField' => 'id','conditions' => array('id !=' => $notifyUsersData['user_id'],'access_level_id'=>2, 'enabled' => 'Y', 'is_deleted' => 'N')))->toArray();
				$notificationsDataArr = array();
				if (isset($user_data) &&  $user_data !='' && !empty($user_data)) {
					$check_noti_on = $this->NotificationStatus->find('list', array('valueField' => 'user_id','conditions' => array('NotificationStatus.user_id IN' => $user_data, 'NotificationStatus.other_user_id' =>  $notifyUsersData['user_id'])
					))->toArray();
					
					if (isset($check_noti_on) &&  $check_noti_on !='' && !empty($check_noti_on)) {
						$notificationsDataArr = array_diff($user_data, $check_noti_on);
						
						foreach(array_chunk($notificationsDataArr, 100) as  $key => $notifyAllData ) { 
							$deviceTknArr = [];
							foreach($notifyAllData as $key => $notify_user_id) {
								$userDeviceTkn = $this->Users->find('all', array('conditions' => array('id' => $notify_user_id,'access_level_id'=>2, 'enabled' => 'Y', 'is_deleted' => 'N')))->first();
								$deviceTknArr[] = $userDeviceTkn['device_token'];
							}
							
							if(isset($user_data_song['user']['full_name']) && $user_data_song['user']['full_name'] !=''){
								$username = $user_data_song['user']['full_name'];
							}else{
								$username = $user_data_song['user']['phone_number'];
							}
							if($notifyUsersData['type'] == 'challenge'){
								$message =  ''.$username.' created new challenge.';
							}elseif($notifyUsersData['type'] =='challengeaccept'){
								$message =  ''.$username.' accepted new challenge.';
							}else{
								$message =  ''.$username.' uploaded new song.';
							}
							$url = 'https://fcm.googleapis.com/fcm/send';
							$server_key = 'AIzaSyAdC4iSaZt2KbwOlFvR7lza_QsV5u_m_e4'; 
							
							$keyDataArr = array('title'=>'Meena Geet', 'body'=>$message, 'sound'=>'mySound', 'badge' => '1', 'notification_type' => 'Song_upload', 'song_id' => $song_id, 'songname' => $song_name, 'challenge_id' => null, 'user_id' => null);
							
							$arrayToSend = array
								(
									'registration_ids' =>$deviceTknArr,
									'priority' => "high",
									'data'  => $keyDataArr
								);
							
							$headers = array(
								'Content-Type:application/json',
								'Authorization:key='.$server_key
							);	
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));
							$result = curl_exec($ch);
							curl_close($ch);
							if(isset($return) && $return == 'success'){
								$arr =  json_decode($result);
								//return  $arr->success;
							}			
						}
						//delete user
						$deleteNotifyUser = $this->NotifyUsers->get($notifyUsersData['id']); // Return article with id 12
						$this->NotifyUsers->delete($deleteNotifyUser);
						// puch notification code here			
					}else{
						
						foreach(array_chunk($user_data, 100) as  $key => $notifyAllData ) { 
							$deviceTknArr = [];
							foreach($notifyAllData as $key => $notify_user_id) {
								$userDeviceTkn = $this->Users->find('all', array('conditions' => array('id' => $notify_user_id,'access_level_id'=>2, 'enabled' => 'Y', 'is_deleted' => 'N')))->first();
									$deviceTknArr[] = $userDeviceTkn['device_token'];
							}
							if(isset($user_data_song['user']['full_name']) && $user_data_song['user']['full_name'] !=''){
								$username = $user_data_song['user']['full_name'];
							}else{
								$username = $user_data_song['user']['phone_number'];
							}
							if($notifyUsersData['type'] == 'challenge'){
								$message =  ''.$username.' created new challenge.';
							}elseif($notifyUsersData['type'] =='challengeaccept'){
								$message =  ''.$username.' accepted new challenge.';
							}else{
								$message =  ''.$username.' uploaded new song.';
							}
							$url = 'https://fcm.googleapis.com/fcm/send';
							$server_key = 'AIzaSyAdC4iSaZt2KbwOlFvR7lza_QsV5u_m_e4'; 
								
							$keyDataArr = array('title'=>'Meena Geet', 'body'=>$message, 'sound'=>'mySound', 'badge' => '1', 'notification_type' => 'Song_upload', 'song_id' => $song_id, 'songname' => $song_name, 'challenge_id' => null, 'user_id' => null);
								
							$arrayToSend = array
									(
										'registration_ids' =>$deviceTknArr,
										'priority' => "high",
										'data'  => $keyDataArr
									);
								
							$headers = array(
									'Content-Type:application/json',
									'Authorization:key='.$server_key
							);	
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));
							$result = curl_exec($ch);
							curl_close($ch);
							if(isset($return) && $return == 'success'){
								$arr =  json_decode($result);
								//return  $arr->success;
							}			
						}
							//delete user
							$deleteNotifyUser = $this->NotifyUsers->get($notifyUsersData['id']); // Return article with id 12
							$this->NotifyUsers->delete($deleteNotifyUser);
						
					}
				}
				
			}	
		}
		echo 'Done';die;
		
	}
	
	
	
/****************** song paly or stop rewards cron api **********************/
    public function cronSongPlayStopDaily()
    {
		
		$curdate = date('Y-m-d', strtotime("+ 1 day")).' 00:02:00';
		date_default_timezone_set('Asia/Kolkata');
		$this->loadModel('UserRewards');
		$chkRewards =  $this->UserRewards->find('all', array('conditions' => array('is_deleted'=>'N','enabled' => 'Y','play_status' => 1)))->order(['id'=>'desc'])->hydrate(false)->toArray();
		
		if(!empty($chkRewards)){
			foreach($chkRewards as $chkReward){
			$cdate = strtotime(date('Y-m-d H:i:s'));
				$start_time = strtotime($chkReward['start_time']->i18nFormat('yyyy-MM-dd HH:mm:ss'));
				$diff = $cdate - $start_time;
				if($diff >= 0) {
					$totaltime = $diff + $chkReward['time'];
					$userrewards = $this->UserRewards->get($chkReward['id']);
					$userrewards->time = 0;
					$userrewards->start_time = $curdate;
					//$userrewards->play_status = 0;
					$userrewardsdetails = $this->UserRewards->save($userrewards);
					/*if($this->UserRewards->save($userrewards)){
						
						//echo "<pre>";print_r($chkReward);exit;
						$userreward = $this->UserRewards->get($chkReward['id']);
							$userreward->time = 0;
							$userreward->start_time = $curdate;
							$userreward->play_status = 1;
							$this->UserRewards->save($userreward);
					}*/
					
				}
			}	
			die('ok');
		}else{
			die('empty');
		}	
		
		
	
	}
	
	
	
}
