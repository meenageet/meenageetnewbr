<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Validation\Validation;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Gnello;


class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['emailVerify','sendOtp','verifyOtp','frontFblogin','frontLogin', 'logout','frontRegister','forgotPassword','resetPassword','subscription','forgetPassword','accountVerify','test','getState','getCity','gettinglocationdata','verification','login']);
        
        
 		$user_data = $this->Users->find()->where(['id'=>$this->Auth->user('id')])->hydrate(false)->first();
		if ($user_data['enabled']=='N'){
			$this->Flash->error('Your Account Have Been Deactive By Admin Please Contact To Site Adminstrator.');
			return $this->redirect($this->Auth->logout()); 
		} 
		
		
        
    }
    
}
