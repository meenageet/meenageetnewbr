<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
 
 
 
 
class PagesController extends AppController
{
	public function beforeFilter(Event $event)
    {
		
		 parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['noAccess','faq','content','home','NotFound','contactUs','aboutUs','search','searchCategory','searchAll','statesList']);
    }
    
    public function index()
    {
	
		return $this->redirect(['controller'=>'Pages','action' => 'home']);
		
	}
    
    public function content($slug = null)
    {
		$this->set('title',$this->project_title. '::'.ucwords(str_replace(array('_','-'),' ',$slug)));
		$pageContent = $this->Pages->find('all',[ 'fields'=>['id','title','slug','description'],'conditions'=>['slug' => $slug]])->first();
		
		
		$this->set('slug',$slug);
		
		
		if(!empty($pageContent)) $this->set('content',$pageContent);
		else return $this->redirect(['controller'=>'Pages','action' => 'not_found']);
	}
    public function contactUs()
    {  
		$this->set('title',$this->project_title. ':: Contact Us');
		$this->set('contactActive','active');
		if($this->Auth->user('id') != '')
		{
			$this->loadModel('Users');
			$users= $this->Users->find('all',['fields'=>['username','email','full_name'],'conditions' =>['id'=>$this->Auth->user('id')]])->hydrate(false)->toArray();
		}
		$this->loadModel('ContactUs');
		$contactus = $this->ContactUs->newEntity();
		if ($this->request->is(['post' ,'put'])) 
		{
			//pr($this->request->data);die;
			$contactus = $this->ContactUs->patchEntity($contactus, $this->request->data);
			if ($newNetwork = $this->ContactUs->save($contactus)) {
				$this->Flash->success(__('Successfully submitted !!'));
				return $this->redirect(['controller'=>'pages','action' =>'contact-us','?' => array('action'=>'success')]);
				//return $this->redirect(['controller'=>'pages','action' =>'contact-us']);
				
			}else $this->Flash->error(__('Someting went wrong !!'));
		}	
		
		//$this->set(array('contactus'=>$contactus,'users' => $users));
		$this->set(array('contactus'=>$contactus));
	}
    
    
     public function aboutUs()
    {  
		$this->set('title',$this->project_title. ':: About Us');
		$this->set('aboutActive','active');

	}
	
	public function home()
	{
		return $this->redirect(['controller'=>'Users','action' => 'login','prefix' => 'admin']);
	}
	
	public function search()
	{
		if ($this->request->is('ajax')) 
		{
			$this->loadModel('Posts');
			$searchData = array();
			$searchData['AND'][] = array('Posts.enabled'=>'Y','Posts.is_deleted' => 'N',"user.enabled"=>'Y',"category.enabled"=>'Y');
			if($this->Auth->user('id') != '')
			{
				$this->loadModel('BlockUsers');
				$block= $this->BlockUsers->find('all',['fields'=>['user_id'],'conditions' =>['block_user_id'=>$this->Auth->user('id')]])->hydrate(false)->toArray();
				if(!empty($block)){
					$searchData['AND'][] = array('Posts.user_id NOT IN' =>array_column($block,'user_id'));	
				}
			}
			$userLocation = $this->request->session()->read('user_location');
			$searchData['AND'][] = [['post_status !=' => 'C']];
			$unit = 6371 ;
			$distance = " ( ".$unit." * acos( cos( radians(" . $userLocation['latitude'] . ") ) * cos( radians(Posts.latitude ) ) 
			* cos( radians(Posts.longitude) - radians(" . $userLocation['longitude']. ")) + sin(radians(" .$userLocation['latitude']. ")) 
			* sin( radians(Posts.latitude))))";
			$active_premium =  "(CASE 'active' WHEN CURDATE() between start_date and end_date THEN 0 ELSE 1 END)";   
			$posts= $this->Posts->find('all',[
			'fields'=>['Posts.user_id','Posts.id','user.premium','Posts.post_status','Posts.title','Posts.currency_symbol','Posts.price','Posts.country','distance'=>$distance,'active'=>$active_premium],
			'contain'=>['p_image','user','category'],
			'conditions'=>$searchData,
			'order'=>['distance'=>'ASC','active'=>'DESC','user.premium'=>'DESC','Posts.id'=>'DESC'],
			 'limit' =>16,
			]);
			$this->set('Posts',$this->Paginator->paginate($posts));
		}
	}
	
	
	public function noAccess()
	{
		$this->set('title',$this->project_title. ':: 401');
	}
	
	
	public function NotFound()
	{
		$this->set('title',$this->project_title.' :: Page not found');
	}
	
	public function statesList()
	{
		$this->loadModel('States');
		$this->set('States', $this->States->find('all', ['conditions'=>array('States.enabled'=>'Y', 'States.deleted'=>'N'),'order'=>array('States.id'=>'ASC')])->contain(['Cities'])->toArray());
		
	}
	
}
