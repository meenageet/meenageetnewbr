<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;


class ContactUsController extends AppController
{
	
    
	
    public function manage(){
		$this->set('title' , 'Contact us');
		$searchData = array();
		
		$this->set('ContactUs',$this->Paginator->paginate(
			$this->ContactUs, [
				'limit' => 10,
				'order'=>['id'=>'desc'],
				'conditions' => $searchData
			])
		);
		
	}
	
	
	public function search(){
		if ($this->request->is('ajax')) {
			$searchData = array();
				if(isset($this->request->data['key']) && $this->request->data['key']!=''){
					$search = $this->request->data['key'];
					$searchData['OR'][] = array('email LIKE' => '%'.$search. '%');
					$searchData['OR'][] = array('name LIKE' => '%'.$search. '%');
					$searchData['OR'][] = array('subject LIKE' => '%'.$search. '%');
				
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',((10)*($this->request->query('page'))) - (10 -1));
				}
				else {$this->set('serial_num',1);}
			$this->set('ContactUs',$this->Paginator->paginate(
			$this->ContactUs, [
				'limit' => 10,
				'order'=>['id'=>'desc'],
				'conditions' => $searchData
			])
		);
			
			
		}
	}
	
	public function search3(){
		
		if ($this->request->is('ajax')) {
			
			$searchData = array();
			$searchData['AND'][] = array("Categories.is_deleted" => 'N',"ParentCategories.is_deleted" => 'N');
				if(isset($this->request->data['key'])){
					$search = $this->request->data['key'];
					$searchData['OR'][] = array('category_name LIKE' => '%'.$search.'%');
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
				}
				else {$this->set('serial_num',1);}
			
			$this->set('Categories',$this->Paginator->paginate(
						$this->Categories, [
							'contain'=>['ParentCategories'],
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
			
		}
	}
	
	
	public function Detail($id = null)
    {
		$this->set('title' , 'Contact Us');
        $ContactUs = $this->ContactUs->get($id);
		if ($this->request->is(['post' ,'put'])) {

            $query = $this->ContactUs->query ();
            $query->update ()
                ->set ( [ 'status' => 1,'reply_subject' => $this->request->data['reply_subject'],'reply_message' => $this->request->data['reply_message'] ] )
                ->where ( [ 'id' => $id ] )
                ->execute ();
				if($_SERVER['SERVER_NAME'] != '192.168.1.253' )
				{
					// success email

					$email = new Email('default');
					$email->from([$this->Auth->user('email') => 'Br Classified'] )
					->to([$ContactUs->email])
					->subject($this->request->data['reply_subject'])
					->emailFormat('html')
					->send($this->request->data['reply_message']);
				}
				$this->Flash->success(__('Reply sent.'));
                return $this->redirect(['controller'=>'contact_us','action' => 'manage']);

        }
       $this->set('ContactUs', $ContactUs);
    }
    
	
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			$query = $this->ContactUs->query();
			$query->delete()
			->where(['id' => $this->request->data['id']])
			->execute();
			echo 1;
		}
		die;
		
	}
	
	
}
