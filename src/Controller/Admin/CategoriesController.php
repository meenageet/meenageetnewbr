<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class CategoriesController extends AppController
{
	public function add()
    {
		$this->set('title' , $this->project_title.'!: Add category');
        $category = $this->Categories->newEntity();
        
        if ($this->request->is(['post' ,'put'])) {
            
            if(isset($this->request->data['parent_category_id']) && $this->request->data['parent_category_id'] !=''){
				  $this->request->data['parent_category_id'] = $this->request->data['parent_category_id'];
			}
			$this->request->data['slug'] = trim($this->slugify($this->request->data['category_name']));
          
			$category = $this->Categories->patchEntity($category, $this->request->data);
		//	echo "<pre>";print_r($this->request->data['cat_logo_web']);die;
            if ($newNetwork = $this->Categories->save($category)) {
				$cate_app1=$cate_app2=$cate_app3 = $cate_web = '';
				if(isset($this->request->data['cat_logo_web']) && !empty($_FILES['cat_logo_web']['tmp_name']) )
				{
					$filename = '';
					$newfilename = $this->request->data['cat_logo_web']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['cat_logo_web']['tmp_name'], $_FILES['cat_logo_web']['type'], 'uploads/category_web/', $filename)) $cate_web  = $filename;
						
				}
				/*
				if(isset($this->request->data['cat_logo_app1']) && !empty($_FILES['cat_logo_app1']['tmp_name']) )
				{
					$filename = '';
					$newfilename = $this->request->data['cat_logo_app1']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['cat_logo_app1']['tmp_name'], $_FILES['cat_logo_app1']['type'], 'uploads/category_app/3x/', $filename)) $cate_app1  = $filename;
						
				}
				if(isset($this->request->data['cat_logo_app2']) && !empty($_FILES['cat_logo_app2']['tmp_name']) )
				{
					$filename = '';
					$newfilename = $this->request->data['cat_logo_app2']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['cat_logo_app2']['tmp_name'], $_FILES['cat_logo_app2']['type'], 'uploads/category_app/2x/', $filename)) $cate_app2  = $filename;
						
				}
				if(isset($this->request->data['cat_logo_app3']) && !empty($_FILES['cat_logo_app3']['tmp_name']) )
				{
					$filename = '';
					$newfilename = $this->request->data['cat_logo_app3']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['cat_logo_app3']['tmp_name'], $_FILES['cat_logo_app3']['type'], 'uploads/category_app/1x/', $filename)) $cate_app3  = $filename;
						
				}
				
				$category = $this->Categories->get($newNetwork['id']);
				$category['cat_logo_app1'] = $cate_app1;
				$category['cat_logo_app2'] = $cate_app2;
				$category['cat_logo_app3'] = $cate_app3;
				*/
				$category['cat_logo_web'] = $cate_web;
				$this->Categories->save($category);
				
                $this->Flash->success(__('Category detail has been saved.'));
                return $this->redirect(['controller'=>'Categories','action' => 'add']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
        
        $this->loadModel('ParentCategories');
		$this->set('ParentCategories',$this->Categories->find('list', ['keyField' => 'id','valueField' => 'category_name','conditions'=>array('enabled'=>'Y', 'is_deleted'=>'N'),'order'=>array('category_name'=>'ASC')])->toArray());
		
        $this->set('category', $category);
    }
	
	public function edit($id = null)
    {
		$this->set('title' , $this->project_title.'!: Edit category');
        $category = $this->Categories->get($id);
      
        if ($this->request->is(['post' ,'put'])) {
			$oldLogoWeb = $category['cat_logo_web'];
			/*$oldLogoApp1 = $category['cat_logo_app1'];
			$oldLogoApp2 = $category['cat_logo_app2'];
			$oldLogoApp3 = $category['cat_logo_app3'];
			*/
			
		    if(isset($this->request->data['parent_category_id']) && $this->request->data['parent_category_id'] !=''){
				  $this->request->data['parent_category_id'] = $this->request->data['parent_category_id'];
			}else{
				 $this->request->data['parent_category_id'] = '0';
			}
			
			$this->request->data['slug'] = trim($this->slugify($this->request->data['category_name']));
			$Category = $this->Categories->patchEntity($category, $this->request->data);
			
			if($_FILES['cat_logo_web']['name'] == '') $Category->cat_logo_web = $oldLogoWeb;
			/*if($_FILES['cat_logo_app1']['name'] == '') $Category->cat_logo_app1 = $oldLogoApp1;
			if($_FILES['cat_logo_app2']['name'] == '') $Category->cat_logo_app2 = $oldLogoApp2;
			if($_FILES['cat_logo_app3']['name'] == '') $Category->cat_logo_app3 = $oldLogoApp3;
			*/
			if ($this->Categories->save($Category)) 
			{
				$Category = $this->Categories->get($id);
				if(isset($this->request->data['cat_logo_web']) && !empty($_FILES['cat_logo_web']['tmp_name']) )
				{
					$newfilename = $this->request->data['cat_logo_web']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['cat_logo_web']['tmp_name'], $_FILES['cat_logo_web']['type'], 'uploads/category_web/', $filename)) $Category->cat_logo_web = $filename;
					
				}/*
				if(isset($this->request->data['cat_logo_app1']) && !empty($_FILES['cat_logo_app1']['tmp_name']) )
				{
					$newfilename = $this->request->data['cat_logo_app1']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['cat_logo_app1']['tmp_name'], $_FILES['cat_logo_app1']['type'], 'uploads/category_app/3x/', $filename)) $this->createThumbnail($filename, 'uploads/category_app/3x/', 'uploads/category_app/thumb_app', 90,90); $Category->cat_logo_app1 = $filename;
				}
				if(isset($this->request->data['cat_logo_app2']) && !empty($_FILES['cat_logo_app2']['tmp_name']) )
				{
					$newfilename = $this->request->data['cat_logo_app2']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['cat_logo_app2']['tmp_name'], $_FILES['cat_logo_app2']['type'], 'uploads/category_app/2x/', $filename)) $this->createThumbnail($filename, 'uploads/category_app/2x/', 'uploads/category_app/thumb_app', 90,90); $Category->cat_logo_app2 = $filename;
				}
				if(isset($this->request->data['cat_logo_app3']) && !empty($_FILES['cat_logo_app3']['tmp_name']) )
				{
					$newfilename = $this->request->data['cat_logo_app3']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['cat_logo_app3']['tmp_name'], $_FILES['cat_logo_app3']['type'], 'uploads/category_app/1x/', $filename)) 
						{ 	 
							$this->createThumbnail($filename, 'uploads/category_app/1x/', 'uploads/category_app/thumb_app', 90,90); 
							$Category->cat_logo_app3 = $filename;
						}
				}
				*/
				$this->Categories->save($Category);
				$this->Flash->success(__('Category has been saved.'));
                return $this->redirect(['controller'=>'Categories','action' => 'manage']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
      
       $this->loadModel('ParentCategories');
       $this->set('ParentCategories',$this->Categories->find('list', ['keyField' => 'id','valueField' => 'category_name','conditions'=>array('enabled'=>'Y', 'is_deleted'=>'N'),'order'=>array('category_name'=>'ASC')])->toArray());
       $this->set('category', $category);
    }
   
    public function manage(){
		$this->set('title' , $this->project_title.'!: Categories');
		$searchData = array();
		$searchData['AND'][] = array("Categories.is_deleted" => 'N');
		$this->set('Categories',$this->Paginator->paginate(
						$this->Categories, [
						    'contain'=>['ParentCategories'],
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
		
	}
	

	public function search(){
		
		if ($this->request->is('ajax')) {
			
			$searchData = array();
			$searchData['AND'][] = array("Categories.is_deleted" => 'N');
				if(isset($this->request->data['key'])){
					$search = $this->request->data['key'];
					$searchData['OR'][] = array('Categories.category_name LIKE' => '%'.$search.'%');
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
				}
				else {$this->set('serial_num',1);}
			
			$this->set('Categories',$this->Paginator->paginate(
						$this->Categories, [
							'contain'=>['ParentCategories'],
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
			
		}
	}
	
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$category = $this->Categories->get($this->request->data['id']); // Return article with id 12
			$category->enabled = $this->request->data['status'];
			$this->Categories->save($category);
			$this->loadModel('Posts');
			$this->Posts->updateAll(array('enabled'=>$this->request->data['status']), array('category_id'=>$this->request->data['id']));
			echo 1;
		}
		die;
		
		
	}
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			
			$category = $this->Categories->get($this->request->data['id']); // Return article with id 12
			$category->is_deleted = 'Y';
			$category->enabled = 'N';
			$this->Categories->save($category);
			$this->loadModel('Posts');
			$this->Posts->updateAll(array('enabled'=>'N'), array('category_id'=>$this->request->data['id']));
			
			echo 1;
		}
		die;
		
	}
	
	
}
