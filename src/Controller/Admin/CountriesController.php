<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class CountriesController extends AppController
{
	
     public function add()
    {
		
		$this->set('title' , $this->project_title.'!: Add Country');
        $country = $this->Countries->newEntity();
        
        if ($this->request->is(['post' ,'put'])) {
			
			
			$this->request->data['iso_code'] = strtoupper($this->request->data['iso_code']);
			$country = $this->Countries->patchEntity($country, $this->request->data);
			
            if ($newNetwork = $this->Countries->save($country)) {
				$this->Flash->success(__('Country detail has been saved.'));
                return $this->redirect(['controller'=>'Countries','action' => 'add']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
       
        $this->set('country', $country);
    }
	
    public function manage(){
		
		$this->set('title' ,  $this->project_title.'!: Countries');
		$searchData = array();
		$searchData['AND'][] = array('is_deleted' => 'N');
		$this->set('Countries',$this->Paginator->paginate(
						$this->Countries, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
		
	
	}
	
	
	public function search(){
		
		if ($this->request->is('ajax')) {
			
			$searchData = array();
			$searchData['AND'][] = array('is_deleted' => 'N');
		if(isset($this->request->data['key']) && $this->request->data['key'] != ''){
			$search = $this->request->data['key'];
			$searchData['OR'][] = array('name LIKE' => '%'.$search.'%');
			$this->set('key',$this->request->data['key']);
		}
		
		if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
		}
		else {
			$this->set('serial_num',1);
		}
		
			$this->set('Countries',$this->Paginator->paginate(
						$this->Countries, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
		}
	}
	
	public function edit($id = null)
    {
		$this->set('title' ,  $this->project_title.'!: Edit Country detail');
        $country = $this->Countries->get($id);
        if ($this->request->is(['post' ,'put'])) {
			$this->request->data['iso_code'] = strtoupper($this->request->data['iso_code']);
			
			$country = $this->Countries->patchEntity($country, $this->request->data);
			
						
			if ($this->Countries->save($country)) {
				
				$this->Flash->success(__('Country detail has been saved.'));
                return $this->redirect(['controller'=>'Countries','action' => 'manage']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
      
        $this->set('country', $country);
    }
	public function status(){
		if ($this->request->is('ajax')) { 
			$country = $this->Countries->get($this->request->data['id']); // Return article with id 12
			$country->enabled = $this->request->data['status'];
			$this->Countries->save($country);
			echo 1;
		}
		die;
		
		
	}
	public function delete(){
		
		if ($this->request->is('ajax')) { 
			
			$country = $this->Countries->get($this->request->data['id']); // Return article with id 12
			$country->is_deleted = 'Y';
			$country->enabled = 'N';
			$this->Countries->save($country);
			
			echo 1;
		}
		die;
		
	}
	

	
}
