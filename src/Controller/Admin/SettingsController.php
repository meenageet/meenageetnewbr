<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class SettingsController extends AppController
{
	
    
	// Image Module
    public function manage()
    {
		
		$this->set('title' , 'Ipix Service Provider!: Setting');
		$searchData = array();
		$this->set('Settings',$this->Paginator->paginate(
			$this->Settings, [
				
				'order'=>['id'=>'desc'],
				'conditions' => $searchData
			])
		);
		
	}
	
	
	
	
	public function search(){
		if ($this->request->is('ajax')) {
			$searchData = array();
				if(isset($this->request->data['key'])){
					$search = $this->request->data['key'];
					$searchData['OR'][] = array('module_name LIKE' => '%'.$search.'%');
					$searchData['OR'][] = array('minimum_limit LIKE' => '%'.$search.'%');
					$searchData['OR'][] = array('resize1 LIKE' => '%'.$search.'%');
					$searchData['OR'][] = array('resize2 LIKE' => '%'.$search.'%');
					$searchData['OR'][] = array('resize3 LIKE' => '%'.$search.'%');
								
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
				}
				else {$this->set('serial_num',1);}
			$this->set('Settings',$this->Paginator->paginate(
				$this->Settings, [
					//'limit' => 10,
					'order'=>['id'=>'desc'],
					'conditions' => $searchData
				])
			);
			
			
		}
	}
	
	
	public function edit($id = null)
    {
		$this->set('title' , 'Ipix Service Provider!: Edit setting');
        $setting = $this->Settings->get($id);
       
        if ($this->request->is(['post' ,'put'])) {
			
			$setting = $this->Settings->patchEntity($setting, $this->request->data);
			//pr($setting);				
			if ($this->Settings->save($setting)) {
				$this->Flash->success(__('setting detail has been saved.'));
                return $this->redirect(['controller'=>'settings','action' => 'manage']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
        $this->set('setting', $setting);
    }
	
	// Contact us
	public function contact()
	{
		$this->set('title' , 'Ipix Service Provider!: contact us setting');
		
		$this->loadModel('ContactUs');
		$ContactUs = $this->ContactUs->newEntity();
		if ($this->request->is(['post' ,'put'])) {
			
			$k=0;
			foreach($this->request->data['phone']['value'] as $val)
			{
				if($val !='')
				{
					$addr= array();
					$addr['type'] = 'contact';
					if(isset($this->request->data['phone']['id'][$k])){
						$addr['id'] = $this->request->data['phone']['id'][$k];
					}
					$addr['value'] = $val;
					$contact = $this->ContactUs->newEntity();
					$contact = $this->ContactUs->patchEntity($contact, $addr);
					$this->ContactUs->save($contact);
				}
				$k++;
			}
			$l=0;
			foreach($this->request->data['email']['value'] as $val)
			{
				if($val !='')
				{
					$addr= array();
					$addr['type'] = 'email';
					if(isset($this->request->data['email']['id'][$l])){
						$addr['id'] = $this->request->data['email']['id'][$l];
					}
					$addr['value'] = $val;
					$contact = $this->ContactUs->newEntity();
					$contact = $this->ContactUs->patchEntity($contact, $addr);
					$this->ContactUs->save($contact);
				}
				$l++;
			}
		
		}
			
		//die;
		
		
		$email = $this->ContactUs->find('all')
		->where(['type'=>'email'])->hydrate(false)->toArray();
		$phone = $this->ContactUs->find('all')
		->where(['type'=>'contact'])->hydrate(false)->toArray();
		$this->set('email',$email);
		$this->set('contact',$phone);
		$this->set('contact_us',$ContactUs);
				
	}
	
	
	
	
	
}
