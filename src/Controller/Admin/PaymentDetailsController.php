<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class PaymentDetailsController extends AppController
{
	
	public function detail($id= null)
	{
		$this->set('title' , $this->project_title.'!: View User Rewards');
		$data= $this->UserRewards->find('all',['contain'=>['user'],'conditions' =>['UserRewards.id'=>$id,'user.enabled'=>'Y','UserRewards.is_deleted'=>'N']])->hydrate(false)->first();
		$this->set('data',$data);
		
	}
   
    public function manage()
    {
		$this->set('title' , $this->project_title.'!: User Payment Details');
		
		$searchData = array();
		$searchData['AND'][] = array("user.enabled"=>'Y',"PaymentDetails.is_deleted" => 'N');
				
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
            if($search['payment_status'] == 'pending') $searchData['AND'][] =array('PaymentDetails.status' => 'P');
            if($search['payment_status'] == 'complete') $searchData['AND'][] =array('PaymentDetails.status' => 'C');
            if($search['payment_type'] == 'paytm') $searchData['AND'][] =array('PaymentDetails.type' => 'P');
            if($search['payment_type'] == 'upi') $searchData['AND'][] =array('PaymentDetails.type' => 'U');
            if($search['payment_type'] == 'bank') $searchData['AND'][] =array('PaymentDetails.type' => 'B');
                        
		}
		//pr($searchData);die;
		
		$this->set('PaymentDetails',$this->Paginator->paginate(
			$this->PaymentDetails, [
				'contain' => ['user'],
				'limit' => $this->pagination_limit,
				'order' => array('PaymentDetails.id'=>'DESC'),
				'conditions' => $searchData,
			])
		);
		
		$this->set('users',$this->PaymentDetails->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
            return $row['user']['full_name'];
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','PaymentDetails.is_deleted'=>'N'),'group'=>array('PaymentDetails.user_id'),'order' =>array('user.full_name')])->toArray());
		
	}
	
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$this->loadModel('Users');
			$post = $this->PaymentDetails->get($this->request->data['id']); // Return article with id 12
			$post->status = $this->request->data['status'];
			$this->PaymentDetails->save($post);
			$userData = $this->Users->find('all',array('fields'=>array('phone_number','device_type','device_token'),'conditions'=>array('id'=>$post->user_id)))->hydrate(false)->first();	
			if($this->request->data['status'] == 'C'){
				$message = 'Your payment '.$post->point.'Rs. has been done successfully.';
				$this->sendPushNotificationPayment('payment_noti', $post->user_id, $message);
				$this->getOtpMobileAdmin($userData['phone_number'],$message);
			}else{
				$message = 'Now your payment '.$post->point.'Rs. are pending.';
				$this->sendPushNotificationPayment('payment_noti', $post->user_id, $message);
				$this->getOtpMobileAdmin($userData['phone_number'],$message);
			}
			echo 1;
		}
		die;
	}
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			$post = $this->PaymentDetails->get($this->request->data['id']); // Return article with id 12
			$post->is_deleted = 'Y';
			$this->PaymentDetails->save($post);
			echo 1;
		}
		die;
		
	}
	
}
