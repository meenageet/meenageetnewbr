<?php
namespace App\Controller\Admin;
use App\Controller\AppController; // HAVE TO USE App\Controller\AppController
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class TransactionsController extends AppController
{
	
	public function detail($id= null)
	{
		$this->set('title' , $this->project_title.'!: View Challenge');
	}
   
    public function manage()
    {
		$this->set('title' , $this->project_title.'!: All Transactions');
		
		$searchData = array();
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['user_id'] != '') $searchData['AND'][] = array('user.id' => $search['user_id']);
            if($search['user_status'] == 'c') $searchData['AND'][] = array('Transactions.type' => 'C');
			if($search['user_status'] == 'u') $searchData['AND'][] = array('Transactions.type' => 'U');    
			if($search['payment_type'] == 's') $searchData['AND'][] = array('Transactions.status' => 'S');    
			if($search['payment_type'] == 'f') $searchData['AND'][] = array('Transactions.status' => 'F');  
			if($search['from_date'] != '') $searchData['AND'][] = array('date(Transactions.created) >=' => $search['from_date']);
			if($search['to_date'] != '') $searchData['AND'][] = array('date(Transactions.created) <=' => $search['to_date']);  
			$transactions = $this->Transactions->find('all')->select()
								->contain([
									'user','challenge'
								])
								->where($searchData)
								->order(['Transactions.id'=>'desc'])
								->hydrate(false)->toArray();
			if($search['export_type']=='c'){
                    $filename = 'export.csv';
                    $file = fopen(WWW_ROOT."uploads/".$filename,"w");
                    $headers = array('S.No.','User Name','Coins','Order Id','Transaction Id','Type','Amount','Wallet','Date');
                    fputcsv($file,$headers);
                    $k=1;
                    foreach($transactions as $data){
                        $arr = array();
                        $arr['S.No.'] = $k;
                        $arr['User'] = $data['user']['full_name'];
                        $arr['orderid'] = $data['orderid'];
                        $arr['Transaction_id'] =($data['txnid']!='')?$data['txnid']:'-';
                        $arr['type'] =($data['type']!='C')? 'Pending':'Challenge';
                        $arr['Amount'] = $data['txnamount'];
                        $arr['Wallet'] = $data['gatewayname'];
                        $arr['Date'] = $data['txndate']->format('d M Y');
                        fputcsv($file,$arr);
                        $k++;
                    }
                    fclose($file);
                    $this->response->file("uploads/".$filename, array(
                        'download' => true,
                        'name' => 'Report'.time().$filename
                    ));
                }
		}
		$this->set('Transactions',$this->Paginator->paginate(
						$this->Transactions, [
						    'contain'=>['user','challenge'],
							'limit' => $this->pagination_limit,
							'order'=>['Transactions.id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
		$this->set('users',$this->Transactions->find('list', ['keyField' => function ($row) {
            return $row['user']['id'];
		 },'valueField' => function ($row) {
            return $row['user']['full_name'];
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y'),'group'=>array('Transactions.user_id'),'order' =>array('user.full_name')])->toArray());
		
		
	}
	
	
	
	public function search(){
		if ($this->request->is('ajax')) {
			$searchData = array();
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				if($search['user_id'] != '') $searchData['AND'][] = array('user.id' => $search['user_id']);
				if($search['user_status'] == 'c') $searchData['AND'][] = array('Transactions.type' => 'C');
				if($search['user_status'] == 'u') $searchData['AND'][] = array('Transactions.type' => 'U');    
				if($search['payment_type'] == 's') $searchData['AND'][] = array('Transactions.status' => 'S');    
				if($search['payment_type'] == 'f') $searchData['AND'][] = array('Transactions.status' => 'F');    
			}
			//pr($searchData);die;
			
			
			$this->set('Transactions',$this->Paginator->paginate(
							$this->Transactions, [
								'contain'=>['user','challenge'],
								'limit' => $this->pagination_limit,
								'order'=>['Transactions.id'=>'desc'],
								'conditions'=>$searchData,
							])
					);
			
			$this->set('users',$this->Transactions->find('list', ['keyField' => function ($row) {
				return $row['user']['id'];
			 },'valueField' => function ($row) {
				return $row['user']['full_name'];
			 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y'),'group'=>array('Transactions.user_id'),'order' =>array('user.full_name')])->toArray());
			
			

		}
	}
	
	
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$challenge = $this->Challenges->get($this->request->data['id']); // Return article with id 12
			$challenge->enabled = $this->request->data['status'];
			$this->Challenges->save($challenge);
			echo 1;
		}
		die;
	}
	
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			$challenge = $this->Challenges->get($this->request->data['id']); // Return article with id 12
			$challenge->is_deleted = 'Y';
			$challenge->enabled = 'N';
			$this->Challenges->save($challenge);
			echo 1;
		}
		die;
		
	}
	
	
	
}
