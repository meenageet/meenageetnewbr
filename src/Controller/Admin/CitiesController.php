<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class CitiesController extends AppController
{
	public $paginate=['limit'=>5];

	public function initialize(){
        parent::initialize();
    }

	
	public function index(){
		$this->set('title' , 'Task Chat!: Cities');
		$searchData = array();
		$cities=$this->Cities->find('all', ['order'=>'Cities.id desc'])
					->contain(['States'=>['Countries']])
					->where(['Cities.deleted'=>'N']);
		$data=$cities->toArray();
		$this->set('Cities',$cities);		
	}
	
    public function add(){
		$this->set('title' , 'Task Chat!: Add city');
        $city= $this->Cities->newEntity();     
        if ($this->request->is(['post' ,'put'])) {	 
		   $this->request->data['slug'] = trim($this->slugify($this->request->data['city_name']));       
           $city = $this->Cities->patchEntity($city, $this->request->data);               
           if ($newNetwork = $this->Cities->save($city)) {				
				$this->Flash->success(__('city detail has been saved.'));
                return $this->redirect(['controller'=>'Cities','action' => 'index']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }       

        $this->loadModel('Countries');
        $this->set('country',$this->Countries->find('list', ['keyField' => 'id','valueField' => 'country_name'])->toArray());
		$this->set('city', $city);
    }

    public function edit($id = null)
    {
		$this->set('title' , 'Task Chat!: Edit city');
		$city=$this->Cities->find("all")->where(['Cities.id'=>$id])->contain(['States'=>['Countries']])->first();
        if ($this->request->is(['post' ,'put'])) {	
			 $this->request->data['slug'] = trim($this->slugify($this->request->data['city_name'])); 	        
        	//$stateid['state_id']=$this->request->data['state_id']; 
        	//echo '<pre>';	print_r($this->request->data()); die;	        
			$city = $this->Cities->patchEntity($city, $this->request->data());									
			if ($this->Cities->save($city)) {
				 $this->Flash->success(__('city has been saved.'));
                return $this->redirect(['controller'=>'Cities','action' => 'index']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }        
        $this->loadModel('States');
        $this->loadModel('Countries');
        $this->set('country',$this->Countries->find('list', ['keyField' => 'id','valueField' => 'country_name'])->toArray());
        $states=$this->States->find('list', ['keyField' => 'id','valueField' => 'state_name'])->where(['States.country_id'=>$city['state']['country']['id']]);
        $this->set('state',$states);  
               
        $this->set('city', $city);
    }
	
    
	
	public function search(){
		if ($this->request->is('ajax')) {
			$searchData = array();
				if(isset($this->request->data['key'])){
					$search = $this->request->data['key'];
					$searchData['OR'][] = array('city_name LIKE' => '%'.$search.'%');
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
				}
				else {$this->set('serial_num',1);}
			$this->set('Cities',$this->Paginator->paginate(
				$this->Cities, [
					'limit' => $this->pagination_limit,
					'order'=>['id'=>'desc'],
					'conditions' => $searchData,
					'contain'=>array('States'),	
				])
			);
			
			
		}
	}
	
	
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$buscatsubattr = $this->Cities->get($this->request->data['id']); // Return article with id 12
			$buscatsubattr->enabled = $this->request->data['status'];
			$this->Cities->save($buscatsubattr);
			echo 1;
		}
		die;		
	}
	
	public function popularStatus(){
		if ($this->request->is('ajax')) { 
			$buscatsubattr = $this->Cities->get($this->request->data['id']); // Return article with id 12
			$buscatsubattr->popular_status = $this->request->data['status'];
			$this->Cities->save($buscatsubattr);
			echo 1;
		}
		die;		
	}
	
	public function delete(){
		if ($this->request->is('ajax')) { 			
			$query = $this->Cities->query();
			$query->update()
			->set(['deleted'=>'Y'])
			->where(['id' => $this->request->data['id']])
			->execute();
			echo 1;
		}
		die;		
	}
	
	public function import(){
		$card =  $this->Cities->newEntity();
		if($this->request->is(['post','put'])){
			
			$card = $this->Cities->patchEntity($card,$this->request->data);
			
			if(!$card->errors()){
				if (move_uploaded_file($_FILES['import']['tmp_name'], 'uploads/csv/' . $_FILES["import"]['name'])) {
						
					$msg = 	$this->read($_FILES["import"]['name']);
					if(!empty($msg)){
							if(array_key_exists("1",$msg)){
								$this->Flash->error(__($msg[1]));
							}else{
								foreach($msg as $k=>$m){
									foreach($m as $n){
										$this->Flash->error(__($n ." at row no. ".$k));
									}
								}
							}
							return $this->redirect(['controller'=>'Cities','action' => 'manage']);
					}
					else
					{
						$sucess = $this->upload($_FILES["import"]['name']);
						if($sucess==1){
							$this->Flash->success(__('Data has been saved.'));
						}
						
						return $this->redirect(['controller'=>'Cities','action' => 'manage']);
					}
					} 
			}else{
				$this->Flash->error(__('Invalid csv file'));
				return $this->redirect(['controller'=>'Cities','action' => 'manage']);
			}
			
		}
		die;
		
	}	
	
	public function upload($filename)
	{
		
		$file = fopen("uploads/csv/".$filename."","r");
		$i=1;
		$success=0;
		while(! feof($file))
		{
			$data = fgetcsv($file);
			if($i!=1)
			{
				if($data[0] != '')
				{
					$rec= array();							
					$rec['state_id'] = intval($data[0]);
					$rec['city_name'] = $data[1];
					$rec['latitude'] = $data[2];
					$rec['longitude'] = $data[3];
					
					$cate = $this->Cities->newEntity();
					$cate  = $this->Cities->patchEntity($cate,$rec);
					
				
					if ($newNetwork = $this->Cities->save($cate)) {
						
						$success=1;
							
						
					}
					
					
				}
			}
		$i++;
		}
		return $success;
	}
	public function read($filename)
	{
		
		$file = fopen("uploads/csv/".$filename."","r");
		$i = 1;
		$return_arr =  array();
		$this->loadModel('States');
		while(! feof($file))
		  {
			 $data = fgetcsv($file);
			 if($i!=1){
				if($data[0] != '' ) 
				{ 

					$query = $this->States->find()->select(['id'])->where(['id =' => $data[0]])->first();	
					if(empty($query)){
						$return_arr[$i][] = "State id does not exist";
					}
					$rec= array();							
					
					$rec['state_id'] = intval($data[0]);
					$rec['city_name'] = $data[1];
					$rec['latitude'] = $data[2];
					$rec['longitude'] = $data[3];
				
					$cate = $this->Cities->newEntity();
					$cate  = $this->Cities->patchEntity($cate,$rec);
					
				
					if($cate->errors()){
						
						foreach($cate->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$return_arr[$i][] =  $error_text;
							} 
							
						}
					
					}
				
				}
		 
			}else{
				
				if($data[0] !='state_id' || trim($data[1]) !='city_name'  || trim($data[2]) !='latitude' || trim($data[3]) !='longitude' ){
					
					$return_arr[$i] = "Required fields are state_id, city_name, latitude, longitude";
					break;
				}
		
			}
		$i++;
		
		}
		return $return_arr;
			fclose($file);
		die;
	}
	
	
	
	public function export(){
			$filename = 'export.csv';
			$file = fopen("uploads/csv/".$filename,"w");
			$headers = array('id','State','City name','enabled');
			fputcsv($file,$headers);
			$query = $this->Cities->find('all',array('contain'=>array('States'),'fields'=>array('Cities.id','States.state_name','Cities.city_name','Cities.enabled'),'order'=>array('States.state_name asc','Cities.city_name asc')))->hydrate(false);
			$insert_arr=  array();
			foreach($query->toArray() as $record){
				$insert_arr =array();
				$insert_arr[] = $record['id'];
				$insert_arr[] = $record['state']['state_name'];
				$insert_arr[] = $record['city_name'];
				$insert_arr[] = $record['enabled'];
				fputcsv($file,$insert_arr);
			}
			fclose($file);
			$this->response->file("uploads/csv/".$filename, array(
			 'download' => true,
			 'name' => 'Cities'.date('Y-m-d H:i').'.csv'
			)); 
			return $this->response;die;
		
	}
	
	public function demo(){
			$filename = 'subattirbutes.csv';
			$this->response->file("uploads/import/".$filename, array(
			 'download' => true,
			 'name' => $filename
			)); 
			return $this->response;
			die;
		
	}
	
	/**
     * Get State.
     *
     * @return mixed
     */
    public function getState () 
	{
		if ($this->request->is('ajax')) { 
			$this->loadModel('States');
			$id = $this->request->data['getId'];
			$statecount = $this->States->find('all',['conditions' =>['country_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->count();
			$statedata = $this->States->find('all',['conditions' =>['country_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->all();
			//print_r($statedata);die;
			$option = "";
			$option .= "<option value=''>Select State</option>";
			if ($statecount > 0) {
				
				foreach ($statedata as $stateval) {
					$option .= "<option value='" . $stateval['id'] . "'>" . $stateval['state_name'] . "</option>";
				}
			}
			echo  $option; die;
		}
	}
}
