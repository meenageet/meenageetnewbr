<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;

class SongUploadsController extends AppController
{
				
	public function detail($id= null)
	{
		$this->set('title' , $this->project_title.'!: View Post');
		$data= $this->SongUploads->find('all',['contain'=>['user','category','subcategory','likes'],'conditions' =>['SongUploads.id'=>$id,'user.enabled'=>'Y','SongUploads.is_deleted'=>'N']])->hydrate(false)->first();
		$this->set('data',$data);
		
	}
   
    public function manage()
    {
		$this->set('title' , $this->project_title.'!: Songs');
		
		$searchData = array();
		$searchData['AND'][] = array("user.enabled"=>'Y',"SongUploads.is_deleted" => 'N');
		$orderData = array();
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['title'] != '') $searchData['AND'][] = array('title LIKE' => '%'.$search['title'].'%');
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
            
            if($search['sort_type'] == 'like_desc') {
				$orderData = array('SongUploads.total_likes'=>'DESC');
			}
            if($search['sort_type'] == 'like_asc') {
				$orderData = array('SongUploads.total_likes'=>'ASC');
			} 
			if($search['sort_type'] == '') {
				$orderData = array('SongUploads.id'=>'DESC');
			}
			
			if($search['sort_type'] == 'song_desc') {
				$orderData = array('SongUploads.id'=>'DESC');
			}
            if($search['sort_type'] == 'song_asc') {
				$orderData = array('SongUploads.id'=>'ASC');
			} 
		}else{
			$orderData = array('SongUploads.id'=>'DESC');
		}
		//pr($searchData);die;
		
		$this->loadModel('Likes');
		$this->set('SongUploads',$this->Paginator->paginate(
			$this->SongUploads, [
				'contain' => ['user','category','subcategory'],
				'limit' => $this->pagination_limit,
				'order' => $orderData,
				'conditions' => $searchData,
			])
		);
		
		$this->set('users',$this->SongUploads->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
			if($row['user']['full_name'] !=''){ return $row['user']['full_name']; }else{ return $row['user']['phone_number']; }
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','SongUploads.is_deleted'=>'N'),'group'=>array('SongUploads.user_id'),'order' =>array('user.full_name')])->toArray());

		
	}
	
	public function search(){
		if ($this->request->is('ajax')) {
			$searchData = array();
			$searchData['AND'][] = array("user.enabled"=>'Y',"SongUploads.is_deleted" => 'N');
			$orderData = array();
				if ($this->request->is(['post' ,'put']) ) 
				{
					if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
					$search = $this->request->data;
					if($search['title'] != '') $searchData['AND'][] = array('title LIKE' => '%'.$search['title'].'%');
					if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
					
					if($search['sort_type'] == 'like_desc') {
						$orderData = array('SongUploads.total_likes'=>'DESC');
					}
					if($search['sort_type'] == 'like_asc') {
						$orderData = array('SongUploads.total_likes'=>'ASC');
					} 
					if($search['sort_type'] == '') {
						$orderData = array('SongUploads.id'=>'DESC');
					}
					
					if($search['sort_type'] == 'song_desc') {
						$orderData = array('SongUploads.id'=>'DESC');
					}
					if($search['sort_type'] == 'song_asc') {
						$orderData = array('SongUploads.id'=>'ASC');
					} 
				}else{
					$orderData = array('SongUploads.id'=>'DESC');
				}
				//pr($searchData);die;
				
				$this->loadModel('Likes');
				$this->set('SongUploads',$this->Paginator->paginate(
					$this->SongUploads, [
						'contain' => ['user','category','subcategory'],
						'limit' => $this->pagination_limit,
						'order' => $orderData,
						'conditions' => $searchData,
					])
				);
				
				$this->set('users',$this->SongUploads->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
					if($row['user']['full_name'] !=''){ return $row['user']['full_name']; }else{ return $row['user']['phone_number']; }
				 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','SongUploads.is_deleted'=>'N'),'group'=>array('SongUploads.user_id'),'order' =>array('user.full_name')])->toArray());

				
		}
	}
	
	/****************** singer *************************/
	public function singerManage()
    {
		$this->set('title' , $this->project_title.'!: Songs');
		$this->loadModel('RequestDjSingers');
		$searchData = array();
		$singerlist = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('type' => 'S', 'is_approved' => 'Y', 'is_deleted' => 'N')])->toArray();
		$searchData['AND'][] = array("SongUploads.user_id IN" => $singerlist,"user.enabled"=>'Y',"SongUploads.is_deleted" => 'N');
		$orderData = array();
		//$orderData = array('SongUploads.id'=>'desc');
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['title'] != '') $searchData['AND'][] = array('title LIKE' => '%'.$search['title'].'%');
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
            
            if($search['sort_type'] == 'like_desc') {
				$orderData = array('SongUploads.total_likes'=>'DESC');
			}
            if($search['sort_type'] == 'like_asc') {
				$orderData = array('SongUploads.total_likes'=>'ASC');
			} 
			if($search['sort_type'] == '') {
				$orderData = array('SongUploads.id'=>'DESC');
			}
			
			if($search['sort_type'] == 'song_desc') {
				$orderData = array('SongUploads.id'=>'DESC');
			}
            if($search['sort_type'] == 'song_asc') {
				$orderData = array('SongUploads.id'=>'ASC');
			} 
		}else{
			$orderData = array('SongUploads.id'=>'DESC');
		}
		//pr($searchData);die;
		
		$this->loadModel('Likes');
		$this->set('SongUploads',$this->Paginator->paginate(
			$this->SongUploads, [
				'contain' => ['user','category','subcategory'],
				'limit' => $this->pagination_limit,
				'order' => $orderData,
				'conditions' => $searchData,
			])
		);
		
		$this->set('users',$this->SongUploads->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
           if($row['user']['full_name'] !=''){ return $row['user']['full_name']; }else{ return $row['user']['phone_number']; }
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','SongUploads.is_deleted'=>'N'),'group'=>array('SongUploads.user_id'),'order' =>array('user.full_name')])->toArray());

		
	}
	
	public function searchsinger(){
		if ($this->request->is('ajax')) {
			$searchData = array();
			$this->loadModel('RequestDjSingers');
			$singerlist = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('type' => 'S', 'is_approved' => 'Y', 'is_deleted' => 'N')])->toArray();
			$searchData['AND'][] = array("SongUploads.user_id IN" => $singerlist,"user.enabled"=>'Y',"SongUploads.is_deleted" => 'N');
			$orderData = array();
				if ($this->request->is(['post' ,'put']) ) 
				{
					if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
					$search = $this->request->data;
					if($search['title'] != '') $searchData['AND'][] = array('title LIKE' => '%'.$search['title'].'%');
					if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
					
					if($search['sort_type'] == 'like_desc') {
						$orderData = array('SongUploads.total_likes'=>'DESC');
					}
					if($search['sort_type'] == 'like_asc') {
						$orderData = array('SongUploads.total_likes'=>'ASC');
					} 
					if($search['sort_type'] == '') {
						$orderData = array('SongUploads.id'=>'DESC');
					}
					
					if($search['sort_type'] == 'song_desc') {
						$orderData = array('SongUploads.id'=>'DESC');
					}
					if($search['sort_type'] == 'song_asc') {
						$orderData = array('SongUploads.id'=>'ASC');
					} 
				}else{
					$orderData = array('SongUploads.id'=>'DESC');
				}
				//pr($searchData);die;
				
				$this->loadModel('Likes');
				$this->set('SongUploads',$this->Paginator->paginate(
					$this->SongUploads, [
						'contain' => ['user','category','subcategory'],
						'limit' => $this->pagination_limit,
						'order' => $orderData,
						'conditions' => $searchData,
					])
				);
				
				$this->set('users',$this->SongUploads->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
					if($row['user']['full_name'] !=''){ return $row['user']['full_name']; }else{ return $row['user']['phone_number']; }
				 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','SongUploads.is_deleted'=>'N'),'group'=>array('SongUploads.user_id'),'order' =>array('user.full_name')])->toArray());

				
		}
	}
	
	/****************** dj *************************/
	public function djManage()
    {
		$this->set('title' , $this->project_title.'!: Songs');
		$this->loadModel('RequestDjSingers');
		$searchData = array();
		$djlist = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('type' => 'D', 'is_approved' => 'Y', 'is_deleted' => 'N')])->toArray();
		$searchData['AND'][] = array("SongUploads.user_id IN" => $djlist,"user.enabled"=>'Y',"SongUploads.is_deleted" => 'N');
		$orderData = array();
		//$orderData = array('SongUploads.id'=>'desc');
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['title'] != '') $searchData['AND'][] = array('title LIKE' => '%'.$search['title'].'%');
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
            
            if($search['sort_type'] == 'like_desc') {
				$orderData = array('SongUploads.total_likes'=>'DESC');
			}
            if($search['sort_type'] == 'like_asc') {
				$orderData = array('SongUploads.total_likes'=>'ASC');
			} 
			if($search['sort_type'] == '') {
				$orderData = array('SongUploads.id'=>'DESC');
			}
			
			if($search['sort_type'] == 'song_desc') {
				$orderData = array('SongUploads.id'=>'DESC');
			}
            if($search['sort_type'] == 'song_asc') {
				$orderData = array('SongUploads.id'=>'ASC');
			} 
		}else{
			$orderData = array('SongUploads.id'=>'DESC');
		}
		//pr($searchData);die;
		
		$this->loadModel('Likes');
		$this->set('SongUploads',$this->Paginator->paginate(
			$this->SongUploads, [
				'contain' => ['user','category','subcategory'],
				'limit' => $this->pagination_limit,
				'order' => $orderData,
				'conditions' => $searchData,
			])
		);
		
		$this->set('users',$this->SongUploads->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
            if($row['user']['full_name'] !=''){ return $row['user']['full_name']; }else{ return $row['user']['phone_number']; }
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','SongUploads.is_deleted'=>'N'),'group'=>array('SongUploads.user_id'),'order' =>array('user.full_name')])->toArray());

		
	}
	
	public function searchdj(){
		if ($this->request->is('ajax')) {
			$this->loadModel('RequestDjSingers');
			$searchData = array();
			$djlist = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('type' => 'D', 'is_approved' => 'Y', 'is_deleted' => 'N')])->toArray();
			$searchData['AND'][] = array("SongUploads.user_id IN" => $djlist,"user.enabled"=>'Y',"SongUploads.is_deleted" => 'N');			
			$orderData = array();
				if ($this->request->is(['post' ,'put']) ) 
				{
					if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
					$search = $this->request->data;
					if($search['title'] != '') $searchData['AND'][] = array('title LIKE' => '%'.$search['title'].'%');
					if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
					
					if($search['sort_type'] == 'like_desc') {
						$orderData = array('SongUploads.total_likes'=>'DESC');
					}
					if($search['sort_type'] == 'like_asc') {
						$orderData = array('SongUploads.total_likes'=>'ASC');
					} 
					if($search['sort_type'] == '') {
						$orderData = array('SongUploads.id'=>'DESC');
					}
					
					if($search['sort_type'] == 'song_desc') {
						$orderData = array('SongUploads.id'=>'DESC');
					}
					if($search['sort_type'] == 'song_asc') {
						$orderData = array('SongUploads.id'=>'ASC');
					} 
				}else{
					$orderData = array('SongUploads.id'=>'DESC');
				}
				//pr($searchData);die;
				
				$this->loadModel('Likes');
				$this->set('SongUploads',$this->Paginator->paginate(
					$this->SongUploads, [
						'contain' => ['user','category','subcategory'],
						'limit' => $this->pagination_limit,
						'order' => $orderData,
						'conditions' => $searchData,
					])
				);
				
				$this->set('users',$this->SongUploads->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
					if($row['user']['full_name'] !=''){ return $row['user']['full_name']; }else{ return $row['user']['phone_number']; }
				 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','SongUploads.is_deleted'=>'N'),'group'=>array('SongUploads.user_id'),'order' =>array('user.full_name')])->toArray());

				
		}
	}
	
	
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$post = $this->SongUploads->get($this->request->data['id']); // Return article with id 12
			$post->enabled = $this->request->data['status'];
			$this->SongUploads->save($post);
			echo 1;
		}
		die;
		
		
	}
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			$post = $this->SongUploads->get($this->request->data['id']); // Return article with id 12
			$post->is_deleted = 'Y';
			$post->enabled = 'N';
			$this->SongUploads->save($post);
			echo 1;
		}
		die;
		
	}
	public function deletelikes()
	{
		$this->loadModel('Likes');
		if ($this->request->is('ajax')) { 
			$post = $this->Likes->get($this->request->data['id']); // Return article with id 12
			$this->Likes->delete($post);
			echo 1;
		}
		die;
		
	}
	public function deletecomment()
	{
		$this->loadModel('Comments');
		if ($this->request->is('ajax')) { 
			$post = $this->Comments->get($this->request->data['id']); // Return article with id 12
			$this->Comments->delete($post);
			echo 1;
		}
		die;
		
	}
	
	
	public function like()
    {
		$this->set('title' , $this->project_title.'!: Likes');
		
		$this->loadModel('Likes');
		$this->layout ='/song-uploads/like';
		
		//$this->viewBuilder()->layout('song-uploads/like');
		
		$searchData = array();
		$searchData['AND'][] =array('Likes.status' => '1','Likes.is_deleted' => 'N');
		if ($this->request->is(['post' ,'put'])) 
		{
			
			$search = $this->request->data;
			
			if($search['title'] != '') $searchData['OR'][] =array('SongUploads.title LIKE' => '%'.$search['title'].'%');
			if($search['title'] != '') $searchData['OR'][] =array('Users.full_name LIKE' => '%'.$search['title'].'%');
			if($search['like_type'] == 'profile') $searchData['AND'][] =array('Likes.profile_id !=' => '0');
			if($search['like_type'] == 'song') $searchData['AND'][] =array('Likes.song_id !=' => '0');
            
		}
		 //print_r($searchData);die;
		$this->set('datalike',$this->Paginator->paginate(
			$this->Likes, [
			   'contain'=>['Users','SongUploads','Profiles'],
				'limit' => $this->pagination_limit,
				'order'=>['id'=>'desc'],
				'conditions'=>$searchData,
			])
		);
		
	}
	
	public function searchlike(){
		if ($this->request->is('ajax')) {
			$this->loadModel('Likes');			
			$searchData = array();
			$searchData['AND'][] =array('Likes.status' => '1','Likes.is_deleted' => 'N');
			
			if ($this->request->is(['post' ,'put'])) 
			{
				
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				
				if($search['title'] != '') $searchData['OR'][] =array('SongUploads.title LIKE' => '%'.$search['title'].'%');
				if($search['title'] != '') $searchData['OR'][] =array('Users.full_name LIKE' => '%'.$search['title'].'%');
				if($search['like_type'] == 'profile') $searchData['AND'][] =array('Likes.profile_id !=' => '0');
				if($search['like_type'] == 'song') $searchData['AND'][] =array('Likes.song_id !=' => '0');
				
			}
			
			$this->set('datalike',$this->Paginator->paginate(
				$this->Likes, [
				   'contain'=>['Users','SongUploads','Profiles'],
					'limit' => $this->pagination_limit,
					'order'=>['id'=>'desc'],
					'conditions'=>$searchData,
				])
			);
			
		}
	}
	
	public function unlike()
    {
		$this->set('title' , $this->project_title.'!: Likes');
		
		$this->loadModel('Likes');
		$this->layout ='/song-uploads/like';
		
		//$this->viewBuilder()->layout('song-uploads/like');
		
		$searchData = array();
		$searchData['AND'][] =array('Likes.status' => '0','Likes.is_deleted' => 'N');
		if ($this->request->is(['post' ,'put'])) 
		{
			
			$search = $this->request->data;
			
			if($search['title'] != '') $searchData['OR'][] =array('SongUploads.title LIKE' => '%'.$search['title'].'%');
			if($search['title'] != '') $searchData['OR'][] =array('Users.full_name LIKE' => '%'.$search['title'].'%');
			if($search['like_type'] == 'profile') $searchData['AND'][] =array('Likes.profile_id !=' => '0');
			if($search['like_type'] == 'song') $searchData['AND'][] =array('Likes.song_id !=' => '0');
            
		}
		// pr($searchData);die;
		$this->set('datalike',$this->Paginator->paginate(
			$this->Likes, [
			   'contain'=>['Users','SongUploads','Profiles'],
				'limit' => $this->pagination_limit,
				'order'=>['id'=>'desc'],
				'conditions'=>$searchData,
			])
		);
		
	}
	
	public function searchunlike(){
		if ($this->request->is('ajax')) {
			$this->loadModel('Likes');			
			$searchData = array();
			$searchData['AND'][] =array('Likes.status' => '0','Likes.is_deleted' => 'N');
			if ($this->request->is(['post' ,'put'])) 
			{
				
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
			
				if($search['title'] != '') $searchData['OR'][] =array('SongUploads.title LIKE' => '%'.$search['title'].'%');
				if($search['title'] != '') $searchData['OR'][] =array('Users.full_name LIKE' => '%'.$search['title'].'%');
				if($search['like_type'] == 'profile') $searchData['AND'][] =array('Likes.profile_id !=' => '0');
				if($search['like_type'] == 'song') $searchData['AND'][] =array('Likes.song_id !=' => '0');
				
			}
			// pr($searchData);die;
			$this->set('datalike',$this->Paginator->paginate(
				$this->Likes, [
				   'contain'=>['Users','SongUploads','Profiles'],
					'limit' => $this->pagination_limit,
					'order'=>['id'=>'desc'],
					'conditions'=>$searchData,
				])
			);
			
		}
	}
	
	public function comments($id= null)
    {
		$this->set('title' , $this->project_title.'!: Comments');
		
		$this->loadModel('Comments');
		$this->layout ='/song-uploads/comments';
		
		$searchData = array();
		$searchData['AND'][] = array("Comments.is_deleted" => 'N');
		if(!empty($id)) {
			$searchData['AND'][] = array("Comments.song_id" => $id);
		}
		if ($this->request->is(['post' ,'put']) ) 
		{
			$search = $this->request->data;
			if($search['title'] != '') $searchData['OR'][] =array('SongUploads.title LIKE' => '%'.$search['title'].'%');
			if($search['title'] != '') $searchData['OR'][] =array('Users.full_name LIKE' => '%'.$search['title'].'%');
			if($search['user_id'] != '') $searchData['AND'][] = array('Users.id' => $search['user_id']);
			       
		}
		//pr($searchData);die;
		$this->set('datalike',$this->Paginator->paginate(
			$this->Comments, [
			   'contain'=>['Users','SongUploads'],
				'limit' => $this->pagination_limit,
				'order'=>['id'=>'desc'],
				'conditions'=>$searchData,
			])
		);	
		
		$this->set('users',$this->Comments->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
           if($row['user']['full_name'] !=''){ return $row['user']['full_name']; }else{ return $row['user']['phone_number']; }
		 },'contain'=>array('Users'),'conditions'=>array('Users.enabled'=>'Y','Comments.is_deleted'=>'N'),'group'=>array('Comments.user_id'),'order' =>array('Users.full_name')])->toArray());
		
	}
	
	
	public function add()
    {
		$this->set('title' , $this->project_title.'!: Add Song');
       $songUploaddats = $this->SongUploads->newEntity();
        
        if ($this->request->is(['post' ,'put'])) {
			
			if(!empty($this->request->data['picture_id'])){
			$imageData = $this->request->data['picture_id'];
			list($type, $imageData) = explode(';', $imageData);
			list(,$extension) = explode('/',$type);
			list(,$imageData)      = explode(',', $imageData);
			$fileName = uniqid().'.'.$extension;
			$imageData = base64_decode($imageData);
			file_put_contents(path_song_image_custom.$fileName, $imageData);
			
			$aws_upload_img = path_song_image_custom.$fileName;
			$uploadimg = $this->Aws->bucketUpload($aws_upload_img, $fileName, path_song_image_folder);
			
			unlink($aws_upload_img);
			
			//file_put_contents(path_song_upload_image.$fileName, $imageData);
			$this->request->data['image'] = $fileName;
			//$this->request->data['custom_audio_img'] = 1;
		}
      
			//echo "<pre>";print_r($_FILES);exit;
			/************* before audio file upload then after save file in database*******/
			if(isset($_FILES['audio']))
			{	
				$totalaudio = count($_FILES['audio']['name']);
				$songidArr = '';
				for( $i=0 ; $i < $totalaudio ; $i++ ) {
					$songUpload = $this->SongUploads->newEntity();
					$audio_base_url = path_song_upload_audio;		
					$fileName = $_FILES['audio']['name'][$i];
					$songaudio = $this->uploadFileAudioAdmin($_FILES['audio']['name'][$i],$_FILES['audio']['tmp_name'][$i],'audio');
					if(isset($songaudio) && $songaudio!=''){
						$newname = $songaudio;
						$this->request->data['audio'] = $songaudio;
					}
					$ffmpeg = FFMpeg::create(); 
					
					$ffprobe    = FFProbe::create();
					
					
					
					$video = $ffmpeg->open($audio_base_url.$newname);
					$durationMp3   = $ffprobe->format($audio_base_url.$newname)->get('duration');
					$hours = floor($durationMp3 / 3600);
					$mins = floor($durationMp3 / 60 % 60);
					$secs = floor($durationMp3 % 60);
					$timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
					$this->request->data['duration']  = $timeFormat;
					$audio_size = $this->formatSizeUnits($_FILES['audio']['size'][$i]);
					$this->request->data['size']  = $audio_size;
					$this->request->data['slug'] = trim($this->slugify($this->request->data['title']));
					
					
					
					
					/*************** save code*****************/
					//echo "<pre>";print_r($this->request->data);exit;
					$songUpload = $this->SongUploads->patchEntity($songUpload, $this->request->data,['validate'=>'AddSong']);
					
					//echo "<pre>";print_r($songUpload);exit;
					$before_image = $songUpload->image;
					if ($saved_songUpload = $this->SongUploads->save($songUpload)) {
						$songUpload_id = $saved_songUpload->id;
						$songidArr = $songUpload_id;
						$songUpload = $this->SongUploads->get($songUpload_id);
						$audio = $song_image = '';
						/******************* upload image **************************/
						if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
						{
							$image_base_url = path_song_upload_image;
							$songimages = $this->uploadFile($_FILES['image'],'song');
							
							if(isset($songimages) && $songimages!=''){
								$songUpload['image'] = $songimages;
							}else{
								$songUpload->image = $before_image;
							}
						} else  $songUpload->image = $before_image;
						/******************* end upload image **************************/
						$this->SongUploads->save($songUpload);
					} else {
						$this->Flash->error(__('Some Errors Occurred.'));
					}
				}
				
				//get song Id
				$songDetails = $this->SongUploads->get($songidArr);
				$this->loadModel('NotifyUsers');
				$NotifyUser = $this->NotifyUsers->newEntity();
				$notifyData['user_id'] = $songDetails['user_id'];
				$notifyData['song_id'] = $songDetails['id'];
				$notifyData['type'] = 'song';
				$NotifyUser = $this->NotifyUsers->patchEntity($NotifyUser, $notifyData);
				$this->NotifyUsers->save($NotifyUser);
				
				$this->Flash->success(__('Song detail has been saved.'));
				return $this->redirect(['controller'=>'SongUploads','action' => 'add']);	
			}
        }
        
        $this->loadModel('ParentCategories');
        $this->loadModel('Categories');
        $this->loadModel('RequestDjSingers');
		$this->set('ParentCategories',$this->Categories->find('list', ['keyField' => 'id','valueField' => 'category_name','conditions'=>array('parent_category_id'=>'0','enabled'=>'Y', 'is_deleted'=>'N'),'order'=>array('category_name'=>'ASC')])->toArray());
		
		$this->set('users',$this->RequestDjSingers->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
           if($row['user']['full_name'] !=''){ return $row['user']['full_name']; }else{ return $row['user']['phone_number']; }
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','RequestDjSingers.is_approved'=>'Y','RequestDjSingers.is_deleted'=>'N'),'group'=>array('RequestDjSingers.user_id'),'order' =>array('user.full_name')])->toArray());
        
		$this->set('songUpload', $songUploaddats);
    }
    
    public function edit($id = null)
    {
		$this->set('title' , $this->project_title.'!: Edit Song');
		$this->loadModel('ParentCategories');
        $this->loadModel('Categories');
        $this->loadModel('RequestDjSingers');
		$songUpload = $this->SongUploads->find("all")->contain(['category','subcategory'])->where(['SongUploads.id' => $id])->first();
		$before_image = $songUpload->image;
		$before_audio = $songUpload->audio;
		
		if ($this->request->is(['post' ,'put'])) {
			//echo "<pre>";print_r($this->request->data);exit;
				
            
			$this->request->data['slug'] = trim($this->slugify($this->request->data['title']));
          
			$songUpload = $this->SongUploads->patchEntity($songUpload, $this->request->data,['validate'=>'EditSong']);
			//$before_image = $songUpload->image;
			//$before_audio = $songUpload->audio;
			//echo '<pre>'; print_r($songUpload); die;
			//echo "<pre>";print_r($this->request->data);die;
			
			if(!empty($this->request->data['picture_id'])){
			$imageData = $this->request->data['picture_id'];
			list($type, $imageData) = explode(';', $imageData);
			list(,$extension) = explode('/',$type);
			list(,$imageData)      = explode(',', $imageData);
			$fileName = uniqid().'.'.$extension;
			$imageData = base64_decode($imageData);
			
			file_put_contents(path_song_image_custom.$fileName, $imageData);
			
			$aws_upload_img = path_song_image_custom.$fileName;
			$uploadimg = $this->Aws->bucketUpload($aws_upload_img, $fileName, path_song_image_folder);
			unlink($aws_upload_img);
			$songUpload['image'] = $fileName;
			//$songUpload['custom_audio_img'] = 1;
		}
		
            if ($saved_songUpload = $this->SongUploads->save($songUpload)) {
				$songUpload_id = $saved_songUpload->id;
				$songUpload = $this->SongUploads->get($songUpload_id);
				
				$audio = $song_image = '';
				
				/******************* upload image **************************/
				if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
				{
					$image_base_url = path_song_upload_image;
					$songimages = $this->uploadFile($_FILES['image'],'song');
					
					if(isset($songimages) && $songimages!=''){
						$songUpload['image'] = $songimages;
					}else{
						$songUpload->image = $before_image;
					}
				} else  $songUpload->image = $before_image;
				/******************* end upload image **************************/
				
				/******************* upload audio **************************/
				if(isset($_FILES['audio']) && $_FILES['audio']['tmp_name'] !='')
				{					
					$fileName = $_FILES['audio']['name'];
					$songaudio = $this->uploadFile($_FILES['audio'],'audio');
					
					if(isset($songaudio) && $songaudio!=''){
						$songUpload['audio'] = $songaudio;
					}else{
						$songUpload->audio = $before_audio;
					}
				} else  $songUpload->audio = $before_audio;
				/******************* end upload audio **************************/
				
				$audio_size = $this->formatSizeUnits($_FILES['audio']['size']);
				
				$songUpload->size = $audio_size;
				
				$this->SongUploads->save($songUpload);
				
                $this->Flash->success(__('Song detail has been updated.'));
                return $this->redirect(['action' => 'manage']);
            } else {
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
        
        
		$this->set('ParentCategories',$this->Categories->find('list', ['keyField' => 'id','valueField' => 'category_name','conditions'=>array('parent_category_id'=>'0','enabled'=>'Y', 'is_deleted'=>'N'),'order'=>array('category_name'=>'ASC')])->toArray());
		$subcat = $this->Categories->find('list', ['keyField' => 'id','valueField' => 'category_name'])->where(['Categories.parent_category_id'=>$songUpload['category']['id']]);
        $this->set('subcat',$subcat);  
		
		
		$this->set('users',$this->RequestDjSingers->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
           if($row['user']['full_name'] !=''){ return $row['user']['full_name']; }else{ return $row['user']['phone_number']; }
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','RequestDjSingers.is_approved'=>'Y','RequestDjSingers.is_deleted'=>'N'),'group'=>array('RequestDjSingers.user_id'),'order' =>array('user.full_name')])->toArray());
        
		$this->set('songUpload', $songUpload);
    }
    
    /**
     * Get Sub Category.
     *
     * @return mixed
     */
    public function getSubcat() 
	{
		if ($this->request->is('ajax')) { 
			$this->loadModel('Categories');
			$id = $this->request->data['getId'];
			$subcatcount = $this->Categories->find('all',['conditions' =>['parent_category_id' => $id,'enabled' => 'Y','is_deleted'=>'N']])->hydrate(false)->count();
			$subcatdata = $this->Categories->find('all',['conditions' =>['parent_category_id' => $id,'enabled' => 'Y','is_deleted'=>'N']])->hydrate(false)->all();
			//print_r($statedata);die;
			$option = "";
			$option .= "<option value=''>Select Sub Category</option>";
			if ($subcatcount > 0) {
				
				foreach ($subcatdata as $subcatval) {
					$option .= "<option value='" . $subcatval['id'] . "'>" . $subcatval['category_name'] . "</option>";
				}
			}
			echo  $option; die;
		}
	}		
	
}
