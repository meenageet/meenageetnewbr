<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class PostsController extends AppController
{
	
	public function detail($id= null)
	{
		$this->set('title' , $this->project_title.'!: View Post');
		$data= $this->Posts->find('all',['contain'=>['user'],'conditions' =>['Posts.id'=>$id,'user.enabled'=>'Y']])->hydrate(false)->first();
		$this->set('data',$data);
		
	}
   
    public function manage()
    {
		$this->set('title' , $this->project_title.'!: Ads');
		
		$searchData = array();
		$searchData['AND'][] = array("user.enabled"=>'Y',"Posts.is_deleted" => 'N',"Posts.enabled !=" => 'P');
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['title'] != '') $searchData['AND'][] =array('title LIKE' => '%'.$search['title'].'%');
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
		}
		$this->set('Posts',$this->Paginator->paginate(
						$this->Posts, [
						    'contain'=>['user'],
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
		$this->set('users',$this->Posts->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
            return $row['user']['full_name'];
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','Posts.is_deleted'=>'N'),'group'=>array('Posts.user_id'),'order' =>array('user.full_name')])->toArray());
	}
	public function search(){
		if ($this->request->is('ajax')) {
			$searchData = array();
			$searchData['AND'][] = array("user.enabled"=>'Y',"Posts.is_deleted" => 'N',"Posts.enabled !=" => 'P');
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				if($search['title'] != '') $searchData['AND'][] =array('title LIKE' => '%'.$search['title'].'%');
				if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
			}
			$this->set('Posts',$this->Paginator->paginate(
							$this->Posts, [
								'contain'=>['user'],
								'limit' => $this->pagination_limit,
								'order'=>['id'=>'desc'],
								'conditions'=>$searchData,
							])
					);
			$this->set('users',$this->Posts->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
				return $row['user']['full_name'];
			 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','Posts.is_deleted'=>'N'),'group'=>array('Posts.user_id'),'order' =>array('user.full_name')])->toArray());
		}
	}
	
	
	public function pendingManage()
    {
		$this->set('title' , $this->project_title.'!: Ads');
		
		$searchData = array();
		$searchData['AND'][] = array("user.enabled"=>'Y',"Posts.is_deleted" => 'N',"Posts.enabled" => 'P');
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['title'] != '') $searchData['AND'][] =array('title LIKE' => '%'.$search['title'].'%');
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
		}
		$this->set('Posts',$this->Paginator->paginate(
						$this->Posts, [
						    'contain'=>['user'],
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
		$this->set('users',$this->Posts->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
            return $row['user']['full_name'];
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','Posts.is_deleted'=>'N'),'group'=>array('Posts.user_id'),'order' =>array('user.full_name')])->toArray());
	}
	public function searchpending(){
		if ($this->request->is('ajax')) {
			$searchData = array();
			$searchData['AND'][] = array("user.enabled"=>'Y',"Posts.is_deleted" => 'N',"Posts.enabled" => 'P');
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				if($search['title'] != '') $searchData['AND'][] =array('title LIKE' => '%'.$search['title'].'%');
				if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
			}
			$this->set('Posts',$this->Paginator->paginate(
							$this->Posts, [
								'contain'=>['user'],
								'limit' => $this->pagination_limit,
								'order'=>['id'=>'desc'],
								'conditions'=>$searchData,
							])
					);
			$this->set('users',$this->Posts->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
				return $row['user']['full_name'];
			 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','Posts.is_deleted'=>'N'),'group'=>array('Posts.user_id'),'order' =>array('user.full_name')])->toArray());
		}
	}
	
	
	
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$post = $this->Posts->get($this->request->data['id']); // Return article with id 12
			$post->enabled = $this->request->data['status'];
			$this->Posts->save($post);
			echo 1;
		}
		die;
		
		
	}
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			$post = $this->Posts->get($this->request->data['id']); // Return article with id 12
			$post->is_deleted = 'Y';
			$post->enabled = 'N';
			$this->Posts->save($post);
			echo 1;
		}
		die;
		
	}
	
	
	
}
