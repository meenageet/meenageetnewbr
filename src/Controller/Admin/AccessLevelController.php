<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class AccessLevelController extends AppController
{
	
    public function add()
    {
		$this->set('title' , 'Ipix Service Provider!: Add AccessLevel');
		$AccessLevel = $this->AccessLevel->newEntity($this->request->data);
        
		if ($this->request->is('post')) {
			
			$AccessLevel = $this->AccessLevel->patchEntity($AccessLevel, $this->request->data);
            if ($this->AccessLevel->save($AccessLevel)) {
                $this->Flash->success(__('AccessLevel has been saved.'));
                return $this->redirect(['controller'=>'access_level','action' => 'add']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
        $this->set('AccessLevel', $AccessLevel);
    }
    public function manage(){
		$this->set('title' , 'Ipix Service Provider!: AccessLevel');
		$searchData = array();
		$searchData['AND'][] = array("id != "=>'1');
		$this->set('AccessLevel',$this->Paginator->paginate(
						$this->AccessLevel, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
								'conditions'=>$searchData,
						])
				);

	}
	
	public function search(){
		if ($this->request->is('ajax')) {
			$searchData = array();
				if(isset($this->request->data['key'])){
					$search = $this->request->data['key'];
					$searchData['AND'][] = array("id != "=>'1');
					$searchData['AND'][] = array("title LIKE "=>'%'.$search.'%');
					
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
				}
				else {$this->set('serial_num',1);}
			$this->set('AccessLevel',$this->Paginator->paginate(
						$this->AccessLevel, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);

			
			
		}
	}
	
	public function edit($id = null){
		
		$this->set('title' , 'Ipix Service Provider!: Edit AccessLevel');
		$AccessLevel  = $this->AccessLevel->get($id);
		if ($this->request->is(['post','put'])) {
			$AccessLevel = $this->AccessLevel->patchEntity($AccessLevel, $this->request->data);
		
			if ($this->AccessLevel->save($AccessLevel)) {
				$this->Flash->success(__('AccessLevel  has been updated.'));
				return $this->redirect(['controller'=>'access_level','action' => 'manage']);
				
			}
			
		}
		
		$this->set('AccessLevel',$AccessLevel);
		
		
	}
	public function status(){
		if ($this->request->is('ajax')) { 
			$AccessLevel = $this->AccessLevel->get($this->request->data['id']); // Return article with id 12
			$AccessLevel->enabled = $this->request->data['status'];
			$this->AccessLevel->save($AccessLevel);
			echo 1;
		}
		die;
		
	}
	public function delete(){
		if ($this->request->is('ajax')) { 
			
			$query = $this->AccessLevel->query();
			$query->delete()
			->where(['id' => $this->request->data['id']])
			->execute();
			echo 1;
		}
		die;
		
	}
   
}
