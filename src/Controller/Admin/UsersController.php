<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

class UsersController extends AppController
{
	
	
	public function edit($id = null)
	{				
		$this->set('title' , $this->project_title.'!: Edit User');
		$this->loadModel('Cities');
		$user  = $this->Users->get($id);
		
		$before_image = $user->image;
		if ($this->request->is(['post','put'])) {
			
			$user = $this->Users->patchEntity($user, $this->request->data);
			if ($this->Users->save($user)) {
				$user = $this->Users->get($id);
				if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
				{
					
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $_FILES['image']['name']);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['image']['tmp_name'], $_FILES['image']['type'], 'uploads/user/', $filename))
					{
						$url = BASEURL."uploads/user/".$filename;
						$info = getimagesize($url);
						$width = 400;
						$aspectRatio = $info[1] / $info[0];
						$height = (int)($aspectRatio * $width);
						$this->createThumbnail($filename, 'uploads/user', 'uploads/user_images', $width,$height); 
						$user->image = $filename;
					}
					else  $user->image = $before_image;
				}else  $user->image = $before_image;
				$this->Users->save($user);
				$this->Flash->success(__('User has been updated.'));
				return $this->redirect(['action' => 'manage']);
			}
		}
		if($user->dob != '') $user->dob = date('Y-m-d',strtotime($user->dob->format('Y-m-d')));
		$this->set('user', $user);
		
	}
	
	public function pushNotifications()
	{
		$this->set('title' , $this->project_title.' !: Send Notifcation');
	
	}

    public function add()
    {
		$this->set('title' , $this->project_title.' !: Add User');
		$this->loadModel('Cities');
        $user = $this->Users->newEntity();
		if ($this->request->is('post')) 
		{
			$otpNumber = rand(1111,9999);
			$this->request->data['username'] = str_replace(' ', '', trim($this->request->data['full_name'],' ')).rand(1111,9999);
			$this->request->data['otp_number'] = $otpNumber;
			$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$refer_code = "";
			for ($i = 0; $i < 10; $i++) {
				$refer_code .= $chars[mt_rand(0, strlen($chars)-1)];
			}
			$this->request->data['refer_code'] = $refer_code;	
			$user = $this->Users->patchEntity($user,$this->request->data);
			
			if ($usrDetail = $this->Users->save($user)) 
			{
				
					// success email
					$this->loadModel('EmailTemplate');
					$template = $this->EmailTemplate->find('all',array('conditions'=>array('title'=>'signup')))->hydrate(false)->first();
					$template['description'] = str_replace('{PROJECT_TITLE}', $this->project_title, $template['description']);
					$template['description'] = str_replace('{FULL_NAME}', $this->request->data['full_name'], $template['description']);
					$template['description'] = str_replace('{EMAIL}', $this->request->data['email'], $template['description']);
					$template['description'] = str_replace('{PASSWORD}', $otpNumber, $template['description']);
					
					$email = new Email('default');
					$email->from([$template['from_email'] => $template['from_name']] )
					->to($this->request->data['email'])
					->subject($template['subject'])
					->emailFormat('html')
					->send($template['description']); 
					
					//$this->getOtpMobile($usrDetail['phone_number'],$usrDetail['otp_number']);
					
				$this->Flash->success(__('User has been saved.'));
				return $this->redirect(['controller'=>'users','action' => 'add']);
			}else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
		$this->set('user', $user);
    }
     
    public function manage(){
		$this->set('title' , $this->project_title.'!: Users');
		$searchData = array();
		$searchData['AND'][] = array("access_level_id "=>'2' , "Users.is_deleted !=" => 'Y');
		
		$this->set('Users',$this->Paginator->paginate(
						$this->Users, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'contain'=>['country','requestdjsinger'],
							'conditions'=>$searchData,
						])
				);

		
	}
	 
	
	public function search(){
		if ($this->request->is('ajax')) {
			$searchData = array();
			$searchData['AND'][] = array("access_level_id "=>'2',"Users.is_deleted !=" => 'Y');
				if(isset($this->request->data['key']) && $this->request->data['key']!=''){
					$search = $this->request->data['key'];
					$searchData['OR'][] = array('full_name LIKE' => '%'.$search.'%');
					$searchData['OR'][] = array('email LIKE' => '%'.$search.'%');
					$searchData['OR'][] = array('phone_number LIKE' => '%'.$search.'%');
					
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
				}
				else {$this->set('serial_num',1);}
			$this->set('Users',$this->Paginator->paginate(
						$this->Users, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'contain'=>['country','requestdjsinger'],
							'conditions'=>$searchData,
						])
				);

		}
	}
	
	
	 public function subscriptionList($userId = null){
		$this->set('title' , $this->project_title.'!: User Payment Details');
		$this->loadModel('RequestDjSingers');
		$this->loadModel('PurchasePlans');
		$response = [];
			if(isset($userId))
			{ 		
				$responseRequest  = $this->RequestDjSingers->find('all',array('conditions'=>array('user_id'=>$userId)))->hydrate(false)->first();
				if(isset($responseRequest) && !empty($responseRequest)){
					$response['message'] = 'No need plan.';
					$this->set('response',$response['message']);
				}else{
					$freeSubsDays = $this->Users->find('all', array(
						'fields' => array('Users.subscription_duration_month'),
						'conditions' => array('Users.access_level_id' => 1),
					))->first();
					
					/*********** Check if user has bought a plan or not ************/
					$userPlan = $this->PurchasePlans->find('all', array(
						'fields' => array('PurchasePlans.id', 'PurchasePlans.status', 'PurchasePlans.created'),
						'conditions' => array('PurchasePlans.user_id' => $userId, 'PurchasePlans.status' => 'C', 'PurchasePlans.is_deleted' => 'N'),
						'contain'=>[
							'Premiums'=>['fields' => ['id','duration_type','duration']]
						]
					))->first();
					
					if($userPlan['premium']['duration_type'] == 'Monthly') { 
						$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."months", strtotime($userPlan['created'])));
					} elseif($userPlan['premium']['duration_type'] == 'Yearly') {	
						$planExpireDate = date('Y-m-d', strtotime("+".$userPlan['premium']['duration'].' '."years", strtotime($userPlan['created'])));
					}
					
					//calulation current time
					date_default_timezone_set('Asia/Kolkata');
					$currrenttime = time();
					$current_date = date('Y-m-d', $currrenttime);
				
					if ($userPlan =='' && empty($userPlan)) { 
						
						$free_subscription = $this->Users->find('all', array(
								'fields' => array('Users.id', 'Users.free_subscription_time', 'Users.created'),
								'conditions' => array('Users.id' => $userId),
						))->first();
						
						if (isset($free_subscription['free_subscription_time']) && $free_subscription['free_subscription_time'] !='' && !empty($free_subscription['free_subscription_time'])) {
							$subsExpireDate =  date('Y-m-d', strtotime($free_subscription['free_subscription_time']));
							$previousSubsExpireDate = date('Y-m-d', strtotime('-1 day', strtotime($subsExpireDate)));
							
							if($current_date >= $subsExpireDate) {
								$free_plan = $this->Users->get($free_subscription['id']);
								$free_plan->free_subscription_time = null;
								$this->Users->save($free_plan);
								
								// user plan details
								$response['plan_type'] = 'Free Subscription';
								$response['plan_purchase_date'] = date('Y-m-d', strtotime($free_subscription['created']));
								$response['plan_expire_date'] = $subsExpireDate;
								$response['plan_duration_type'] = 'monthly';
								$response['plan_duration'] = $freeSubsDays['subscription_duration_month'];
								$response['is_running'] = false;
								$response['used_time'] = ((strtotime($response['plan_expire_date'])-strtotime($response['plan_purchase_date']))/86400).' days';
								$response['remaining_time'] = '0 days';
								$response['message'] = 'Your free subscription plan has been expired.';
								
							} else {
								if ($current_date == $previousSubsExpireDate) {
									$response['message'] = 'Your free subscription plan is going to expire today.';
								} 
								// user plan details
								$response['plan_type'] = 'Free Subscription';
								$response['plan_purchase_date'] = date('Y-m-d', strtotime($free_subscription['created']));
								$response['plan_expire_date'] = $subsExpireDate;
								$response['plan_duration_type'] = 'monthly';
								$response['plan_duration'] = $freeSubsDays['subscription_duration_month'];
								$response['is_running'] = true;
								$response['used_time'] = ((strtotime($current_date)-strtotime($response['plan_purchase_date']))/86400).' days';
								$response['remaining_time'] = ((strtotime($response['plan_expire_date'])-strtotime($current_date))/86400).' days';
							} 
						} else {
							// user plan details
							$response['plan_type'] = 'premium';
							$response['is_running'] = false;
							$response['message'] = 'Your plan has been expired.';
						}
					} elseif($current_date >= $planExpireDate) {
						$purchase_plan = $this->PurchasePlans->get($userPlan['id']);
						$purchase_plan->status = 'E';
						$this->PurchasePlans->save($purchase_plan);
						
						// user plan details
						$response['plan_type'] = 'premium';
						$response['plan_purchase_date'] = date('Y-m-d', strtotime($userPlan['created']));
						$response['plan_expire_date'] = $planExpireDate;
						$response['plan_duration_type'] = $userPlan['premium']['duration_type'];
						$response['plan_duration'] = $userPlan['premium']['duration'];
						$response['is_running'] = false;
						$response['used_time'] = ((strtotime($response['plan_expire_date'])-strtotime($response['plan_purchase_date']))/86400).' days';
						$response['remaining_time'] = '0 days';
						$response['message'] = 'Your plan has been expired.';
						
					} else {
						// user plan details
						$response['plan_type'] = 'premium';
						$response['plan_purchase_date'] = date('Y-m-d', strtotime($userPlan['created']));
						$response['plan_expire_date'] = $planExpireDate;
						$response['plan_duration_type'] = $userPlan['premium']['duration_type'];
						$response['plan_duration'] = $userPlan['premium']['duration'];
						$response['is_running'] = true;
						$response['used_time'] = ((strtotime($current_date)-strtotime($response['plan_purchase_date']))/86400).' days';
						$response['remaining_time'] = ((strtotime($response['plan_expire_date'])-strtotime($current_date))/86400).' days';
						$response['message'] = '';
						
					}
				}
			}
			$this->set('response',$response);
		
	}
	
	
	public function subscriptionsearch($id = null){
		if ($this->request->is('ajax')) {
			$searchData = array();
			$this->loadModel('PaymentDetails');
			$searchData['AND'][] = array("user.enabled"=>'Y',"PaymentDetails.is_deleted" => 'N','PaymentDetails.user_id'=>$id);
					
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				
				if($search['payment_status'] == 'pending') $searchData['AND'][] =array('PaymentDetails.status' => 'P');
				if($search['payment_status'] == 'complete') $searchData['AND'][] =array('PaymentDetails.status' => 'C');
				if($search['payment_type'] == 'paytm') $searchData['AND'][] =array('PaymentDetails.type' => 'P');
				if($search['payment_type'] == 'upi') $searchData['AND'][] =array('PaymentDetails.type' => 'U');
				if($search['payment_type'] == 'bank') $searchData['AND'][] =array('PaymentDetails.type' => 'B');
							
			}
			$this->set('PaymentDetails',$this->Paginator->paginate(
				$this->PaymentDetails, [
					'contain' => ['user'],
					'limit' => $this->pagination_limit,
					'order' => array('PaymentDetails.id'=>'DESC'),
					'conditions' => $searchData,
				])
			);
			
			$this->set('users',$this->PaymentDetails->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
				return $row['user']['full_name'];
			 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','PaymentDetails.is_deleted'=>'N'),'group'=>array('PaymentDetails.user_id'),'order' =>array('user.full_name')])->toArray());
		}
	}
	
	
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$user = $this->Users->get($this->request->data['id']); // Return article with id 12
			if($this->request->data['status'] == 'N'){
				$message = "Your account has been Deactivated";
				$this->sendPushNotificationPayment('user_deactive', $this->request->data['id'], $message);
			}else{
				$message = "Your account has been Activated";
				$this->sendPushNotificationPayment('user_active', $this->request->data['id'], $message);
			}
			$user->enabled = $this->request->data['status'];
			$this->Users->save($user);
			echo 1;
		}
		die;
		
	}
	
	public function changeUserProfile($user_id = NULL){
		$this->set('title' , $this->project_title.' !: Become User Profile');
		
		$this->loadModel('Users');
		$this->loadModel('RequestDjSingers');
        $userProfile = $this->RequestDjSingers->newEntity();
		if ($this->request->is('post')) 
		{
			$userData = $this->Users->find('all', array('conditions' => array('Users.id' => $user_id)))->first();
			if(isset($userData) && $userData !=''){
				$this->request->data['user_id'] = $user_id;
				$userProfile = $this->RequestDjSingers->patchEntity($userProfile, $this->request->data,['validate'=>'ApiAdd']);					
				if($saved_request = $this->RequestDjSingers->save($userProfile))
				{
					$image_base_url = path_IDProof_image;
					$request_id = $saved_request->id;
					$requestDjSinger = $this->RequestDjSingers->get($request_id);
					
					if(isset($_FILES['id_proof']) && $_FILES['id_proof']['tmp_name'] !='')
					{
						$id_proofimages = $this->uploadFile($_FILES['id_proof'],'idproof');
						$requestDjSinger->id_proof = isset($id_proofimages) ? $id_proofimages :'';
					}else{
						$requestDjSinger->id_proof = $before_image;
					}
					$this->RequestDjSingers->save($requestDjSinger);
					
					$this->Flash->success(__('Profile has been saved.'));
					return $this->redirect(['controller'=>'users','action' => 'manage']);	
				}else{
					$this->Flash->error(__('Some Errors Occurred.'));
				}
			}else{
					$this->Flash->error(__('Some Errors Occurred.'));
				}
        }
		$this->set('userProfile', $userProfile);
	}
	
	
	public function verifiedstatus(){
		if ($this->request->is('ajax')) { 
			$user = $this->Users->get($this->request->data['id']); // Return article with id 12
			$user->no_verified = $this->request->data['status'];
			$this->Users->save($user);
			echo 1;
		}
		die;
		
	}
	
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			
			$user = $this->Users->get($this->request->data['id']); // Return article with id 12
			$user->is_deleted = 'Y';
			$user->enabled = 'N';
			$this->Users->save($user);
			$message = "User has been deleted";
			$this->sendPushNotificationPayment('user_delete', $user->id, $message);
			echo 1;
		}
		die;
		
	}
	
	public function deleteSingle()
	{
		if ($this->request->is('ajax')) { 
			
			$user = $this->Users->get($this->request->data['id']); // Return article with id 12
			$user->single_registered = 'N';
			$user->looking_for = '';
			$user->about_me = '';
			$this->Users->save($user);
			echo 1;
		}
		die;
		
	}
	
	
    public function login()
    {
		$this->set('title' , $this->project_title.'!: Login');
        if ($this->request->is('post')) {

            if (Validation::email($this->request->data['username'])) {
                $this->Auth->config('authenticate', [
                    'Form' => [
                        'fields' => ['username' => 'email']
                    ]
                ]);
                $this->Auth->constructAuthenticate();
                $this->request->data['email'] = $this->request->data['username'];
                unset($this->request->data['username']);
            }

            $user = $this->Auth->identify();

            if ($user && $user['enabled']=='Y' && $user['is_deleted']=='N') {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }

            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }
	
	public function changepassword(){
		$this->set('title',$this->project_title.': Change password');
		$users  = $this->Users->get($this->Auth->user('id'));
		
		if($this->request->is(['post','put'])){
			
			$users = $this->Users->patchEntity($users, [
                'old_password'  => $this->request->data['old_password'],
                'password'      => $this->request->data['new_password'],
                'new_password'     => $this->request->data['new_password'],
                'confirm_password'     => $this->request->data['confirm_password']
            ],
            ['validate' => 'password']
			);
			
			if($this->Users->save($users)){
				$this->Flash->success('Your password has been updated.');
				$this->redirect(['controller'=>'users','action'=>'changepassword']);
			}else{
				$this->Flash->error('Some Errors Occurred.');
			}
			
		}
		$this->set('users',$users);
	}
	
	public function changeProfile(){
		$this->set('title',$this->project_title.': Change Profiel Image');
		$users  = $this->Users->get($this->Auth->user('id'));
		$before_image = $users->image;
		if($this->request->is(['post','put'])){
			if(isset($_FILES['image']) && $_FILES['image']['tmp_name'] !='')
			{	
				$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $_FILES['image']['name']);
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
				
				if ($this->uploadImage($_FILES['image']['tmp_name'], $_FILES['image']['type'], 'uploads/user/', $filename))
				{
					$url = BASEURL."uploads/user/".$filename;
					$info = getimagesize($url);
					$width = 400;
					$aspectRatio = $info[1] / $info[0];
					$height = (int)($aspectRatio * $width);
					$this->createThumbnail($filename, 'uploads/user', 'uploads/user_images', $width,$height); 
					$users->image = $filename;
					
				}
				else  $users->image = $before_image;
			}
			
			if($this->Users->save($users)){
				$this->Flash->success('Your Profile image has been updated.');
				$this->redirect(['controller'=>'users','action'=>'changeProfile']);
			}else{
				$this->Flash->error('Some Errors Occurred.');
			}
			
		}
		$this->set('users',$users);
	}
	
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    
    
    public function testst(){
		
		$url = "https://fcm.googleapis.com/fcm/send";
		$token = "fva-ijdbFmI:APA91bEbSb7JnxsjBwsNXJPkFPB0CEa1BBXJdTt-pnJIJS2EzifYmd2cf8aQbLYNc3UKTyBwsQxNzpY791sQ84MIcGD8qspXp7BMZbZlslpHZ_6kTqcOlviQKmcAVt13TycaQswJtqSCvPE6JuJf94qJ_w8h-YaL0Q";
		$serverKey = 'AIzaSyD6VPqrkTBGCUO0mA4AG8wMrPgQEGdd0kc';
		$title = "Title";
		$body = "Body of the message";
		$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
		$json = json_encode($arrayToSend);
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Authorization: key='. $serverKey;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,

		"POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
		//Send the request
		$response = curl_exec($ch);
		//Close request
		if ($response === FALSE) {
		die('FCM Send Error: ' . curl_error($ch));
		}
		curl_close($ch);
		die('ssss');
	}
	
	/**
     * Get State.
     *
     * @return mixed
     */
    public function getState () 
	{
		if ($this->request->is('ajax')) { 
			$this->loadModel('States');
			$id = $this->request->data['getId'];
			$statecount = $this->States->find('all',['conditions' =>['country_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->count();
			$statedata = $this->States->find('all',['conditions' =>['country_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->all();
			//print_r($statedata);die;
			$option = "";
			$option .= "<option value=''>Select State</option>";
			if ($statecount > 0) {
				
				foreach ($statedata as $stateval) {
					$option .= "<option value='" . $stateval['id'] . "'>" . $stateval['state_name'] . "</option>";
				}
			}
			echo  $option; die;
		}
	}
	
	/**
     * Get City.
     *
     * @return mixed
     */
    public function getCity () 
	{
		if ($this->request->is('ajax')) { 
			$this->loadModel('Cities');
			$id = $this->request->data['getId'];
			$citycount = $this->Cities->find('all',['conditions' =>['state_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->count();
			$citydata = $this->Cities->find('all',['conditions' =>['state_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->all();
			$option = "";
			$option .= "<option value=''>Select City</option>";
			if ($citycount > 0) {
				foreach ($citydata as $cityval) {
					$option .= "<option value='" . $cityval['id'] . "'>" . $cityval['city_name'] . "</option>";
				}
			}
			
			echo $option;die;
		}
	}
	
	/*
	 * 
	 * It will return complete location , lat, long  etc
	 * 
	 * */

	public function gettinglocationdata($address = null)
	{
		$address = str_replace(' ','+',$address);
		$url = "https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=AIzaSyAO2O2MxYYOXmIVt4m-X9Lq7nRmWCx11iQ";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response);
		$main_array = array();
		if($response_a->status == "OK")
		{
			$main_array['latitude'] = $response_a->results[0]->geometry->location->lat;
			$main_array['longitude'] = $response_a->results[0]->geometry->location->lng;
			//echo "<pre>";print_r($response_a->results);die;
			foreach($response_a->results as $address_components)
			{
				if($address_components->address_components)
				{
					foreach($address_components->address_components as $address_data)
					{
						if($address_data->types[0] && $address_data->types[0] == 'country')
						{
							$main_array['country_code'] = $address_data->short_name;
						}
						if($address_data->types[0] && $address_data->types[0] == 'locality')
						{
							$main_array['city_name'] = $address_data->short_name;
						}
						if($address_data->types[0] && $address_data->types[0] == 'postal_code')
						{
							$main_array['pincode'] = $address_data->short_name;
						}
					}
				}
			}
			
		}
		return $main_array;
	}
	
	//set location miles function
	public function locationMiles(){
		$this->set('title',$this->project_title.': Set Location Miles');
		$users  = $this->Users->get($this->Auth->user('id'));
		
		if($this->request->is(['post','put'])){
			
			$users = $this->Users->patchEntity($users, [
                'miles'  => $this->request->data['miles']
            ],
            ['validate' => 'miles']
			);
			
			if($this->Users->save($users)){
				$this->Flash->success('Your miles has been updated.');
				$this->redirect(['controller'=>'users','action'=>'locationMiles']);
			}else{
				$this->Flash->error('Some Errors Occurred.');
			}
			
		}
		$this->set('users',$users);
	}
	
	
	public function requestList($slug = NULL){
		 
		$this->set('title' , $this->project_title.'!: Request list for Singer / DJ');
		$this->loadModel('RequestDjSingers');
		$this->set('slug' , $slug);
		$searchData = array();		
		$searchData['AND'][] = array("RequestDjSingers.is_deleted !=" => 'Y',"RequestDjSingers.is_approved" => 'Y');
		
		if(isset($slug) && $slug !=''){
			$searchData['AND'][] = array('RequestDjSingers.type' => $slug);
		}
		if ($this->request->is(['post' ,'put'])) 
		{
			$search = $this->request->data;
			if(isset($slug) && $slug !=''){
				if($search['title'] != '') $searchData['OR'][] =array('user.full_name LIKE' => '%'.$search['title'].'%');
			}else{
				if($search['title'] != '') $searchData['OR'][] =array('user.full_name LIKE' => '%'.$search['title'].'%');
				if($search['request_type'] == 'singer') $searchData['AND'][] = array('RequestDjSingers.type' => 'S');
				if($search['request_type'] == 'dj') $searchData['AND'][] = array('RequestDjSingers.type' => 'D'); 
			}
		}
		
		$this->set('RequestDjSingers',$this->Paginator->paginate(
			$this->RequestDjSingers, [
				'limit' => $this->pagination_limit,
				'order'=>['id'=>'desc'],
				'contain'=>['user'],
				'conditions'=>$searchData,
			])
		);
		
	}
	
	
	public function requestsearch($slug = null){
		if ($this->request->is('ajax')) {
			$this->loadModel('RequestDjSingers');
			$this->set('slug' , $slug);
			$searchData = array();
			$searchData['AND'][] = array("RequestDjSingers.is_deleted !=" => 'Y',"RequestDjSingers.is_approved" => 'Y');
				if(isset($slug) && $slug !=''){
					$searchData['AND'][] = array('RequestDjSingers.type' => $slug);
				}
				if(isset($this->request->data['key']) && $this->request->data['key']!=''){
					$search = $this->request->data['key'];
					
					if(isset($slug) && $slug !=''){
						if($search['title'] != '') $searchData['OR'][] =array('user.full_name LIKE' => '%'.$search['title'].'%');
						$searchData['AND'][] = array('RequestDjSingers.type' => $slug);
					}else{
						if($search['title'] != '') $searchData['OR'][] =array('user.full_name LIKE' => '%'.$search['title'].'%');
						if($search['request_type'] == 'singer') $searchData['AND'][] = array('RequestDjSingers.type' => 'S');
						if($search['request_type'] == 'dj') $searchData['AND'][] = array('RequestDjSingers.type' => 'D'); 
					}
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
				}
				else {$this->set('serial_num',1);}
			$this->set('RequestDjSingers',$this->Paginator->paginate(
						$this->RequestDjSingers, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'contain'=>['user'],
							'conditions'=>$searchData,
						])
				);

		}
	}
	
	
	public function requestnotApprovedList(){
		 
		$this->set('title' , $this->project_title.'!: Request list for Singer / DJ');
		$this->loadModel('RequestDjSingers');
		$searchData = array();		
		$searchData['AND'][] = array("RequestDjSingers.is_deleted !=" => 'Y',"RequestDjSingers.is_approved !=" => 'Y');
		
		if ($this->request->is(['post' ,'put'])) 
		{
			$search = $this->request->data;
			if($search['title'] != '') $searchData['OR'][] =array('user.full_name LIKE' => '%'.$search['title'].'%');
			if($search['request_type'] == 'singer') $searchData['AND'][] = array('RequestDjSingers.type' => 'S');
			if($search['request_type'] == 'dj') $searchData['AND'][] = array('RequestDjSingers.type' => 'D');       
		}
		
		$this->set('RequestDjSingers',$this->Paginator->paginate(
			$this->RequestDjSingers, [
				'limit' => $this->pagination_limit,
				'order'=>['id'=>'desc'],
				'contain'=>['user'],
				'conditions'=>$searchData,
			])
		);
		
	}
	
	
	public function requestnotapprovedsearch(){
		if ($this->request->is('ajax')) {
			$this->loadModel('RequestDjSingers');
			$searchData = array();
			$searchData['AND'][] = array("RequestDjSingers.is_deleted !=" => 'Y',"RequestDjSingers.is_approved !=" => 'Y');
				if(isset($this->request->data['key']) && $this->request->data['key']!=''){
					$search = $this->request->data['key'];
					$searchData['OR'][] = array('user.full_name LIKE' => '%'.$search.'%');
					$searchData['OR'][] = array('RequestDjSingers.type LIKE' => '%'.$search.'%');
					
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
				}
				else {$this->set('serial_num',1);}
			$this->set('RequestDjSingers',$this->Paginator->paginate(
						$this->RequestDjSingers, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'contain'=>['user'],
							'conditions'=>$searchData,
						])
				);

		}
	}
	
	
	
	public function verifiedrequest(){
		$this->loadModel('RequestDjSingers');
		if ($this->request->is('ajax')) { 
			$requestDjSinger = $this->RequestDjSingers->get($this->request->data['id']); // Return article with id 12
			$requestDjSinger->is_approved = $this->request->data['status'];
			$this->RequestDjSingers->save($requestDjSinger);
			echo 1;
		}
		die;
		
	}
	
    public function requestdelete()
	{
		$this->loadModel('RequestDjSingers');
		if ($this->request->is('ajax')) { 
			
			$requestDjSinger = $this->RequestDjSingers->get($this->request->data['id']); // Return article with id 12
			$this->RequestDjSingers->delete($requestDjSinger);
			echo 1;
		}
		die;
		
	}
	
	public function minimumrewards() {
		$this->set('title',$this->project_title.': Change minimum reward points');
		$users  = $this->Users->get($this->Auth->user('id'));
		
		if($this->request->is(['post','put'])) {
			
			$users = $this->Users->patchEntity($users, [
                'minimum_rewards'  => $this->request->data['minimum_rewards']
            ],
            ['validate' => 'minRewards']
			);
			
			if($this->Users->save($users)){
				$this->Flash->success('Your minimum reward points has been updated.');
				$this->redirect(['controller'=>'users','action'=>'minimumrewards']);
			} else {
				$this->Flash->error('Some Errors Occurred.');
			}
			
		}
		$this->set('users',$users);
	}
	
	public function appusetime() {
		$this->set('title',$this->project_title.': Change app use time');
		$users  = $this->Users->get($this->Auth->user('id'));
		$usetime = $this->Users->find('all', array(
			'fields' => array('Users.app_use_time'),
			'conditions' => array('Users.access_level_id' => 1),
		))->first();
		//print_r($usetime);exit;
		if($this->request->is(['post','put'])) {
			$app_use_time  = $this->request->data['app_use_time'];
			$app_use_seconds = $app_use_time * 3600;
			
			
			$users = $this->Users->patchEntity($users, [
                'app_use_time'  => $app_use_seconds
            ],
            ['validate' => 'appUseTime']
			);
			
			if($this->Users->save($users)){
				
				
				$this->set('usetime',$usetime);
				$this->Flash->success('Your app use time has been updated.');
				$this->redirect(['controller'=>'users','action'=>'appusetime']);
			} else {
				$this->Flash->error('Some Errors Occurred.');
			}
			
		}
		$this->set('users',$users);
		$this->set('usetime',$usetime);
	}
	
	public function freesubscription() {
		$this->set('title',$this->project_title.': Change free subscription duration');
		$users  = $this->Users->get($this->Auth->user('id'));
		
		if($this->request->is(['post','put'])) {
			
			$users = $this->Users->patchEntity($users, [
                'subscription_duration_month'  => $this->request->data['subscription_duration_month']
            ],
            ['validate' => 'subscriptionDuration']
			);
			
			if($this->Users->save($users)){
				
				$this->Flash->success('Your free subscription duration has been updated.');
				$this->redirect(['controller'=>'users','action'=>'freesubscription']);
			} else {
				$this->Flash->error('Some Errors Occurred.');
			}
			
		}
		$this->set('users',$users);
	}
	
	public function manageRewardTime() {
		$this->set('title',$this->project_title.': Change manage reward time');
		$users  = $this->Users->get($this->Auth->user('id'));
		
		if($this->request->is(['post','put'])) {
			//print_r($this->request->data['rewards_time']);die;
			$users = $this->Users->patchEntity($users, ['rewards_time'  => $this->request->data['rewards_time'],'app_use_reward_point'  => $this->request->data['app_use_reward_point']],['validate' => 'manageRewardsTime']);
			
			if($userdata = $this->Users->save($users)){
				$message = 'Reward time / points has been updated';
				if($userdata['rewards_time'] == 1){
					$this->sendPushNotificationAdmin('admin_reward_time', null, null, null, null, $message,1);
				}else{
					$this->sendPushNotificationAdmin('admin_reward_time', null, null, null, null, $message,0);
				}
				
				$this->Flash->success('Reward time / points has been updated.');
				$this->redirect(['controller'=>'users','action'=>'manage_reward_time']);
			} else {
				$this->Flash->error('Some Errors Occurred.');
			}
			
		}
		$this->set('users',$users);
	}
    
}
