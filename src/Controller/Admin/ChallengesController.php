<?php
namespace App\Controller\Admin;
use App\Controller\AppController; // HAVE TO USE App\Controller\AppController
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class ChallengesController extends AppController
{
	
	public function detail($id= null)
	{
		$this->set('title' , $this->project_title.'!: View Challenge');
		$this->loadModel('ChallengeLikes');
		$challengesData = $this->Challenges->find('all', array(
					'fields' => array('Challenges.id','Challenges.remaining_second', 'Challenges.remaining_time', 'Challenges.challenge_time', 'Challenges.image', 'Challenges.accepted_user_image','Challenges.status'),
					'conditions' => array(
						'Challenges.id' => $id,
						'Challenges.is_deleted' => 'N'
					),
					'contain'=>[
						'user1'=>['fields' => ['id','full_name','image']],
						'user2'=>['fields' => ['id','full_name','image']],
						'song'=>['fields' => ['title','image','audio']],
						'acceptusersong'=>['fields' => ['title','image','audio']]
					],
					'order'=>['Challenges.id'=>'desc']
				))->hydrate(false)->first();
		
		$data= $this->Challenges->find('all',['contain'=>['user1','user2','song'],'conditions' =>['Challenges.id'=>$id]])->hydrate(false)->first();
		$this->set('data',$challengesData);
	//	echo "<pre>";
		//print_r($challengesData);die;
		
		$total_challenge_likes1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$id,'ch_user_id'=>$challengesData['user1']['id'],'status'=>1])->count();
		$this->set('total_challenge_likes1',$total_challenge_likes1);
		
		$total_challenge_unlikes1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$id,'ch_user_id'=>$challengesData['user1']['id'],'status'=>0])->count();
		$this->set('total_challenge_unlikes1',$total_challenge_unlikes1);
		
		$total_challenge_likes2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$id,'ch_user_id'=>$challengesData['user2']['id'],'status'=>1])->count();
		$this->set('total_challenge_likes2',$total_challenge_likes2);
		
		$total_challenge_unlikes2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$id,'ch_user_id'=>$challengesData['user2']['id'],'status'=>0])->count();
		$this->set('total_challenge_unlikes2',$total_challenge_unlikes2);
		
		
		//calulation time
		date_default_timezone_set('Asia/Kolkata');		
		$cdate = strtotime(date('Y-m-d H:i:s'));
		$start_time = strtotime($challengesData['remaining_time']->i18nFormat('yyyy-MM-dd HH:mm:ss'));
		$diff = $start_time - $cdate;		
		if($challengesData['remaining_second'] == 0) {
			$this->set('remaining_time',0);
		} else {
			$difftime = $this->secondsToTime($diff);
			$this->set('remaining_time',$difftime);
		}
		
		
	}
   
    public function manage()
    {
		$this->set('title' , $this->project_title.'!: Challenges');
		
		$searchData = array();
		$searchData['AND'][] = array("Challenges.is_deleted" => 'N');
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['user_id'] != '') $searchData['AND'][] = array('user1.id' => $search['user_id']);
            if($search['challenge_type'] == 'singer') $searchData['AND'][] = array('Challenges.type' => 'S');
			if($search['challenge_type'] == 'dj') $searchData['AND'][] = array('Challenges.type' => 'D');    
		}
		//pr($searchData);die;
		
		
		$this->set('Challenges',$this->Paginator->paginate(
						$this->Challenges, [
						    'contain'=>['user1','user2','song'],
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
		
		$this->set('users',$this->Challenges->find('list', ['keyField' => 'user1.id','valueField' => function ($row) {
            return $row['user1']['full_name'];
		 },'contain'=>array('user1'),'conditions'=>array('user1.enabled'=>'Y','Challenges.is_deleted'=>'N'),'group'=>array('Challenges.user_id'),'order' =>array('user1.full_name')])->toArray());

		
	}
	
	
	
	public function search(){
		if ($this->request->is('ajax')) {
			$searchData = array();
			$searchData['AND'][] = array("Challenges.is_deleted" => 'N');
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				if($search['user_id'] != '') $searchData['AND'][] = array('user1.id' => $search['user_id']);
				if($search['challenge_type'] == 'singer') $searchData['AND'][] = array('Challenges.type' => 'S');
				if($search['challenge_type'] == 'dj') $searchData['AND'][] = array('Challenges.type' => 'D');    
			}
			//pr($searchData);die;
			
			
			$this->set('Challenges',$this->Paginator->paginate(
							$this->Challenges, [
								'contain'=>['user1','user2','song'],
								'limit' => $this->pagination_limit,
								'order'=>['id'=>'desc'],
								'conditions'=>$searchData,
							])
					);
			
			$this->set('users',$this->Challenges->find('list', ['keyField' => 'user1.id','valueField' => function ($row) {
				return $row['user1']['full_name'];
			 },'contain'=>array('user1'),'conditions'=>array('user1.enabled'=>'Y','Challenges.is_deleted'=>'N'),'group'=>array('Challenges.user_id'),'order' =>array('user1.full_name')])->toArray());

		}
	}
	
	
	 public function winnerChallenge()
    {
		$this->set('title' , $this->project_title.'!: Challenges');
		$this->loadModel('ChallengeWinners');
		
		$searchData = array();
		$searchData['AND'][] = array("Challenges.is_deleted" => 'N');
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if(isset($search['user_id']) && $search['user_id'] != '') $searchData['AND'][] = array('ChallengeWinners.user_id' => $search['user_id']);
		}
		//print_r($searchData);die;
		$this->set('Challenges',$this->Paginator->paginate(
						$this->ChallengeWinners, [
						    'contain'=>[
								'Users',
								'Users2',
								'Challenges'=> function(\Cake\ORM\Query $q) {
										return $q->find('all', array(
											'fields' => ['id','remaining_second', 'remaining_time','challenge_time', 'image', 'accepted_user_image'],
											'conditions' => array('Challenges.is_deleted' => 'N'),
											'contain'=>[
												'user1'=>['fields' => ['id','full_name','image']],
												'user2'=>['fields' => ['id','full_name','image']],
												'song'=>['fields' => ['title','image','audio']],
												'acceptusersong'=>['fields' => ['title','image','audio','total_likes']]
											],
											'order'=>['song.total_likes'=>'DESC'],	
										));
									}
							],
							'limit' => $this->pagination_limit,
							'conditions'=>$searchData,
						])
				);
		
		$this->set('users',$this->ChallengeWinners->find('list', ['keyField' => function ($row) {
            return $row['user']['id'];
		 },'valueField' => function ($row) {
            return $row['user']['full_name'];
		 },'contain'=>array('Users'),'conditions'=>array('Users.enabled'=>'Y','ChallengeWinners.is_deleted'=>'N'),'order' =>array('Users.full_name')])->toArray());

		
	}
	
	 public function winnersearch()
    {
		$this->set('title' , $this->project_title.'!: Challenges');
		
		if ($this->request->is('ajax')) {
			$this->loadModel('ChallengeWinners');
			$searchData = array();
			$searchData['AND'][] = array("Challenges.is_deleted" => 'N');
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				if($search['user_id'] != '') $searchData['AND'][] = array('ChallengeWinners.user_id' => $search['user_id']);
			}
			$this->set('Challenges',$this->Paginator->paginate(
							$this->ChallengeWinners, [
						    'contain'=>[
								'Users',
								'Users2',
								'Challenges'=> function(\Cake\ORM\Query $q) {
										return $q->find('all', array(
											'fields' => ['id','remaining_second', 'remaining_time','challenge_time', 'image', 'accepted_user_image'],
											'conditions' => array('Challenges.is_deleted' => 'N'),
											'contain'=>[
												'user1'=>['fields' => ['id','full_name','image']],
												'user2'=>['fields' => ['id','full_name','image']],
												'song'=>['fields' => ['title','image','audio']],
												'acceptusersong'=>['fields' => ['title','image','audio','total_likes']]
											],
											'order'=>['song.total_likes'=>'DESC'],	
										));
									}
							],
							'limit' => $this->pagination_limit,
							//'limit' => 1,
							'conditions'=>$searchData,
						])
					);
			
			$this->set('users',$this->ChallengeWinners->find('list', ['keyField' => function ($row) {
            return $row['user']['full_name'];
		 },'valueField' => function ($row) {
				return $row['user']['full_name'];
			 },'contain'=>array('Users'),'conditions'=>array('Users.enabled'=>'Y','ChallengeWinners.is_deleted'=>'N'),'order' =>array('Users.full_name')])->toArray());
		}
		
	}
	
	public function winnerDetail($id= null)
	{
		$this->set('title' , $this->project_title.'!: View Challenge');
		$this->loadModel('ChallengeLikes');
		$challengesData = $this->Challenges->find('all', array(
					'fields' => array('Challenges.id','Challenges.remaining_second', 'Challenges.remaining_time', 'Challenges.challenge_time', 'Challenges.image', 'Challenges.accepted_user_image','Challenges.status'),
					'conditions' => array(
						'Challenges.id' => $id,
						'Challenges.is_deleted' => 'N'
					),
					'contain'=>[
						'user1'=>['fields' => ['id','full_name','image']],
						'user2'=>['fields' => ['id','full_name','image']],
						'song'=>['fields' => ['title','image','audio']],
						'acceptusersong'=>['fields' => ['title','image','audio']]
					],
					'order'=>['Challenges.id'=>'desc']
				))->hydrate(false)->first();
		
		$data= $this->Challenges->find('all',['contain'=>['user1','user2','song'],'conditions' =>['Challenges.id'=>$id]])->hydrate(false)->first();
		$this->set('data',$challengesData);
	//	echo "<pre>";
		//print_r($challengesData);die;
		
		$total_challenge_likes1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$id,'ch_user_id'=>$challengesData['user1']['id'],'status'=>1])->count();
		$this->set('total_challenge_likes1',$total_challenge_likes1);
		
		$total_challenge_unlikes1 = $this->ChallengeLikes->find()->where(['challenge_id'=>$id,'ch_user_id'=>$challengesData['user1']['id'],'status'=>0])->count();
		$this->set('total_challenge_unlikes1',$total_challenge_unlikes1);
		
		$total_challenge_likes2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$id,'ch_user_id'=>$challengesData['user2']['id'],'status'=>1])->count();
		$this->set('total_challenge_likes2',$total_challenge_likes2);
		
		$total_challenge_unlikes2 = $this->ChallengeLikes->find()->where(['challenge_id'=>$id,'ch_user_id'=>$challengesData['user2']['id'],'status'=>0])->count();
		$this->set('total_challenge_unlikes2',$total_challenge_unlikes2);
		
		
		//calulation time
		date_default_timezone_set('Asia/Kolkata');		
		$cdate = strtotime(date('Y-m-d H:i:s'));
		$start_time = strtotime($challengesData['remaining_time']->i18nFormat('yyyy-MM-dd HH:mm:ss'));
		$diff = $start_time - $cdate;		
		if($challengesData['remaining_second'] == 0) {
			$this->set('remaining_time',0);
		} else {
			$difftime = $this->secondsToTime($diff);
			$this->set('remaining_time',$difftime);
		}
		
		
	}
   
	
	public function pointredeem() { 
		if ($this->request->is('ajax')) { 
				$this->loadModel('HistoryRewards');
			//print_r($this->request->data); die;	
			$previous_points = 0;
			$redeem_ids = $_POST['checkbox_id'];
			$point = $this->request->data['points'];
			
			foreach($redeem_ids as $redeem_id)
			{
								
				$history_reward = $this->HistoryRewards->newEntity();
				$history_reward = $this->HistoryRewards->patchEntity($history_reward, $this->request->data);
				//insert reward point
				$history_reward->user_id = $redeem_id;
				$history_reward->point = $point;
				$history_reward->type = 'C'; 
				$this->HistoryRewards->save($history_reward);
			}
			echo 1;
		}
		die;
	}
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$challenge = $this->Challenges->get($this->request->data['id']); // Return article with id 12
			$challenge->enabled = $this->request->data['status'];
			$this->Challenges->save($challenge);
			echo 1;
		}
		die;
	}
	
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			$challenge = $this->Challenges->get($this->request->data['id']); // Return article with id 12
			$challenge->is_deleted = 'Y';
			$challenge->enabled = 'N';
			$this->Challenges->save($challenge);
			echo 1;
		}
		die;
		
	}
	
	
	
}
