<?php
namespace App\Controller\Admin;
use App\Controller\AppController; // HAVE TO USE App\Controller\AppController
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class UserRewardsController extends AppController
{
	
	public function detail($user_id = null)
    {
		$this->set('title' , $this->project_title.'!: User Rewards History');
		
		$this->loadModel('Users');
		$this->loadModel('HistoryRewards');
		
		$searchData = array();
		$searchData['AND'][] = array('user.enabled'=>'Y','HistoryRewards.is_deleted' => 'N','HistoryRewards.user_id'=>$user_id);
				
		/*if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['from_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) >=' => $search['from_date']);
			if($search['to_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) <=' => $search['to_date']);
            //if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
                        
		}*/
		//print_r($searchData);die;
		
		
		$this->set('HistoryRewards',$this->Paginator->paginate(
			$this->HistoryRewards, [
				'contain' => ['user'],
				'limit' => $this->pagination_limit,
				'order' => array('HistoryRewards.id'=>'DESC'),
				'conditions' => $searchData,
			])
		);
		
		$this->set('users',$this->HistoryRewards->find('list', ['keyField' => 'user.id','valueField' => function ($row) {
            return $row['user']['full_name'];
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','HistoryRewards.is_deleted'=>'N'),'group'=>array('HistoryRewards.user_id'),'order' =>array('user.full_name')])->toArray());
		
	}
   
    public function manage()
    {
		$this->set('title' , $this->project_title.'!: User Rewards');
		
		$this->loadModel('Users');
		$appusetime = $this->Users->find('all', array(
			'fields' => array('Users.app_use_time'),
			'conditions' => array('Users.access_level_id' => 1),
		))->first();
		$this->set('appusetime', $appusetime);
		$searchData = array();
		$searchData['AND'][] = array("user.enabled"=>'Y',"UserRewards.is_deleted" => 'N',"UserRewards.time !=" => 0);
				
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['from_date'] != '') $searchData['AND'][] = array('date(UserRewards.start_time) >=' => $search['from_date']);
			if($search['to_date'] != '') $searchData['AND'][] = array('date(UserRewards.start_time) <=' => $search['to_date']);
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
                        
		}
		//print_r($searchData);die;
		
		
		$this->set('UserRewards',$this->Paginator->paginate(
			$this->UserRewards, [
				'contain' => ['user'],
				'limit' => $this->pagination_limit,
				'order' => array('UserRewards.start_time'=>'DESC'),
				'conditions' => $searchData,
			])
		);
		
		$this->set('users',$this->UserRewards->find('list', ['keyField' => function ($row) {
            return $row['user']['id'];
		 },'valueField' => function ($row) {
			if($row['user']['full_name'] == ''){
				return $row['user']['phone_number'];
			}else{
				return $row['user']['full_name'];
			}
            
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','UserRewards.is_deleted'=>'N'),'group'=>array('UserRewards.user_id'),'order' =>array('user.full_name')])->toArray());
		
		//pr($this->UserRewards);exit;
	}
	
	public function search()
    {
		$this->set('title' , $this->project_title.'!: User Rewards');
		if ($this->request->is('ajax')) {
			$this->loadModel('Users');
			$appusetime = $this->Users->find('all', array(
				'fields' => array('Users.app_use_time'),
				'conditions' => array('Users.access_level_id' => 1),
			))->first();
			$this->set('appusetime', $appusetime);
			$searchData = array();
			$searchData['AND'][] = array("user.enabled"=>'Y',"UserRewards.is_deleted" => 'N',"UserRewards.time !=" => 0);
					
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				if($search['from_date'] != '') $searchData['AND'][] = array('date(UserRewards.start_time) >=' => $search['from_date']);
				if($search['to_date'] != '') $searchData['AND'][] = array('date(UserRewards.start_time) <=' => $search['to_date']);
				if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
							
			}
			$this->set('UserRewards',$this->Paginator->paginate(
				$this->UserRewards, [
					'contain' => ['user'],
					'limit' => $this->pagination_limit,
					'order' => array('UserRewards.start_time'=>'DESC'),
					'conditions' => $searchData,
				])
			);
			
			$this->set('users',$this->UserRewards->find('list', ['keyField' => function ($row) {
            return $row['user']['id'];
		 },'valueField' => function ($row) {
				if($row['user']['full_name'] == ''){
					return $row['user']['phone_number'];
				}else{
					return $row['user']['full_name'];
				}
			 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','UserRewards.is_deleted'=>'N'),'group'=>array('UserRewards.user_id'),'order' =>array('user.full_name')])->toArray());
		}
	}
	
	
	
	public function topSingerSongs()
    {
		$this->set('title' , $this->project_title.'!: Top singer songs according to monthly rewards');
		
		$this->loadModel('HistoryRewards');
		$this->loadModel('Users');
		$appusetime = $this->Users->find('all', array(
			'fields' => array('Users.app_use_time'),
			'conditions' => array('Users.access_level_id' => 1),
		))->first();
		$this->set('appusetime', $appusetime);
		$searchData = array();
		$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
		$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
		$searchData['AND'][] = array('HistoryRewards.type'=>'S',"user.enabled"=>'Y',"HistoryRewards.is_deleted" => 'N');
				
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['from_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) >=' => $search['from_date']);
			if($search['to_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) <=' => $search['to_date']);
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
                        
		}else{
			$searchData['AND'][] = array('date(HistoryRewards.created) >=' => $last_month_start);
			$searchData['AND'][] = array('date(HistoryRewards.created) <=' => $last_month_end);
		}		
		$this->set('UserRewards',$this->Paginator->paginate(
			$this->HistoryRewards, [
				'contain' => ['user'=>['fields' => ['id','full_name']]],
				'limit' => $this->pagination_limit,
				'order' => array('HistoryRewards.id'=>'DESC'),
				'conditions' => $searchData,
			])
		);
		
		$this->set('users',$this->HistoryRewards->find('list', ['keyField' => function ($row) {
            return $row['user']['id'];
		 },'valueField' => function ($row) {
            return $row['user']['full_name'];
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','HistoryRewards.type'=>'S'),'group'=>array('HistoryRewards.user_id'),'order' =>array('user.full_name')])->toArray());
		
	}
	
	public function singersearch()
    {
		$this->set('title' , $this->project_title.'!: Top singer songs according to monthly rewards');
		if ($this->request->is('ajax')) {
			$this->loadModel('HistoryRewards');
			$this->loadModel('Users');
			$appusetime = $this->Users->find('all', array(
				'fields' => array('Users.app_use_time'),
				'conditions' => array('Users.access_level_id' => 1),
			))->first();
			$this->set('appusetime', $appusetime);
			$searchData = array();
			$searchData['AND'][] = array('HistoryRewards.type'=>'S',"HistoryRewards.is_deleted"=> 'N');
					
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				if($search['from_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) >=' => $search['from_date']);
				if($search['to_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) <=' => $search['to_date']);
				if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
							
			}
			//print_r($searchData);die;		
			$this->set('UserRewards',$this->Paginator->paginate(
				$this->HistoryRewards, [
					'contain' => ['user'=>['fields' => ['id','full_name']]],
					'limit' => $this->pagination_limit,
					'order' => array('HistoryRewards.id'=>'DESC'),
					'conditions' => $searchData
				]));
			
			$this->set('users',$this->HistoryRewards->find('list', ['keyField' => function ($row) {
				return $row['user']['id'];
			 },'valueField' => function ($row) {
				return $row['user']['full_name'];
			 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','HistoryRewards.type'=>'S'),'group'=>array('HistoryRewards.user_id'),'order' =>array('user.full_name')])->toArray());
		}
	}
	
	
	
	public function topDjSongs()
    {
		$this->set('title' , $this->project_title.'!: Top dj songs according to monthly rewards');
		
		$this->loadModel('HistoryRewards');
		$this->loadModel('Users');
		$appusetime = $this->Users->find('all', array(
			'fields' => array('Users.app_use_time'),
			'conditions' => array('Users.access_level_id' => 1),
		))->first();
		$this->set('appusetime', $appusetime);
		$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
		$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
		$searchData = array();
		$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
		$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
		$searchData['AND'][] = array('HistoryRewards.type'=>'D',"user.enabled"=>'Y',"HistoryRewards.is_deleted" => 'N');
				
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['from_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) >=' => $search['from_date']);
			if($search['to_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) <=' => $search['to_date']);
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
                        
		}else{
			$searchData['AND'][] = array('date(HistoryRewards.created) >=' => $last_month_start);
			$searchData['AND'][] = array('date(HistoryRewards.created) <=' => $last_month_end);
		}	
		
		$this->set('UserRewards',$this->Paginator->paginate(
			$this->HistoryRewards, [
				'contain' => ['user'=>['fields' => ['id','full_name']]],
				'limit' => $this->pagination_limit,
				'order' => array('HistoryRewards.id'=>'DESC'),
				'conditions' => $searchData,
			])
		);
		
		$this->set('users',$this->HistoryRewards->find('list', ['keyField' => function ($row) {
            return $row['user']['id'];
		 },'valueField' => function ($row) {
            return $row['user']['full_name'];
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','HistoryRewards.type'=>'D'),'group'=>array('HistoryRewards.user_id'),'order' =>array('user.full_name')])->toArray());
		
	}
	
	
	public function searchdj()
    {
		$this->set('title' , $this->project_title.'!: Top dj songs according to monthly rewards');
		if ($this->request->is('ajax')) {
			$this->loadModel('HistoryRewards');
			$this->loadModel('Users');
			$appusetime = $this->Users->find('all', array(
				'fields' => array('Users.app_use_time'),
				'conditions' => array('Users.access_level_id' => 1),
			))->first();
			$this->set('appusetime', $appusetime);
			$searchData = array();
			$searchData['AND'][] = array('HistoryRewards.type'=>'D',"user.enabled"=>'Y',"HistoryRewards.is_deleted" => 'N');
					
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				if($search['from_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) >=' => $search['from_date']);
				if($search['to_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) <=' => $search['to_date']);
				if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
							
			}		
			$this->set('UserRewards',$this->Paginator->paginate(
				$this->HistoryRewards, [
					'contain' => ['user'=>['fields' => ['id','full_name']]],
					'limit' => $this->pagination_limit,
					'order' => array('HistoryRewards.point'=>'DESC'),
					'conditions' => $searchData,
				])
			);
			
			$this->set('users',$this->HistoryRewards->find('list', ['keyField' => function ($row) {
				return $row['user']['id'];
			 },'valueField' => function ($row) {
				return $row['user']['full_name'];
			 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','HistoryRewards.type'=>'D'),'group'=>array('HistoryRewards.user_id'),'order' =>array('user.full_name')])->toArray());
		}
	}
	
	
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$post = $this->UserRewards->get($this->request->data['id']); // Return article with id 12
			$post->enabled = $this->request->data['status'];
			$this->UserRewards->save($post);
			echo 1;
		}
		die;
	}
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			$post = $this->UserRewards->get($this->request->data['id']); // Return article with id 12
			$post->is_deleted = 'Y';
			$post->enabled = 'N';
			$this->UserRewards->save($post);
			echo 1;
		}
		die;
	}
	
	public function deletehistory()
	{
		if ($this->request->is('ajax')) { 
			$this->loadModel('HistoryRewards');
			$post = $this->HistoryRewards->get($this->request->data['id']); // Return article with id 12
			$post->is_deleted = 'Y';
			$this->HistoryRewards->save($post);
			echo 1;
		}
		die;
		
	}
	
	
	
	public function topTaskRewards()
    {
		$this->set('title' , $this->project_title.'!: Top Task song according to daily rewards');
		$last_10_day = date("Y-n-j", strtotime("10 days ago"));
		$this->loadModel('HistoryRewards');
		$this->loadModel('Users');
		$appusetime = $this->Users->find('all', array(
			'fields' => array('Users.app_use_time'),
			'conditions' => array('Users.access_level_id' => 1),
		))->first();
		$this->set('appusetime', $appusetime);
		//$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
		//$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
		$searchData = array();
		//$last_month_start = date("Y-n-j", strtotime("first day of previous month"));
		//$last_month_end = date("Y-n-j", strtotime("last day of previous month"));
		$searchData['AND'][] = array('HistoryRewards.type'=>'T',"user.enabled"=>'Y',"HistoryRewards.is_deleted" => 'N','DATE(HistoryRewards.created) >=' => $last_10_day);
				
		if ($this->request->is(['post' ,'put']) ) 
		{
			if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
			$search = $this->request->data;
			if($search['from_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) >=' => $search['from_date']);
			if($search['to_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) <=' => $search['to_date']);
            if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
                        
		}else{
			$searchData['AND'][] = array('date(HistoryRewards.created) >=' => $last_10_day);
			//$searchData['AND'][] = array('date(HistoryRewards.created) <=' => $last_month_end);
		}	
		
		$this->set('UserRewards',$this->Paginator->paginate(
			$this->HistoryRewards, [
				'contain' => ['user'=>['fields' => ['id','full_name']]],
				'limit' => $this->pagination_limit,
				'order' => array('HistoryRewards.id'=>'DESC'),
				'conditions' => $searchData,
			])
		);
		
		$this->set('users',$this->HistoryRewards->find('list', ['keyField' => function ($row) {
            return $row['user']['id'];
		 },'valueField' => function ($row) {
            return $row['user']['full_name'];
		 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','HistoryRewards.type'=>'T'),'group'=>array('HistoryRewards.user_id'),'order' =>array('user.full_name')])->toArray());
		
	}
	
	
	public function searchtask()
    {
		$this->set('title' , $this->project_title.'!: Top Task songs according to daily rewards');
		if ($this->request->is('ajax')) {
			$this->loadModel('HistoryRewards');
			$this->loadModel('Users');
			$appusetime = $this->Users->find('all', array(
				'fields' => array('Users.app_use_time'),
				'conditions' => array('Users.access_level_id' => 1),
			))->first();
			$last_10_day = date("Y-n-j", strtotime("10 days ago"));
			$this->set('appusetime', $appusetime);
			$searchData = array();
			$searchData['AND'][] = array('HistoryRewards.type'=>'T',"user.enabled"=>'Y',"HistoryRewards.is_deleted" => 'N','DATE(HistoryRewards.created) >=' => $last_10_day);
					
			if ($this->request->is(['post' ,'put']) ) 
			{
				if(array_key_exists('key',$this->request->data)) parse_str($this->request->data['key'], $this->request->data);
				$search = $this->request->data;
				if($search['from_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) >=' => $search['from_date']);
				if($search['to_date'] != '') $searchData['AND'][] = array('date(HistoryRewards.created) <=' => $search['to_date']);
				if($search['user_id'] != '') $searchData['AND'][] = array('user.id'=>$search['user_id']);
							
			}	else{
			$searchData['AND'][] = array('date(HistoryRewards.created) >=' => $last_10_day);
			//$searchData['AND'][] = array('date(HistoryRewards.created) <=' => $last_month_end);
		}		
			$this->set('UserRewards',$this->Paginator->paginate(
				$this->HistoryRewards, [
					'contain' => ['user'=>['fields' => ['id','full_name']]],
					'limit' => $this->pagination_limit,
					'order' => array('HistoryRewards.point'=>'DESC'),
					'conditions' => $searchData,
				])
			);
			
			$this->set('users',$this->HistoryRewards->find('list', ['keyField' => function ($row) {
				return $row['user']['id'];
			 },'valueField' => function ($row) {
				return $row['user']['full_name'];
			 },'contain'=>array('user'),'conditions'=>array('user.enabled'=>'Y','HistoryRewards.type'=>'T'),'group'=>array('HistoryRewards.user_id'),'order' =>array('user.full_name')])->toArray());
		}
	}
	
	
}
