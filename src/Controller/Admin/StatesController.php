<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class StatesController extends AppController
{
	public $paginate=['limit'=>5];

	 public function initialize()
    {
        parent::initialize();
    }

	public function index(){
		$this->set('title' , $this->project_title.'!: States');
		$searchData = array('deleted'=>'N');		
		$this->set('States',$this->States->find('all', ['order'=>'States.id desc'])->where(['States.deleted'=>'N'])->contain('Countries'));
	}

	public function add(){
		$this->set('title' , $this->project_title.'!: Add state');
        $state= $this->States->newEntity();        
        if ($this->request->is(['post' ,'put'])) {
           $state = $this->States->patchEntity($state, $this->request->data);           
            if ($this->States->save($state)) {				
				$this->Flash->success(__('State detail has been saved.'));
                return $this->redirect(['controller'=>'States','action' => 'index']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }       
        $this->loadModel('Countries');
        $this->set('country',$this->Countries->find('list', ['keyField' => 'id','valueField' => 'country_name'])->toArray());
		$this->set('state', $state);
    }
	
	public function edit($id = null){
		$this->set('title' , $this->project_title.'!: Edit State');
        $state=$this->States->find("all")->where(['States.id'=>$id])->contain(['Countries'])->first();
        if ($this->request->is(['post' ,'put'])) {
			$state = $this->States->patchEntity($state, $this->request->data);
			if ($this->States->save($state)) {
				 $this->Flash->success(__('State has been saved.'));
                return $this->redirect(['controller'=>'States','action' => 'index']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
        $this->loadModel('Countries');
        $this->set('country',$this->Countries->find('list', ['keyField' => 'id','valueField' => 'country_name'])->toArray());
        $this->set('state', $state);
    }	

    public function delete(){
		if ($this->request->is('ajax')) { 
			$data=array('deleted'=>'Y');
			$query = $this->States->query();
			$query->update()
			->set($data)
			->where(['id' => $this->request->data['id']])
			->execute();
			echo 1;
		}
		die;		
	}

	public function status(){
		if ($this->request->is('ajax')) { 
			$buscatsubattr = $this->States->get($this->request->data['id']); // Return article with id 12
			$buscatsubattr->enabled = $this->request->data['status'];
			$this->States->save($buscatsubattr);
			echo 1;
		}
		die;		
	}

	public function import(){
		$card =  $this->States->newEntity();
		if($this->request->is(['post','put'])){
			
			$card = $this->States->patchEntity($card,$this->request->data);
			
			if(!$card->errors()){
				if (move_uploaded_file($_FILES['import']['tmp_name'], 'uploads/csv/' . $_FILES["import"]['name'])) {
						
					$msg = 	$this->read($_FILES["import"]['name']);
					if(!empty($msg)){
							if(array_key_exists("1",$msg)){
								$this->Flash->error(__($msg[1]));
							}else{
								foreach($msg as $k=>$m){
									foreach($m as $n){
										$this->Flash->error(__($n ." at row no. ".$k));
									}
								}
							}
							return $this->redirect(['controller'=>'States','action' => 'manage']);
					}
					else
					{
						$sucess = $this->upload($_FILES["import"]['name']);
						if($sucess==1){
							$this->Flash->success(__('Data has been saved.'));
						}
						
						return $this->redirect(['controller'=>'States','action' => 'manage']);
					}
					} 
			}else{
				$this->Flash->error(__('Invalid csv file'));
				return $this->redirect(['controller'=>'States','action' => 'manage']);
			}
			
		}
		die;		
	}		
	public function upload($filename){		
		$file = fopen("uploads/csv/".$filename."","r");
		$i=1;
		$success=0;
		while(! feof($file))
		{
			$data = fgetcsv($file);
			if($i!=1)
			{
				if($data[0] != '')
				{
					$rec= array();							
					$rec['country_id'] = intval($data[0]);
					$rec['state_name'] = $data[1];
					
					$cate = $this->States->newEntity();
					$cate  = $this->States->patchEntity($cate,$rec);
					
				
					if ($newNetwork = $this->States->save($cate)) {
						
						$success=1;
							
						
					}
					
					
				}
			}
		$i++;
		}
		return $success;
	}

	public function read($filename){		
		$file = fopen("uploads/csv/".$filename."","r");
		$i = 1;
		$return_arr =  array();
		$this->loadModel('Countries');
		while(! feof($file))
		  {
			 $data = fgetcsv($file);
			 if($i!=1){
				if($data[0] != '' ) 
				{ 

					$query = $this->Countries->find()->select(['id'])->where(['id =' => $data[0]])->first();	
					if(empty($query)){
						$return_arr[$i][] = "Country id does not exist";
					}
					
					
					$rec= array();							
					
					$rec['country_id'] = intval($data[0]);
					$rec['state_name'] = $data[1];
				
					$cate = $this->States->newEntity();
					$cate  = $this->States->patchEntity($cate,$rec);
					
				
					if($cate->errors()){
						
						foreach($cate->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$return_arr[$i][] =  $error_text;
							} 
							
						}
					
					}
				
				}
		 
			}else{
				
				if($data[0] !='country_id' || trim($data[1]) !='state_name' ){
					
					$return_arr[$i] = "Required fields are country_id, state_name";
					break;
				}
		
			}
		$i++;
		
		}
		return $return_arr;
			fclose($file);
		die;
	}	

	public function export(){
			$filename = 'export.csv';
			$file = fopen("uploads/csv/".$filename,"w");
			$headers = array('id','State','City name','enabled');
			fputcsv($file,$headers);
			$query = $this->States->find('all',array('contain'=>array('Countries'),'fields'=>array('States.id','Countries.name','States.state_name','States.enabled'),'order'=>array('Countries.name asc','States.state_name asc')))->hydrate(false);
			$insert_arr=  array();
			foreach($query->toArray() as $record){
				$insert_arr =array();
				$insert_arr[] = $record['id'];
				$insert_arr[] = $record['country']['name'];
				$insert_arr[] = $record['state_name'];
				$insert_arr[] = $record['enabled'];
				fputcsv($file,$insert_arr);
			}
			fclose($file);
			$this->response->file("uploads/csv/".$filename, array(
			 'download' => true,
			 'name' => 'States'.date('Y-m-d H:i').'.csv'
			)); 
			return $this->response;die;		
	}

}
