<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class AreasController extends AppController
{
	public $paginate=['limit'=>5];

	public function initialize(){
        parent::initialize();
    }

	
	public function index() {
		$this->set('title' , 'Task Chat!: Areas');
		$searchData = array();
		$areas=$this->Areas->find('all', ['order'=>'Areas.id desc'])
					->contain(['Cities'=>['States'=>['Countries']]])
					->where(['Areas.deleted'=>'N']);
		$data = $areas->toArray();
		$this->set('Areas',$areas);		
	}
	
    public function add() {
		$this->set('title' , 'Task Chat!: Add area');
        $area = $this->Areas->newEntity();     
        if ($this->request->is(['post' ,'put'])) {	 
			
			    $this->request->data['city_id'] = $this->request->data['city_id'];
			    $this->request->data['area_name'] = $this->request->data['area_name'];
			    $this->request->data['slug'] = trim($this->slugify($this->request->data['area_name'])); 
           $area = $this->Areas->patchEntity($area, $this->request->data);   
           //echo '<pre>'; print_r($area); die;            
           if ($newNetwork = $this->Areas->save($area)) {				
				$this->Flash->success(__('area detail has been saved.'));
                return $this->redirect(['controller'=>'Areas','action' => 'index']);
            } else {
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }       

        $this->loadModel('Countries');
        $this->set('country', $this->Countries->find('list', ['keyField' => 'id','valueField' => 'country_name'])->toArray());
		$this->set('area', $area);
    }

    public function edit($id = null)
    {
		$this->set('title' , $this->project_title.'!: Edit Area');
		$area = $this->Areas->find("all")->where(['Areas.id'=>$id])->contain(['Cities'=>['States'=>['Countries']]])->first();
		//echo '<pre>'; print_r($area); die;
        if ($this->request->is(['post' ,'put'])) {	//echo '<pre>'; print_r($this->request->data()); die;		        	        
			 $this->request->data['slug'] = trim($this->slugify($this->request->data['area_name'])); 
        	//$cityid['city_id'] = $this->request->data['city_id'];
			$area = $this->Areas->patchEntity($area, $this->request->data());								
			if ($this->Areas->save($area)) { //echo '<pre>'; print_r($area); die;	
				 $this->Flash->success(__('area has been saved.'));
                return $this->redirect(['controller'=>'Areas','action' => 'index']);
            } else {
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        } 
        $this->loadModel('Cities');       
        $this->loadModel('States');
        $this->loadModel('Countries');
        
        $this->set('country', $this->Countries->find('list', ['keyField' => 'id','valueField' => 'country_name'])->toArray());
       // print_r($this->Countries->find('list', ['keyField' => 'id','valueField' => 'country_name'])->toArray()); die;
        
        $states = $this->States->find('list', ['keyField' => 'id','valueField' => 'state_name'])->where(['States.country_id'=>$area['city']['state']['country']['id']]);
        $this->set('state', $states);  
        
        $cities = $this->Cities->find('list', ['keyField' => 'id','valueField' => 'city_name'])->where(['Cities.state_id'=>$area['city']['state']['id']]);
        $this->set('city', $cities);  
               
        $this->set('area', $area);
    }
	
	public function search() {
		if ($this->request->is('ajax')) {
			$searchData = array();
			if (isset($this->request->data['key'])){
				$search = $this->request->data['key'];
				$searchData['OR'][] = array('area_name LIKE' => '%'.$search.'%');
				$this->set('key', $this->request->data['key']);
			}
			if ($this->request->query('page')) { 
				$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
			}
			else { $this->set('serial_num',1); }
			$this->set('Areas',$this->Paginator->paginate(
				$this->Areas, [
					'limit' => $this->pagination_limit,
					'order'=>['id'=>'desc'],
					'conditions' => $searchData,
					'contain'=>array('Cities'),	
				])
			);
		}
	}
	
	
	
	public function status() {
		if ($this->request->is('ajax')) { 
			$buscatsubattr = $this->Areas->get($this->request->data['id']); // Return article with id 12
			$buscatsubattr->enabled = $this->request->data['status'];
			$this->Areas->save($buscatsubattr);
			echo 1;
		}
		die;		
	}
	
	
	
	public function popularStatus(){
		if ($this->request->is('ajax')) { 
			$buscatsubattr = $this->Areas->get($this->request->data['id']); // Return article with id 12
			$buscatsubattr->popular_status = $this->request->data['status'];
			$this->Areas->save($buscatsubattr);
			echo 1;
		}
		die;		
	}
	
	
	public function delete() {
		if ($this->request->is('ajax')) { 			
			$query = $this->Areas->query();
			$query->update()
			->set(['deleted'=>'Y'])
			->where(['id' => $this->request->data['id']])
			->execute();
			echo 1;
		}
		die;		
	}
	
	public function import() {
		$card =  $this->Areas->newEntity();
		if($this->request->is(['post','put'])) {
			
			$card = $this->Areas->patchEntity($card, $this->request->data);
			
			if(!$card->errors()) {
				if (move_uploaded_file($_FILES['import']['tmp_name'], 'uploads/csv/' . $_FILES["import"]['name'])) {
						
					$msg = 	$this->read($_FILES["import"]['name']);
					if(!empty($msg)) {
							if(array_key_exists("1",$msg)) {
								$this->Flash->error(__($msg[1]));
							} else {
								foreach($msg as $k=>$m){
									foreach($m as $n){
										$this->Flash->error(__($n ." at row no. ".$k));
									}
								}
							}
							return $this->redirect(['controller'=>'Areas','action' => 'manage']);
					}
					else
					{
						$sucess = $this->upload($_FILES["import"]['name']);
						if($sucess == 1) {
							$this->Flash->success(__('Data has been saved.'));
						}
						
						return $this->redirect(['controller'=>'Areas','action' => 'manage']);
					}
				} 
			} else {
				$this->Flash->error(__('Invalid csv file'));
				return $this->redirect(['controller'=>'Areas','action' => 'manage']);
			}
			
		}
		die;
		
	}	
	
	public function upload($filename)
	{
		$file = fopen("uploads/csv/".$filename."","r");
		$i=1;
		$success=0;
		while(! feof($file))
		{
			$data = fgetcsv($file);
			if($i!=1)
			{
				if($data[0] != '')
				{
					$rec= array();							
					$rec['city_id'] = intval($data[0]);
					$rec['area_name'] = $data[1];
					
					$cate = $this->Areas->newEntity();
					$cate  = $this->Areas->patchEntity($cate,$rec);
				
					if ($newNetwork = $this->Areas->save($cate)) {
						
						$success=1;
					}
				}
			}
			$i++;
		}
		return $success;
	}
	public function read($filename)
	{
		
		$file = fopen("uploads/csv/".$filename."","r");
		$i = 1;
		$return_arr =  array();
		$this->loadModel('Cities');
		while(! feof($file))
		  {
			 $data = fgetcsv($file);
			 if($i!=1){
				if($data[0] != '' ) 
				{ 

					$query = $this->Cities->find()->select(['id'])->where(['id =' => $data[0]])->first();	
					if(empty($query)){
						$return_arr[$i][] = "City id does not exist";
					}
					$rec= array();							
					
					$rec['city_id'] = intval($data[0]);
					$rec['area_name'] = $data[1];
				
					$cate = $this->Areas->newEntity();
					$cate  = $this->Areas->patchEntity($cate,$rec);
					
				
					if($cate->errors()){
						
						foreach($cate->errors() as $field_key =>  $error_data)
						{
							foreach($error_data as $error_text)
							{
								$return_arr[$i][] =  $error_text;
							} 
							
						}
					
					}
				
				}
		 
			} else {
				
				if($data[0] !='city_id' || trim($data[1]) !='area_name' ) {
					
					$return_arr[$i] = "Required fields are city_id, area_name";
					break;
				}
			}
			$i++;
		}
		return $return_arr;
		fclose($file);
		die;
	}
	
	
	
	public function export() {
		$filename = 'export.csv';
		$file = fopen("uploads/csv/".$filename,"w");
		$headers = array('id','City','Area name','enabled');
		fputcsv($file,$headers);
		$query = $this->Areas->find('all',array('contain'=>array('Cities'),'fields'=>array('Areas.id','Cities.city_name','Areas.area_name','Areas.enabled'),'order'=>array('Cities.city_name asc','Areas.area_name asc')))->hydrate(false);
		$insert_arr=  array();
		foreach($query->toArray() as $record){
			$insert_arr =array();
			$insert_arr[] = $record['id'];
			$insert_arr[] = $record['city']['city_name'];
			$insert_arr[] = $record['area_name'];
			$insert_arr[] = $record['enabled'];
			fputcsv($file,$insert_arr);
		}
		fclose($file);
		$this->response->file("uploads/csv/".$filename, array(
		 'download' => true,
		 'name' => 'Areas'.date('Y-m-d H:i').'.csv'
		)); 
		return $this->response;die;
		
	}
	
	public function demo(){
			$filename = 'subattirbutes.csv';
			$this->response->file("uploads/import/".$filename, array(
			 'download' => true,
			 'name' => $filename
			)); 
			return $this->response;
			die;
		
	}
	
	/**
     * Get State.
     *
     * @return mixed
     */
    public function getState () 
	{
		if ($this->request->is('ajax')) { 
			$this->loadModel('States');
			$id = $this->request->data['getId'];
			$statecount = $this->States->find('all',['conditions' =>['country_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->count();
			$statedata = $this->States->find('all',['conditions' =>['country_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->all();
			//print_r($statedata);die;
			$option = "";
			$option .= "<option value=''>Select State</option>";
			if ($statecount > 0) {
				
				foreach ($statedata as $stateval) {
					$option .= "<option value='" . $stateval['id'] . "'>" . $stateval['state_name'] . "</option>";
				}
			}
			echo  $option; die;
		}
	}
	
	/**
     * Get City.
     *
     * @return mixed
     */
    public function getCity () 
	{
		if ($this->request->is('ajax')) { 
			$this->loadModel('Cities');
			$id = $this->request->data['getId'];
			$citycount = $this->Cities->find('all',['conditions' =>['state_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->count();
			$citydata  = $this->Cities->find('all',['conditions' =>['state_id' => $id,'enabled' => 'Y','deleted'=>'N']])->hydrate(false)->all();
			//print_r($citydata);die;
			$option = "";
			$option .= "<option value=''>Select City</option>";
			if ($citycount > 0) {
				
				foreach ($citydata as $cityval) {
					$option .= "<option value='" . $cityval['id'] . "'>" . $cityval['city_name'] . "</option>";
				}
			}
			echo  $option; die;
		}
	}
}
