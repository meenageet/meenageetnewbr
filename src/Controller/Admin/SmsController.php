<?php
namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

class SmsController extends AppController
{

    public function index()
    {
        $this->set('title','Send SMS');
		$this->loadModel('Users');
		$this->loadModel('Settings');
        $users = $this->Users->find('list',['conditions'=>['id !='=>1],'keyField' =>'id','valueField' =>  function ($row) {
			if($row['email'] !=''){ return $row['full_name']; }else{ return $row['phone_number'].'(email not available)'; }
		 }, 'order' => ['full_name' => 'asc']])->hydrate(false)->toArray();
        $settingsData = $this->Settings->find('all', array('conditions' => array('module_name' => 'email_from')))->first();
      // echo "<pre>"; print_r($users);die;
        if ($this->request->is(['post' ,'put']))
        {
            $data = $this->request->data;
            
            if(!empty($data['email']))
            {	
				if(SENDMAIL == 1)
                    {
						$userIdArr = [];
						foreach($data['email'] as $datakey=>$dataval){
							$userIdArr[] = $dataval;
						}
						$usersData =  $this->Users->find('all')->select(['id','email','phone_number'])
										->where(['id IN'=>$userIdArr])
										->order(['email'=>'desc'])
										->hydrate(false)->toArray();
						//print_r($usersData);die;
						foreach($usersData as $dataval){
							$message = $data['sms'];
							$this->sendPushNotificationPayment('payment_noti', $dataval['id'], $message);
							if(isset($dataval['email']) && $dataval['email'] !=''){
								/*$user['msg'] = $data['sms'];
								$user['subject'] = $data['subject'];
								$email = new Email('default');
								$email->viewVars(['data'=>$user]);
								$email->from([$settingsData['value']] )
									->to($dataval['email'])
									->subject($data['subject'])
									->emailFormat('html')
									->template('sms')
									->send();*/
							}else{
								//print_r('2');
								$this->getOtpMobileAdmin($dataval['phone_number'],$data['subject']);
							}
						}
                    }
                $this->Flash->success('Messages are sent.');
            }
            else
            {
                $this->Flash->error('No user selected.');
            }

        }
        $this->set('users',$users);
    }
}
