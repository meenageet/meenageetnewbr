<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


class BannersController extends AppController
{
	public function add()
    {
		$this->set('title' , $this->project_title.'!: Add Banner');
        $banner = $this->Banners->newEntity();
        
        if ($this->request->is(['post' ,'put'])) {
            $banner = $this->Banners->patchEntity($banner, $this->request->data);
            if ($newNetwork = $this->Banners->save($banner)) {
				$banner_web = '';
				if(isset($this->request->data['image']) && !empty($_FILES['image']['tmp_name']) )
				{
					$filename = '';
					$newfilename = $this->request->data['image']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['image']['tmp_name'], $_FILES['image']['type'], 'uploads/banner_app/', $filename)) $banner_web  = $filename;
						
				}
				
				$banner['image'] = $banner_web;
				$this->Banners->save($banner);
				
                $this->Flash->success(__('Banner detail has been saved.'));
                return $this->redirect(['controller'=>'Banners','action' => 'add']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
        
        $this->set('banner', $banner);
    }
	
	public function edit($id = null)
    {
		$this->set('title' , $this->project_title.'!: Edit Banner');
        $banner = $this->Banners->get($id);
      
        if ($this->request->is(['post' ,'put'])) {
			$oldLogoWeb = $banner['image'];
			
			$banners = $this->Banners->patchEntity($banner, $this->request->data);
			
			if($_FILES['image']['name'] == '') $banners->image = $oldLogoWeb;
			if ($this->Banners->save($banners)) 
			{
				$banners = $this->Banners->get($id);
				if(isset($this->request->data['image']) && !empty($_FILES['image']['tmp_name']) )
				{
					$newfilename = $this->request->data['image']['name'];
					$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $newfilename);
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
					
					if ($this->uploadImage($_FILES['image']['tmp_name'], $_FILES['image']['type'], 'uploads/banner_app/', $filename)) $banners->image = $filename;
					
				}
				$this->Banners->save($banners);
				$this->Flash->success(__('Banner has been saved.'));
                return $this->redirect(['controller'=>'Banners','action' => 'manage']);
            }else{
				$this->Flash->error(__('Some Errors Occurred.'));
			}
        }
       $this->set('banner', $banner);
    }
   
    public function manage(){
		$this->set('title' , $this->project_title.'!: Banners');
		$searchData = array();
		$this->set('banners',$this->Paginator->paginate(
						$this->Banners, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
		
	}
	

	public function search(){
		
		if ($this->request->is('ajax')) {
			
			$searchData = array();
				if(isset($this->request->data['key'])){
					$search = $this->request->data['key'];
					$searchData['OR'][] = array('text LIKE' => '%'.$search.'%');
					$this->set('key',$this->request->data['key']);
				}
				if($this->request->query('page')) { 
					$this->set('serial_num',(($this->pagination_limit)*($this->request->query('page'))) - ($this->pagination_limit -1));
				}
				else {$this->set('serial_num',1);}
			
			$this->set('banners',$this->Paginator->paginate(
						$this->Banners, [
							'limit' => $this->pagination_limit,
							'order'=>['id'=>'desc'],
							'conditions'=>$searchData,
						])
				);
			
		}
	}
	
	
	public function status(){
		if ($this->request->is('ajax')) { 
			$banner = $this->Banners->get($this->request->data['id']); // Return article with id 12
			$banner->enabled = $this->request->data['status'];
			$this->Banners->save($banner);
			echo 1;
		}
		die;
		
		
	}
	public function delete()
	{
		if ($this->request->is('ajax')) { 
			$query = $this->Banners->query();
			$query->delete()
			->where(['id' => $this->request->data['id']])
			->execute();
			echo 1;
		}
		die;
		
	}
	
	
}
