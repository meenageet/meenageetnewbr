<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
// src/Controller/UsersController.php

namespace App\Controller\Admin;


use App\Controller\AppController; // HAVE TO USE App\Controller\AppController

//namespace App\Controller;

//use App\Controller\AppController;
use Cake\Event\Event;

	class PagesController extends AppController
	{
		public function index()
		{
			$this->dashboard();	
		}
		public function forbidden(){
			if($this->request->referer()!='/') $this->request->session()->write('Config.referer', $this->request->referer());
			$this->set('title' , $this->project_title.'!: Access forbidden');
			
		}
		public function dashboard(){
			$this->set('title' , $this->project_title.'!: Dashboard');
			$this->loadModel('Users');
			$this->loadModel('Banners');
			$this->loadModel('Categories');
			$this->loadModel('SongUploads');
			$this->loadModel('Challenges');
			$this->loadModel('Likes');
			$this->loadModel('Comments');
			$this->loadModel('UserRewards');
			$this->loadModel('Downloads');
			$this->loadModel('PaymentDetails');
			$this->loadModel('RequestDjSingers');
			$this->loadModel('Posts');
			$this->loadModel('Transactions');
			
			$singerlist = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('type' => 'S', 'is_approved' => 'Y', 'is_deleted' => 'N')])->toArray();
			
			$djlist = $this->RequestDjSingers->find('list', ['keyField' => 'id','valueField' => 'user_id','conditions'=>array('type' => 'D', 'is_approved' => 'Y', 'is_deleted' => 'N')])->toArray();
			
			//total users
			$this->set('totalUsers',$this->Users->find('all' ,['conditions'=>['access_level_id'=>'2', 'is_deleted'=>'N' ]])->count());
			
			//total singer users
			if(isset($singerlist) && !empty($singerlist)){
				$this->set('totalSingerUsers',$this->Users->find('all' ,['conditions'=>["id IN" => $singerlist,'access_level_id'=>'2', 'is_deleted'=>'N' ]])->count());
			}else{
				$this->set('totalSingerUsers',0);
			}
			
		
			//total DJ users
			if(isset($djlist) && !empty($djlist)){
				$this->set('totalDjUsers',$this->Users->find('all' ,['conditions'=>["id IN" => $djlist,'access_level_id'=>'2', 'is_deleted'=>'N' ]])->count());
			}else{
				$this->set('totalDjUsers',0);
			}
			
			//total banners
			$this->set('totalBanners',$this->Banners->find('all' ,['conditions'=>['enabled'=>'Y']])->count());
			//total categories
			$this->set('totalCategories',$this->Categories->find('all' ,['conditions'=>["is_deleted" => 'N']])->count());
			
			//total dj  songs
			if(isset($djlist) && !empty($djlist)){
				$this->set('totaldjSongUploads',$this->SongUploads->find('all' ,['conditions'=>["user_id IN" => $djlist,'is_deleted'=>'N' ]])->count());
			}else{
				$this->set('totaldjSongUploads',0);
			}
			
			//total singer songs
			if(isset($singerlist) && !empty($singerlist)){
				$this->set('totalsingerSongUploads',$this->SongUploads->find('all' ,['conditions'=>["user_id IN" => $singerlist,'is_deleted'=>'N' ]])->count());
			}else{
				$this->set('totalsingerSongUploads',0);
			}
			//total cahllenges
			$this->set('totalChallenges',$this->Challenges->find('all' ,['conditions'=>['is_deleted'=>'N' ]])->count());
			//total likes
			$this->set('totalLikes',$this->Likes->find('all' ,['conditions'=>['status'=>'1','is_deleted'=>'N' ]])->count());
			//total Unlikes
			$this->set('totalUnLikes',$this->Likes->find('all' ,['conditions'=>[ 'status'=>'0','is_deleted'=>'N' ]])->count());
			//total commnets
			$this->set('totalComments',$this->Comments->find('all' ,['conditions'=>[ 'is_deleted'=>'N' ]])->count());
			
			$appusetime = $this->Users->find('all', array(
				'fields' => array('Users.app_use_time'),
				'conditions' => array('Users.access_level_id' => 1),
			))->first();
			//total user rewards current
			$this->set('totalUserRewards',$this->UserRewards->find('all' ,['conditions'=>['is_deleted'=>'N' ]])->count());
			//total download songs
			$this->set('totalDownloads',$this->Downloads->find('all' ,['conditions'=>['is_deleted'=>'N' ]])->count());
			//total Payment Details
			$this->set('totalPaymentDetails',$this->PaymentDetails->find('all' ,['conditions'=>['MONTH(created)' => date('m'),'status'=>'C' ,'is_deleted'=>'N' ]])->count());
			$this->set('totalCompletePaymentAmount', $this->PaymentDetails->find()->select(['sum' => $this->PaymentDetails->find()->func()->sum('PaymentDetails.point')])->where(['MONTH(PaymentDetails.created)' => date('m'),'PaymentDetails.status'=>'C' ,'PaymentDetails.is_deleted'=>'N' ])->first());
			//total Pending Posts
			$this->set('totalPendingPostDetails',$this->Posts->find('all' ,['conditions'=>["is_deleted" => 'N',"enabled" => 'P']])->count());
			//total Public Posts
			$this->set('totalPublicPostDetails',$this->Posts->find('all' ,['conditions'=>["is_deleted" => 'N',"enabled !=" => 'P']])->count());
			//total Challenge Transactions Details
			$this->set('totalCTransactions',$this->Transactions->find('all' ,['conditions'=>['MONTH(txndate)' => date('m'),'status'=>'S' ,'type'=>'C' ]])->count());
			$this->set('tcchallenagetAmount', $this->Transactions->find()->select(['sum' => $this->Transactions->find()->func()->sum('Transactions.txnamount')])->where(['MONTH(Transactions.txndate)' => date('m'),'Transactions.status'=>'S' ,'Transactions.type'=>'C' ])->first());
			//total User Transactions Details
			$this->set('totalUTransactions',$this->Transactions->find('all' ,['conditions'=>['MONTH(txndate)' => date('m'),'status'=>'S' ,'type'=>'U' ]])->count());
			$this->set('tcusertAmount', $this->Transactions->find()->select(['sum' => $this->Transactions->find()->func()->sum('Transactions.txnamount')])->where(['MONTH(Transactions.txndate)' => date('m'),'Transactions.status'=>'S' ,'Transactions.type'=>'U' ])->first());
			 
		}
		
		public function manage(){
			
			$this->set('title' , $this->project_title.'!: Cms Pages');
			$this->set('Pages', $this->Pages->newEntity($this->request->data));
			$this->set('cmsPages',$this->Pages->find('list',array('keyField'=>'id' , 'valueField'=> 'title'))->toArray());
			
			if ($this->request->is(['post' ,'put'])) {
				
				$Pages  = $this->Pages->get($this->request->data['id']);
				$this->request->data['slug'] = strtolower(preg_replace("/[\s_]/", "-", $this->request->data['title']));
				$Pages = $this->Pages->patchEntity($Pages, $this->request->data);
				
				if ($this->Pages->save($Pages)) {
					$this->Flash->success(__('Cms page has been saved.'));
					return $this->redirect(['controller'=>'Pages','action' => 'manage']);
				}else{
					$this->Flash->error(__('Some Errors Occurred.'));
				}
			}
		
		}
		public function search(){
			if ($this->request->is('ajax')) {
					if(isset($this->request->data['cms_id'])){
						
						 $this->set('cmsDetails',$this->Pages->get($this->request->data['cms_id']));
					}
			}
		}
		
		
		public function chart()
		{
			
			if ($this->request->is('ajax')) {
					if(isset($this->request->data['mode'])){
						
						$to_date = date('Y-m-d', strtotime($this->request->data['to']));
						$from_date = date('Y-m-d', strtotime($this->request->data['from']));
						$this->loadModel('Users');
						$query = $this->Users->find();
						$charts = $ven = $usr = $tv =   array();
						$users = 	$query->select([
								'count' => $query->func()->count('id'),
								'published_date' => 'DATE(created)'
							])
							->where(['access_level_id' => 2,'is_deleted' => 'N','DATE(created) >=' => $to_date,'DATE(created) <=' =>  $from_date,/* function ($exp, $q) {
								
									return $exp->between('created', date('Y-m-d H:i:s', strtotime($this->request->data['to'])), date('Y-m-d H:i:s', strtotime($this->request->data['from'])));
								} */])
							->group('published_date')->hydrate(false)->toArray();
							//pr($users);die;
						if($users){
								foreach($users as $value){
										$a = array();
										$a[0]  = strtotime($value['published_date'])*1000;
										$a[1]  = $value['count'];
										
										$usr[] = $a;
								}
						} 
						
						$charts['users'] = $usr;
						
						
						echo json_encode($charts);die;
					}
			}
		}
		
		/***************** manage rewards for top singer **********************/
		public function manageRewardTopSinger(){
			
			$this->set('title' , $this->project_title.'!: Cms Pages');
			$this->loadModel('ManagerTopSingerDjRewards');
			
			if ($this->request->is('post')) 
			{
				foreach($this->request->data as $id=>$value)
				{
					$this->ManagerTopSingerDjRewards->updateAll(array('point'=>$value),array('id'=>$id));
				}
				$this->Flash->success('Detail has been updated');
			}
			$site_parameters_arr = $this->ManagerTopSingerDjRewards->find('all',array('conditions'=>array('type'=>'S')))->hydrate(false)->toArray();	
			$site_parameters_arr_arry = array();
			foreach($site_parameters_arr as $data)
			{
				$site_parameters_arr_arry[$data['title']]['id'] = $data['id'];
				$site_parameters_arr_arry[$data['title']]['point'] = $data['point'];
			}
			$this->set('site_parameters_arr_arry',$site_parameters_arr_arry);
		}
		/***************** manage rewards for top dj **********************/
		public function manageRewardTopDj(){
			
			$this->set('title' , $this->project_title.'!: Cms Pages');
			$this->loadModel('ManagerTopSingerDjRewards');
			
			if ($this->request->is('post')) 
			{
				foreach($this->request->data as $id=>$value)
				{
					$this->ManagerTopSingerDjRewards->updateAll(array('point'=>$value),array('id'=>$id));
				}
				$this->Flash->success('Detail has been updated');
			}
			$site_parameters_arr = $this->ManagerTopSingerDjRewards->find('all',array('conditions'=>array('type'=>'D')))->hydrate(false)->toArray();	
			$site_parameters_arr_arry = array();
			foreach($site_parameters_arr as $data)
			{
				$site_parameters_arr_arry[$data['title']]['id'] = $data['id'];
				$site_parameters_arr_arry[$data['title']]['point'] = $data['point'];
			}
			$this->set('site_parameters_arr_arry',$site_parameters_arr_arry);
		
		}
		public function manageRewardSearch(){
			if ($this->request->is('ajax')) {
				$this->loadModel('ManagerTopSingerDjRewards');
					if(isset($this->request->data['cms_id'])){
						
						 $this->set('cmsDetails',$this->ManagerTopSingerDjRewards->get($this->request->data['cms_id']));
					}
			}
		}
		
		/***************** manage Themes **********************/
		public function manageTheme(){
			
			$this->set('title' , $this->project_title.'!: Manage Theme');
			$this->loadModel('Themes');
			
			if ($this->request->is('post')) 
			{	//echo "<pre>"; print_r($this->request->data);die;
				foreach($this->request->data as $id=>$value)
				{
					$themeData = $this->Themes->find('all', array('conditions' => array('Themes.id' => $id)))->first();
					$before_image = $themeData->image;
					if(isset($value) && $value['tmp_name'] !='')
					{
						$filename = preg_replace('/[^a-zA-Z0-9.]/', '_', $value['name']);
						$ext = pathinfo($filename, PATHINFO_EXTENSION);
						$filename = basename($filename, '.' . $ext) . time() . '.' . $ext;
						if ($this->uploadImage($value['tmp_name'], $value['type'], 'uploads/user/', $filename))
						{
							$themeImage = $filename;
						}else {
							 $themeImage = $before_image;
						 }
					}else {
						$themeImage = $before_image;
					}
					$this->Themes->updateAll(array('image'=>$themeImage),array('id'=>$id));
				}
				$this->Flash->success('Detail has been updated');
			}
			$site_parameters_arr = $this->Themes->find('all',array('conditions'=>array()))->hydrate(false)->toArray();	
			$site_parameters_arr_arry = array();
			foreach($site_parameters_arr as $data)
			{
				$site_parameters_arr_arry[$data['type']]['id'] = $data['id'];
				$site_parameters_arr_arry[$data['type']]['name'] = $data['name'];
				$site_parameters_arr_arry[$data['type']]['image'] = $data['image'];
				$site_parameters_arr_arry[$data['type']]['type'] = $data['type'];
				$site_parameters_arr_arry[$data['type']]['status'] = $data['status'];
			}
			$this->set('site_parameters_arr_arry',$site_parameters_arr_arry);
		}
		public function themestatus(){
			$this->loadModel('Themes');
			if ($this->request->is('ajax')) {
				
				$chkTheme =  $this->Themes->find('all', array('conditions' => array('status' => 'Y')))->first();
				
				if(isset($chkTheme) && !empty($chkTheme)){
					if($this->request->data['status'] == 'N'){
						$theme = $this->Themes->get($this->request->data['id']); // Return article with id 12
						$theme->status = $this->request->data['status'];
						$this->Themes->save($theme);
						echo 1;
					}else{
						echo 2;
					}
					
				}else{
					
					$theme = $this->Themes->get($this->request->data['id']); // Return article with id 12
					$theme->status = $this->request->data['status'];
					$this->Themes->save($theme);
					echo 1;
				}
				
			}
			die;
			
		}
}
