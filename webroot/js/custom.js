$(document).ready(function(){
  $(".post_title p").each(function(i){
    len=$(this).text().length;
    if(len>35)
    {
      $(this).text($(this).text().substr(0,35)+'...');
    }
  });       
});
/***************LOGIN SIGNUP **********************/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
/*******************product counter**********************/


// $(window).scroll(function(){
//   var sticky = $('.sticky'),
//       scroll = $(window).scrollTop();

//   if (scroll >= 100) sticky.addClass('fixed');
//   else sticky.removeClass('fixed');
// });

/***************** go to perticular section click on # (anchor tag) *******************/
  $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
    
  });




/***************ADD POST PAGE tab section *****************/
$('.tab_listwrap li').click(function(){
	if($('.tab_listwrap li').hasClass('active')){
		$('.tab_listwrap li').removeClass('active');
	}
	
	
});
$('.tab_listwrap li').click(function(){
	$(this).addClass('active');
});

$(document).ready(function(){
    $('.table_display').css('height', $(window).height()-50);
    
    // Comma, not colon ----^
});


 // $('.login_tab').show();
 //    $('.newscrren-forgot').hide();
 //    $('.newscrren-verification').hide();
 //    $('.newscrren-verificationotp').hide();
 //    $('.newscrren-verificationcmplt').hide();


  $('.forgot').click(function(){
    $('.login_tab').hide();
    $('.newscrren-forgot').show();
  });

  $('.otp').click(function(){
    $('.newscrren-verificationotp').show();
    $('.verify_data').hide();
  });


  // $('.goverification').click(function(){
  //   $('.login_tab').hide();
  //   $('.newscrren-forgot').hide();
  //   $('.newscrren-verification').show();
  //   $('.newscrren-verificationotp').hide();
  // });
  // $('.otp').click(function(){
  //   $('.newscrren-verificationotp').show();
  //   $('.login_tab').hide();
  //   $('.newscrren-forgot').hide();
  //   $('.newscrren-verification').hide();
  //   $('.newscrren-verificationcmplt').hide();
  //  });
  // $('.verificationbtn').click(function(){
  //     $('.newscrren-verificationotp').show();
  //     $('.login_tab').hide();
  //     $('.newscrren-forgot').hide();
  //     $('.newscrren-verification').hide();
  // });
  //   $('.verificationotp').click(function(){
  //     $('.newscrren-verificationotp').hide();
  //     $('.newscrren-verificationcmplt').show();
  //     $('.login_tab').hide();
  //     $('.newscrren-forgot').hide();
  //     $('.newscrren-verification').hide();
  // });
